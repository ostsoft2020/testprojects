#region Using directives

using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.ConstrainedExecution;

#endregion

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyTitle(@"")]
[assembly: AssemblyDescription(@"")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(@"Gimecs")]
[assembly: AssemblyProduct(@"DomainSpecificLanguage")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: System.Resources.NeutralResourcesLanguage("en")]

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion(@"1.0.0.0")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]
[assembly: ReliabilityContract(Consistency.MayCorruptProcess, Cer.None)]

//
// Make the Dsl project internally visible to the DslPackage assembly
//
[assembly: InternalsVisibleTo(@"Gimecs.DomainSpecificLanguage.DslPackage, PublicKey=002400000480000094000000060200000024000052534131000400000100010069D0A810A3CAF4B06ED723543E5BCA22B84C4DED6D2BCC732755B6B35EAF36B50D8F1F9AA2D290DEB5C7CBF3513838A2AFA040713EF930C80C233ABD1C14248379120B4AB0474B793A99D3964D70474BA8A61B6FA20BF66004BCA5B5463144DA07C2514ABE3CD5AC9D98C0BFCF2C671360A4FA3A780CFABB5C3C9150C31118E6")]