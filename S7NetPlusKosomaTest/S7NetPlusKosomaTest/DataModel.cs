﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Threading;

namespace S7NetPlusKosomaTest
{
	public class DataModel : INotifyPropertyChanged
	{
		private Dispatcher dispatcher;

		private bool running;
		private bool executing;

		private string dmc2;
		private string dmc3;
		private string dmc4;
		private string dmc5;
		private string dmc6;
		private string dmc7;
		private string dmc8;
		private string dmc9;

		public DataModel(Dispatcher dispatcher)
		{
			this.dispatcher = dispatcher;
		}

		public bool Running 
		{
			get => running;
			set 
			{
				if (value != running)
				{
					running = value;
					raisePropertyChanged();
				}
			} 
		}

		public bool Executing
		{
			get => executing;
			set
			{
				if (value != executing)
				{
					executing = value;
					raisePropertyChanged();
				}
			}
		}

		public string DMC2
		{
			get => dmc2;
			set
			{
				if (value != dmc2)
				{
					dmc2 = value;
					raisePropertyChanged();
				}
			}
		}

		public string DMC3
		{
			get => dmc3;
			set
			{
				if (value != dmc3)
				{
					dmc3 = value;
					raisePropertyChanged();
				}
			}
		}

		public string DMC4
		{
			get => dmc4;
			set
			{
				if (value != dmc4)
				{
					dmc4 = value;
					raisePropertyChanged();
				}
			}
		}

		public string DMC5
		{
			get => dmc5;
			set
			{
				if (value != dmc5)
				{
					dmc5 = value;
					raisePropertyChanged();
				}
			}
		}

		public string DMC6
		{
			get => dmc6;
			set
			{
				if (value != dmc6)
				{
					dmc6 = value;
					raisePropertyChanged();
				}
			}
		}

		public string DMC7
		{
			get => dmc7;
			set
			{
				if (value != dmc7)
				{
					dmc7 = value;
					raisePropertyChanged();
				}
			}
		}

		public string DMC8
		{
			get => dmc8;
			set
			{
				if (value != dmc8)
				{
					dmc8 = value;
					raisePropertyChanged();
				}
			}
		}

		public string DMC9
		{
			get => dmc9;
			set
			{
				if (value != dmc9)
				{
					dmc9 = value;
					raisePropertyChanged();
				}
			}
		}


		public event PropertyChangedEventHandler PropertyChanged;

		private void raisePropertyChanged([CallerMemberName] string propertyName = "")
		{
			dispatcher.Invoke(() => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName)));
		}
	}
}
