﻿using S7.Net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Timers;
using System.Windows;

namespace S7NetPlusKosomaTest
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private const string fileName = "log.txt";

		private DataModel dataModel;

		private Timer timer = new Timer(500);
		private int dataBlockNumber;
		private Plc plc;
		private string filePath;

		public MainWindow()
		{
			InitializeComponent();

			filePath = AppDomain.CurrentDomain.BaseDirectory + "\\" + fileName;

			dataModel = new DataModel(Dispatcher);

			timer.AutoReset = true;
			timer.Elapsed += timer_Elapsed;
		}

		private void connectButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				logsTextBox.Text = "";

				dataBlockNumber = Convert.ToInt32(dataBlockTextBox.Text);

				plc = new Plc(CpuType.S71500, ipAddressTextBox.Text, 0, 1);

				plc.Open();

				if (plc.IsConnected)
				{
					if (!File.Exists(filePath))
						File.CreateText(filePath);

					readCyclicalData();
					readDmcs();
					setConnectedState(true);
					timer.Start();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.GetBaseException().Message);
			}
		}

		private void disconnectButton_Click(object sender, RoutedEventArgs e)
		{
			if (plc?.IsConnected == true)
				plc.Close();

			setConnectedState(false);
		}

		private void timer_Elapsed(object sender, ElapsedEventArgs e)
		{
			try
			{
				bool prevExecuting = dataModel.Executing;

				readCyclicalData();

				if (prevExecuting = dataModel.Executing)
					return;

				if (dataModel.Executing)
					Dispatcher.Invoke(() => logsTextBox.Text += "<<<<<<<<<<<<<\nCycle started at " + DateTime.Now + "\n");
				else
				{
					Dispatcher.Invoke(() => logsTextBox.Text += "Cycle finished at at " + DateTime.Now + "\n");

					readDmcs();

					Dispatcher.Invoke(() =>
						{
							foreach (string dmcLog in getDmcLog())
								logsTextBox.Text += dmcLog;

							logsTextBox.Text += ">>>>>>>>>>>>>\n";
						}
					);
				}
			}
			catch (Exception ex)
			{
				Dispatcher.Invoke(() =>	logsTextBox.Text += "!!!!!!!!!!!!!\nError occured at " + DateTime.Now + ": " + ex.GetBaseException().Message + "\n");
			}
		}

		private string[] getDmcLog()
		{
			List<string> result = new List<string>();

			result.Add("DMC2 = " + dataModel.DMC2 + "\n");
			result.Add("DMC3 = " + dataModel.DMC3 + "\n");
			result.Add("DMC4 = " + dataModel.DMC4 + "\n");
			result.Add("DMC5 = " + dataModel.DMC5 + "\n");
			result.Add("DMC6 = " + dataModel.DMC6 + "\n");
			result.Add("DMC7 = " + dataModel.DMC7 + "\n");
			result.Add("DMC8 = " + dataModel.DMC8 + "\n");
			result.Add("DMC9 = " + dataModel.DMC9 + "\n");

			return result.ToArray();
		}
		
		private void addLogToFile(string action)
		{
			List<string> lines = new List<string>();

			lines.Add("<record>\n");

			lines.Add("\tAction = " + action + "\n");
			lines.Add("\tDateAndTime = " + DateTime.Now + "\n");
			lines.Add("\tRunning = " + dataModel.Running + "\n");
			lines.Add("\tRunning = " + dataModel.Running + "\n");

			foreach (string dmcLog in getDmcLog())
				lines.Add("\t" + dmcLog);

			lines.Add("</record>\n");

			File.AppendAllLines(filePath, lines);
		}

		private void readCyclicalData()
		{
			dataModel.Running = (bool)plc.Read(DataType.DataBlock, dataBlockNumber, 22, VarType.Bit, 1, 0);
			dataModel.Executing = (bool)plc.Read(DataType.DataBlock, dataBlockNumber, 312, VarType.Bit, 1, 0);
		}

		private void readDmcs()
		{
			dataModel.DMC2 = getStringFromBytes(plc.ReadBytes(DataType.DataBlock, dataBlockNumber, 40, 34));
			dataModel.DMC3 = getStringFromBytes(plc.ReadBytes(DataType.DataBlock, dataBlockNumber, 74, 34));
			dataModel.DMC4 = getStringFromBytes(plc.ReadBytes(DataType.DataBlock, dataBlockNumber, 108, 34));
			dataModel.DMC5 = getStringFromBytes(plc.ReadBytes(DataType.DataBlock, dataBlockNumber, 142, 34));
			dataModel.DMC6 = getStringFromBytes(plc.ReadBytes(DataType.DataBlock, dataBlockNumber, 176, 34));
			dataModel.DMC7 = getStringFromBytes(plc.ReadBytes(DataType.DataBlock, dataBlockNumber, 210, 34));
			dataModel.DMC8 = getStringFromBytes(plc.ReadBytes(DataType.DataBlock, dataBlockNumber, 244, 34));
			dataModel.DMC9 = getStringFromBytes(plc.ReadBytes(DataType.DataBlock, dataBlockNumber, 278, 34));
		}

		private string getStringFromBytes(byte[] bytes)
		{
			byte length = bytes[1];

			if (length == 0)
				return "";

			byte[] stringContent = new byte[bytes.Length - 2];

			Array.Copy(bytes, 2, stringContent, 0, stringContent.Length);

			return Encoding.ASCII.GetString(stringContent);
		}

		private void setConnectedState(bool value)
		{
			connectButton.IsEnabled = !value;
			disconnectButton.IsEnabled = value;

			readsCountTextBox.IsEnabled = value;
			runningCheckBox.IsEnabled = value;
			executingCheckBox.IsEnabled = value;
			dmc2TextBox.IsEnabled = value;
			dmc3TextBox.IsEnabled = value;
			dmc4TextBox.IsEnabled = value;
			dmc5TextBox.IsEnabled = value;
			dmc6TextBox.IsEnabled = value;
			dmc7TextBox.IsEnabled = value;
			dmc8TextBox.IsEnabled = value;
			dmc9TextBox.IsEnabled = value;
			logsTextBox.IsEnabled = value;
		}
	}
}
