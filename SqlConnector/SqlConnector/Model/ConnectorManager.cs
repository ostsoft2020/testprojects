﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SqlConnector.Model
{
	public class ConnectorManager
	{
		private object syncObject = new object();

		public List<Connector> Connectors { get; } = new List<Connector>();

		/// <summary>
		/// Creates connector and add it to Connectors collection
		/// </summary>
		/// <param name="displayName"></param>
		/// <param name="browseName"></param>
		/// <param name="connectorType"></param>
		/// <param name="connectionUri"></param>
		/// <returns>Index in Connectors collection</returns>
		public int CreateConnector(string displayName, string browseName, ConnectorType connectorType, string connectionUri)
		{
			if (string.IsNullOrWhiteSpace(displayName))
				throw new ArgumentOutOfRangeException("Display name is not specified");

			if (string.IsNullOrWhiteSpace(browseName))
				throw new ArgumentOutOfRangeException("Browse name is not specified");

			Connector connector;

			switch (connectorType)
			{
				case ConnectorType.OpcUa:
					throw new NotImplementedException();

				case ConnectorType.DataBase:
					connector = new DatabaseConnector(displayName, browseName, connectionUri);
					break;

				default:
					throw new NotImplementedException();
			}

			if (connector != null && Connectors.Any(c => c.DisplayName == displayName || c.BrowseName == browseName))
				throw new Exception($"Display name {displayName} or browse name '{browseName}' already used");

			lock (syncObject)
			{
				Connectors.Add(connector);

				return Connectors.Count - 1;
			}
		}

		public void RemoveConnector(int index)
		{
			if (index < 0)
				throw new IndexOutOfRangeException("Connector index should be equal or greater then zero");

			if (index >= Connectors.Count)
				throw new IndexOutOfRangeException("Connector index should be less then connectors count");

			lock (syncObject)
				Connectors.RemoveAt(index);
		}
	}
}
