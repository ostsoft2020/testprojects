﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.OracleClient;

namespace SqlConnector.Model
{
	public class DatabaseConnector : Connector
	{
		#region Fields

		private DatabaseType dataBaseType;

		#endregion

		#region Properties

		public override ConnectorType ConnectorType => ConnectorType.DataBase;

		public override bool ConnectionIsOpen => Connection.State == ConnectionState.Open;
		public override bool ConnectionIsClosed => Connection.State == ConnectionState.Closed;

		internal IDbConnection Connection { get; private set; }

		public DatabaseType DataBaseType
		{
			get { return dataBaseType; }
			set
			{
				if (value == dataBaseType)
					return;

				string connectionString = Connection.ConnectionString;

				Connection.Dispose();

				dataBaseType = value;

				Connection = createConnectionByDataBaseType(dataBaseType, connectionString);
			}
		}

		public string ConnectionString
		{
			get { return Connection.ConnectionString; }
			set 
			{
				if (!ConnectionIsClosed)
					throw new Exception("Connection should be closed before connection string change");

				Connection.ConnectionString = value; 
			}
		}

		public int ConnectionTimeout
		{
			get { return Connection.ConnectionTimeout; }
		}

		public string Database
		{
			get { return Connection.Database; }
		}

		#endregion

		#region Constructors

		public DatabaseConnector(string displayName, string browseName, string connectionString)
			: base(displayName, browseName)
		{
			Connection = new SqlConnection(connectionString);
			dataBaseType = DatabaseType.SqlServer;
		}

		#endregion

		#region Methods

		public override bool Test(out string errorMessage)
		{
			try
			{
				IDbConnection testConnection = createConnectionByDataBaseType(dataBaseType, Connection.ConnectionString);

				testConnection.Open();

				errorMessage = "";
				return true;
			}
			catch (Exception e)
			{
				errorMessage = e.GetBaseException().Message;
				return false;
			}
		}

		private static IDbConnection createConnectionByDataBaseType(DatabaseType dataBaseType, string connectionString)
		{
			switch (dataBaseType)
			{
				case DatabaseType.SqlServer:
					return new SqlConnection(connectionString);

				case DatabaseType.Oracle:
					return new OracleConnection(connectionString);

				default:
					throw new NotImplementedException();
			}
		}

		#endregion
	}
}
