﻿namespace SqlConnector.Model
{
	public abstract class Connector
	{
		#region Constructors

		protected Connector(string displayName, string browseName)
		{
			DisplayName = displayName;
			BrowseName = browseName;
		}

		#endregion

		#region Properties

		public string DisplayName { get; protected set; }

		public string BrowseName { get; protected set; }

		public abstract ConnectorType ConnectorType { get; }

		public abstract bool ConnectionIsOpen { get; }
		public abstract bool ConnectionIsClosed { get; }

		#endregion

		#region Methods

		public abstract bool Test(out string errorMessage);

		#endregion
	}
}
