﻿using System;
using System.IO.Ports;
using System.Windows;
using System.Windows.Controls;

namespace UartCommunication
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private SerialPort serialPort1 = new ();
		private SerialPort serialPort2 = new();

		public MainWindow()
		{
			InitializeComponent();

			configureSerialPort(serialPort1);
			configureSerialPort(serialPort2);

			foreach (var portName in SerialPort.GetPortNames())
			{
				port1ComboBox.Items.Add(portName);
				port2ComboBox.Items.Add(portName);
			}

			if (port1ComboBox.Items.Count > 0)
			{
				port1ComboBox.SelectedIndex = 0;

				if (port2ComboBox.Items.Count > 1)
					port2ComboBox.SelectedIndex = 1;
				else
					port2ComboBox.SelectedIndex = 0;
			}			
		}

		private void configureSerialPort(SerialPort serialPort)
		{
			serialPort.BaudRate = 9600;
			serialPort.Parity = Parity.None;
			serialPort.DataBits = 8; 
			serialPort.StopBits = StopBits.One;
			serialPort.Handshake = Handshake.None;

			serialPort.ReadTimeout = 500;
			serialPort.WriteTimeout = 500;
		}

		private void open(SerialPort serialPort, Button openButton, Button closeButton, Button readButton, Button writeButton)
		{
			try
			{
				if (!serialPort.IsOpen)
					serialPort.Open();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
			finally
			{
				openButton.IsEnabled = !serialPort.IsOpen;
				closeButton.IsEnabled = serialPort.IsOpen;
				readButton.IsEnabled = serialPort.IsOpen;
				writeButton.IsEnabled = serialPort.IsOpen;
			}
		}

		private void close(SerialPort serialPort, Button openButton, Button closeButton, Button readButton, Button writeButton)
		{
			try
			{
				if (serialPort.IsOpen)
					serialPort.Close();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
			finally
			{
				openButton.IsEnabled = !serialPort.IsOpen;
				closeButton.IsEnabled = serialPort.IsOpen;
				readButton.IsEnabled = serialPort.IsOpen;
				writeButton.IsEnabled = serialPort.IsOpen;
			}
		}

		private void read(SerialPort serialPort, TextBox targetTextBox)
		{
			if (serialPort.IsOpen)
				try 
				{
					targetTextBox.Text = serialPort.ReadLine();
				}
				catch (Exception e)
				{
					MessageBox.Show(e.Message);
				}
		}

		private void write(SerialPort serialPort, TextBox sourceTextBox)
		{
			if (serialPort.IsOpen)
				try
				{
					 serialPort.WriteLine(sourceTextBox.Text);
				}
				catch (Exception e)
				{
					MessageBox.Show(e.Message);
				}
		}

		private void port1OpenButton_Click(object sender, RoutedEventArgs e)
		{
			if (string.IsNullOrEmpty(port1ComboBox.Text))
			{
				MessageBox.Show("Port 1 is not selected");
				return;
			}

			serialPort1.PortName = port1ComboBox.Text;

			open(serialPort1, port1OpenButton, port1CloseButton, port1ReadButton, port1WriteButton);
		}

		private void port1CloseButton_Click(object sender, RoutedEventArgs e)
		{
			close(serialPort1, port1OpenButton, port1CloseButton, port1ReadButton, port1WriteButton);
		}

		private void port1ReadButton_Click(object sender, RoutedEventArgs e)
		{
			read(serialPort1, port1ReceivedText);
		}

		private void port1WriteButton_Click(object sender, RoutedEventArgs e)
		{
			write(serialPort1, port1TextToSend);
		}

		private void port2OpenButton_Click(object sender, RoutedEventArgs e)
		{
			if (string.IsNullOrEmpty(port2ComboBox.Text))
			{
				MessageBox.Show("Port 2 is not selected");
				return;
			}

			serialPort2.PortName = port2ComboBox.Text;

			open(serialPort2, port2OpenButton, port2CloseButton, port2ReadButton, port2WriteButton);
		}

		private void port2CloseButton_Click(object sender, RoutedEventArgs e)
		{
			close(serialPort2, port2OpenButton, port2CloseButton, port2ReadButton, port2WriteButton);
		}

		private void port2ReadButton_Click(object sender, RoutedEventArgs e)
		{
			read(serialPort2, port2ReceivedText);
		}

		private void port2WriteButton_Click(object sender, RoutedEventArgs e)
		{
			write(serialPort2, port2TextToSend);
		}
	}
}
