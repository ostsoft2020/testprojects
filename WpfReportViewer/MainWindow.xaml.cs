﻿using Microsoft.Reporting.WinForms;
using System;
using System.Configuration;
using System.Windows;

namespace WpfReportViewer
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private ReportViewer reportViewer;
		private long? protocolId;

		public MainWindow()
		{
			InitializeComponent();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			connectionStringTextBox.Text = ConfigurationManager.ConnectionStrings["Calibration"].ConnectionString;

			reportLocaleComboBox.ItemsSource = new string[] { "en", "de", "es", "it", "pl"};
			reportLocaleComboBox.Text = "de";

			reportViewer = new ReportViewer();

			windowsFormsHost.Child = reportViewer;

			refreshReport();
		}

		private void renderButton_Click(object sender, RoutedEventArgs e)
		{
			refreshReport();
		}

		private void refreshReport()
		{
			try
			{
				long tempProtocolId;

				if (long.TryParse(protocolIdTextBox.Text, out tempProtocolId))
					protocolId = tempProtocolId;

				Report.Load(connectionStringTextBox.Text, reportLocaleComboBox.Text, reportViewer.LocalReport, ref protocolId);

				protocolIdTextBox.Text = protocolId.ToString();

				reportViewer.RefreshReport();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}
	}
}
