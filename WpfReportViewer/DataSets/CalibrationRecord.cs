﻿namespace WpfReportViewer.DataSets
{
	internal class CalibrationRecord
	{
		public float? SamplingPeriod { get; set; }
		public short? RecordNumber { get; set; }
		public float? CalibrationCurrent { get; set; }
		public int? MeasurementsСount { get; set; }
		public float? MeasuringDuration { get; set; }
		public float? Temperature { get; set; }
		public float? SupplyVoltage1 { get; set; }
		public float? SupplyVoltage2 { get; set; }
		public float? SupplyVoltage3 { get; set; }
		public bool? IsSupplyVoltageOk { get; set; }
		public bool? IsIdleVoltageOk { get; set; }
		public string? Parameter { get; set; }
		public string? Unit { get; set; }
		public bool? IsToleranceStandardComplied { get; set; }
		public float? SetValue { get; set; }
		public float? MeasuredValue { get; set; }
		public float? DisplayValue { get; set; }
		public byte? CalibrationTarget { get; set; }
		public float? Deviation { get; set; }
		public float? ToleranceBasisAbsolute { get; set; }
		public float? TolerancePercentage { get; set; }
		public float? ToleranceAbsolute { get; set; }
		public bool? IsOk { get; set; }
	}
}
