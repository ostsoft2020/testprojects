﻿namespace WpfReportViewer.DataSets
{
	internal class SystemInformation
	{
		public string? Name { get; set; }
		public byte[] Logo { get; set; }
		public string? LegalName { get; set; }
		public string? Address { get; set; }
		public string? Phone { get; set; }
		public string? Fax { get; set; }
		public string? Email { get; set; }
		public string? WebSite { get; set; }
	}
}
