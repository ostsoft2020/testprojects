﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WpfReportViewer.DataSources
{
    public class ReportItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string LegalName { get; set; }
    }
}
