﻿namespace WpfReportViewer.DataSets
{
	internal class ToleranceDefinition
	{
		public short? Parameter { get; set; }
		public string? Unit { get; set; }
		public short? CalibrationTarget { get; set; }
	}
}
