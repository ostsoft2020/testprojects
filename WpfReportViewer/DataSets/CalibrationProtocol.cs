﻿using Microsoft.VisualBasic;
using System;

namespace WpfReportViewer.DataSets
{
	public class CalibrationProtocol
	{
		public string? ProtocolNumber { get; set; }
		public string? CalibrationUnitType { get; set; }
		public string? CalibrationUnitManufacturer { get; set; }
		public string? CalibrationUnitSerialNumber { get; set; }
		public DateTime? CalibrationUnitCalibrationDate { get; set; }
		public string? WeldingUnitType { get; set; }
		public string? WeldingUnitManufacturer { get; set; }
		public string? WeldingUnitCode { get; set; }
		public string? WeldingUnitInventoryNumber { get; set; }
		public string? Customer { get; set; }
		public float? PowerLineLength { get; set; }
		public string? VoltageTapLocation { get; set; }
		public int? CalibrationProcess { get; set; }
		public string? Comment { get; set; }
		public DateTime? NextCalibrationDate { get; set; }
		public DateTime? CalibrationDate { get; set; }
		public string? Examiner { get; set; }
		public int? RegulationType { get; set; }
		public double? SupplyVoltageStandard { get; set; }
		public float? SupplyVoltageTolerance { get; set; }
		public double? SupplyVoltageToleranceAbsolute { get; set; }
		public bool? IsSupplyVoltageCalibrated { get; set; }
		public double? IdleVoltage { get; set; }
		public float? IdleVoltageTolerance { get; set; }
		public double? IdleVoltageToleranceAbsolute { get; set; }
		public bool? IsIdleVoltageCalibrated { get; set; }
		public bool? IsOk { get; set; }
		public int? NotCalibratedRecordCount { get; set; }
	}
}
