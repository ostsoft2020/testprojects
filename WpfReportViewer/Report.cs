﻿using Microsoft.Data.SqlClient;
using Microsoft.Reporting.WinForms;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Xml;

namespace WpfReportViewer
{
	public class Report
	{
		public static void Load(string connectionString, string reportLocale, LocalReport report, ref long? calibrationProtocolId)
		{
			if (string.IsNullOrWhiteSpace(reportLocale))
				reportLocale = "en";

			string reportFileName = "CalibrationProtocol_" + reportLocale + ".rdlc";

			if (!File.Exists(reportFileName))
				throw new FileNotFoundException(reportFileName);

			using var fileStream = new FileStream(reportFileName, FileMode.Open);
			report.LoadReportDefinition(fileStream);
			report.EnableHyperlinks = true;

			fileStream.Position = 0;
			XmlDocument reportXml = new XmlDocument();
			reportXml.Load(fileStream);

			Dictionary<string, string> queries = getQueries(reportXml);

			string? defaultCalibrationProtocolIdQuery = null;
			queries.TryGetValue("DefaultCalibrationProtocolDataSet", out defaultCalibrationProtocolIdQuery);

			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				connection.Open();

				if (!calibrationProtocolId.HasValue && !string.IsNullOrEmpty(defaultCalibrationProtocolIdQuery))
					calibrationProtocolId = getDefaultCalibrationProtocolId(connection, defaultCalibrationProtocolIdQuery);

				report.SetParameters(new[] { new ReportParameter("ProtocolId", calibrationProtocolId.ToString()) });

				foreach (KeyValuePair<string, string> query in queries)
					report.DataSources.Add(new ReportDataSource(query.Key, getDataSource(query.Key, connection, query.Value, calibrationProtocolId ?? 0)));
			}
		}

		private static DataTable getDataSource(string dataSetName, SqlConnection connection, string query, long calibrationProtocolId)
		{
			using (SqlCommand command = new SqlCommand(query, connection))
			{
				if (query.Contains("@ProtocolId"))
					command.Parameters.Add(new SqlParameter("@ProtocolId", calibrationProtocolId));

				SqlDataReader reader = command.ExecuteReader();

				DataTable table = new DataTable(dataSetName);

				table.Load(reader);

				return table;
			}
		}

		private static long getDefaultCalibrationProtocolId(SqlConnection connection, string defaultCalibrationProtocolIdQuery)
		{
			using (SqlCommand command = new SqlCommand(defaultCalibrationProtocolIdQuery, connection))
				return (long)command.ExecuteScalar();
		}

		private static Dictionary<string, string> getQueries(XmlDocument reportXml)
		{
			XmlNamespaceManager namespaceManager = new XmlNamespaceManager(reportXml.NameTable);
			namespaceManager.AddNamespace("r", "http://schemas.microsoft.com/sqlserver/reporting/2016/01/reportdefinition");

			Dictionary<string, string> queries = new();

			foreach (XmlNode dataSetNode in reportXml.SelectNodes("/r:Report/r:DataSets/r:DataSet", namespaceManager))
			{
				XmlNode queryNode = dataSetNode.SelectSingleNode("r:Query/r:CommandText", namespaceManager);

				if (queryNode == null)
					continue;

				queries.Add(dataSetNode.Attributes["Name"].Value, queryNode.InnerText);
			}

			return queries;
		}
	}
}
