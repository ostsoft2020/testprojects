﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Xml;

namespace JobDataParser
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void rawDataTextBox1_TextChanged(object sender, TextChangedEventArgs e)
		{
			(int? bytesCount, string parcedData) parceResult = parceJobRawData(rawData1TextBox.Text);

			job1DataLengthTextBox.Text = parceResult.bytesCount?.ToString();
			jobData1TextBox.Text = parceResult.parcedData;
		}

		private void rawDataTextBox2_TextChanged(object sender, TextChangedEventArgs e)
		{
			(int? bytesCount, string parcedData) parceResult = parceJobRawData(rawData2TextBox.Text);

			job2DataLengthTextBox.Text = parceResult.bytesCount?.ToString();
			jobData2TextBox.Text = parceResult.parcedData;
		}

		private void clearJob1Button_Click(object sender, RoutedEventArgs e)
		{
			rawData1TextBox.Text = "";
		}

		private void clearJob2Button_Click(object sender, RoutedEventArgs e)
		{
			rawData2TextBox.Text = "";
		}

		private (int? bytesCount, string parcedData) parceJobRawData(string rawData)
		{
			if (string.IsNullOrWhiteSpace(rawData))
				return (null, "");

			try
			{
				int prefixIndex = rawData.IndexOf(", 0x");

				if (prefixIndex >= 0)
					rawData = rawData.Remove(0, prefixIndex + 4);

				rawData = rawData.Replace("-", "");

				byte[] bytes = new byte[rawData.Length / 2];

				for (int i = 0; i < bytes.Length; i++)
					bytes[i] = Convert.ToByte(rawData.Substring(i * 2, 2), 16);

				string jobXmlString = Encoding.UTF8.GetString(bytes);

				XmlDocument jobXmlDocumet = new XmlDocument();

				jobXmlDocumet.LoadXml(jobXmlString);

				StringBuilder result = new StringBuilder();

				foreach (XmlNode node in jobXmlDocumet.ChildNodes[0].ChildNodes)
					result.AppendLine(node.Name.PadRight(33) + ": " + node.InnerText);

				return (bytes.Length, result.ToString());
			}
			catch (Exception e)
			{
				return (null, "Invalid job raw data format:\n" + e.GetBaseException().Message);
			}
		}
	}
}
