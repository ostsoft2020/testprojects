/******************************************************************************
**
** <auto-generated>
**     This code was generated by a tool: UaModeler
**     Runtime Version: 1.6.3, using .NET Server 3.0.0 template (version 0)
**
**     Changes to this file may cause incorrect behavior and will be lost if
**     the code is regenerated.
** </auto-generated>
**
** Copyright (c) 2006-2020 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** Project: .NET OPC UA SDK information model for namespace http://home.org/Decendant/
**
** Description: OPC Unified Architecture Software Development Kit.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Created: 07.05.2020
**
******************************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Xml;
using System.Runtime.Serialization;
using UnifiedAutomation.UaBase;

namespace Home.Descendant
{
    #region DataType Identifiers
    /// <summary>
    /// A class that declares constants for all DataTypes in the Model.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("UaModeler", "1.6.3")]
    public static partial class DataTypes
    {
    }
    #endregion

    #region Object Identifiers
    /// <summary>
    /// A class that declares constants for all Objects in the Model.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("UaModeler", "1.6.3")]
    public static partial class Objects
    {
    }
    #endregion

    #region ObjectType Identifiers
    /// <summary>
    /// A class that declares constants for all ObjectTypes in the Model.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("UaModeler", "1.6.3")]
    public static partial class ObjectTypes
    {
        /// <summary>
        /// The identifier for the DescendantType ObjectType.
        /// </summary>
        public const uint DescendantType = 1002;

    }
    #endregion

    #region Method Identifiers
    /// <summary>
    /// A class that declares constants for all Methods in the Model.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("UaModeler", "1.6.3")]
    public static partial class Methods
    {
        /// <summary>
        /// The identifier for the Reset Method.
        /// </summary>
        public const uint DescendantType_Reset = 7001;

    }
    #endregion

    #region ReferenceType Identifiers
    /// <summary>
    /// A class that declares constants for all ReferenceTyped in the Model.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("UaModeler", "1.6.3")]
    public static partial class ReferenceTypes
    {
    }
    #endregion

    #region Variable Identifiers
    /// <summary>
    /// A class that declares constants for all Variables in the Model.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("UaModeler", "1.6.3")]
    public static partial class Variables
    {
    }
    #endregion

    #region VariableTypes Identifiers
    /// <summary>
    /// A class that declares constants for all VariableTypes in the Model.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("UaModeler", "1.6.3")]
    public static partial class VariableTypes
    {
    }
    #endregion

    #region DataType Node Identifiers
    /// <summary>
    /// A class that declares constants for all DataTypes in the Model.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("UaModeler", "1.6.3")]
    public static partial class DataTypeIds
    {
    }
    #endregion

    #region Method Node Identifiers
    /// <summary>
    /// A class that declares constants for all Methods in the Model.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("UaModeler", "1.6.3")]
    public static partial class MethodIds
    {
        /// <summary>
        /// The identifier for the DescendantType_Reset Method.
        /// </summary>
        public static readonly ExpandedNodeId DescendantType_Reset = new ExpandedNodeId(Methods.DescendantType_Reset, Namespaces.Descendant);

    }
    #endregion

    #region Object Node Identifiers
    /// <summary>
    /// A class that declares constants for all Objects in the Model.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("UaModeler", "1.6.3")]
    public static partial class ObjectIds
    {
    }
    #endregion

    #region ObjectType Node Identifiers
    /// <summary>
    /// A class that declares constants for all Objects in the Model.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("UaModeler", "1.6.3")]
    public static partial class ObjectTypeIds
    {
        /// <summary>
        /// The identifier for the DescendantType ObjectType.
        /// </summary>
        public static readonly ExpandedNodeId DescendantType = new ExpandedNodeId(ObjectTypes.DescendantType, Namespaces.Descendant);

    }
    #endregion

    #region ReferenceType Node Identifiers
    /// <summary>
    /// A class that declares constants for all ReferenceTypes in the Model.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("UaModeler", "1.6.3")]
    public static partial class ReferenceTypeIds
    {
    }
    #endregion

    #region Variable Node Identifiers
    /// <summary>
    /// A class that declares constants for all Variables in the Model.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("UaModeler", "1.6.3")]
    public static partial class VariableIds
    {
    }
    #endregion

    #region VariableType Node Identifiers
    /// <summary>
    /// A class that declares constants for all VariableType in the Model.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("UaModeler", "1.6.3")]
    public static partial class VariableTypeIds
    {
    }
    #endregion

    #region BrowseName Declarations
    /// <summary>
    /// Declares all of the BrowseNames used in the Model.
    /// </summary>
    public static partial class BrowseNames
    {
        /// <summary>
        /// The BrowseName for the DescendantType component.
        /// </summary>
        public const string DescendantType = "DescendantType";
        /// <summary>
        /// The BrowseName for the Reset component.
        /// </summary>
        public const string Reset = "Reset";
    }
    #endregion

    #region Namespace Declarations
    /// <summary>
    /// Defines constants for all namespaces referenced by the Model.
    /// </summary>
    public static partial class Namespaces
    {
        /// <summary>
        /// The URI for the OpcUa namespace (.NET code namespace is 'Opc.Ua').
        /// </summary>
        public const string OpcUa = "http://opcfoundation.org/UA/";

        /// <summary>
        /// The URI for the OpcUaXsd namespace (.NET code namespace is 'Opc.Ua').
        /// </summary>
        public const string OpcUaXsd = "http://opcfoundation.org/UA/2008/02/Types.xsd";

        /// <summary>
        /// The URI for the Descendant namespace.
        /// </summary>
        public const string Descendant = "http://home.org/Decendant/";

        /// <summary>
        /// The URI for the DescendantXsd namespace.
        /// </summary>
        public const string DescendantXsd = "http://home.org/Decendant/Types.xsd";
    }
    #endregion
}

