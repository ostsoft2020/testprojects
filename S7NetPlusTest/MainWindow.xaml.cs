﻿using S7.Net;
using System;
using System.Configuration;
using System.Linq;
using System.Windows;

namespace S7NetPlusTest
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			foreach (CpuType cpuType in Enum.GetValues(typeof(CpuType)))
				cpuTypeComboBox.Items.Add(cpuType);

			cpuTypeComboBox.SelectedValue = Enum.GetNames(typeof(CpuType)).Contains(ConfigurationManager.AppSettings["CpuType"])
				? Enum.Parse(typeof(CpuType), ConfigurationManager.AppSettings["CpuType"])
				: CpuType.S71500;

			ipTextBox.Text = ConfigurationManager.AppSettings["IpAddress"] ?? "192.168.0.1";
			portTextBox.Text = ConfigurationManager.AppSettings["Port"] ?? "102";
			rackTextBox.Text = ConfigurationManager.AppSettings["Rack"] ?? "0";
			slotTextBox.Text = ConfigurationManager.AppSettings["Slot"] ?? "1";
			dataBlockTextBox.Text = ConfigurationManager.AppSettings["DataBlockNumber"] ?? "900";
		}

		private async void connectButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				await PlcCommunicator.ConnectAsync((CpuType)cpuTypeComboBox.SelectedItem, ipTextBox.Text, int.Parse(portTextBox.Text), short.Parse(rackTextBox.Text), short.Parse(slotTextBox.Text));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.GetBaseException().Message);
			}

			refreshControlsEnebledState();
		}

		private void disconnectButton_Click(object sender, RoutedEventArgs e)
		{
			PlcCommunicator.Disconnect();

			refreshControlsEnebledState();
		}

		private async void readButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				byte[] bytes = await PlcCommunicator.ReadBytesAsync(int.Parse(dataBlockTextBox.Text), int.Parse(startByteTextBox.Text), int.Parse(bytesCountTextBox.Text));

				readBytesTextBox.Text = BitConverter.ToString(bytes).Replace("-", " ");
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.GetBaseException().Message);
			}

			refreshControlsEnebledState();
		}

		private void refreshControlsEnebledState()
        {
			connectButton.IsEnabled = !PlcCommunicator.Connected;
			disconnectButton.IsEnabled = PlcCommunicator.Connected;
			readButton.IsEnabled = PlcCommunicator.Connected;
		}
	}
}
