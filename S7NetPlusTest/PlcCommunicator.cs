﻿using S7.Net;
using System;
using System.Threading.Tasks;

namespace S7NetPlusTest
{
	public static class PlcCommunicator
	{
		private static Plc plc;

		public static bool Connected => (plc != null) ? plc.IsConnected : false;

		/// <summary>
		/// Port by default is 102
		/// </summary>
		public static async Task ConnectAsync(CpuType cpu, string ip, int port, short rack, short slot)
		{
			Plc plc = new Plc(cpu, ip, port, rack, slot);

			Task openTask = plc.OpenAsync();

			if (await Task.WhenAny(openTask, Task.Delay(3000)) != openTask)
				throw new TimeoutException("Connection to S7 controller has not been open during timeout");
			else
				PlcCommunicator.plc = plc;
		}

		public static void Disconnect()
		{
			plc?.Close();
			plc = null;
		}

		public static async Task<byte[]> ReadBytesAsync(int dataBlock, int startByteAddress, int bytesCount)
		{
			if (plc == null)
				throw new Exception("Plc was connection was not configured");

			if (!plc.IsConnected)
				throw new Exception("Plc is not connected");

			return await plc.ReadBytesAsync(DataType.DataBlock, dataBlock, startByteAddress, bytesCount);
		}
	}
}
