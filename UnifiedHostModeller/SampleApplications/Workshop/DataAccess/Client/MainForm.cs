using System;
using System.Linq;
using System.Windows.Forms;
using Opc.Ua;
using Opc.Ua.Client;
using Opc.Ua.Client.Controls;
using System.Collections.Generic;
using UnifiedStudio;
using System.Configuration;
using UnifiedHostModeler;
using UnifiedHostModeler.Helpers;

namespace Quickstarts.DataAccessClient
{
	/// <summary>
	/// The main form for a simple Data Access Client application.
	/// </summary>
	public partial class MainForm : Form
    {
        #region Constructors

        /// <summary>
        /// Creates an empty form.
        /// </summary>
        private MainForm()
        {
            InitializeComponent();
            Icon = ClientUtils.GetAppIcon();

            setCenterPanelControl(null);
            setRightPanelControl(null);
        }

        /// <summary>
        /// Creates a form which uses the specified client configuration.
        /// </summary>
        /// <param name="configuration">The configuration to use.</param>
        public MainForm(ApplicationConfiguration configuration)
        {
            InitializeComponent();
            Icon = ClientUtils.GetAppIcon();

            ConnectServerCTRL.Configuration = configuration;
            ConnectServerCTRL.ServerUrl = ConfigurationManager.AppSettings["ServerUrl"]?.ToString() ?? "opc.tcp://localhost:62541";
            ConnectServerCTRL.UseSecurity = (ConfigurationManager.AppSettings["ServerUrl"]?.ToString() ?? "false").ToLower() == false.ToString();

            setCenterPanelControl(null);
            setRightPanelControl(null);
        }

        #endregion

        #region Private Fields

        private Session session;
        private NodeManager nodeManager;
        private TreeNode devicesTreeNode;
        private TreeNode deviceTypesTreeNode;
        private TreeNode variableTypesTreeNode;

        #endregion

        #region Private Methods

        /// <summary>
        /// Populates the branch in the tree view.
        /// </summary>
        private void populateBranch(
            TreeNode parentNode,
            BrowseDescription[] childBrowseDesriptions,
            bool inTypeHierarchy,
            Func<ReferenceDescription, bool> childFilter = null)
        {
            try
            {
                if (parentNode.Nodes.Count == 1 && parentNode.Nodes[0].Text == "")
					parentNode.Nodes.Clear();

                List<ReferenceDescription> references =
					OpcUaNavigationHelper.Browse(session, new BrowseDescriptionCollection(childBrowseDesriptions), false).OrderBy(r => r.DisplayName.ToString()).ToList();

                if (childFilter != null)
                    for (int i = references.Count - 1; i >= 0; i--)
                        if (!childFilter(references[i]))
                            references.RemoveAt(i);

                // process results.
                for (int ii = 0; ii < references.Count; ii++)
                {
                    ReferenceDescription opcNodeReference = references[ii];

                    // add node.
                    TreeNode treeNode = new TreeNode(Utils.Format("{0}", opcNodeReference));
                    NodeDescriptor nodeDescriptor = new NodeDescriptor(inTypeHierarchy, opcNodeReference);
                    treeNode.Tag = nodeDescriptor;
					parentNode.Nodes.Add(treeNode);

                    bool? hasChildrenChecked = null;

                    NodeDescriptor parentDescriptor = null;
                    if (treeNode.Parent != null && treeNode.Parent.Tag != null)
                        parentDescriptor = (NodeDescriptor)treeNode.Parent.Tag;

                    // calculate node kind and assing context menu
                    switch (opcNodeReference.NodeClass)
                    {
                        // if we found object type then we treat this node as device type
                        case NodeClass.ObjectType:
                            nodeDescriptor.NodeKind = NodeKind.DeviceType;
                            break;

                        case NodeClass.Object:
                            // if this is an object and it has user namespace index and it's patrent is the Devices folder then we trewat this node as device
                            if (session.TypeTree.IsTypeOf(opcNodeReference.TypeDefinition, session.GetDynamicDeviceTypeNodeId()))
                            {
                                nodeDescriptor.NodeKind = NodeKind.Device;
                                break;
                            }

                            // if node is not the device then we compare node name to known node names
                            switch (opcNodeReference.BrowseName.Name)
                            {
                                // ParameterSet
                                case Constants.ParametersNodeName:
                                    nodeDescriptor.NodeKind = NodeKind.ParameterSet;
                                    break;

                                // MethodSet
                                case Constants.MethodsNodeName:
                                    nodeDescriptor.NodeKind = NodeKind.MethodSet;
                                    break;

                                // ProgramSet
                                case Constants.ProgramsNodeName:
                                    nodeDescriptor.NodeKind = NodeKind.ProgramSet;
                                    break;

                                // ConnectorSet
                                case Constants.ConnectorsNodeName:
                                    nodeDescriptor.NodeKind = NodeKind.ConnectorSet;
                                    break;

                                // WorkflowSet
                                case Constants.WorkflowsNodeName:
                                    nodeDescriptor.NodeKind = NodeKind.WorkflowSet;
                                    break;

                                // if node does not have known name
                                default:
                                    // check the parent node kind
                                    if (parentDescriptor != null)
                                        switch (parentDescriptor.NodeKind)
                                        {
                                            case NodeKind.ConnectorSet:
                                                nodeDescriptor.NodeKind = NodeKind.Connector;
                                                break;

                                            case NodeKind.ProgramSet:
                                                nodeDescriptor.NodeKind = NodeKind.Program;
                                                break;

                                            case NodeKind.WorkflowSet:
                                                nodeDescriptor.NodeKind = NodeKind.Workflow;
                                                break;

											case NodeKind.SimpleVariable:
                                            case NodeKind.ComplexVariable:
											case NodeKind.SimpleParameter:
											case NodeKind.ComplexParameter:
											case NodeKind.SimpleParameterReference:
											case NodeKind.ComplexParameterReference:
												if (opcNodeReference.BrowseName.Name == Constants.VariableBindingNodeName)
												    nodeDescriptor.NodeKind = NodeKind.Binding;
                                                else
													nodeDescriptor.NodeKind = NodeKind.Object;
												break;

											default:
                                                if (session.TypeTree.IsTypeOf(opcNodeReference.TypeDefinition, Constants.FunctionalGroupTypeNodeId))
                                                    nodeDescriptor.NodeKind = NodeKind.FunctionalGroup;
                                                else
                                                    nodeDescriptor.NodeKind = NodeKind.Object;
                                                break;
                                        }
                                    else
                                        nodeDescriptor.NodeKind = NodeKind.Object;
                                    break;
                            }
                            break;

                        case NodeClass.VariableType:
				            nodeDescriptor.NodeKind = NodeKind.VariableType;
                            break;

                        case NodeClass.Variable:
                            switch (parentDescriptor?.NodeKind)
                            {
                                case NodeKind.VariableType:
                                    nodeDescriptor.NodeKind = NodeKind.VariableTypeChild;
                                    break;

                                case NodeKind.ParameterSet:
                                    nodeDescriptor.NodeKind =
                                        (!OpcUaHelper.isVariableComplex(session, nodeDescriptor.NodeId))
                                            ? NodeKind.SimpleParameter
                                            : NodeKind.ComplexParameter;
                                    break;

                                case NodeKind.FunctionalGroup:
                                    nodeDescriptor.NodeKind =
                                        (!OpcUaHelper.isVariableComplex(session, nodeDescriptor.NodeId))
                                            ? NodeKind.SimpleParameterReference
                                            : NodeKind.ComplexParameterReference;
                                    break;

                                default:
                                    nodeDescriptor.NodeKind =
                                        (!OpcUaHelper.isVariableComplex(session, nodeDescriptor.NodeId))
                                            ? NodeKind.SimpleVariable
                                            : NodeKind.ComplexVariable;
                                    break;
                            }
                            break;

                        case NodeClass.Method:
				            nodeDescriptor.NodeKind = NodeKind.Method;
                            break;
                    }

                    if (!hasChildrenChecked.HasValue)
                        hasChildrenChecked = OpcUaHelper.CheckNodeHasChildren(session, nodeDescriptor.NodeId, nodeDescriptor.NodeKind);
                    if (hasChildrenChecked == true)
                        treeNode.Nodes.Add(new TreeNode()); // to show [+] in the tree

                    assignContextMenuForTreeNode(treeNode);
                }
            }
            catch (Exception exception)
            {
                ClientUtils.HandleException("Error!", exception);
            }
        }

        private void assignContextMenuForTreeNode(TreeNode treeNode)
        {
            NodeDescriptor nodeDescriptor = (NodeDescriptor)treeNode.Tag;

            treeNode.ContextMenuStrip = defaultContextMenuStrip;

            switch (nodeDescriptor.NodeKind)
            {
                case NodeKind.Object:
                    break;

                case NodeKind.UnknownVariable:
                    break;

                case NodeKind.VariableType:
                    treeNode.ContextMenuStrip = variableTypeContextMenu;
                    break;

                case NodeKind.VariableTypeChild:
                    treeNode.ContextMenuStrip = variableTypeChildContextMenu;
                    break;

                case NodeKind.DeviceType:
                    treeNode.ContextMenuStrip = deviceTypeContextMenu;
                    break;

                case NodeKind.Device:
                    treeNode.ContextMenuStrip = deviceContextMenu;
                    break;

                case NodeKind.ParameterSet:
                    if (!nodeDescriptor.InTypeHierarchy)
                        treeNode.ContextMenuStrip = parameterSetContextMenu;
                    break;

				case NodeKind.SimpleVariable:
				case NodeKind.ComplexVariable:
				case NodeKind.SimpleParameter:
                case NodeKind.ComplexParameter:
                case NodeKind.SimpleParameterReference:
                case NodeKind.ComplexParameterReference:
					if (!nodeDescriptor.InTypeHierarchy)
						treeNode.ContextMenuStrip = variableContextMenu;
                    //else
                    //    treeNode.ContextMenuStrip = copyNodeIdToClipboardContextMenu;
                    break;

                case NodeKind.MethodSet:
                    break;

                case NodeKind.Method:
                    break;

                case NodeKind.ConnectorSet:
                    if (!nodeDescriptor.InTypeHierarchy)
                        treeNode.ContextMenuStrip = connectorSetContextMenu;
                    break;

                case NodeKind.Connector:
                    if (!nodeDescriptor.InTypeHierarchy)
                        treeNode.ContextMenuStrip = connectorContextMenu;
                    break;

                case NodeKind.ProgramSet:
                    if (!nodeDescriptor.InTypeHierarchy)
                        treeNode.ContextMenuStrip = programSetContextMenu;
                    break;

                case NodeKind.Program:
                    if (!nodeDescriptor.InTypeHierarchy)
                        treeNode.ContextMenuStrip = programContextMenu;
                    break;

                case NodeKind.WorkflowSet:
                    if (!nodeDescriptor.InTypeHierarchy)
                        treeNode.ContextMenuStrip = workflowSetContextMenu;
                    break;

                case NodeKind.Workflow:
                    if (!nodeDescriptor.InTypeHierarchy)
                        treeNode.ContextMenuStrip = workflowContextMenu;
                    break;

                case NodeKind.FunctionalGroup:
                    if (!nodeDescriptor.InTypeHierarchy)
                        treeNode.ContextMenuStrip = functionalGroupContextMenu;
                    break;

                case NodeKind.Binding:
					if (!nodeDescriptor.InTypeHierarchy)
						treeNode.ContextMenuStrip = bindingContextMenu;
					break;
			}
		}

        private bool fillNodeChildren(TreeNode node)
        {
            if (node == null)
                return false;

			try
			{
                // check if node has already been expanded once.
                if (node.Nodes.Count > 0 && node.Nodes[0].Text != string.Empty)
                    return true;

                // get the source for the node.
                NodeDescriptor nodeReferenceDescriptor = node.Tag as NodeDescriptor;
                ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

                if (reference == null || reference.NodeId.IsAbsolute)
                    return false;

                // populate children
                BrowseDescription[] browseDesriptions;
				Func<ReferenceDescription, bool> childFilter = null;

				// for types we need to show types hierarchy
				if ((NodeId)reference.NodeId == session.GetDynamicDeviceTypeNodeId())
                    browseDesriptions = new BrowseDescription[]
                    { 
                        // find all of the components of the node.
                        new BrowseDescription()
                        {
                            NodeId = ExpandedNodeId.ToNodeId(reference.NodeId, session.NamespaceUris),
                            BrowseDirection = BrowseDirection.Forward,
                            ReferenceTypeId = ReferenceTypeIds.HasSubtype,
                            IncludeSubtypes = true,
                            NodeClassMask = (uint)NodeClass.ObjectType,
                            ResultMask = (uint)BrowseResultMask.All,
                        }
                    };
                else if ((NodeId)reference.NodeId == Constants.UnifiedVariableTypeNodeId)
                    browseDesriptions = new BrowseDescription[]
                    { 
                        // find all of the components of the node.
                        new BrowseDescription()
                        {
                            NodeId = ExpandedNodeId.ToNodeId(reference.NodeId, session.NamespaceUris),
                            BrowseDirection = BrowseDirection.Forward,
                            ReferenceTypeId = ReferenceTypeIds.HasSubtype,
                            IncludeSubtypes = true,
                            NodeClassMask = (uint)NodeClass.VariableType,
                            ResultMask = (uint)BrowseResultMask.All,
                        }
                    };
                else // for instances we need to show instance hierarchy
                {
                    // browse child objects for all nodes kind
                    uint nodeClassMask = (uint)NodeClass.Object;

                    switch (nodeReferenceDescriptor.NodeKind)
                    {
                        case NodeKind.Object:
                        case NodeKind.UnknownVariable:

                        case NodeKind.ParameterSet:
                        case NodeKind.ComplexParameter:
                        case NodeKind.ComplexParameterReference:
                        case NodeKind.ComplexVariable:
                        case NodeKind.VariableType:
                        case NodeKind.VariableTypeChild:
                        case NodeKind.Program:
                        case NodeKind.Connector:
							nodeClassMask |= (uint)NodeClass.Variable;
                            break;

                        case NodeKind.FunctionalGroup:
                            nodeClassMask |= (uint)NodeClass.Variable; // NodeClass.Method, but need to filter build-in methods
                            break;

                        case NodeKind.Method:
                            nodeClassMask |= (uint)NodeClass.Method;
                            break;

                        case NodeKind.Device:
                            childFilter = rd => !session.TypeTree.IsTypeOf(rd.ReferenceTypeId, ReferenceTypeIds.HasAddIn);
                            break;

                        case NodeKind.Binding:
							nodeClassMask |= (uint)NodeClass.Variable;
							break;
					}

					browseDesriptions = new BrowseDescription[]
                    { 
                        // find all of the components of the node.
                        new BrowseDescription()
                        {
                            NodeId = ExpandedNodeId.ToNodeId(reference.NodeId, session.NamespaceUris),
                            BrowseDirection = BrowseDirection.Forward,
                            ReferenceTypeId = ReferenceTypeIds.Aggregates,
                            IncludeSubtypes = true,
                            NodeClassMask = nodeClassMask,
                            ResultMask = (uint)BrowseResultMask.All,
                        },

                        // find all nodes organized by the node.
                        new BrowseDescription()
                        {
                            NodeId = ExpandedNodeId.ToNodeId(reference.NodeId, session.NamespaceUris),
                            BrowseDirection = BrowseDirection.Forward,
                            ReferenceTypeId = ReferenceTypeIds.Organizes,
                            IncludeSubtypes = true,
                            NodeClassMask = nodeClassMask,
                            ResultMask = (uint)BrowseResultMask.All,
                        }
                    };
                }

				populateBranch(node, browseDesriptions, nodeReferenceDescriptor?.InTypeHierarchy ?? false, childFilter);

                nodeAttributesControl.DisplayAttributes(session, (NodeId)reference.NodeId);
            }
            catch (Exception exception)
            {
                ClientUtils.HandleException(Text, exception);
            }

            return true;
        }

        private void refreshDevicesSubNodes()
        {
            if (devicesTreeNode == null)
                return;

            devicesTreeNode.Nodes.Clear();

            // populate DeviceSet branch
            BrowseDescription[] browseDesriptions = new BrowseDescription[]
            { 
                    // find all nodes organized by the node.
                    new BrowseDescription()
                    {
                        NodeId = session.GetDeviceSetNodeId(),
                        BrowseDirection = BrowseDirection.Forward,
                        ReferenceTypeId = ReferenceTypeIds.Organizes,
                        IncludeSubtypes = true,
                        NodeClassMask = (uint)(NodeClass.Object),
                        ResultMask = (uint)BrowseResultMask.All,
                    }
            };

            populateBranch(devicesTreeNode, browseDesriptions, false);

            nodeAttributesControl.DisplayAttributes(session, session.GetDeviceSetNodeId());
        }

        private void refreshDeviceTypeSubNodes()
        {
            if (deviceTypesTreeNode == null)
                return;

            deviceTypesTreeNode.Nodes.Clear();

            // populate UnifiedControllerType branch
            BrowseDescription[] browseDesriptions = new BrowseDescription[]
            { 
                    // find all of the components of the node.
                    new BrowseDescription()
                    {
                        NodeId = session.GetDynamicDeviceTypeNodeId(),
                        BrowseDirection = BrowseDirection.Forward,
                        ReferenceTypeId = ReferenceTypeIds.HasSubtype,
                        IncludeSubtypes = true,
                        NodeClassMask = (uint)NodeClass.ObjectType,
                        ResultMask = (uint)BrowseResultMask.All,
                    },
            };

            populateBranch(deviceTypesTreeNode, browseDesriptions, true);

            nodeAttributesControl.DisplayAttributes(session, session.GetDynamicDeviceTypeNodeId());
        }

        private void refreshVariableTypeSubNodes()
        {
            if (variableTypesTreeNode == null)
                return;

            variableTypesTreeNode.Nodes.Clear();

            // populate UnifiedVariableType branch
            BrowseDescription[] browseDesriptions = new BrowseDescription[]
            { 
                    // find all of the components of the node.
                    new BrowseDescription()
                    {
                        NodeId = Constants.UnifiedVariableTypeNodeId,
                        BrowseDirection = BrowseDirection.Forward,
                        ReferenceTypeId = ReferenceTypeIds.HasSubtype,
                        IncludeSubtypes = true,
                        NodeClassMask = (uint)NodeClass.VariableType,
                        ResultMask = (uint)BrowseResultMask.All,
                    },
            };

            populateBranch(variableTypesTreeNode, browseDesriptions, true);

            nodeAttributesControl.DisplayAttributes(session, session.GetDynamicDeviceTypeNodeId());
        }

        private void setCenterPanelControl(UserControl centerPanelControl)
        {
            foreach (Control control in centerButtomPanel.Controls)
                control.Visible = false;

            (centerPanelControl ?? emptyControlCenter).Visible = true;
        }

        private void setRightPanelControl(UserControl rightPanelControl)
        {
            foreach (Control control in topSplitContainer.Panel2.Controls)
                control.Visible = false;

            (rightPanelControl ?? emptyControlRight).Visible = true;
        }

		private NodeId getDataTypeNodeIdByName(string dataTypeName)
		{
			return new NodeId((uint)(int)Enum.Parse(typeof(BuiltInType), dataTypeName));
		}

		private NodeId getVariableTypeNodeIdByName(string variableTypeName)
		{
			if (TypeTreeHelper.SupportedVariableTypes.TryGetValue(variableTypeName, out var variableDescriptor))
				return variableDescriptor.NodeId;

			throw new Exception($"Variable of type {variableTypeName} is not supported");
		}

		#endregion

		#region Event Handlers

		/// <summary>
		/// Connects to a server.
		/// </summary>
		private async void server_ConnectMI_ClickAsync(object sender, EventArgs e)
        {
            try
            {
                await ConnectServerCTRL.Connect();
            }
            catch (Exception exception)
            {
                ClientUtils.HandleException(this.Text, exception);
            }
        }

        /// <summary>
        /// Disconnects from the current session.
        /// </summary>
        private void server_DisconnectMI_Click(object sender, EventArgs e)
        {
            try
            {
                ConnectServerCTRL.Disconnect();
                session = null;
                nodeManager = null;
            }
            catch (Exception exception)
            {
                ClientUtils.HandleException(this.Text, exception);
            }
        }

        /// <summary>
        /// Prompts the user to choose a server on another host.
        /// </summary>
        private void server_DiscoverMI_Click(object sender, EventArgs e)
        {
            try
            {
                ConnectServerCTRL.Discover(null);
            }
            catch (Exception exception)
            {
                ClientUtils.HandleException(this.Text, exception);
            }
        }

        /// <summary>
        /// Updates the application after connecting to or disconnecting from the server.
        /// </summary>
        private void server_ConnectComplete(object sender, EventArgs e)
        {
            try
            {
                session = ConnectServerCTRL.Session;
				nodeManager = (session != null) ? new NodeManager(session) : null;

				/*Assembly[] withEncodableTypes = new[] { typeof(BindingSettings).Assembly  };
                if ((withEncodableTypes != null ? ((uint)withEncodableTypes.Length > 0U ? 1 : 0) : 0) != 0 && this.session.Factory != null)
                    foreach(Assembly ass in withEncodableTypes)
                        session.Factory.AddEncodeableTypes(ass);*/

				if (session == null)
                {
                    browseNodesTreeView.Nodes.Clear();
                    browseNodesTreeView.Enabled = false;
                    return;
                }

                TypeTreeHelper.ReloadTypeTree(session);

                // create Devices node
                ReferenceDescription devicesReference = new ReferenceDescription();
                devicesReference.NodeId = session.GetDeviceSetNodeId();
                INode dedevicesNode = session.NodeCache.Find(devicesReference.NodeId);
                if (dedevicesNode != null)
                {
                    devicesReference.BrowseName = dedevicesNode.BrowseName;
                    devicesReference.DisplayName = dedevicesNode.DisplayName;
                }

                devicesTreeNode = new TreeNode(Constants.DevicesTreeNodeName);
                devicesTreeNode.ContextMenuStrip = devicesContextMenu;
                devicesTreeNode.Tag = new NodeDescriptor(false, devicesReference) { NodeKind = NodeKind.Devices };
                browseNodesTreeView.Nodes.Add(devicesTreeNode);
                refreshDevicesSubNodes();

                // create Types node
                TreeNode typesTreeNode = new TreeNode(Constants.TypesTreeNodeName);
                typesTreeNode.ContextMenuStrip = null;
                typesTreeNode.Tag = new NodeDescriptor(true, null) { NodeKind = NodeKind.Empty };
                browseNodesTreeView.Nodes.Add(typesTreeNode);

                // create Device Types node
                ReferenceDescription deviceTypesReference = new ReferenceDescription();
                deviceTypesReference.NodeId = session.GetDynamicDeviceTypeNodeId();
                INode deviceTypesNode = session.NodeCache.Find(deviceTypesReference.NodeId);
                if (deviceTypesNode != null)
                {
                    deviceTypesReference.BrowseName = deviceTypesNode.BrowseName;
                    deviceTypesReference.DisplayName = deviceTypesNode.DisplayName;
                }

                deviceTypesTreeNode = new TreeNode(Constants.DeviceTypesTreeNodeName);
                deviceTypesTreeNode.ContextMenuStrip = deviceTypesContextMenu;
                deviceTypesTreeNode.Tag = new NodeDescriptor(true, deviceTypesReference);
                typesTreeNode.Nodes.Add(deviceTypesTreeNode);
                refreshDeviceTypeSubNodes();

                // create Variable Types node
                ReferenceDescription variableTypesReference = new ReferenceDescription();
                variableTypesReference.NodeId = Constants.UnifiedVariableTypeNodeId;
                INode variableTypesNode = session.NodeCache.Find(variableTypesReference.NodeId);
                if (variableTypesNode != null)
                {
                    variableTypesReference.BrowseName = variableTypesNode.BrowseName;
                    variableTypesReference.DisplayName = variableTypesNode.DisplayName;
                }

                variableTypesTreeNode = new TreeNode(Constants.VarableTypesTreeNodeName);
                variableTypesTreeNode.ContextMenuStrip = variableTypesContextMenu;
                variableTypesTreeNode.Tag = new NodeDescriptor(true, variableTypesReference);
                typesTreeNode.Nodes.Add(variableTypesTreeNode);
                refreshVariableTypeSubNodes();

                setCenterPanelControl(null);
                setRightPanelControl(null);

                browseNodesTreeView.Enabled = true;

                centerTopTextBox.Text = "";
            }
            catch (Exception exception)
            {
                ClientUtils.HandleException(this.Text, exception);
            }
        }

        /// <summary>
        /// Updates the application after a communicate error was detected.
        /// </summary>
        private void server_ReconnectStarting(object sender, EventArgs e)
        {
            try
            {
                browseNodesTreeView.Enabled = false;

                nodeAttributesControl.Clear();
            }
            catch (Exception exception)
            {
                ClientUtils.HandleException(this.Text, exception);
            }
        }

        /// <summary>
        /// Updates the application after reconnecting to the server.
        /// </summary>
        private void server_ReconnectComplete(object sender, EventArgs e)
        {
            try
            {
                session = ConnectServerCTRL.Session;
				nodeManager = (session != null) ? new NodeManager(session) : null;

				if (session != null)
                    TypeTreeHelper.ReloadTypeTree(session);

                browseNodesTreeView.Enabled = true;
            }
            catch (Exception exception)
            {
                ClientUtils.HandleException(this.Text, exception);
            }
        }

        /// <summary>
        /// Cleans up when the main form closes.
        /// </summary>
        private void mainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ConnectServerCTRL.Disconnect();
        }

        /// <summary>
        /// Fetches the children for a node the first time the node is expanded in the tree view.
        /// </summary>
        private void browseNodesTV_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            if (!fillNodeChildren(e.Node))
                e.Cancel = true;
        }

        /// <summary>
        /// Updates the display after a node is selected.
        /// </summary>
        private void browseNodesTV_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node != null)
                centerTopTextBox.Text = " Selected ietem: " + e.Node.FullPath;
            else
                centerTopTextBox.Text = "";

            try
            {
                // get the source for the node.
                NodeDescriptor nodeReferenceDescriptor = e.Node.Tag as NodeDescriptor;
                ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

                setCenterPanelControl(null);

                if (reference == null || reference.NodeId.IsAbsolute)
                {
                    setRightPanelControl(null);
                    return;
                }

                void refreshPropertyGrid()
                {
                    // right panel
                    nodeAttributesControl.DisplayAttributes(session, (NodeId)reference.NodeId);
                    setRightPanelControl(nodeAttributesControl);
                }

                refreshPropertyGrid();

                switch (nodeReferenceDescriptor.NodeKind)
                {
                    case NodeKind.DeviceType:
                    case NodeKind.Device:
                        deviceInfoControl.DisplayDevice(session, (NodeId)reference.NodeId, nodeReferenceDescriptor.InTypeHierarchy);
                        setCenterPanelControl(deviceInfoControl);
                        break;

                    case NodeKind.Devices:
                        devicesControl.DisplayDevices(session, (NodeId)reference.NodeId);
                        setCenterPanelControl(devicesControl);
                        break;

					case NodeKind.Object:
					case NodeKind.ParameterSet:
                    case NodeKind.ComplexVariable:
                    case NodeKind.VariableType:
                    case NodeKind.VariableTypeChild:
                    case NodeKind.ComplexParameter:
                    case NodeKind.ComplexParameterReference:
                    case NodeKind.Binding:
                        parametersControl.DisplayParameters(session, nodeManager, (NodeId)reference.NodeId, refreshPropertyGrid);
                        setCenterPanelControl(parametersControl);
                        break;

                    case NodeKind.SimpleVariable:
                    case NodeKind.SimpleParameter:
                    case NodeKind.SimpleParameterReference:
                        variableControl.DisplayVariable(session, (NodeId)reference.NodeId);
                        setCenterPanelControl(variableControl);
                        break;

                    case NodeKind.MethodSet:
                        break;

                    case NodeKind.Method:
                        break;

                    case NodeKind.ConnectorSet:
                        connectorsControl.DisplayConnectors(session, (NodeId)reference.NodeId, nodeReferenceDescriptor.InTypeHierarchy);
                        setCenterPanelControl(connectorsControl);
                        break;

                    case NodeKind.Connector:
                        ReferenceDescription connectorTypeReference =
							OpcUaNavigationHelper.Browse(session, nodeReferenceDescriptor.NodeId, BrowseDirection.Forward, NodeClass.ObjectType, new NodeId[] { ReferenceTypeIds.HasTypeDefinition }).FirstOrDefault();

                        switch (connectorTypeReference?.DisplayName.ToString())
                        {
                            case "DatabaseConnectorType":
                                databaseConnectorControl.DisplayConnector(session, (NodeId)reference.NodeId, nodeReferenceDescriptor.InTypeHierarchy);
                                setCenterPanelControl(databaseConnectorControl);
                                break;

                            case "OpcUaConnectorType":
                                opcUaConnectorControl.DisplayConnector(session, (NodeId)reference.NodeId, nodeReferenceDescriptor.InTypeHierarchy);
                                setCenterPanelControl(opcUaConnectorControl);
                                break;

                            case "S7TcpIpConnectorType":
                                s7TcpIpConnectorControl.DisplayConnector(session, (NodeId)reference.NodeId, nodeReferenceDescriptor.InTypeHierarchy);
                                setCenterPanelControl(s7TcpIpConnectorControl);
                                break;
							default:
								parametersControl.DisplayParameters(session, nodeManager, (NodeId)reference.NodeId, refreshPropertyGrid);
								setCenterPanelControl(parametersControl);
								break;
						}
						break;

                    case NodeKind.ProgramSet:
                        programsControl.DisplayPrograms(session, (NodeId)reference.NodeId, nodeReferenceDescriptor.InTypeHierarchy);
                        setCenterPanelControl(programsControl);
                        break;

                    case NodeKind.Program:
                        ReferenceDescription programTypeReference =
							OpcUaNavigationHelper.Browse(session, nodeReferenceDescriptor.NodeId, BrowseDirection.Forward, NodeClass.ObjectType, new NodeId[] { ReferenceTypeIds.HasTypeDefinition }).FirstOrDefault();

                        NodeId connectorSetNodeId = null;
                        NodeId parameterSetNodeId = null;

                        foreach (TreeNode node in e.Node.Parent.Parent.Nodes)
                        {
                            if (node.Text == Constants.ConnectorsNodeName)
                                connectorSetNodeId = ((NodeDescriptor)node.Tag).NodeId;

                            if (node.Text == Constants.ParametersNodeName)
                                parameterSetNodeId = ((NodeDescriptor)node.Tag).NodeId;

                            if (connectorSetNodeId != null && parameterSetNodeId != null)
                                break;
                        }

                        string progranTypeName = programTypeReference?.DisplayName.ToString();

                        switch (progranTypeName)
                        {
                            case "BindOpcUaVariablesProgramType":
                                bingOpcUaVariableProgramControl.DisplayProgram(session, (NodeId)reference.NodeId, connectorSetNodeId, parameterSetNodeId);
                                setCenterPanelControl(bingOpcUaVariableProgramControl);
                                break;

                            case "BindS7TcpIpVariablesProgramType":
                                bingS7TcpIpVariableProgramControl.DisplayProgram(session, (NodeId)reference.NodeId, connectorSetNodeId, parameterSetNodeId);
                                setCenterPanelControl(bingS7TcpIpVariableProgramControl);
                                break;

                            case "OpcUaConditionMonitoringProgramType":
                                ConditionMonitoringProgramControl.DisplayProgram(session, (NodeId)reference.NodeId, connectorSetNodeId, "OpcUaConnectorType");
                                setCenterPanelControl(ConditionMonitoringProgramControl);
                                break;

                            case "S7TcpIpConditionMonitoringProgramType":
                                ConditionMonitoringProgramControl.DisplayProgram(session, (NodeId)reference.NodeId, connectorSetNodeId, "S7TcpIpConnectorType");
                                setCenterPanelControl(ConditionMonitoringProgramControl);
                                break;
                            default:
								parametersControl.DisplayParameters(session, nodeManager, (NodeId)reference.NodeId, refreshPropertyGrid);
								setCenterPanelControl(parametersControl);
                                break;
						}
						break;

                    case NodeKind.WorkflowSet:
                        workflowsControl.DisplayWorkflows(session, (NodeId)reference.NodeId, nodeReferenceDescriptor.InTypeHierarchy);
                        setCenterPanelControl(workflowsControl);
                        break;

                    case NodeKind.Workflow:
                        editWorkflowControl.DisplayWorkflow(session, (NodeId)reference.NodeId);
                        setCenterPanelControl(editWorkflowControl);
                        break;
                }
            }
            catch (Exception exception)
            {
                ClientUtils.HandleException(Text, exception);
            }
        }

        /// <summary>
        /// Ensures the correct node is selected before displaying the context menu.
        /// </summary>
        private void browseNodesTV_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                browseNodesTreeView.SelectedNode = browseNodesTreeView.GetNodeAt(e.X, e.Y);
            }
            catch (Exception exception)
            {
                ClientUtils.HandleException(Text, exception);
            }
        }

        private void topSplitContainer_Layout(object sender, LayoutEventArgs e)
        {
            //resizeControls();
        }

		#endregion
	}
}