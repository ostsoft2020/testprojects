﻿namespace UnifiedHostModeler
{
	public interface IDevice
	{
		string Manufacturer { get; set; }
		string ManufacturerUri { get; set; }
		string Model { get; set; }
		string SerialNumber { get; set; }
		string HardwareRevision { get; set; }
		string SoftwareRevision { get; set; }
		string DeviceRevision { get; set; }
		string DeviceClass { get; set; }
		string ProductCode { get; set; }
		bool Enabled { get; set; }
		bool Running { get; set; }
		string Error { get; set; }
	}
}
