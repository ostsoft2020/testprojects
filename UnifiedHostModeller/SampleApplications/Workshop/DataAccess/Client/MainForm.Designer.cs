/* ========================================================================
 * Copyright (c) 2005-2019 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

namespace Quickstarts.DataAccessClient
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.MenuBar = new System.Windows.Forms.MenuStrip();
			this.fILEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ServerMI = new System.Windows.Forms.ToolStripMenuItem();
			this.Server_DiscoverMI = new System.Windows.Forms.ToolStripMenuItem();
			this.Server_ConnectMI = new System.Windows.Forms.ToolStripMenuItem();
			this.Server_DisconnectMI = new System.Windows.Forms.ToolStripMenuItem();
			this.Server_SetUserMI = new System.Windows.Forms.ToolStripMenuItem();
			this.HelpMI = new System.Windows.Forms.ToolStripMenuItem();
			this.Help_ContentsMI = new System.Windows.Forms.ToolStripMenuItem();
			this.statusBar = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.ConnectServerCTRL = new Opc.Ua.Client.Controls.ConnectServerCtrl();
			this.addPropertyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.parameterSetContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
			this.deviceTypesContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.addDeviceTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.deviceTypeContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.createInstanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.addFunctionaGroupMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
			this.removeTypeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
			this.copyNodeIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deviceContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.deleteDeviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
			this.workflowSetContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.addWorkflowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
			this.workflowContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.removeWorkflowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
			this.programSetContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.addProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem12 = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
			this.programContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.removeProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
			this.connectorSetContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.addConnectorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem9 = new System.Windows.Forms.ToolStripMenuItem();
			this.connectorContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.deleteConnectorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem10 = new System.Windows.Forms.ToolStripMenuItem();
			this.functionalGroupContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.addFunctionalGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.assignParameterMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
			this.removeFunctionalGroupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem11 = new System.Windows.Forms.ToolStripMenuItem();
			this.copyNodeIdToClipboardContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.copyNodeIdToClipboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem18 = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem13 = new System.Windows.Forms.ToolStripMenuItem();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.browseNodesTreeView = new System.Windows.Forms.TreeView();
			this.centerPanel = new System.Windows.Forms.Panel();
			this.centerButtomPanel = new System.Windows.Forms.Panel();
			this.devicesControl = new UnifiedStudio.Controls.DevicesControl();
			this.editWorkflowControl = new UnifiedStudio.Controls.EditWorkflowControl();
			this.bingOpcUaVariableProgramControl = new UnifiedStudio.Controls.BingOpcUaVariableProgramControl();
			this.s7TcpIpConnectorControl = new UnifiedStudio.Controls.S7TcpIpConnectorControl();
			this.deviceInfoControl = new UnifiedHostModeler.Views.Device.DeviceInfoControl();
			this.ConditionMonitoringProgramControl = new UnifiedStudio.Controls.ConditionMonitoringProgramControl();
			this.connectorsControl = new UnifiedStudio.Controls.ConnectorsControl();
			this.emptyControlCenter = new UnifiedStudio.Controls.EmptyControl();
			this.parametersControl = new UnifiedStudio.Controls.ParametersControl();
			this.variableControl = new UnifiedStudio.Controls.VariableControl();
			this.opcUaConnectorControl = new UnifiedStudio.Controls.OpcUaConnectorControl();
			this.databaseConnectorControl = new UnifiedStudio.Controls.DatabaseConnectorControl();
			this.bingS7TcpIpVariableProgramControl = new UnifiedStudio.Controls.BindS7TcpIpVariableProgramControl();
			this.workflowsControl = new UnifiedStudio.Controls.WorkflowsControl();
			this.programsControl = new UnifiedStudio.Controls.ProgramsControl();
			this.centerSplitPanel = new System.Windows.Forms.Panel();
			this.centerTopTextBox = new System.Windows.Forms.TextBox();
			this.topSplitContainer = new System.Windows.Forms.SplitContainer();
			this.nodeAttributesControl = new UnifiedStudio.Controls.NodeAttributesControl();
			this.emptyControlRight = new UnifiedStudio.Controls.EmptyControl();
			this.defaultContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.toolStripMenuItem19 = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem16 = new System.Windows.Forms.ToolStripMenuItem();
			this.variableTypesContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.addVariableTypeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem23 = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem14 = new System.Windows.Forms.ToolStripMenuItem();
			this.variableTypeContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.removeVariableTypeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem21 = new System.Windows.Forms.ToolStripSeparator();
			this.addPropertyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.addVariableMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem25 = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem15 = new System.Windows.Forms.ToolStripMenuItem();
			this.variableTypeChildContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.removeVariableChildMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator17 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem26 = new System.Windows.Forms.ToolStripMenuItem();
			this.refreshToolStripMenuItem17 = new System.Windows.Forms.ToolStripMenuItem();
			this.devicesContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.addDeviceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem22 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem24 = new System.Windows.Forms.ToolStripMenuItem();
			this.bindingContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.deleteBindingMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator18 = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem27 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem28 = new System.Windows.Forms.ToolStripMenuItem();
			this.variableContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.addVariableHistoryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.removeVariableHistoryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
			this.addVariableBindingMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.addVariableBindingSeparator = new System.Windows.Forms.ToolStripSeparator();
			this.editParameterMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deassignParameterMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deleteParameterMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.parameterSpecificMenuItemSeparator = new System.Windows.Forms.ToolStripSeparator();
			this.toolStripMenuItem31 = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem32 = new System.Windows.Forms.ToolStripMenuItem();
			this.MenuBar.SuspendLayout();
			this.statusBar.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			this.parameterSetContextMenu.SuspendLayout();
			this.deviceTypesContextMenu.SuspendLayout();
			this.deviceTypeContextMenu.SuspendLayout();
			this.deviceContextMenu.SuspendLayout();
			this.workflowSetContextMenu.SuspendLayout();
			this.workflowContextMenu.SuspendLayout();
			this.programSetContextMenu.SuspendLayout();
			this.programContextMenu.SuspendLayout();
			this.connectorSetContextMenu.SuspendLayout();
			this.connectorContextMenu.SuspendLayout();
			this.functionalGroupContextMenu.SuspendLayout();
			this.copyNodeIdToClipboardContextMenu.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.centerPanel.SuspendLayout();
			this.centerButtomPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.topSplitContainer)).BeginInit();
			this.topSplitContainer.Panel1.SuspendLayout();
			this.topSplitContainer.Panel2.SuspendLayout();
			this.topSplitContainer.SuspendLayout();
			this.defaultContextMenuStrip.SuspendLayout();
			this.variableTypesContextMenu.SuspendLayout();
			this.variableTypeContextMenu.SuspendLayout();
			this.variableTypeChildContextMenu.SuspendLayout();
			this.devicesContextMenu.SuspendLayout();
			this.bindingContextMenu.SuspendLayout();
			this.variableContextMenu.SuspendLayout();
			this.SuspendLayout();
			// 
			// MenuBar
			// 
			this.MenuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fILEToolStripMenuItem,
            this.ServerMI,
            this.HelpMI});
			this.MenuBar.Location = new System.Drawing.Point(0, 0);
			this.MenuBar.Name = "MenuBar";
			this.MenuBar.Size = new System.Drawing.Size(1287, 24);
			this.MenuBar.TabIndex = 1;
			this.MenuBar.Text = "menuStrip1";
			// 
			// fILEToolStripMenuItem
			// 
			this.fILEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openProjectToolStripMenuItem,
            this.saveProjectToolStripMenuItem,
            this.toolStripMenuItem2,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
			this.fILEToolStripMenuItem.Name = "fILEToolStripMenuItem";
			this.fILEToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fILEToolStripMenuItem.Text = "&File";
			// 
			// openProjectToolStripMenuItem
			// 
			this.openProjectToolStripMenuItem.Name = "openProjectToolStripMenuItem";
			this.openProjectToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
			this.openProjectToolStripMenuItem.Text = "Import project";
			// 
			// saveProjectToolStripMenuItem
			// 
			this.saveProjectToolStripMenuItem.Name = "saveProjectToolStripMenuItem";
			this.saveProjectToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
			this.saveProjectToolStripMenuItem.Text = "Export project";
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(147, 6);
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(147, 6);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
			this.exitToolStripMenuItem.Text = "E&xit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
			// 
			// ServerMI
			// 
			this.ServerMI.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Server_DiscoverMI,
            this.Server_ConnectMI,
            this.Server_DisconnectMI,
            this.Server_SetUserMI});
			this.ServerMI.Name = "ServerMI";
			this.ServerMI.Size = new System.Drawing.Size(51, 20);
			this.ServerMI.Text = "Server";
			// 
			// Server_DiscoverMI
			// 
			this.Server_DiscoverMI.Name = "Server_DiscoverMI";
			this.Server_DiscoverMI.Size = new System.Drawing.Size(133, 22);
			this.Server_DiscoverMI.Text = "Discover...";
			this.Server_DiscoverMI.Click += new System.EventHandler(this.server_DiscoverMI_Click);
			// 
			// Server_ConnectMI
			// 
			this.Server_ConnectMI.Name = "Server_ConnectMI";
			this.Server_ConnectMI.Size = new System.Drawing.Size(133, 22);
			this.Server_ConnectMI.Text = "Connect";
			this.Server_ConnectMI.Click += new System.EventHandler(this.server_ConnectMI_ClickAsync);
			// 
			// Server_DisconnectMI
			// 
			this.Server_DisconnectMI.Name = "Server_DisconnectMI";
			this.Server_DisconnectMI.Size = new System.Drawing.Size(133, 22);
			this.Server_DisconnectMI.Text = "Disconnect";
			this.Server_DisconnectMI.Click += new System.EventHandler(this.server_DisconnectMI_Click);
			// 
			// Server_SetUserMI
			// 
			this.Server_SetUserMI.Name = "Server_SetUserMI";
			this.Server_SetUserMI.Size = new System.Drawing.Size(133, 22);
			this.Server_SetUserMI.Text = "Set User...";
			this.Server_SetUserMI.Click += new System.EventHandler(this.server_SetUserMI_Click);
			// 
			// HelpMI
			// 
			this.HelpMI.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Help_ContentsMI});
			this.HelpMI.Name = "HelpMI";
			this.HelpMI.Size = new System.Drawing.Size(44, 20);
			this.HelpMI.Text = "Help";
			// 
			// Help_ContentsMI
			// 
			this.Help_ContentsMI.Name = "Help_ContentsMI";
			this.Help_ContentsMI.Size = new System.Drawing.Size(122, 22);
			this.Help_ContentsMI.Text = "Contents";
			// 
			// statusBar
			// 
			this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
			this.statusBar.Location = new System.Drawing.Point(0, 768);
			this.statusBar.Name = "statusBar";
			this.statusBar.Size = new System.Drawing.Size(1287, 22);
			this.statusBar.TabIndex = 2;
			// 
			// toolStripStatusLabel
			// 
			this.toolStripStatusLabel.Name = "toolStripStatusLabel";
			this.toolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 89.70588F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.29412F));
			this.tableLayoutPanel1.Controls.Add(this.ConnectServerCTRL, 0, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 24);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(1287, 29);
			this.tableLayoutPanel1.TabIndex = 5;
			// 
			// ConnectServerCTRL
			// 
			this.ConnectServerCTRL.Configuration = null;
			this.ConnectServerCTRL.DisableDomainCheck = false;
			this.ConnectServerCTRL.Dock = System.Windows.Forms.DockStyle.Top;
			this.ConnectServerCTRL.Location = new System.Drawing.Point(3, 3);
			this.ConnectServerCTRL.MaximumSize = new System.Drawing.Size(2048, 23);
			this.ConnectServerCTRL.MinimumSize = new System.Drawing.Size(500, 23);
			this.ConnectServerCTRL.Name = "ConnectServerCTRL";
			this.ConnectServerCTRL.PreferredLocales = null;
			this.ConnectServerCTRL.ServerUrl = "";
			this.ConnectServerCTRL.SessionName = null;
			this.ConnectServerCTRL.Size = new System.Drawing.Size(1148, 23);
			this.ConnectServerCTRL.StatusStrip = this.statusBar;
			this.ConnectServerCTRL.TabIndex = 5;
			this.ConnectServerCTRL.UserIdentity = null;
			this.ConnectServerCTRL.UseSecurity = true;
			this.ConnectServerCTRL.ReconnectStarting += new System.EventHandler(this.server_ReconnectStarting);
			this.ConnectServerCTRL.ReconnectComplete += new System.EventHandler(this.server_ReconnectComplete);
			this.ConnectServerCTRL.ConnectComplete += new System.EventHandler(this.server_ConnectComplete);
			// 
			// addPropertyToolStripMenuItem
			// 
			this.addPropertyToolStripMenuItem.Name = "addPropertyToolStripMenuItem";
			this.addPropertyToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
			this.addPropertyToolStripMenuItem.Text = "Add parameter";
			this.addPropertyToolStripMenuItem.Click += new System.EventHandler(this.addParameterMenuItem_Click);
			// 
			// parameterSetContextMenu
			// 
			this.parameterSetContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addPropertyToolStripMenuItem,
            this.toolStripSeparator2,
            this.toolStripMenuItem7,
            this.refreshToolStripMenuItem2});
			this.parameterSetContextMenu.Name = "parametersContextMenu";
			this.parameterSetContextMenu.Size = new System.Drawing.Size(154, 76);
			// 
			// toolStripSeparator2
			// 
			this.toolStripSeparator2.Name = "toolStripSeparator2";
			this.toolStripSeparator2.Size = new System.Drawing.Size(150, 6);
			// 
			// toolStripMenuItem7
			// 
			this.toolStripMenuItem7.Name = "toolStripMenuItem7";
			this.toolStripMenuItem7.Size = new System.Drawing.Size(153, 22);
			this.toolStripMenuItem7.Text = "Copy node ID";
			this.toolStripMenuItem7.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem2
			// 
			this.refreshToolStripMenuItem2.Name = "refreshToolStripMenuItem2";
			this.refreshToolStripMenuItem2.Size = new System.Drawing.Size(153, 22);
			this.refreshToolStripMenuItem2.Text = "Refresh";
			this.refreshToolStripMenuItem2.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// deviceTypesContextMenu
			// 
			this.deviceTypesContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addDeviceTypeToolStripMenuItem,
            this.toolStripSeparator1,
            this.toolStripMenuItem6,
            this.refreshToolStripMenuItem1});
			this.deviceTypesContextMenu.Name = "addTypeContextMenu";
			this.deviceTypesContextMenu.Size = new System.Drawing.Size(160, 76);
			// 
			// addDeviceTypeToolStripMenuItem
			// 
			this.addDeviceTypeToolStripMenuItem.Name = "addDeviceTypeToolStripMenuItem";
			this.addDeviceTypeToolStripMenuItem.Size = new System.Drawing.Size(159, 22);
			this.addDeviceTypeToolStripMenuItem.Text = "Add device type";
			this.addDeviceTypeToolStripMenuItem.Click += new System.EventHandler(this.addDeviceTypeStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this.toolStripSeparator1.Name = "toolStripSeparator1";
			this.toolStripSeparator1.Size = new System.Drawing.Size(156, 6);
			// 
			// toolStripMenuItem6
			// 
			this.toolStripMenuItem6.Name = "toolStripMenuItem6";
			this.toolStripMenuItem6.Size = new System.Drawing.Size(159, 22);
			this.toolStripMenuItem6.Text = "Copy node ID";
			this.toolStripMenuItem6.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem1
			// 
			this.refreshToolStripMenuItem1.Name = "refreshToolStripMenuItem1";
			this.refreshToolStripMenuItem1.Size = new System.Drawing.Size(159, 22);
			this.refreshToolStripMenuItem1.Text = "Refresh";
			this.refreshToolStripMenuItem1.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// deviceTypeContextMenu
			// 
			this.deviceTypeContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createInstanceToolStripMenuItem,
            this.addFunctionaGroupMenuItem,
            this.toolStripMenuItem4,
            this.removeTypeToolStripMenuItem,
            this.toolStripMenuItem5,
            this.copyNodeIDToolStripMenuItem,
            this.refreshToolStripMenuItem});
			this.deviceTypeContextMenu.Name = "removeTypeContextMenu";
			this.deviceTypeContextMenu.Size = new System.Drawing.Size(193, 126);
			// 
			// createInstanceToolStripMenuItem
			// 
			this.createInstanceToolStripMenuItem.Name = "createInstanceToolStripMenuItem";
			this.createInstanceToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
			this.createInstanceToolStripMenuItem.Text = "Create device instance";
			this.createInstanceToolStripMenuItem.Click += new System.EventHandler(this.createInstanceMenuItem_Click);
			// 
			// addFunctionaGroupMenuItem
			// 
			this.addFunctionaGroupMenuItem.Name = "addFunctionaGroupMenuItem";
			this.addFunctionaGroupMenuItem.Size = new System.Drawing.Size(192, 22);
			this.addFunctionaGroupMenuItem.Text = "Add functional group";
			this.addFunctionaGroupMenuItem.Click += new System.EventHandler(this.addFunctionaGroupMenuItem_Click);
			// 
			// toolStripMenuItem4
			// 
			this.toolStripMenuItem4.Name = "toolStripMenuItem4";
			this.toolStripMenuItem4.Size = new System.Drawing.Size(189, 6);
			// 
			// removeTypeToolStripMenuItem
			// 
			this.removeTypeToolStripMenuItem.Name = "removeTypeToolStripMenuItem";
			this.removeTypeToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
			this.removeTypeToolStripMenuItem.Text = "Remove device type";
			this.removeTypeToolStripMenuItem.Click += new System.EventHandler(this.deleteDeviceTypeMenuItem_Click);
			// 
			// toolStripMenuItem5
			// 
			this.toolStripMenuItem5.Name = "toolStripMenuItem5";
			this.toolStripMenuItem5.Size = new System.Drawing.Size(189, 6);
			// 
			// copyNodeIDToolStripMenuItem
			// 
			this.copyNodeIDToolStripMenuItem.Name = "copyNodeIDToolStripMenuItem";
			this.copyNodeIDToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
			this.copyNodeIDToolStripMenuItem.Text = "Copy node ID";
			this.copyNodeIDToolStripMenuItem.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem
			// 
			this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
			this.refreshToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
			this.refreshToolStripMenuItem.Text = "Refresh";
			this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// deviceContextMenu
			// 
			this.deviceContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteDeviceToolStripMenuItem,
            this.toolStripSeparator4,
            this.toolStripMenuItem9,
            this.refreshToolStripMenuItem4});
			this.deviceContextMenu.Name = "deleteInstanceContextMenu";
			this.deviceContextMenu.Size = new System.Drawing.Size(147, 76);
			// 
			// deleteDeviceToolStripMenuItem
			// 
			this.deleteDeviceToolStripMenuItem.Name = "deleteDeviceToolStripMenuItem";
			this.deleteDeviceToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
			this.deleteDeviceToolStripMenuItem.Text = "Delete device";
			this.deleteDeviceToolStripMenuItem.Click += new System.EventHandler(this.deleteDeviceMenuItem_Click);
			// 
			// toolStripSeparator4
			// 
			this.toolStripSeparator4.Name = "toolStripSeparator4";
			this.toolStripSeparator4.Size = new System.Drawing.Size(143, 6);
			// 
			// toolStripMenuItem9
			// 
			this.toolStripMenuItem9.Name = "toolStripMenuItem9";
			this.toolStripMenuItem9.Size = new System.Drawing.Size(146, 22);
			this.toolStripMenuItem9.Text = "Copy node ID";
			this.toolStripMenuItem9.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem4
			// 
			this.refreshToolStripMenuItem4.Name = "refreshToolStripMenuItem4";
			this.refreshToolStripMenuItem4.Size = new System.Drawing.Size(146, 22);
			this.refreshToolStripMenuItem4.Text = "Refresh";
			this.refreshToolStripMenuItem4.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// workflowSetContextMenu
			// 
			this.workflowSetContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addWorkflowToolStripMenuItem,
            this.toolStripSeparator5,
            this.toolStripMenuItem10,
            this.refreshToolStripMenuItem5});
			this.workflowSetContextMenu.Name = "addWorkflowContextMenu";
			this.workflowSetContextMenu.Size = new System.Drawing.Size(149, 76);
			// 
			// addWorkflowToolStripMenuItem
			// 
			this.addWorkflowToolStripMenuItem.Name = "addWorkflowToolStripMenuItem";
			this.addWorkflowToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
			this.addWorkflowToolStripMenuItem.Text = "Add workflow";
			this.addWorkflowToolStripMenuItem.Click += new System.EventHandler(this.addWorkflowMenuItem_Click);
			// 
			// toolStripSeparator5
			// 
			this.toolStripSeparator5.Name = "toolStripSeparator5";
			this.toolStripSeparator5.Size = new System.Drawing.Size(145, 6);
			// 
			// toolStripMenuItem10
			// 
			this.toolStripMenuItem10.Name = "toolStripMenuItem10";
			this.toolStripMenuItem10.Size = new System.Drawing.Size(148, 22);
			this.toolStripMenuItem10.Text = "Copy node ID";
			this.toolStripMenuItem10.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem5
			// 
			this.refreshToolStripMenuItem5.Name = "refreshToolStripMenuItem5";
			this.refreshToolStripMenuItem5.Size = new System.Drawing.Size(148, 22);
			this.refreshToolStripMenuItem5.Text = "Refresh";
			this.refreshToolStripMenuItem5.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// workflowContextMenu
			// 
			this.workflowContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeWorkflowToolStripMenuItem,
            this.toolStripSeparator6,
            this.toolStripMenuItem11,
            this.refreshToolStripMenuItem6});
			this.workflowContextMenu.Name = "removeWorkflowContextMenu";
			this.workflowContextMenu.Size = new System.Drawing.Size(170, 76);
			// 
			// removeWorkflowToolStripMenuItem
			// 
			this.removeWorkflowToolStripMenuItem.Name = "removeWorkflowToolStripMenuItem";
			this.removeWorkflowToolStripMenuItem.Size = new System.Drawing.Size(169, 22);
			this.removeWorkflowToolStripMenuItem.Text = "Remove workflow";
			this.removeWorkflowToolStripMenuItem.Click += new System.EventHandler(this.deleteWorkflowMenuItem_Click);
			// 
			// toolStripSeparator6
			// 
			this.toolStripSeparator6.Name = "toolStripSeparator6";
			this.toolStripSeparator6.Size = new System.Drawing.Size(166, 6);
			// 
			// toolStripMenuItem11
			// 
			this.toolStripMenuItem11.Name = "toolStripMenuItem11";
			this.toolStripMenuItem11.Size = new System.Drawing.Size(169, 22);
			this.toolStripMenuItem11.Text = "Copy node ID";
			this.toolStripMenuItem11.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem6
			// 
			this.refreshToolStripMenuItem6.Name = "refreshToolStripMenuItem6";
			this.refreshToolStripMenuItem6.Size = new System.Drawing.Size(169, 22);
			this.refreshToolStripMenuItem6.Text = "Refresh";
			this.refreshToolStripMenuItem6.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// programSetContextMenu
			// 
			this.programSetContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addProgramToolStripMenuItem,
            this.toolStripSeparator7,
            this.toolStripMenuItem12,
            this.refreshToolStripMenuItem7});
			this.programSetContextMenu.Name = "addProgramContextMenu";
			this.programSetContextMenu.Size = new System.Drawing.Size(147, 76);
			// 
			// addProgramToolStripMenuItem
			// 
			this.addProgramToolStripMenuItem.Name = "addProgramToolStripMenuItem";
			this.addProgramToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
			this.addProgramToolStripMenuItem.Text = "Add program";
			this.addProgramToolStripMenuItem.Click += new System.EventHandler(this.addProgramMenuItem_Click);
			// 
			// toolStripSeparator7
			// 
			this.toolStripSeparator7.Name = "toolStripSeparator7";
			this.toolStripSeparator7.Size = new System.Drawing.Size(143, 6);
			// 
			// toolStripMenuItem12
			// 
			this.toolStripMenuItem12.Name = "toolStripMenuItem12";
			this.toolStripMenuItem12.Size = new System.Drawing.Size(146, 22);
			this.toolStripMenuItem12.Text = "Copy node ID";
			this.toolStripMenuItem12.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem7
			// 
			this.refreshToolStripMenuItem7.Name = "refreshToolStripMenuItem7";
			this.refreshToolStripMenuItem7.Size = new System.Drawing.Size(146, 22);
			this.refreshToolStripMenuItem7.Text = "Refresh";
			this.refreshToolStripMenuItem7.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// programContextMenu
			// 
			this.programContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeProgramToolStripMenuItem,
            this.toolStripSeparator8,
            this.toolStripMenuItem13,
            this.refreshToolStripMenuItem8});
			this.programContextMenu.Name = "removeProgramContextMenu";
			this.programContextMenu.Size = new System.Drawing.Size(167, 76);
			// 
			// removeProgramToolStripMenuItem
			// 
			this.removeProgramToolStripMenuItem.Name = "removeProgramToolStripMenuItem";
			this.removeProgramToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
			this.removeProgramToolStripMenuItem.Text = "Remove program";
			this.removeProgramToolStripMenuItem.Click += new System.EventHandler(this.deleteProgramMenuItem_Click);
			// 
			// toolStripSeparator8
			// 
			this.toolStripSeparator8.Name = "toolStripSeparator8";
			this.toolStripSeparator8.Size = new System.Drawing.Size(163, 6);
			// 
			// toolStripMenuItem13
			// 
			this.toolStripMenuItem13.Name = "toolStripMenuItem13";
			this.toolStripMenuItem13.Size = new System.Drawing.Size(166, 22);
			this.toolStripMenuItem13.Text = "Copy node ID";
			this.toolStripMenuItem13.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem8
			// 
			this.refreshToolStripMenuItem8.Name = "refreshToolStripMenuItem8";
			this.refreshToolStripMenuItem8.Size = new System.Drawing.Size(166, 22);
			this.refreshToolStripMenuItem8.Text = "Refresh";
			this.refreshToolStripMenuItem8.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// connectorSetContextMenu
			// 
			this.connectorSetContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addConnectorToolStripMenuItem,
            this.toolStripSeparator9,
            this.toolStripMenuItem14,
            this.refreshToolStripMenuItem9});
			this.connectorSetContextMenu.Name = "addConnectorContextMenu";
			this.connectorSetContextMenu.Size = new System.Drawing.Size(154, 76);
			// 
			// addConnectorToolStripMenuItem
			// 
			this.addConnectorToolStripMenuItem.Name = "addConnectorToolStripMenuItem";
			this.addConnectorToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
			this.addConnectorToolStripMenuItem.Text = "Add connector";
			this.addConnectorToolStripMenuItem.Click += new System.EventHandler(this.addConnectorMenuItem_Click);
			// 
			// toolStripSeparator9
			// 
			this.toolStripSeparator9.Name = "toolStripSeparator9";
			this.toolStripSeparator9.Size = new System.Drawing.Size(150, 6);
			// 
			// toolStripMenuItem14
			// 
			this.toolStripMenuItem14.Name = "toolStripMenuItem14";
			this.toolStripMenuItem14.Size = new System.Drawing.Size(153, 22);
			this.toolStripMenuItem14.Text = "Copy node ID";
			this.toolStripMenuItem14.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem9
			// 
			this.refreshToolStripMenuItem9.Name = "refreshToolStripMenuItem9";
			this.refreshToolStripMenuItem9.Size = new System.Drawing.Size(153, 22);
			this.refreshToolStripMenuItem9.Text = "Refresh";
			this.refreshToolStripMenuItem9.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// connectorContextMenu
			// 
			this.connectorContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteConnectorToolStripMenuItem,
            this.toolStripSeparator10,
            this.toolStripMenuItem15,
            this.refreshToolStripMenuItem10});
			this.connectorContextMenu.Name = "removeConnectorContextMenu";
			this.connectorContextMenu.Size = new System.Drawing.Size(165, 76);
			// 
			// deleteConnectorToolStripMenuItem
			// 
			this.deleteConnectorToolStripMenuItem.Name = "deleteConnectorToolStripMenuItem";
			this.deleteConnectorToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
			this.deleteConnectorToolStripMenuItem.Text = "Delete connector";
			this.deleteConnectorToolStripMenuItem.Click += new System.EventHandler(this.deleteConnectorMenuItem_Click);
			// 
			// toolStripSeparator10
			// 
			this.toolStripSeparator10.Name = "toolStripSeparator10";
			this.toolStripSeparator10.Size = new System.Drawing.Size(161, 6);
			// 
			// toolStripMenuItem15
			// 
			this.toolStripMenuItem15.Name = "toolStripMenuItem15";
			this.toolStripMenuItem15.Size = new System.Drawing.Size(164, 22);
			this.toolStripMenuItem15.Text = "Copy node ID";
			this.toolStripMenuItem15.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem10
			// 
			this.refreshToolStripMenuItem10.Name = "refreshToolStripMenuItem10";
			this.refreshToolStripMenuItem10.Size = new System.Drawing.Size(164, 22);
			this.refreshToolStripMenuItem10.Text = "Refresh";
			this.refreshToolStripMenuItem10.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// functionalGroupContextMenu
			// 
			this.functionalGroupContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addFunctionalGroupToolStripMenuItem,
            this.assignParameterMenuItem,
            this.toolStripMenuItem3,
            this.removeFunctionalGroupToolStripMenuItem,
            this.toolStripSeparator11,
            this.toolStripMenuItem16,
            this.refreshToolStripMenuItem11});
			this.functionalGroupContextMenu.Name = "functionalGroupContextMenu";
			this.functionalGroupContextMenu.Size = new System.Drawing.Size(210, 126);
			// 
			// addFunctionalGroupToolStripMenuItem
			// 
			this.addFunctionalGroupToolStripMenuItem.Name = "addFunctionalGroupToolStripMenuItem";
			this.addFunctionalGroupToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.addFunctionalGroupToolStripMenuItem.Text = "Add child group";
			this.addFunctionalGroupToolStripMenuItem.Click += new System.EventHandler(this.addFunctionaGroupMenuItem_Click);
			// 
			// assignParameterMenuItem
			// 
			this.assignParameterMenuItem.Name = "assignParameterMenuItem";
			this.assignParameterMenuItem.Size = new System.Drawing.Size(209, 22);
			this.assignParameterMenuItem.Text = "Assign parameter";
			this.assignParameterMenuItem.Click += new System.EventHandler(this.assignParameterMenuItem_Click);
			// 
			// toolStripMenuItem3
			// 
			this.toolStripMenuItem3.Name = "toolStripMenuItem3";
			this.toolStripMenuItem3.Size = new System.Drawing.Size(206, 6);
			// 
			// removeFunctionalGroupToolStripMenuItem
			// 
			this.removeFunctionalGroupToolStripMenuItem.Name = "removeFunctionalGroupToolStripMenuItem";
			this.removeFunctionalGroupToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
			this.removeFunctionalGroupToolStripMenuItem.Text = "Remove functional group";
			this.removeFunctionalGroupToolStripMenuItem.Click += new System.EventHandler(this.removeFunctionalGroupMenuItem_Click);
			// 
			// toolStripSeparator11
			// 
			this.toolStripSeparator11.Name = "toolStripSeparator11";
			this.toolStripSeparator11.Size = new System.Drawing.Size(206, 6);
			// 
			// toolStripMenuItem16
			// 
			this.toolStripMenuItem16.Name = "toolStripMenuItem16";
			this.toolStripMenuItem16.Size = new System.Drawing.Size(209, 22);
			this.toolStripMenuItem16.Text = "Copy node ID";
			this.toolStripMenuItem16.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem11
			// 
			this.refreshToolStripMenuItem11.Name = "refreshToolStripMenuItem11";
			this.refreshToolStripMenuItem11.Size = new System.Drawing.Size(209, 22);
			this.refreshToolStripMenuItem11.Text = "Refresh";
			this.refreshToolStripMenuItem11.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// copyNodeIdToClipboardContextMenu
			// 
			this.copyNodeIdToClipboardContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyNodeIdToClipboardToolStripMenuItem,
            this.toolStripSeparator13,
            this.toolStripMenuItem18,
            this.refreshToolStripMenuItem13});
			this.copyNodeIdToClipboardContextMenu.Name = "copyNodeIdToClipboardContextMenu";
			this.copyNodeIdToClipboardContextMenu.Size = new System.Drawing.Size(215, 76);
			// 
			// copyNodeIdToClipboardToolStripMenuItem
			// 
			this.copyNodeIdToClipboardToolStripMenuItem.Name = "copyNodeIdToClipboardToolStripMenuItem";
			this.copyNodeIdToClipboardToolStripMenuItem.Size = new System.Drawing.Size(214, 22);
			this.copyNodeIdToClipboardToolStripMenuItem.Text = "Copy Node Id to clipboard";
			this.copyNodeIdToClipboardToolStripMenuItem.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// toolStripSeparator13
			// 
			this.toolStripSeparator13.Name = "toolStripSeparator13";
			this.toolStripSeparator13.Size = new System.Drawing.Size(211, 6);
			// 
			// toolStripMenuItem18
			// 
			this.toolStripMenuItem18.Name = "toolStripMenuItem18";
			this.toolStripMenuItem18.Size = new System.Drawing.Size(214, 22);
			this.toolStripMenuItem18.Text = "Copy node ID";
			this.toolStripMenuItem18.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem13
			// 
			this.refreshToolStripMenuItem13.Name = "refreshToolStripMenuItem13";
			this.refreshToolStripMenuItem13.Size = new System.Drawing.Size(214, 22);
			this.refreshToolStripMenuItem13.Text = "Refresh";
			this.refreshToolStripMenuItem13.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.browseNodesTreeView);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.centerPanel);
			this.splitContainer1.Size = new System.Drawing.Size(1060, 715);
			this.splitContainer1.SplitterDistance = 250;
			this.splitContainer1.TabIndex = 0;
			// 
			// browseNodesTreeView
			// 
			this.browseNodesTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.browseNodesTreeView.Location = new System.Drawing.Point(0, 0);
			this.browseNodesTreeView.Name = "browseNodesTreeView";
			this.browseNodesTreeView.Size = new System.Drawing.Size(250, 715);
			this.browseNodesTreeView.TabIndex = 1;
			this.browseNodesTreeView.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.browseNodesTV_BeforeExpand);
			this.browseNodesTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.browseNodesTV_AfterSelect);
			this.browseNodesTreeView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.browseNodesTV_MouseDown);
			// 
			// centerPanel
			// 
			this.centerPanel.Controls.Add(this.centerButtomPanel);
			this.centerPanel.Controls.Add(this.centerSplitPanel);
			this.centerPanel.Controls.Add(this.centerTopTextBox);
			this.centerPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.centerPanel.Location = new System.Drawing.Point(0, 0);
			this.centerPanel.Name = "centerPanel";
			this.centerPanel.Size = new System.Drawing.Size(806, 715);
			this.centerPanel.TabIndex = 16;
			// 
			// centerButtomPanel
			// 
			this.centerButtomPanel.Controls.Add(this.devicesControl);
			this.centerButtomPanel.Controls.Add(this.editWorkflowControl);
			this.centerButtomPanel.Controls.Add(this.bingOpcUaVariableProgramControl);
			this.centerButtomPanel.Controls.Add(this.s7TcpIpConnectorControl);
			this.centerButtomPanel.Controls.Add(this.deviceInfoControl);
			this.centerButtomPanel.Controls.Add(this.ConditionMonitoringProgramControl);
			this.centerButtomPanel.Controls.Add(this.connectorsControl);
			this.centerButtomPanel.Controls.Add(this.emptyControlCenter);
			this.centerButtomPanel.Controls.Add(this.parametersControl);
			this.centerButtomPanel.Controls.Add(this.variableControl);
			this.centerButtomPanel.Controls.Add(this.opcUaConnectorControl);
			this.centerButtomPanel.Controls.Add(this.databaseConnectorControl);
			this.centerButtomPanel.Controls.Add(this.bingS7TcpIpVariableProgramControl);
			this.centerButtomPanel.Controls.Add(this.workflowsControl);
			this.centerButtomPanel.Controls.Add(this.programsControl);
			this.centerButtomPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.centerButtomPanel.Location = new System.Drawing.Point(0, 23);
			this.centerButtomPanel.Name = "centerButtomPanel";
			this.centerButtomPanel.Size = new System.Drawing.Size(806, 692);
			this.centerButtomPanel.TabIndex = 2;
			// 
			// devicesControl
			// 
			this.devicesControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.devicesControl.Location = new System.Drawing.Point(0, 0);
			this.devicesControl.Name = "devicesControl";
			this.devicesControl.Size = new System.Drawing.Size(806, 692);
			this.devicesControl.TabIndex = 51;
			// 
			// editWorkflowControl
			// 
			this.editWorkflowControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.editWorkflowControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.editWorkflowControl.Location = new System.Drawing.Point(0, 0);
			this.editWorkflowControl.Name = "editWorkflowControl";
			this.editWorkflowControl.Size = new System.Drawing.Size(806, 692);
			this.editWorkflowControl.TabIndex = 49;
			// 
			// bingOpcUaVariableProgramControl
			// 
			this.bingOpcUaVariableProgramControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.bingOpcUaVariableProgramControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.bingOpcUaVariableProgramControl.Location = new System.Drawing.Point(0, 0);
			this.bingOpcUaVariableProgramControl.Name = "bingOpcUaVariableProgramControl";
			this.bingOpcUaVariableProgramControl.Size = new System.Drawing.Size(806, 692);
			this.bingOpcUaVariableProgramControl.TabIndex = 45;
			this.bingOpcUaVariableProgramControl.Visible = false;
			// 
			// s7TcpIpConnectorControl
			// 
			this.s7TcpIpConnectorControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.s7TcpIpConnectorControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.s7TcpIpConnectorControl.Location = new System.Drawing.Point(0, 0);
			this.s7TcpIpConnectorControl.Name = "s7TcpIpConnectorControl";
			this.s7TcpIpConnectorControl.Size = new System.Drawing.Size(806, 692);
			this.s7TcpIpConnectorControl.TabIndex = 44;
			// 
			// deviceInfoControl
			// 
			this.deviceInfoControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.deviceInfoControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.deviceInfoControl.Location = new System.Drawing.Point(0, 0);
			this.deviceInfoControl.Name = "deviceInfoControl";
			this.deviceInfoControl.Size = new System.Drawing.Size(806, 692);
			this.deviceInfoControl.TabIndex = 43;
			// 
			// ConditionMonitoringProgramControl
			// 
			this.ConditionMonitoringProgramControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.ConditionMonitoringProgramControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ConditionMonitoringProgramControl.Location = new System.Drawing.Point(0, 0);
			this.ConditionMonitoringProgramControl.Name = "ConditionMonitoringProgramControl";
			this.ConditionMonitoringProgramControl.Size = new System.Drawing.Size(806, 692);
			this.ConditionMonitoringProgramControl.TabIndex = 42;
			// 
			// connectorsControl
			// 
			this.connectorsControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.connectorsControl.Location = new System.Drawing.Point(0, 0);
			this.connectorsControl.Name = "connectorsControl";
			this.connectorsControl.Size = new System.Drawing.Size(806, 692);
			this.connectorsControl.TabIndex = 41;
			// 
			// emptyControlCenter
			// 
			this.emptyControlCenter.Dock = System.Windows.Forms.DockStyle.Fill;
			this.emptyControlCenter.Location = new System.Drawing.Point(0, 0);
			this.emptyControlCenter.Name = "emptyControlCenter";
			this.emptyControlCenter.Size = new System.Drawing.Size(806, 692);
			this.emptyControlCenter.TabIndex = 40;
			// 
			// parametersControl
			// 
			this.parametersControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.parametersControl.Location = new System.Drawing.Point(0, 0);
			this.parametersControl.Name = "parametersControl";
			this.parametersControl.Size = new System.Drawing.Size(806, 692);
			this.parametersControl.TabIndex = 39;
			this.parametersControl.Visible = false;
			// 
			// variableControl
			// 
			this.variableControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.variableControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.variableControl.Location = new System.Drawing.Point(0, 0);
			this.variableControl.Name = "variableControl";
			this.variableControl.Size = new System.Drawing.Size(806, 692);
			this.variableControl.TabIndex = 38;
			this.variableControl.Visible = false;
			// 
			// opcUaConnectorControl
			// 
			this.opcUaConnectorControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.opcUaConnectorControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.opcUaConnectorControl.Location = new System.Drawing.Point(0, 0);
			this.opcUaConnectorControl.Name = "opcUaConnectorControl";
			this.opcUaConnectorControl.Size = new System.Drawing.Size(806, 692);
			this.opcUaConnectorControl.TabIndex = 37;
			this.opcUaConnectorControl.Visible = false;
			// 
			// databaseConnectorControl
			// 
			this.databaseConnectorControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.databaseConnectorControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.databaseConnectorControl.Location = new System.Drawing.Point(0, 0);
			this.databaseConnectorControl.Name = "databaseConnectorControl";
			this.databaseConnectorControl.Size = new System.Drawing.Size(806, 692);
			this.databaseConnectorControl.TabIndex = 36;
			this.databaseConnectorControl.Visible = false;
			// 
			// bingS7TcpIpVariableProgramControl
			// 
			this.bingS7TcpIpVariableProgramControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.bingS7TcpIpVariableProgramControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.bingS7TcpIpVariableProgramControl.Location = new System.Drawing.Point(0, 0);
			this.bingS7TcpIpVariableProgramControl.Name = "bingS7TcpIpVariableProgramControl";
			this.bingS7TcpIpVariableProgramControl.Size = new System.Drawing.Size(806, 692);
			this.bingS7TcpIpVariableProgramControl.TabIndex = 48;
			this.bingS7TcpIpVariableProgramControl.Visible = false;
			// 
			// workflowsControl
			// 
			this.workflowsControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.workflowsControl.Location = new System.Drawing.Point(0, 0);
			this.workflowsControl.Name = "workflowsControl";
			this.workflowsControl.Size = new System.Drawing.Size(806, 692);
			this.workflowsControl.TabIndex = 47;
			this.workflowsControl.Visible = false;
			// 
			// programsControl
			// 
			this.programsControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.programsControl.Location = new System.Drawing.Point(0, 0);
			this.programsControl.Name = "programsControl";
			this.programsControl.Size = new System.Drawing.Size(806, 692);
			this.programsControl.TabIndex = 46;
			this.programsControl.Visible = false;
			// 
			// centerSplitPanel
			// 
			this.centerSplitPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.centerSplitPanel.Location = new System.Drawing.Point(0, 20);
			this.centerSplitPanel.Name = "centerSplitPanel";
			this.centerSplitPanel.Size = new System.Drawing.Size(806, 3);
			this.centerSplitPanel.TabIndex = 4;
			// 
			// centerTopTextBox
			// 
			this.centerTopTextBox.BackColor = System.Drawing.SystemColors.Control;
			this.centerTopTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.centerTopTextBox.Dock = System.Windows.Forms.DockStyle.Top;
			this.centerTopTextBox.Location = new System.Drawing.Point(0, 0);
			this.centerTopTextBox.Margin = new System.Windows.Forms.Padding(5);
			this.centerTopTextBox.Name = "centerTopTextBox";
			this.centerTopTextBox.Size = new System.Drawing.Size(806, 20);
			this.centerTopTextBox.TabIndex = 1;
			// 
			// topSplitContainer
			// 
			this.topSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.topSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
			this.topSplitContainer.Location = new System.Drawing.Point(0, 53);
			this.topSplitContainer.Name = "topSplitContainer";
			// 
			// topSplitContainer.Panel1
			// 
			this.topSplitContainer.Panel1.Controls.Add(this.splitContainer1);
			this.topSplitContainer.Panel1MinSize = 150;
			// 
			// topSplitContainer.Panel2
			// 
			this.topSplitContainer.Panel2.Controls.Add(this.nodeAttributesControl);
			this.topSplitContainer.Panel2.Controls.Add(this.emptyControlRight);
			this.topSplitContainer.Panel2MinSize = 150;
			this.topSplitContainer.Size = new System.Drawing.Size(1287, 715);
			this.topSplitContainer.SplitterDistance = 1060;
			this.topSplitContainer.TabIndex = 6;
			this.topSplitContainer.Layout += new System.Windows.Forms.LayoutEventHandler(this.topSplitContainer_Layout);
			this.topSplitContainer.MouseDown += new System.Windows.Forms.MouseEventHandler(this.browseNodesTV_MouseDown);
			// 
			// nodeAttributesControl
			// 
			this.nodeAttributesControl.AutoSize = true;
			this.nodeAttributesControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.nodeAttributesControl.Location = new System.Drawing.Point(0, 0);
			this.nodeAttributesControl.Name = "nodeAttributesControl";
			this.nodeAttributesControl.Size = new System.Drawing.Size(223, 715);
			this.nodeAttributesControl.TabIndex = 0;
			this.nodeAttributesControl.Visible = false;
			// 
			// emptyControlRight
			// 
			this.emptyControlRight.Dock = System.Windows.Forms.DockStyle.Fill;
			this.emptyControlRight.Location = new System.Drawing.Point(0, 0);
			this.emptyControlRight.Name = "emptyControlRight";
			this.emptyControlRight.Size = new System.Drawing.Size(223, 715);
			this.emptyControlRight.TabIndex = 2;
			// 
			// defaultContextMenuStrip
			// 
			this.defaultContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem19,
            this.refreshToolStripMenuItem16});
			this.defaultContextMenuStrip.Name = "defaultContextMenuStrip";
			this.defaultContextMenuStrip.Size = new System.Drawing.Size(147, 48);
			// 
			// toolStripMenuItem19
			// 
			this.toolStripMenuItem19.Name = "toolStripMenuItem19";
			this.toolStripMenuItem19.Size = new System.Drawing.Size(146, 22);
			this.toolStripMenuItem19.Text = "Copy node ID";
			this.toolStripMenuItem19.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem16
			// 
			this.refreshToolStripMenuItem16.Name = "refreshToolStripMenuItem16";
			this.refreshToolStripMenuItem16.Size = new System.Drawing.Size(146, 22);
			this.refreshToolStripMenuItem16.Text = "Refresh";
			this.refreshToolStripMenuItem16.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// variableTypesContextMenu
			// 
			this.variableTypesContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addVariableTypeMenuItem,
            this.toolStripSeparator15,
            this.toolStripMenuItem23,
            this.refreshToolStripMenuItem14});
			this.variableTypesContextMenu.Name = "removeTypeContextMenu";
			this.variableTypesContextMenu.Size = new System.Drawing.Size(167, 76);
			// 
			// addVariableTypeMenuItem
			// 
			this.addVariableTypeMenuItem.Name = "addVariableTypeMenuItem";
			this.addVariableTypeMenuItem.Size = new System.Drawing.Size(166, 22);
			this.addVariableTypeMenuItem.Text = "Add variable type";
			this.addVariableTypeMenuItem.Click += new System.EventHandler(this.addVariableTypeMenuItem_Click);
			// 
			// toolStripSeparator15
			// 
			this.toolStripSeparator15.Name = "toolStripSeparator15";
			this.toolStripSeparator15.Size = new System.Drawing.Size(163, 6);
			// 
			// toolStripMenuItem23
			// 
			this.toolStripMenuItem23.Name = "toolStripMenuItem23";
			this.toolStripMenuItem23.Size = new System.Drawing.Size(166, 22);
			this.toolStripMenuItem23.Text = "Copy node ID";
			this.toolStripMenuItem23.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem14
			// 
			this.refreshToolStripMenuItem14.Name = "refreshToolStripMenuItem14";
			this.refreshToolStripMenuItem14.Size = new System.Drawing.Size(166, 22);
			this.refreshToolStripMenuItem14.Text = "Refresh";
			this.refreshToolStripMenuItem14.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// variableTypeContextMenu
			// 
			this.variableTypeContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeVariableTypeMenuItem,
            this.toolStripMenuItem21,
            this.addPropertyMenuItem,
            this.addVariableMenuItem,
            this.toolStripSeparator16,
            this.toolStripMenuItem25,
            this.refreshToolStripMenuItem15});
			this.variableTypeContextMenu.Name = "addTypeContextMenu";
			this.variableTypeContextMenu.Size = new System.Drawing.Size(188, 126);
			// 
			// removeVariableTypeMenuItem
			// 
			this.removeVariableTypeMenuItem.Name = "removeVariableTypeMenuItem";
			this.removeVariableTypeMenuItem.Size = new System.Drawing.Size(187, 22);
			this.removeVariableTypeMenuItem.Text = "Remove variable type";
			this.removeVariableTypeMenuItem.Click += new System.EventHandler(this.removeVariableTypeMenuItem_Click);
			// 
			// toolStripMenuItem21
			// 
			this.toolStripMenuItem21.Name = "toolStripMenuItem21";
			this.toolStripMenuItem21.Size = new System.Drawing.Size(184, 6);
			// 
			// addPropertyMenuItem
			// 
			this.addPropertyMenuItem.Name = "addPropertyMenuItem";
			this.addPropertyMenuItem.Size = new System.Drawing.Size(187, 22);
			this.addPropertyMenuItem.Text = "Add property";
			this.addPropertyMenuItem.Click += new System.EventHandler(this.addPropertyMenuItem_Click);
			// 
			// addVariableMenuItem
			// 
			this.addVariableMenuItem.Name = "addVariableMenuItem";
			this.addVariableMenuItem.Size = new System.Drawing.Size(187, 22);
			this.addVariableMenuItem.Text = "Add variable";
			this.addVariableMenuItem.Click += new System.EventHandler(this.addVariableMenuItem_Click);
			// 
			// toolStripSeparator16
			// 
			this.toolStripSeparator16.Name = "toolStripSeparator16";
			this.toolStripSeparator16.Size = new System.Drawing.Size(184, 6);
			// 
			// toolStripMenuItem25
			// 
			this.toolStripMenuItem25.Name = "toolStripMenuItem25";
			this.toolStripMenuItem25.Size = new System.Drawing.Size(187, 22);
			this.toolStripMenuItem25.Text = "Copy node ID";
			this.toolStripMenuItem25.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem15
			// 
			this.refreshToolStripMenuItem15.Name = "refreshToolStripMenuItem15";
			this.refreshToolStripMenuItem15.Size = new System.Drawing.Size(187, 22);
			this.refreshToolStripMenuItem15.Text = "Refresh";
			this.refreshToolStripMenuItem15.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// variableTypeChildContextMenu
			// 
			this.variableTypeChildContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeVariableChildMenuItem,
            this.toolStripSeparator17,
            this.toolStripMenuItem26,
            this.refreshToolStripMenuItem17});
			this.variableTypeChildContextMenu.Name = "addTypeContextMenu";
			this.variableTypeChildContextMenu.Size = new System.Drawing.Size(191, 76);
			// 
			// removeVariableChildMenuItem
			// 
			this.removeVariableChildMenuItem.Name = "removeVariableChildMenuItem";
			this.removeVariableChildMenuItem.Size = new System.Drawing.Size(190, 22);
			this.removeVariableChildMenuItem.Text = "Remove variable child";
			this.removeVariableChildMenuItem.Click += new System.EventHandler(this.removeVariableChildMenuItem_Click);
			// 
			// toolStripSeparator17
			// 
			this.toolStripSeparator17.Name = "toolStripSeparator17";
			this.toolStripSeparator17.Size = new System.Drawing.Size(187, 6);
			// 
			// toolStripMenuItem26
			// 
			this.toolStripMenuItem26.Name = "toolStripMenuItem26";
			this.toolStripMenuItem26.Size = new System.Drawing.Size(190, 22);
			this.toolStripMenuItem26.Text = "Copy node ID";
			this.toolStripMenuItem26.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// refreshToolStripMenuItem17
			// 
			this.refreshToolStripMenuItem17.Name = "refreshToolStripMenuItem17";
			this.refreshToolStripMenuItem17.Size = new System.Drawing.Size(190, 22);
			this.refreshToolStripMenuItem17.Text = "Refresh";
			this.refreshToolStripMenuItem17.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// devicesContextMenu
			// 
			this.devicesContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addDeviceMenuItem,
            this.toolStripSeparator14,
            this.toolStripMenuItem22,
            this.toolStripMenuItem24});
			this.devicesContextMenu.Name = "addTypeContextMenu";
			this.devicesContextMenu.Size = new System.Drawing.Size(147, 76);
			// 
			// addDeviceMenuItem
			// 
			this.addDeviceMenuItem.Name = "addDeviceMenuItem";
			this.addDeviceMenuItem.Size = new System.Drawing.Size(146, 22);
			this.addDeviceMenuItem.Text = "Add device";
			this.addDeviceMenuItem.Click += new System.EventHandler(this.addDeviceMenuItem_Click);
			// 
			// toolStripSeparator14
			// 
			this.toolStripSeparator14.Name = "toolStripSeparator14";
			this.toolStripSeparator14.Size = new System.Drawing.Size(143, 6);
			// 
			// toolStripMenuItem22
			// 
			this.toolStripMenuItem22.Name = "toolStripMenuItem22";
			this.toolStripMenuItem22.Size = new System.Drawing.Size(146, 22);
			this.toolStripMenuItem22.Text = "Copy node ID";
			this.toolStripMenuItem22.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// toolStripMenuItem24
			// 
			this.toolStripMenuItem24.Name = "toolStripMenuItem24";
			this.toolStripMenuItem24.Size = new System.Drawing.Size(146, 22);
			this.toolStripMenuItem24.Text = "Refresh";
			this.toolStripMenuItem24.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// bindingContextMenu
			// 
			this.bindingContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteBindingMenuItem,
            this.toolStripSeparator18,
            this.toolStripMenuItem27,
            this.toolStripMenuItem28});
			this.bindingContextMenu.Name = "defaultContextMenuStrip";
			this.bindingContextMenu.Size = new System.Drawing.Size(152, 76);
			// 
			// deleteBindingMenuItem
			// 
			this.deleteBindingMenuItem.Name = "deleteBindingMenuItem";
			this.deleteBindingMenuItem.Size = new System.Drawing.Size(151, 22);
			this.deleteBindingMenuItem.Text = "Delete binding";
			this.deleteBindingMenuItem.Click += new System.EventHandler(this.deleteBindingMenuItem_Click);
			// 
			// toolStripSeparator18
			// 
			this.toolStripSeparator18.Name = "toolStripSeparator18";
			this.toolStripSeparator18.Size = new System.Drawing.Size(148, 6);
			// 
			// toolStripMenuItem27
			// 
			this.toolStripMenuItem27.Name = "toolStripMenuItem27";
			this.toolStripMenuItem27.Size = new System.Drawing.Size(151, 22);
			this.toolStripMenuItem27.Text = "Copy node ID";
			this.toolStripMenuItem27.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// toolStripMenuItem28
			// 
			this.toolStripMenuItem28.Name = "toolStripMenuItem28";
			this.toolStripMenuItem28.Size = new System.Drawing.Size(151, 22);
			this.toolStripMenuItem28.Text = "Refresh";
			this.toolStripMenuItem28.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// variableContextMenu
			// 
			this.variableContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addVariableHistoryMenuItem,
            this.removeVariableHistoryMenuItem,
            this.toolStripSeparator3,
            this.addVariableBindingMenuItem,
            this.addVariableBindingSeparator,
            this.editParameterMenuItem,
            this.deassignParameterMenuItem,
            this.deleteParameterMenuItem,
            this.parameterSpecificMenuItemSeparator,
            this.toolStripMenuItem31,
            this.toolStripMenuItem32});
			this.variableContextMenu.Name = "removeParameterContextMenu";
			this.variableContextMenu.Size = new System.Drawing.Size(205, 220);
			this.variableContextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.variableContextMenu_Opening);
			// 
			// addVariableHistoryMenuItem
			// 
			this.addVariableHistoryMenuItem.Name = "addVariableHistoryMenuItem";
			this.addVariableHistoryMenuItem.Size = new System.Drawing.Size(204, 22);
			this.addVariableHistoryMenuItem.Text = "Enable Historical Access";
			this.addVariableHistoryMenuItem.Click += new System.EventHandler(this.addVariableHistoryMenuItem_Click);
			// 
			// removeVariableHistoryMenuItem
			// 
			this.removeVariableHistoryMenuItem.Name = "removeVariableHistoryMenuItem";
			this.removeVariableHistoryMenuItem.Size = new System.Drawing.Size(204, 22);
			this.removeVariableHistoryMenuItem.Text = "Disable Historical Access";
			this.removeVariableHistoryMenuItem.Click += new System.EventHandler(this.removeVariableHistoryMenuItem_Click);
			// 
			// toolStripSeparator3
			// 
			this.toolStripSeparator3.Name = "toolStripSeparator3";
			this.toolStripSeparator3.Size = new System.Drawing.Size(201, 6);
			// 
			// addVariableBindingMenuItem
			// 
			this.addVariableBindingMenuItem.Name = "addVariableBindingMenuItem";
			this.addVariableBindingMenuItem.Size = new System.Drawing.Size(204, 22);
			this.addVariableBindingMenuItem.Text = "Add binding";
			this.addVariableBindingMenuItem.Click += new System.EventHandler(this.addBindingMenuItem_Click);
			// 
			// addVariableBindingSeparator
			// 
			this.addVariableBindingSeparator.Name = "addVariableBindingSeparator";
			this.addVariableBindingSeparator.Size = new System.Drawing.Size(201, 6);
			// 
			// editParameterMenuItem
			// 
			this.editParameterMenuItem.Name = "editParameterMenuItem";
			this.editParameterMenuItem.Size = new System.Drawing.Size(204, 22);
			this.editParameterMenuItem.Text = "Edit Parameter";
			this.editParameterMenuItem.Visible = false;
			this.editParameterMenuItem.Click += new System.EventHandler(this.editParameterMenuItem_Click);
			// 
			// deassignParameterMenuItem
			// 
			this.deassignParameterMenuItem.Name = "deassignParameterMenuItem";
			this.deassignParameterMenuItem.Size = new System.Drawing.Size(204, 22);
			this.deassignParameterMenuItem.Text = "Deassign Parameter";
			this.deassignParameterMenuItem.Click += new System.EventHandler(this.deassignParameterMenuItem_Click);
			// 
			// deleteParameterMenuItem
			// 
			this.deleteParameterMenuItem.Name = "deleteParameterMenuItem";
			this.deleteParameterMenuItem.Size = new System.Drawing.Size(204, 22);
			this.deleteParameterMenuItem.Text = "Delete Parameter";
			this.deleteParameterMenuItem.Click += new System.EventHandler(this.deleteParameterMenuItem_Click);
			// 
			// parameterSpecificMenuItemSeparator
			// 
			this.parameterSpecificMenuItemSeparator.Name = "parameterSpecificMenuItemSeparator";
			this.parameterSpecificMenuItemSeparator.Size = new System.Drawing.Size(201, 6);
			// 
			// toolStripMenuItem31
			// 
			this.toolStripMenuItem31.Name = "toolStripMenuItem31";
			this.toolStripMenuItem31.Size = new System.Drawing.Size(204, 22);
			this.toolStripMenuItem31.Text = "Copy node ID";
			this.toolStripMenuItem31.Click += new System.EventHandler(this.copyNodeIdMenuItem_Click);
			// 
			// toolStripMenuItem32
			// 
			this.toolStripMenuItem32.Name = "toolStripMenuItem32";
			this.toolStripMenuItem32.Size = new System.Drawing.Size(204, 22);
			this.toolStripMenuItem32.Text = "Refresh";
			this.toolStripMenuItem32.Click += new System.EventHandler(this.refreshMenuItem_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1287, 790);
			this.Controls.Add(this.topSplitContainer);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.statusBar);
			this.Controls.Add(this.MenuBar);
			this.MainMenuStrip = this.MenuBar;
			this.Name = "MainForm";
			this.Text = "Unified Host Modeler";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mainForm_FormClosing);
			this.MenuBar.ResumeLayout(false);
			this.MenuBar.PerformLayout();
			this.statusBar.ResumeLayout(false);
			this.statusBar.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.parameterSetContextMenu.ResumeLayout(false);
			this.deviceTypesContextMenu.ResumeLayout(false);
			this.deviceTypeContextMenu.ResumeLayout(false);
			this.deviceContextMenu.ResumeLayout(false);
			this.workflowSetContextMenu.ResumeLayout(false);
			this.workflowContextMenu.ResumeLayout(false);
			this.programSetContextMenu.ResumeLayout(false);
			this.programContextMenu.ResumeLayout(false);
			this.connectorSetContextMenu.ResumeLayout(false);
			this.connectorContextMenu.ResumeLayout(false);
			this.functionalGroupContextMenu.ResumeLayout(false);
			this.copyNodeIdToClipboardContextMenu.ResumeLayout(false);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.centerPanel.ResumeLayout(false);
			this.centerPanel.PerformLayout();
			this.centerButtomPanel.ResumeLayout(false);
			this.topSplitContainer.Panel1.ResumeLayout(false);
			this.topSplitContainer.Panel2.ResumeLayout(false);
			this.topSplitContainer.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.topSplitContainer)).EndInit();
			this.topSplitContainer.ResumeLayout(false);
			this.defaultContextMenuStrip.ResumeLayout(false);
			this.variableTypesContextMenu.ResumeLayout(false);
			this.variableTypeContextMenu.ResumeLayout(false);
			this.variableTypeChildContextMenu.ResumeLayout(false);
			this.devicesContextMenu.ResumeLayout(false);
			this.bindingContextMenu.ResumeLayout(false);
			this.variableContextMenu.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuBar;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.ToolStripMenuItem ServerMI;
        private System.Windows.Forms.ToolStripMenuItem Server_DiscoverMI;
        private System.Windows.Forms.ToolStripMenuItem Server_ConnectMI;
        private System.Windows.Forms.ToolStripMenuItem Server_DisconnectMI;
        private System.Windows.Forms.ToolStripMenuItem Server_SetUserMI;
        private System.Windows.Forms.ToolStripMenuItem fILEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private Opc.Ua.Client.Controls.ConnectServerCtrl ConnectServerCTRL;
		private System.Windows.Forms.ToolStripMenuItem openProjectToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveProjectToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem addPropertyToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip parameterSetContextMenu;
		private System.Windows.Forms.ContextMenuStrip deviceTypesContextMenu;
		private System.Windows.Forms.ToolStripMenuItem addDeviceTypeToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip deviceTypeContextMenu;
		private System.Windows.Forms.ToolStripMenuItem removeTypeToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip deviceContextMenu;
		private System.Windows.Forms.ToolStripMenuItem deleteDeviceToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem createInstanceToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem HelpMI;
		private System.Windows.Forms.ToolStripMenuItem Help_ContentsMI;
		private System.Windows.Forms.ContextMenuStrip workflowSetContextMenu;
		private System.Windows.Forms.ToolStripMenuItem addWorkflowToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip workflowContextMenu;
		private System.Windows.Forms.ToolStripMenuItem removeWorkflowToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip programSetContextMenu;
		private System.Windows.Forms.ToolStripMenuItem addProgramToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip programContextMenu;
		private System.Windows.Forms.ToolStripMenuItem removeProgramToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip connectorSetContextMenu;
		private System.Windows.Forms.ToolStripMenuItem addConnectorToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip connectorContextMenu;
		private System.Windows.Forms.ToolStripMenuItem deleteConnectorToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem addFunctionaGroupMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
		private System.Windows.Forms.ContextMenuStrip functionalGroupContextMenu;
		private System.Windows.Forms.ToolStripMenuItem addFunctionalGroupToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
		private System.Windows.Forms.ToolStripMenuItem removeFunctionalGroupToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem assignParameterMenuItem;
		private System.Windows.Forms.ContextMenuStrip copyNodeIdToClipboardContextMenu;
		private System.Windows.Forms.ToolStripMenuItem copyNodeIdToClipboardToolStripMenuItem;
		private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
		private UnifiedStudio.Controls.EmptyControl emptyControlRight;
		private UnifiedStudio.Controls.NodeAttributesControl nodeAttributesControl;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.TreeView browseNodesTreeView;
		private System.Windows.Forms.Panel centerPanel;
		private System.Windows.Forms.Panel centerButtomPanel;
		private UnifiedStudio.Controls.BindS7TcpIpVariableProgramControl bingS7TcpIpVariableProgramControl;
		private UnifiedStudio.Controls.WorkflowsControl workflowsControl;
		private UnifiedStudio.Controls.ProgramsControl programsControl;
		private UnifiedStudio.Controls.BingOpcUaVariableProgramControl bingOpcUaVariableProgramControl;
		private UnifiedStudio.Controls.S7TcpIpConnectorControl s7TcpIpConnectorControl;
		private UnifiedHostModeler.Views.Device.DeviceInfoControl deviceInfoControl;
		private UnifiedStudio.Controls.ConditionMonitoringProgramControl ConditionMonitoringProgramControl;
		private UnifiedStudio.Controls.ConnectorsControl connectorsControl;
		private UnifiedStudio.Controls.EmptyControl emptyControlCenter;
		private UnifiedStudio.Controls.ParametersControl parametersControl;
		private UnifiedStudio.Controls.VariableControl variableControl;
		private UnifiedStudio.Controls.OpcUaConnectorControl opcUaConnectorControl;
		private UnifiedStudio.Controls.DatabaseConnectorControl databaseConnectorControl;
		private System.Windows.Forms.Panel centerSplitPanel;
		private System.Windows.Forms.SplitContainer topSplitContainer;
		private System.Windows.Forms.TextBox centerTopTextBox;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
		private System.Windows.Forms.ToolStripMenuItem copyNodeIDToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem6;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem9;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem10;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem11;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem12;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem13;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem14;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem15;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem16;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem18;
		private System.Windows.Forms.ContextMenuStrip defaultContextMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem19;
		private UnifiedStudio.Controls.EditWorkflowControl editWorkflowControl;
		private UnifiedStudio.Controls.DevicesControl devicesControl;
		private System.Windows.Forms.ContextMenuStrip variableTypesContextMenu;
		private System.Windows.Forms.ToolStripMenuItem addVariableTypeMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem23;
		private System.Windows.Forms.ContextMenuStrip variableTypeContextMenu;
		private System.Windows.Forms.ToolStripMenuItem removeVariableTypeMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem25;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem21;
		private System.Windows.Forms.ToolStripMenuItem addPropertyMenuItem;
		private System.Windows.Forms.ToolStripMenuItem addVariableMenuItem;
		private System.Windows.Forms.ContextMenuStrip variableTypeChildContextMenu;
		private System.Windows.Forms.ToolStripMenuItem removeVariableChildMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator17;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem26;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem1;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem4;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem5;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem6;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem7;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem8;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem9;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem10;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem11;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem13;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem16;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem14;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem15;
		private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem17;
		private System.Windows.Forms.ContextMenuStrip devicesContextMenu;
		private System.Windows.Forms.ToolStripMenuItem addDeviceMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem22;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem24;
		private System.Windows.Forms.ContextMenuStrip bindingContextMenu;
		private System.Windows.Forms.ToolStripMenuItem deleteBindingMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator18;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem27;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem28;
		private System.Windows.Forms.ContextMenuStrip variableContextMenu;
		private System.Windows.Forms.ToolStripMenuItem addVariableBindingMenuItem;
		private System.Windows.Forms.ToolStripSeparator addVariableBindingSeparator;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem31;
		private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem32;
		private System.Windows.Forms.ToolStripMenuItem addVariableHistoryMenuItem;
		private System.Windows.Forms.ToolStripMenuItem removeVariableHistoryMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
		private System.Windows.Forms.ToolStripSeparator parameterSpecificMenuItemSeparator;
		private System.Windows.Forms.ToolStripMenuItem editParameterMenuItem;
		private System.Windows.Forms.ToolStripMenuItem deleteParameterMenuItem;
		private System.Windows.Forms.ToolStripMenuItem deassignParameterMenuItem;
	}
}
