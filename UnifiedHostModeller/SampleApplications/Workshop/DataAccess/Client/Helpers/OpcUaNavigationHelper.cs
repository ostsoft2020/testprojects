﻿using Opc.Ua;
using Opc.Ua.Client;
using Quickstarts;
using System;
using System.Linq;

namespace UnifiedHostModeler.Helpers
{
	public static class OpcUaNavigationHelper
	{
		public static NodeId GetDeviceSetNodeId(this Session session)
		{
			return new NodeId(5001, session.GetDevicesNsIndx());
		}

		public static NodeId GetConnectorTypeNodeId(this Session session)
		{
			return new NodeId("UnifiedConnectorType", session.GetIhCoreTypesNsIndx());
		}

		public static NodeId GetVariableBindingTypeNodeId(this Session session)
		{
			return new NodeId("VariableBindingType", session.GetIhCoreTypesNsIndx());
		}

		public static NodeId GetDynamicDeviceTypeNodeId(this Session session)
		{
			return new NodeId("DynamicDeviceType", session.GetIhDeviceTypesNsIndx());
		}

		public static NodeId GetHasVariableBindingNodeId(this Session session)
		{
			return new NodeId("HasVariableBinding", session.GetIhCoreTypesNsIndx());
		}
		
		public static NodeId GetConfigNodeId(this Session session)
		{
			return new NodeId("Config", session.GetOstsoftConfigNsIndx());
		}
		
		public static NodeId GetConfigSaveNodeId(this Session session)
		{
			return new NodeId(15001, session.GetOstsoftConfigNsIndx());
		}

		public static NodeId GetDeviceMangerNodeId(this Session session)
		{
			return new NodeId("DeviceManager", session.GetIhDeviceInstancesNsIndx());
		}
		
		public static NodeId GetCreateDeviceMethodNodeId(this Session session)
		{
			return new NodeId(20, session.GetIhDeviceInstancesNsIndx());
		}


		public static NodeId ToNodeId(this ExpandedNodeId eNodeId, Session session)
		{
			return ExpandedNodeId.ToNodeId(eNodeId, session.NamespaceUris);
		}

		public static ReferenceDescription GetParentReference(Session session, NodeId start)
		{
			BrowseDescriptionCollection browseDescriptions =
				new BrowseDescriptionCollection()
				{
					new BrowseDescription()
					{
						NodeId = start,
						BrowseDirection = BrowseDirection.Inverse,
						ReferenceTypeId = ReferenceTypeIds.HasChild,
						IncludeSubtypes = true,
						NodeClassMask = (uint) NodeClass.Object | (uint) NodeClass.Variable,
						ResultMask = (uint) BrowseResultMask.All,
					},
					new BrowseDescription()
					{
						NodeId = start,
						BrowseDirection = BrowseDirection.Inverse,
						ReferenceTypeId = ReferenceTypeIds.Organizes,
						IncludeSubtypes = true,
						NodeClassMask = (uint) NodeClass.Object,
						ResultMask = (uint) BrowseResultMask.All,
					},
				};
			ReferenceDescriptionCollection references = FormUtils.Browse(session, browseDescriptions, false);

			return
				references.FirstOrDefault(r => r.ReferenceTypeId == ReferenceTypeIds.HasComponent)
				?? references.FirstOrDefault(r => r.ReferenceTypeId == ReferenceTypeIds.HasProperty)
				?? references.FirstOrDefault(r => session.TypeTree.IsTypeOf(r.ReferenceTypeId, ReferenceTypeIds.HasComponent))
				?? references.FirstOrDefault(r => session.TypeTree.IsTypeOf(r.ReferenceTypeId, ReferenceTypeIds.HasProperty))
				?? references.FirstOrDefault(r => session.TypeTree.IsTypeOf(r.ReferenceTypeId, ReferenceTypeIds.Aggregates))
				?? references.FirstOrDefault(r => session.TypeTree.IsTypeOf(r.ReferenceTypeId, ReferenceTypeIds.HasChild))
				?? references.FirstOrDefault();
		}

		public static ReferenceDescription GetAncestorReference(Session session, ReferenceDescription start, Func<ReferenceDescription, bool> stopper = null)
		{
			ReferenceDescription ancestor = null;
			do
			{
				ancestor = GetParentReference(session, (ancestor ?? start).NodeId.ToNodeId(session));
			}
			while (ancestor != null && stopper?.Invoke(ancestor) == false);

			return ancestor;
		}

		public static ReferenceDescription GetAncestorReference(Session session, NodeId start, Func<ReferenceDescription, bool> stopper = null)
		{
			ReferenceDescription ancestor = null;
			do
			{
				ancestor = GetParentReference(session, ancestor?.NodeId.ToNodeId(session) ?? start);
			}
			while (ancestor != null && stopper?.Invoke(ancestor) == false);

			return ancestor;
		}

		/// <summary>
		/// Browses the address space and returns the references found.
		/// </summary>
		/// <param name="session">The session.</param>
		/// <param name="browseDescription">The set of browse operations to perform.</param>
		/// <returns>
		/// The references found. Null if an error occurred.
		/// </returns>
		public static ReferenceDescriptionCollection Browse(Session session, BrowseDescriptionCollection browseDescription, bool throwOnError, Func<ReferenceDescription, bool> childFilter = null)
		{
			try
			{
				ReferenceDescriptionCollection references = new ReferenceDescriptionCollection();
				BrowseDescriptionCollection unprocessedOperations = new BrowseDescriptionCollection();

				while (browseDescription.Count > 0)
				{
					// start the browse operation.
					BrowseResultCollection results = null;
					DiagnosticInfoCollection diagnosticInfos = null;

					session.Browse(
						null,
						null,
						0,
						browseDescription,
						out results,
						out diagnosticInfos);

					ClientBase.ValidateResponse(results, browseDescription);
					ClientBase.ValidateDiagnosticInfos(diagnosticInfos, browseDescription);

					ByteStringCollection continuationPoints = new ByteStringCollection();

					for (int ii = 0; ii < browseDescription.Count; ii++)
					{
						// check for error.
						if (StatusCode.IsBad(results[ii].StatusCode))
						{
							// this error indicates that the server does not have enough simultaneously active 
							// continuation points. This request will need to be resent after the other operations
							// have been completed and their continuation points released.
							if (results[ii].StatusCode == StatusCodes.BadNoContinuationPoints)
								unprocessedOperations.Add(browseDescription[ii]);

							continue;
						}

						// check if all references have been fetched.
						if (results[ii].References.Count == 0)
							continue;

						// save results.
						references.AddRange(results[ii].References);

						// check for continuation point.
						if (results[ii].ContinuationPoint != null)
							continuationPoints.Add(results[ii].ContinuationPoint);
					}

					// process continuation points.
					ByteStringCollection revisedContiuationPoints = new ByteStringCollection();

					while (continuationPoints.Count > 0)
					{
						// continue browse operation.
						session.BrowseNext(
							null,
							false,
							continuationPoints,
							out results,
							out diagnosticInfos);

						ClientBase.ValidateResponse(results, continuationPoints);
						ClientBase.ValidateDiagnosticInfos(diagnosticInfos, continuationPoints);

						for (int ii = 0; ii < continuationPoints.Count; ii++)
						{
							// check for error.
							if (StatusCode.IsBad(results[ii].StatusCode))
								continue;

							// check if all references have been fetched.
							if (results[ii].References.Count == 0)
								continue;

							// save results.
							references.AddRange(results[ii].References);

							// check for continuation point.
							if (results[ii].ContinuationPoint != null)
								revisedContiuationPoints.Add(results[ii].ContinuationPoint);
						}

						// check if browsing must continue;
						revisedContiuationPoints = continuationPoints;
					}

					// check if unprocessed results exist.
					browseDescription = unprocessedOperations;
				}

				if (childFilter != null)
					for (int i = references.Count - 1; i >= 0; i--)
						if (!childFilter(references[i]))
							references.RemoveAt(i);

				// return complete list.
				return references;
			}
			catch (Exception exception)
			{
				if (throwOnError)
					throw new ServiceResultException(exception, StatusCodes.BadUnexpectedError);

				return null;
			}
		}

		public static ReferenceDescriptionCollection Browse(Session session, NodeId nodeId, BrowseDirection browseDirection, NodeClass nodeClassesMask, NodeId[] referenceTypeIds, bool includeSubtypes = true, BrowseResultMask browseResultMask = BrowseResultMask.All, Func<ReferenceDescription, bool> childFilter = null)
		{
			if (referenceTypeIds == null || referenceTypeIds.Length == 0)
				return new ReferenceDescriptionCollection();

			BrowseDescriptionCollection browseDescriptions = new BrowseDescriptionCollection();

			for (int i = 0; i < referenceTypeIds.Length; i++)
			{
				BrowseDescription browseDescription = new BrowseDescription()
				{
					NodeId = nodeId,
					BrowseDirection = browseDirection,
					ReferenceTypeId = referenceTypeIds[i],
					IncludeSubtypes = includeSubtypes,
					NodeClassMask = (uint)nodeClassesMask,
					ResultMask = (uint)browseResultMask,
				};

				browseDescriptions.Add(browseDescription);
			}

			return Browse(session, browseDescriptions, false, childFilter);
		}

		public static ReferenceDescriptionCollection BrowseChildren(Session session, NodeId nodeId, NodeClass nodeClassesMask, bool includeOrganizes = false, BrowseResultMask browseResultMask = BrowseResultMask.All, Func<ReferenceDescription, bool> childFilter = null)
		{
			BrowseDescriptionCollection browseDescriptions = new BrowseDescriptionCollection
			{
				new BrowseDescription()
				{
					NodeId = nodeId,
					BrowseDirection = BrowseDirection.Forward,
					ReferenceTypeId = ReferenceTypeIds.HasChild,
					IncludeSubtypes = true,
					NodeClassMask = (uint)nodeClassesMask,
					ResultMask = (uint)browseResultMask,
				}
			};

			if (includeOrganizes)
				browseDescriptions.Add(
					new BrowseDescription()
					{
						NodeId = nodeId,
						BrowseDirection = BrowseDirection.Forward,
						ReferenceTypeId = ReferenceTypeIds.Organizes,
						IncludeSubtypes = true,
						NodeClassMask = (uint)nodeClassesMask,
						ResultMask = (uint)browseResultMask,
					}
				);

			return Browse(session, browseDescriptions, false, childFilter);
		}
	}
}
