﻿using Opc.Ua;
using Opc.Ua.Client;
using System;
using System.Xml;
using UnifiedStudio;

namespace UnifiedHostModeler.Helpers
{
	public static class OpcUaHelper
	{
		public static void SaveConfiguration(Session session)
		{
			try
			{
				session.Call(session.GetConfigNodeId(), session.GetConfigSaveNodeId());
			}
			catch (Exception e)
			{
				throw new Exception("Can't save AddressSpace", e);
			}

			//session.Call(Constants.ConfigId, Constants.SaveConfigMethodId);
		}

		public static bool ValidateName(string name, string propertyName, out string errorMessage)
		{
			try
			{
				XmlConvert.VerifyName(name);
			}
			catch (XmlException e)
			{
				errorMessage = $"{propertyName} is not valid: {e.Message}";
				return false;
			}
			catch (ArgumentNullException)
			{
				errorMessage = $"{propertyName} is null or empty";
				return false;
			}

			errorMessage = "";
			return true;
		}

		public static bool CheckNodeHasChildren(Session session, NodeId nodeId, NodeKind nodeKind)
		{
			Func<ReferenceDescription, bool> childFilter = null;
			NodeClass nodeClassMask = 0;

			switch (nodeKind)
			{
				case NodeKind.UnknownVariable:
				case NodeKind.ComplexParameter:
				case NodeKind.ComplexParameterReference:
				case NodeKind.ComplexVariable:
				case NodeKind.SimpleParameter:
				case NodeKind.SimpleParameterReference:
				case NodeKind.SimpleVariable:
				case NodeKind.Method:
				case NodeKind.Connector:
				case NodeKind.Workflow:
					nodeClassMask = NodeClass.Object | NodeClass.Variable;
					break;

				case NodeKind.Program:
				case NodeKind.Device:
				case NodeKind.Devices:
				case NodeKind.DeviceType:
				case NodeKind.Object:
					nodeClassMask = NodeClass.Object | NodeClass.Variable;
					break;

				case NodeKind.VariableType:
				case NodeKind.ParameterSet:
					nodeClassMask = NodeClass.Variable;
					break;

				case NodeKind.MethodSet:
					nodeClassMask = NodeClass.Method;
					break;

				case NodeKind.ConnectorSet:
				case NodeKind.ProgramSet:
				case NodeKind.WorkflowSet:
					nodeClassMask = NodeClass.Object;
					break;

				case NodeKind.FunctionalGroup:
					nodeClassMask = NodeClass.Object | NodeClass.Variable;// | NodeClass.Method;
					break;

				case NodeKind.Binding:
					return true;
			}

			if (nodeClassMask != 0)
			{
				ReferenceDescriptionCollection grandsons = OpcUaNavigationHelper.Browse(
					session,
					nodeId,
					BrowseDirection.Forward,
					nodeClassMask,
					new NodeId[] { ReferenceTypeIds.HasChild, ReferenceTypeIds.Organizes },
					childFilter: childFilter
					);

				return grandsons.Count > 0;
			}
			return false;
		}

		public static bool isVariableComplex(Session session, NodeId nodeId)
		{
			return
				OpcUaNavigationHelper.Browse(
					session,
					nodeId,
					BrowseDirection.Forward,
					NodeClass.Variable,
					new NodeId[] { ReferenceTypeIds.HasComponent, ReferenceTypeIds.HasProperty }
					)
				.Count > 0;
		}
	}
}
