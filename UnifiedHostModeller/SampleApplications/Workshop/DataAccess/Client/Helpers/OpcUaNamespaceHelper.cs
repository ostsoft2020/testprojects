﻿using Opc.Ua.Client;

namespace UnifiedHostModeler.Helpers
{
	internal static class OpcUaNamespacesHelper
	{
		public const string IhDeviceTypesUri = "http://Gimecs.com/UnifiedFactory.IntegrationHost.Devices.Types/";
		public const string IhDeviceInstancesUri = "http://Gimecs.com/UnifiedFactory.IntegrationHost.Devices.Instances/";

		public static ushort GetNsIndx(this Session session, string namespaceUri)
		{
			var index = session.NamespaceUris.GetIndex(namespaceUri);
			if (index < 0)
				throw new System.Exception($"no namespace {namespaceUri} is found on the server");
			return (ushort)index;
		}

		//uri:gimecs.com:Ostsoft.OpcUa.Server.Config
		public static ushort GetOstsoftConfigNsIndx(this Session session) => 2;

		//http://opcfoundation.org/UA/DI/
		public static ushort GetDevicesNsIndx(this Session session)  => 3;

		//http://Gimecs.com/UnifiedFactory.IntegrationHost.Core.Types/
		public static ushort GetIhCoreTypesNsIndx(this Session session)  => 4;

		//http://Gimecs.com/UnifiedFactory.IntegrationHost.Devices.Types/
		public static ushort GetIhDeviceTypesNsIndx(this Session session) => session.GetNsIndx(IhDeviceTypesUri);

		//http://Gimecs.com/UnifiedFactory.IntegrationHost.Devices.Instances/
		public static ushort GetIhDeviceInstancesNsIndx(this Session session) => session.GetNsIndx(IhDeviceInstancesUri);
	}
}
