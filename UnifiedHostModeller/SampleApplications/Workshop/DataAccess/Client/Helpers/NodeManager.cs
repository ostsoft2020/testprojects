﻿using Opc.Ua;
using Opc.Ua.Client;
using System;
using System.Linq;

namespace UnifiedHostModeler.Helpers
{
	public class NodeManager
	{
		public NodeManager(Session session)
		{
			Session = session;
		}

		public Session Session { get; }

		public void Save()
		{
			try
			{
				Session.Call(Session.GetConfigNodeId(), Session.GetConfigSaveNodeId());
			}
			catch (Exception e)
			{
				throw new Exception("Can't save AddressSpace", e);
			}
		}

		public void CreateDevice((NodeId DeviceType, string BrowseName, string DisplayName, string Description, string DeviceClass, string Manufacturer, string Model, string HardwareRevision, string SoftwareRevision, string DeviceRevision, string ProductCode, string SerialNumber) deviceDescriptor)
		{
			Session.Call(
				Session.GetDeviceMangerNodeId(),
				Session.GetCreateDeviceMethodNodeId(),
				deviceDescriptor.DeviceType,
				deviceDescriptor.BrowseName,
				deviceDescriptor.DisplayName,
				deviceDescriptor.Description,
				deviceDescriptor.DeviceClass,
				deviceDescriptor.Manufacturer,
				deviceDescriptor.Model,
				deviceDescriptor.HardwareRevision,
				deviceDescriptor.SoftwareRevision,
				deviceDescriptor.DeviceRevision,
				deviceDescriptor.ProductCode,
				deviceDescriptor.SerialNumber
			);

			Save();
		}

		public void DeleteDevice(NodeId deviceNodeId)
		{
			string deleteMethodName = "DeleteDevice";
			var methodRef =
				OpcUaNavigationHelper.BrowseChildren(Session, Session.GetDeviceMangerNodeId(), NodeClass.Method)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(deleteMethodName, Session.GetIhDeviceInstancesNsIndx()))
				?? throw new Exception($"Can't find method {deleteMethodName} at {Session.GetDeviceMangerNodeId()}");

			Session.Call(
				Session.GetDeviceMangerNodeId(),
				methodRef.NodeId.ToNodeId(Session),
				deviceNodeId
			);

			Save();
		}

		public void CreateConnector(NodeId device, (string BrowseName, string DisplayName, string Description, NodeId Type) connectorDescriptor)
		{
			const string builderName = "ConnectorBuilder";
			var builderRef =
				OpcUaNavigationHelper.BrowseChildren(Session, device, NodeClass.Object)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(builderName, Session.GetIhCoreTypesNsIndx()))
				?? throw new Exception($"Can't find object {builderName} at {device}");
			const string createMethodName = "CreateConnector";
			var methodRef =
				OpcUaNavigationHelper.BrowseChildren(Session, builderRef.NodeId.ToNodeId(Session), NodeClass.Method)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(createMethodName, Session.GetIhCoreTypesNsIndx()))
				?? throw new Exception($"Can't find method {createMethodName} at {builderRef.NodeId}");

			Session.Call(
				builderRef.NodeId.ToNodeId(Session),
				methodRef.NodeId.ToNodeId(Session),
				connectorDescriptor.BrowseName,
				connectorDescriptor.DisplayName,
				connectorDescriptor.Description,
				connectorDescriptor.Type
			);
		}

		public void DeleteConnector(NodeId connectorNodeId)
		{
			var deviceRef =
				OpcUaNavigationHelper.GetAncestorReference(Session, connectorNodeId, r => Session.TypeTree.IsTypeOf(r.TypeDefinition.ToNodeId(Session), Session.GetDynamicDeviceTypeNodeId()))
				?? throw new Exception("Can't find ancestor device for node " + connectorNodeId);
			const string builderName = "ConnectorBuilder";
			var builderRef =
				OpcUaNavigationHelper.BrowseChildren(Session, deviceRef.NodeId.ToNodeId(Session), NodeClass.Object)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(builderName, Session.GetIhCoreTypesNsIndx()))
				?? throw new Exception($"Can't find object {builderName} at {deviceRef.NodeId}");
			const string deleteMethodName = "DeleteConnector";
			var methodRef =
				OpcUaNavigationHelper.BrowseChildren(Session, builderRef.NodeId.ToNodeId(Session), NodeClass.Method)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(deleteMethodName, Session.GetIhCoreTypesNsIndx()))
				?? throw new Exception($"Can't find method {deleteMethodName} at {builderRef.NodeId}");

			Session.Call(
				builderRef.NodeId.ToNodeId(Session),
				methodRef.NodeId.ToNodeId(Session),
				connectorNodeId
			);

			Save();
		}

		public void CreateParameter(NodeId device, (string BrowseName, string DisplayName, string Description, NodeId DataType, bool IsArray, uint ArraySize, bool ReadOnly, NodeId VariableType) parameterDescriptor)
		{
			const string builderName = "Builder";
			var builderRef =
				OpcUaNavigationHelper.BrowseChildren(Session, device, NodeClass.Object)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(builderName, Session.GetIhDeviceTypesNsIndx()))
				?? throw new Exception($"Can't find object {builderName} at {device}");
			const string createMethodName = "CreateParameter";
			var methodRef =
				OpcUaNavigationHelper.BrowseChildren(Session, builderRef.NodeId.ToNodeId(Session), NodeClass.Method)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(createMethodName, Session.GetIhDeviceTypesNsIndx()))
				?? throw new Exception($"Can't find method {createMethodName} at {builderRef.NodeId}");

			Session.Call(
				builderRef.NodeId.ToNodeId(Session),
				methodRef.NodeId.ToNodeId(Session),
				parameterDescriptor.BrowseName,
				parameterDescriptor.DisplayName,
				parameterDescriptor.Description,
				parameterDescriptor.DataType,
				parameterDescriptor.IsArray,
				parameterDescriptor.ArraySize,
				parameterDescriptor.ReadOnly,
				parameterDescriptor.VariableType
			);

			Save();
		}

		public void DeleteParameter(NodeId parameterNodeId)
		{
			var deviceRef =
				OpcUaNavigationHelper.GetAncestorReference(Session, parameterNodeId, r => Session.TypeTree.IsTypeOf(r.TypeDefinition.ToNodeId(Session), Session.GetDynamicDeviceTypeNodeId()))
				?? throw new Exception("Can't find ancestor device for node " + parameterNodeId);
			const string builderName = "Builder";
			var builderRef =
				OpcUaNavigationHelper.BrowseChildren(Session, deviceRef.NodeId.ToNodeId(Session), NodeClass.Object)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(builderName, Session.GetIhDeviceTypesNsIndx()))
				?? throw new Exception($"Can't find object {builderName} at {deviceRef.NodeId}");
			const string deleteMethodName = "DeleteParameter";
			var methodRef =
				OpcUaNavigationHelper.BrowseChildren(Session, builderRef.NodeId.ToNodeId(Session), NodeClass.Method)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(deleteMethodName, Session.GetIhDeviceTypesNsIndx()))
				?? throw new Exception($"Can't find method {deleteMethodName} at {builderRef.NodeId}");

			Session.Call(
				builderRef.NodeId.ToNodeId(Session),
				methodRef.NodeId.ToNodeId(Session),
				parameterNodeId
			);

			Save();
		}

		public void CreateBinding(NodeId variableNodeId, NodeId bindingTypeNodeId)
		{
			Save();
			var deviceRef =
				OpcUaNavigationHelper.GetAncestorReference(Session, variableNodeId, r => Session.TypeTree.IsTypeOf(r.TypeDefinition.ToNodeId(Session), Session.GetDynamicDeviceTypeNodeId()))
				?? throw new Exception("Can't find ancestor device for node " + variableNodeId);
			const string builderName = "BindingBuilder";
			var builderRef =
				OpcUaNavigationHelper.BrowseChildren(Session, deviceRef.NodeId.ToNodeId(Session), NodeClass.Object)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(builderName, Session.GetIhCoreTypesNsIndx()))
				?? throw new Exception($"Can't find object {builderName} at {deviceRef.NodeId}");
			const string createBindingName = "BindVariable";
			var methodRef =
				OpcUaNavigationHelper.BrowseChildren(Session, builderRef.NodeId.ToNodeId(Session), NodeClass.Method)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(createBindingName, Session.GetIhCoreTypesNsIndx()))
				?? throw new Exception($"Can't find method {createBindingName} at {builderRef.NodeId}");

			Session.Call(
				builderRef.NodeId.ToNodeId(Session),
				methodRef.NodeId.ToNodeId(Session),
				variableNodeId,
				bindingTypeNodeId
			);

			Save();
		}

		public void DeleteBinding(NodeId bindingNodeId)
		{
			var deviceRef =
				OpcUaNavigationHelper.GetAncestorReference(Session, bindingNodeId, r => Session.TypeTree.IsTypeOf(r.TypeDefinition.ToNodeId(Session), Session.GetDynamicDeviceTypeNodeId()))
				?? throw new Exception("Can't find ancestor device for node " + bindingNodeId);
			const string builderName = "BindingBuilder";
			var builderRef =
				OpcUaNavigationHelper.BrowseChildren(Session, deviceRef.NodeId.ToNodeId(Session), NodeClass.Object)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(builderName, Session.GetIhCoreTypesNsIndx()))
				?? throw new Exception($"Can't find object {builderName} at {deviceRef.NodeId}");
			const string unbindMethodName = "Unbind";
			var methodRef =
				OpcUaNavigationHelper.BrowseChildren(Session, builderRef.NodeId.ToNodeId(Session), NodeClass.Method)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(unbindMethodName, Session.GetIhCoreTypesNsIndx()))
				?? throw new Exception($"Can't find method {unbindMethodName} at {builderRef.NodeId}");
			var variableId = OpcUaNavigationHelper.GetParentReference(Session, bindingNodeId);

			Session.Call(
				builderRef.NodeId.ToNodeId(Session),
				methodRef.NodeId.ToNodeId(Session),
				variableId.NodeId.ToNodeId(Session)
				);

			Save();
		}

		public void AddVariableHistory(NodeId variableNodeId)
		{
			var deviceRef =
				OpcUaNavigationHelper.GetAncestorReference(Session, variableNodeId, r => Session.TypeTree.IsTypeOf(r.TypeDefinition.ToNodeId(Session), Session.GetDynamicDeviceTypeNodeId()))
				?? throw new Exception("Can't find ancestor device for node " + variableNodeId);
			const string builderName = "HistoryBuilder";
			var builderRef =
				OpcUaNavigationHelper.BrowseChildren(Session, deviceRef.NodeId.ToNodeId(Session), NodeClass.Object)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(builderName, Session.GetIhCoreTypesNsIndx()))
				?? throw new Exception($"Can't find object {builderName} at {deviceRef.NodeId}");
			const string deleteMethodName = "AddDataHistory";
			var methodRef =
				OpcUaNavigationHelper.BrowseChildren(Session, builderRef.NodeId.ToNodeId(Session), NodeClass.Method)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(deleteMethodName, Session.GetIhCoreTypesNsIndx()))
				?? throw new Exception($"Can't find method {deleteMethodName} at {builderRef.NodeId}");

			Session.Call(
				builderRef.NodeId.ToNodeId(Session),
				methodRef.NodeId.ToNodeId(Session),
				variableNodeId
			);

			Save();
		}

		public void RemoveVariableHistory(NodeId variableNodeId)
		{
			var deviceRef =
				OpcUaNavigationHelper.GetAncestorReference(Session, variableNodeId, r => Session.TypeTree.IsTypeOf(r.TypeDefinition.ToNodeId(Session), Session.GetDynamicDeviceTypeNodeId()))
				?? throw new Exception("Can't find ancestor device for node " + variableNodeId);
			const string builderName = "HistoryBuilder";
			var builderRef =
				OpcUaNavigationHelper.BrowseChildren(Session, deviceRef.NodeId.ToNodeId(Session), NodeClass.Object)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(builderName, Session.GetIhCoreTypesNsIndx()))
				?? throw new Exception($"Can't find object {builderName} at {deviceRef.NodeId}");
			const string deleteMethodName = "RemoveDataHistory";
			var methodRef =
				OpcUaNavigationHelper.BrowseChildren(Session, builderRef.NodeId.ToNodeId(Session), NodeClass.Method)?.FirstOrDefault(r => r.BrowseName == new QualifiedName(deleteMethodName, Session.GetIhCoreTypesNsIndx()))
				?? throw new Exception($"Can't find method {deleteMethodName} at {builderRef.NodeId}");

			Session.Call(
				builderRef.NodeId.ToNodeId(Session),
				methodRef.NodeId.ToNodeId(Session),
				variableNodeId
			);

			Save();
		}

		public void ChangeHistorizing(NodeId variableNodeId, bool isHistorizing)
		{
			Session.Write(
				null,
				new WriteValueCollection(new WriteValue[] { new WriteValue { NodeId = variableNodeId, AttributeId = Attributes.Historizing, Value = new DataValue() { Value = isHistorizing } } }),
				out StatusCodeCollection results,
				out DiagnosticInfoCollection diagnosticInfos
			);

			var info = diagnosticInfos?.FirstOrDefault()?.AdditionalInfo;
			
			Save();
		}

	}
}
