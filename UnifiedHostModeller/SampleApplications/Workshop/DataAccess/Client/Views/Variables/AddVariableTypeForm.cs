﻿using System.Linq;
using System.Windows.Forms;
using UnifiedHostModeler;
using UnifiedHostModeler.Helpers;
using UnifiedStudio.Models;

namespace UnifiedStudio.Editors
{
	public partial class AddVariableTypeForm : Form
	{
		public AddVariableTypeForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(out VariableTypeDescriptor typeDescription)
		{
			if (ShowDialog() == DialogResult.OK)
			{
				typeDescription = new VariableTypeDescriptor
				{
					Name = nameTextBox.Text,
					Description = descriptionTextBox.Text,
					DataType = dataTypeComboBox.Text,
					IsArray = isArrayCheckBox.Checked
				};

				return true;
			}
			else
			{
				typeDescription = null;
				return false;
			}
		}

		private void formClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				return;

			if (!OpcUaHelper.ValidateName(nameTextBox.Text, "Name", out string errorMessage))
			{
				MessageBox.Show(errorMessage);
				e.Cancel = true;
			}
		}

		private void AddVariableTypeForm_Load(object sender, System.EventArgs e)
		{
			if (TypeTreeHelper.SupportedVariableTypes.TryGetValue(TypeTreeHelper.SupportedVariableTypes.Keys.First(), out var variableDescriptor))
			{
				dataTypeComboBox.Items.AddRange(variableDescriptor.DataTypes);

				dataTypeComboBox.SelectedItem = variableDescriptor.DataTypes.FirstOrDefault();
			}
		}
	}
}
