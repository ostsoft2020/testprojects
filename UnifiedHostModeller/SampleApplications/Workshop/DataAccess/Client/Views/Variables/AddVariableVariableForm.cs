﻿using System;
using System.Linq;
using System.Windows.Forms;
using UnifiedHostModeler;
using UnifiedHostModeler.Helpers;
using UnifiedStudio.Models;

namespace UnifiedStudio.Editors
{
	public partial class AddVariableVariableForm : Form
	{
		public AddVariableVariableForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(out VariableVariableDescriptor propertyDescription)
		{
			if (ShowDialog() == DialogResult.OK)
			{
				propertyDescription = new VariableVariableDescriptor
				{
					Name = nameTextBox.Text,
					Description = descriptionTextBox.Text,
					VariableType = variableTypeComboBox.Text,
					DataType = dataTypeComboBox.Text,
					IsArray = isArrayCheckBox.Checked,
					ReadOnly = readOnlyCheckBox.Checked
				};

				return true;
			}
			else
			{
				propertyDescription = null;
				return false;
			}
		}

		private void form_Load(object sender, EventArgs e)
		{
			variableTypeComboBox.Items.AddRange(TypeTreeHelper.SupportedVariableTypes.Keys.ToArray());

			variableTypeComboBox.SelectedItem = variableTypeComboBox.Items[0];
		}

		private void formClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				return;

			if (!OpcUaHelper.ValidateName(nameTextBox.Text, "Name", out string errorMessage))
			{
				MessageBox.Show(errorMessage);
				e.Cancel = true;
			}
			else if (string.IsNullOrWhiteSpace(dataTypeComboBox.Text))
			{
				MessageBox.Show("Correct data type should be specified");
				e.Cancel = true;
			}
		}

		private void variableTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			string selectedDataType = dataTypeComboBox.Text;
			dataTypeComboBox.Items.Clear();

			if (TypeTreeHelper.SupportedVariableTypes.TryGetValue(variableTypeComboBox.Text, out var variableDescriptor))
			{
				dataTypeComboBox.Items.AddRange(variableDescriptor.DataTypes);

				if (!string.IsNullOrWhiteSpace(selectedDataType) && variableDescriptor.DataTypes.Contains(selectedDataType))
					dataTypeComboBox.SelectedItem = selectedDataType;

				if (string.IsNullOrWhiteSpace(dataTypeComboBox.Text) && variableDescriptor.DataTypes.Length > 0)
					dataTypeComboBox.SelectedItem = variableDescriptor.DataTypes[0];
			}
		}
	}
}
