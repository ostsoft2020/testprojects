﻿namespace UnifiedStudio.Controls
{
	partial class DatabaseConnectorControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.providerTypeComboBox = new System.Windows.Forms.ComboBox();
			this.connectionStringTextBox = new System.Windows.Forms.TextBox();
			this.testButton = new System.Windows.Forms.Button();
			this.autostartCheckBox = new System.Windows.Forms.CheckBox();
			this.label3 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(14, 14);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(69, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Provider type";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(14, 49);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(89, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Connection string";
			// 
			// providerTypeComboBox
			// 
			this.providerTypeComboBox.FormattingEnabled = true;
			this.providerTypeComboBox.Items.AddRange(new object[] {
            "MSSQL"});
			this.providerTypeComboBox.Location = new System.Drawing.Point(111, 10);
			this.providerTypeComboBox.Name = "providerTypeComboBox";
			this.providerTypeComboBox.Size = new System.Drawing.Size(291, 21);
			this.providerTypeComboBox.TabIndex = 2;
			// 
			// connectionStringTextBox
			// 
			this.connectionStringTextBox.Location = new System.Drawing.Point(111, 46);
			this.connectionStringTextBox.Name = "connectionStringTextBox";
			this.connectionStringTextBox.Size = new System.Drawing.Size(372, 20);
			this.connectionStringTextBox.TabIndex = 3;
			this.connectionStringTextBox.TextChanged += new System.EventHandler(this.connectionStringTextBox_TextChanged);
			// 
			// testButton
			// 
			this.testButton.Location = new System.Drawing.Point(408, 9);
			this.testButton.Name = "testButton";
			this.testButton.Size = new System.Drawing.Size(75, 23);
			this.testButton.TabIndex = 4;
			this.testButton.Text = "Test";
			this.testButton.UseVisualStyleBackColor = true;
			this.testButton.Click += new System.EventHandler(this.testButton_Click);
			// 
			// autostartCheckBox
			// 
			this.autostartCheckBox.AutoSize = true;
			this.autostartCheckBox.Location = new System.Drawing.Point(111, 82);
			this.autostartCheckBox.Name = "autostartCheckBox";
			this.autostartCheckBox.Size = new System.Drawing.Size(15, 14);
			this.autostartCheckBox.TabIndex = 5;
			this.autostartCheckBox.UseVisualStyleBackColor = true;
			this.autostartCheckBox.CheckedChanged += new System.EventHandler(this.autostartCheckBox_CheckedChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(14, 82);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(52, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Auto start";
			// 
			// DatabaseConnectorControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Controls.Add(this.label3);
			this.Controls.Add(this.autostartCheckBox);
			this.Controls.Add(this.testButton);
			this.Controls.Add(this.connectionStringTextBox);
			this.Controls.Add(this.providerTypeComboBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "DatabaseConnectorControl";
			this.Size = new System.Drawing.Size(502, 176);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox providerTypeComboBox;
		private System.Windows.Forms.TextBox connectionStringTextBox;
		private System.Windows.Forms.Button testButton;
		private System.Windows.Forms.CheckBox autostartCheckBox;
		private System.Windows.Forms.Label label3;
	}
}
