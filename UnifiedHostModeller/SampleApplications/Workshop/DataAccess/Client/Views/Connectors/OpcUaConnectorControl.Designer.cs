﻿namespace UnifiedStudio.Controls
{
	partial class OpcUaConnectorControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label2 = new System.Windows.Forms.Label();
			this.endpointUrlTextBox = new System.Windows.Forms.TextBox();
			this.testButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(13, 21);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(63, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Endpoint url";
			// 
			// endpointUrlTextBox
			// 
			this.endpointUrlTextBox.Location = new System.Drawing.Point(82, 18);
			this.endpointUrlTextBox.Name = "endpointUrlTextBox";
			this.endpointUrlTextBox.Size = new System.Drawing.Size(372, 20);
			this.endpointUrlTextBox.TabIndex = 3;
			this.endpointUrlTextBox.TextChanged += new System.EventHandler(this.endpointUrlTextBox_TextChanged);
			// 
			// testButton
			// 
			this.testButton.Location = new System.Drawing.Point(460, 17);
			this.testButton.Name = "testButton";
			this.testButton.Size = new System.Drawing.Size(75, 22);
			this.testButton.TabIndex = 5;
			this.testButton.Text = "Test";
			this.testButton.UseVisualStyleBackColor = true;
			this.testButton.Click += new System.EventHandler(this.testButton_Click);
			// 
			// OpcUaConnectorControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Controls.Add(this.testButton);
			this.Controls.Add(this.endpointUrlTextBox);
			this.Controls.Add(this.label2);
			this.Name = "OpcUaConnectorControl";
			this.Size = new System.Drawing.Size(600, 247);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox endpointUrlTextBox;
		private System.Windows.Forms.Button testButton;
	}
}
