﻿using System.Linq;
using System.Windows.Forms;
using Opc.Ua.Client;
using Opc.Ua;
using System;
using System.Collections.Generic;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Controls
{
	public partial class OpcUaConnectorControl : UserControl
	{
		private Session session;
		private NodeId connectorNodeId;
		private ReferenceDescription testMethodReference;
		private ReferenceDescription enpointUrlReference;

		public OpcUaConnectorControl()
		{
			InitializeComponent();
		}

		public void DisplayConnector(Session session, NodeId nodeId, bool forType)
		{
			this.session = session;
			connectorNodeId = nodeId;

			testButton.Enabled = !forType;

			ReferenceDescriptionCollection childReferences =
				OpcUaNavigationHelper.Browse(session, nodeId, BrowseDirection.Forward, NodeClass.Variable | NodeClass.Method, new NodeId[] { ReferenceTypeIds.HasComponent }, false);

			testMethodReference = childReferences.FirstOrDefault(r => r.BrowseName.Name == "Test");
			enpointUrlReference = childReferences.FirstOrDefault(r => r.BrowseName.Name == "EndpointUrl");

			endpointUrlTextBox.Text = session.ReadValue((NodeId)enpointUrlReference.NodeId).Value?.ToString();
		}

		private void endpointUrlTextBox_TextChanged(object sender, EventArgs e)
		{
			if (enpointUrlReference == null)
				return;

			if (!string.IsNullOrWhiteSpace(endpointUrlTextBox.Text) && !endpointUrlTextBox.Text.Contains("://"))
			{
				endpointUrlTextBox.Text = "opc.tcp://" + endpointUrlTextBox.Text;
				endpointUrlTextBox.SelectionStart = endpointUrlTextBox.Text.Length;
				endpointUrlTextBox.SelectionLength = 0;
				return;
			}

			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)enpointUrlReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = endpointUrlTextBox.Text };

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);

			OpcUaHelper.SaveConfiguration(session);
		}

		private void testButton_Click(object sender, EventArgs e)
		{
			try
			{
				IList<object> methodCallResult = session.Call(connectorNodeId, (NodeId)testMethodReference.NodeId);

				if ((bool)methodCallResult[0])
					MessageBox.Show("Connection established", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
				else
					MessageBox.Show("Connection not established", "Fail", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.GetBaseException().Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
	}
}
