﻿using Opc.Ua;
using Opc.Ua.Client;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Editors
{
	public partial class AddConnectorForm : Form
	{
		private string prevBrowseName = "";
		private readonly Dictionary<string, NodeId> subTypes = new Dictionary<string, NodeId>();

		public AddConnectorForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(Session session, out (string BrowseName, string DisplayName, string Description, NodeId Type) typeDescription)
		{
			ReferenceDescriptionCollection subTypeReferences =
				OpcUaNavigationHelper.Browse(session, session.GetConnectorTypeNodeId(), BrowseDirection.Forward, NodeClass.ObjectType, new NodeId[] { ReferenceTypes.HasSubtype });

			subTypes.Clear();
			foreach (var item in subTypeReferences)
				subTypes.Add(item.DisplayName.ToString().Replace("Type", ""), (NodeId)item.NodeId);

			typeComboBox.Items.Clear();
			typeComboBox.Items.AddRange(subTypes.Keys.ToArray());

			if (ShowDialog() == DialogResult.OK)
			{
				typeDescription = (browseNameTextBox.Text, displayNameTextBox.Text, descriptionTextBox.Text, subTypes[typeComboBox.Text]);
				return true;
			}
			else
			{
				typeDescription = ("", "", "", null);
				return false;
			}
		}

		private void formClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				return;

			if (!OpcUaHelper.ValidateName(browseNameTextBox.Text, "Browse name", out string errorMessage))
			{
				MessageBox.Show(errorMessage);
				e.Cancel = true;
			}
			else if (!OpcUaHelper.ValidateName(displayNameTextBox.Text, "Display name", out errorMessage))
			{
				MessageBox.Show(errorMessage);
				e.Cancel = true;
			}
			else if (!subTypes.ContainsKey(typeComboBox.Text))
			{
				MessageBox.Show("Invalid type specified");
				e.Cancel = true;
			}
		}

		private void browseNameTextBox_TextChanged(object sender, System.EventArgs e)
		{
			if (prevBrowseName == displayNameTextBox.Text)
				displayNameTextBox.Text = browseNameTextBox.Text;

			prevBrowseName = browseNameTextBox.Text;
		}
	}
}
