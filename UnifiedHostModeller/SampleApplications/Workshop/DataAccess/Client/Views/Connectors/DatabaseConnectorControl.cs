﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Opc.Ua;
using Opc.Ua.Client;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Controls
{
	public partial class DatabaseConnectorControl : UserControl
	{
		private Session session;
		private NodeId connectorNodeId;
		private ReferenceDescription testMethodReference;
		private ReferenceDescription providerTypeReference;
		private ReferenceDescription connectionStringReference;
		private ReferenceDescription autoStartReference;

		public DatabaseConnectorControl()
		{
			InitializeComponent();
		}

		public void DisplayConnector(Session session, NodeId nodeId, bool forType)
		{
			this.session = session;
			connectorNodeId = nodeId;

			testButton.Enabled = !forType;

			ReferenceDescriptionCollection childReferences =
				OpcUaNavigationHelper.Browse(session, nodeId, BrowseDirection.Forward, NodeClass.Variable | NodeClass.Method, new NodeId[] { ReferenceTypeIds.HasComponent }, false);

			testMethodReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "Test");
			providerTypeReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "ProviderType");
			connectionStringReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "ConnectionString");
			autoStartReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "AutoStart");

			providerTypeComboBox.SelectedIndex = (int) session.ReadValue((NodeId)providerTypeReference.NodeId).Value;
			connectionStringTextBox.Text = session.ReadValue((NodeId)connectionStringReference.NodeId).Value?.ToString();
			autostartCheckBox.Checked = (bool) session.ReadValue((NodeId)autoStartReference.NodeId).Value;
		}

		private void connectionStringTextBox_TextChanged(object sender, System.EventArgs e)
		{
			if (connectionStringReference == null)
				return;

			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)connectionStringReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = connectionStringTextBox.Text };

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);
		}

		private void autostartCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (autoStartReference == null)
				return;

			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)autoStartReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = autostartCheckBox.Checked };

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);
		}

		private void testButton_Click(object sender, System.EventArgs e)
		{
			try
			{
				IList<object> methodCallResult = session.Call(connectorNodeId, (NodeId)testMethodReference.NodeId);

				if ((bool)methodCallResult[0])
					MessageBox.Show("Connection established", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
				else
					MessageBox.Show("Connection not established", "Fail", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.GetBaseException().Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
	}
}
