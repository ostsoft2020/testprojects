﻿namespace UnifiedStudio.Controls
{
	partial class S7TcpIpConnectorControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.ipAddressTextBox = new System.Windows.Forms.TextBox();
			this.testButton = new System.Windows.Forms.Button();
			this.plcTypeComboBox = new System.Windows.Forms.ComboBox();
			this.slotNumberNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.autostartCheckBox = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.slotNumberNumericUpDown)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(14, 14);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(50, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "PLC type";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(14, 49);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(57, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "IP address";
			// 
			// ipAddressTextBox
			// 
			this.ipAddressTextBox.Location = new System.Drawing.Point(110, 46);
			this.ipAddressTextBox.Name = "ipAddressTextBox";
			this.ipAddressTextBox.Size = new System.Drawing.Size(157, 20);
			this.ipAddressTextBox.TabIndex = 2;
			this.ipAddressTextBox.TextChanged += new System.EventHandler(this.ipAddressTextBox_TextChanged);
			// 
			// testButton
			// 
			this.testButton.Location = new System.Drawing.Point(407, 9);
			this.testButton.Name = "testButton";
			this.testButton.Size = new System.Drawing.Size(75, 23);
			this.testButton.TabIndex = 1;
			this.testButton.Text = "Test";
			this.testButton.UseVisualStyleBackColor = true;
			this.testButton.Click += new System.EventHandler(this.testButton_Click);
			// 
			// plcTypeComboBox
			// 
			this.plcTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.plcTypeComboBox.FormattingEnabled = true;
			this.plcTypeComboBox.Location = new System.Drawing.Point(110, 11);
			this.plcTypeComboBox.Name = "plcTypeComboBox";
			this.plcTypeComboBox.Size = new System.Drawing.Size(157, 21);
			this.plcTypeComboBox.TabIndex = 0;
			this.plcTypeComboBox.SelectedValueChanged += new System.EventHandler(this.plcTypeComboBox_SelectedValueChanged);
			// 
			// slotNumberNumericUpDown
			// 
			this.slotNumberNumericUpDown.Location = new System.Drawing.Point(110, 81);
			this.slotNumberNumericUpDown.Name = "slotNumberNumericUpDown";
			this.slotNumberNumericUpDown.Size = new System.Drawing.Size(51, 20);
			this.slotNumberNumericUpDown.TabIndex = 3;
			this.slotNumberNumericUpDown.ValueChanged += new System.EventHandler(this.slotNumberNumericUpDown_ValueChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(14, 83);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(63, 13);
			this.label3.TabIndex = 8;
			this.label3.Text = "Slot number";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(14, 116);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(52, 13);
			this.label4.TabIndex = 10;
			this.label4.Text = "Auto start";
			// 
			// autostartCheckBox
			// 
			this.autostartCheckBox.AutoSize = true;
			this.autostartCheckBox.Location = new System.Drawing.Point(110, 116);
			this.autostartCheckBox.Name = "autostartCheckBox";
			this.autostartCheckBox.Size = new System.Drawing.Size(15, 14);
			this.autostartCheckBox.TabIndex = 9;
			this.autostartCheckBox.UseVisualStyleBackColor = true;
			this.autostartCheckBox.CheckedChanged += new System.EventHandler(this.autostartCheckBox_CheckedChanged);
			// 
			// S7TcpIpConnectorControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Controls.Add(this.label4);
			this.Controls.Add(this.autostartCheckBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.slotNumberNumericUpDown);
			this.Controls.Add(this.plcTypeComboBox);
			this.Controls.Add(this.testButton);
			this.Controls.Add(this.ipAddressTextBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Name = "S7TcpIpConnectorControl";
			this.Size = new System.Drawing.Size(600, 247);
			((System.ComponentModel.ISupportInitialize)(this.slotNumberNumericUpDown)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox ipAddressTextBox;
		private System.Windows.Forms.Button testButton;
		private System.Windows.Forms.ComboBox plcTypeComboBox;
		private System.Windows.Forms.NumericUpDown slotNumberNumericUpDown;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.CheckBox autostartCheckBox;
	}
}
