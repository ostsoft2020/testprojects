﻿using System.Linq;
using System.Windows.Forms;
using Opc.Ua.Client;
using Opc.Ua;
using System;
using System.Collections.Generic;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Controls
{
	public partial class S7TcpIpConnectorControl : UserControl
	{
		private readonly Dictionary<int, string> plcTypes = new Dictionary<int, string>
		{
			{ 0, "S7 300/400" },
			{ 1, "S7 1200" },
			{ 2, "S7 1500" },
			{ 3, "LOGO" }
		};

		private Session session;
		private NodeId connectorNodeId;
		private ReferenceDescription testMethodReference;
		private ReferenceDescription plcTypeReference;
		private ReferenceDescription ipAddressReference;
		private ReferenceDescription slotNumberReference;
		private ReferenceDescription autoStartReference;

		public S7TcpIpConnectorControl()
		{
			InitializeComponent();

			plcTypeComboBox.Items.AddRange(plcTypes.Select(pt => pt.Value).ToArray());
		}

		public void DisplayConnector(Session session, NodeId nodeId, bool forType)
		{
			this.session = session;
			connectorNodeId = nodeId;

			testButton.Enabled = !forType;

			ReferenceDescriptionCollection childReferences =
				OpcUaNavigationHelper.Browse(session, nodeId, BrowseDirection.Forward, NodeClass.Variable | NodeClass.Method, new NodeId[] { ReferenceTypeIds.HasComponent }, false);

			testMethodReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "Test");
			plcTypeReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "PlcType");
			ipAddressReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "IpAddress");
			slotNumberReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "SlotNumber");
			autoStartReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "AutoStart");

			plcTypeComboBox.Text = (plcTypes.TryGetValue((int) session.ReadValue((NodeId)plcTypeReference.NodeId).Value, out string plcTypeName))
				? plcTypeName
				: plcTypes[0];
			ipAddressTextBox.Text = session.ReadValue((NodeId)ipAddressReference.NodeId).Value?.ToString();
			slotNumberNumericUpDown.Value = Convert.ToDecimal(session.ReadValue((NodeId)slotNumberReference.NodeId).Value);
			autostartCheckBox.Checked = (bool)session.ReadValue((NodeId)autoStartReference.NodeId).Value;
		}

		private void plcTypeComboBox_SelectedValueChanged(object sender, EventArgs e)
		{
			foreach (KeyValuePair<int, string> plcType in plcTypes)
				if (plcType.Value == plcTypeComboBox.Text)
				{
					WriteValue writeValue = new WriteValue();

					writeValue.NodeId = (NodeId)plcTypeReference.NodeId;
					writeValue.AttributeId = Attributes.Value;
					writeValue.Value = new DataValue() { Value =  plcType.Key};

					session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);

					OpcUaHelper.SaveConfiguration(session);

					return;
				}
		}

		private void ipAddressTextBox_TextChanged(object sender, EventArgs e)
		{
			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)ipAddressReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = ipAddressTextBox.Text };

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);

			OpcUaHelper.SaveConfiguration(session);
		}

		private void slotNumberNumericUpDown_ValueChanged(object sender, EventArgs e)
		{
			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)slotNumberReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = Convert.ToInt32(slotNumberNumericUpDown.Value) };

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);

			OpcUaHelper.SaveConfiguration(session);
		}

		private void autostartCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (autoStartReference == null)
				return;

			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)autoStartReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = autostartCheckBox.Checked };

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);

			OpcUaHelper.SaveConfiguration(session);
		}

		private void testButton_Click(object sender, EventArgs e)
		{
			try
			{
				IList<object> methodCallResult = session.Call(connectorNodeId, (NodeId)testMethodReference.NodeId);

				if ((bool)methodCallResult[0])
					MessageBox.Show("Connection established", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
				else
					MessageBox.Show("Connection not established", "Fail", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.GetBaseException().Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
	}
}
