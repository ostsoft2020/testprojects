﻿namespace UnifiedStudio.Controls
{
	partial class ConnectorsControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dataGridView = new System.Windows.Forms.DataGridView();
			this.nameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.enabledColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.enableColumn = new System.Windows.Forms.DataGridViewButtonColumn();
			this.disableColumn = new System.Windows.Forms.DataGridViewButtonColumn();
			this.testColumn = new System.Windows.Forms.DataGridViewButtonColumn();
			this.connectedColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.connectionErrorColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView
			// 
			this.dataGridView.AllowUserToAddRows = false;
			this.dataGridView.AllowUserToDeleteRows = false;
			this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameColumn,
            this.enabledColumn,
            this.enableColumn,
            this.disableColumn,
            this.testColumn,
            this.connectedColumn,
            this.connectionErrorColumn});
			this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridView.Location = new System.Drawing.Point(0, 0);
			this.dataGridView.Name = "dataGridView";
			this.dataGridView.ReadOnly = true;
			this.dataGridView.RowHeadersVisible = false;
			this.dataGridView.Size = new System.Drawing.Size(779, 459);
			this.dataGridView.TabIndex = 0;
			this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentClick);
			// 
			// nameColumn
			// 
			this.nameColumn.DataPropertyName = "Name";
			this.nameColumn.HeaderText = "Name";
			this.nameColumn.Name = "nameColumn";
			this.nameColumn.ReadOnly = true;
			this.nameColumn.Width = 120;
			// 
			// enabledColumn
			// 
			this.enabledColumn.DataPropertyName = "Enabled";
			this.enabledColumn.HeaderText = "Enabled";
			this.enabledColumn.Name = "enabledColumn";
			this.enabledColumn.ReadOnly = true;
			this.enabledColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.enabledColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.enabledColumn.Width = 50;
			// 
			// enableColumn
			// 
			this.enableColumn.HeaderText = "Enable";
			this.enableColumn.Name = "enableColumn";
			this.enableColumn.ReadOnly = true;
			this.enableColumn.Text = "Enable";
			this.enableColumn.UseColumnTextForButtonValue = true;
			this.enableColumn.Width = 50;
			// 
			// disableColumn
			// 
			this.disableColumn.HeaderText = "Disable";
			this.disableColumn.Name = "disableColumn";
			this.disableColumn.ReadOnly = true;
			this.disableColumn.Text = "Disable";
			this.disableColumn.UseColumnTextForButtonValue = true;
			this.disableColumn.Width = 50;
			// 
			// testColumn
			// 
			this.testColumn.HeaderText = "Test";
			this.testColumn.Name = "testColumn";
			this.testColumn.ReadOnly = true;
			this.testColumn.Text = "Test";
			this.testColumn.UseColumnTextForButtonValue = true;
			this.testColumn.Width = 40;
			// 
			// connectedColumn
			// 
			this.connectedColumn.DataPropertyName = "Connected";
			this.connectedColumn.HeaderText = "Connected";
			this.connectedColumn.Name = "connectedColumn";
			this.connectedColumn.ReadOnly = true;
			this.connectedColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.connectedColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.connectedColumn.Width = 65;
			// 
			// connectionErrorColumn
			// 
			this.connectionErrorColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.connectionErrorColumn.DataPropertyName = "ConnectionError";
			this.connectionErrorColumn.HeaderText = "Connection error";
			this.connectionErrorColumn.Name = "connectionErrorColumn";
			this.connectionErrorColumn.ReadOnly = true;
			// 
			// ConnectorsControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.dataGridView);
			this.Name = "ConnectorsControl";
			this.Size = new System.Drawing.Size(779, 459);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridView;
		private System.Windows.Forms.DataGridViewTextBoxColumn nameColumn;
		private System.Windows.Forms.DataGridViewCheckBoxColumn enabledColumn;
		private System.Windows.Forms.DataGridViewButtonColumn enableColumn;
		private System.Windows.Forms.DataGridViewButtonColumn disableColumn;
		private System.Windows.Forms.DataGridViewButtonColumn testColumn;
		private System.Windows.Forms.DataGridViewCheckBoxColumn connectedColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn connectionErrorColumn;
	}
}
