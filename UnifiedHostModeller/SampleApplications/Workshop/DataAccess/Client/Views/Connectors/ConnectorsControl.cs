﻿using System.Windows.Forms;
using Opc.Ua.Client;
using Opc.Ua;
using UnifiedStudio.Models;
using System.Collections.Generic;
using System.ComponentModel;
using System;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Controls
{
	public partial class ConnectorsControl : UserControl
	{
        private Session session;
        private Subscription subscription;
        private readonly BindingList<ConnectorDescriptor> programDescriptors = new BindingList<ConnectorDescriptor>();
        private readonly List<MonitoredItem> monitoredItems = new List<MonitoredItem>();

        public ConnectorsControl()
		{
			InitializeComponent();

            dataGridView.AutoGenerateColumns = false;
        }

        public void DisplayConnectors(Session session, NodeId connectorsNodeId, bool forType)
		{
            removeSubscription();

            this.session = session;

            programDescriptors.Clear();

            enableColumn.Visible = !forType;
            disableColumn.Visible = !forType;
            testColumn.Visible = !forType;

            ReferenceDescriptionCollection references =
				OpcUaNavigationHelper.Browse(session, connectorsNodeId, BrowseDirection.Forward, NodeClass.Object, new NodeId[] { ReferenceTypeIds.HasComponent });

            if (references == null)
                return;

            for (int i = 0; i < references.Count; i++)
            {
                ConnectorDescriptor connectorDescriptor = new ConnectorDescriptor
                {
                    Name = references[i].DisplayName.ToString(),
                    NodeId = (NodeId) references[i].NodeId
                };

                ReferenceDescriptionCollection subReferences =
					OpcUaNavigationHelper.Browse(session, (NodeId) references[i].NodeId, BrowseDirection.Forward, NodeClass.Variable | NodeClass.Method, new NodeId[] { ReferenceTypeIds.HasComponent });

                for (int j = 0; j < subReferences.Count; j++)
                    switch (subReferences[j].BrowseName.Name)
                    {
                        case nameof(connectorDescriptor.Enabled):
                            connectorDescriptor.EnabledNodeId = (NodeId)subReferences[j].NodeId;
                            connectorDescriptor.Enabled = (bool) session.ReadValue(connectorDescriptor.EnabledNodeId).Value;
                            connectorDescriptor.EnabledMonitoredItem = сreateMonitoredItem(subReferences[j]);
                            break;

                        case nameof(connectorDescriptor.Connected):
                            connectorDescriptor.ConnectedNodeId = (NodeId)subReferences[j].NodeId;
                            connectorDescriptor.Connected = (bool) session.ReadValue(connectorDescriptor.ConnectedNodeId).Value;
                            connectorDescriptor.ConnectedMonitoredItem = сreateMonitoredItem(subReferences[j]);
                            break;

                        case nameof(connectorDescriptor.ConnectionError):
                            connectorDescriptor.ConnectionErrorNodeId = (NodeId)subReferences[j].NodeId;
                            connectorDescriptor.ConnectionError = (string) session.ReadValue(connectorDescriptor.ConnectionErrorNodeId).Value;
                            connectorDescriptor.ConnectionErrorMonitoredItem = сreateMonitoredItem(subReferences[j]);
                            break;

                        case "Enable":
                            connectorDescriptor.EnableMethodNodeId = (NodeId)subReferences[j].NodeId;
                            break;

                        case "Disable":
                            connectorDescriptor.DisableMethodNodeId = (NodeId)subReferences[j].NodeId;
                            break;

                        case "Test":
                            connectorDescriptor.TestMethodNodeId = (NodeId)subReferences[j].NodeId;
                            break;
                    }

                programDescriptors.Add(connectorDescriptor);
            }

            dataGridView.DataSource = programDescriptors;
        }

        private void removeSubscription()
        {
            if (subscription != null && subscription != null && monitoredItems != null)
            {
                try
                {
                    foreach (MonitoredItem monitoredItem in monitoredItems)
                    {
                        monitoredItem.Notification -= monitoredItem_Notification;
                        subscription.RemoveItem(monitoredItem);
                    }

                    session.RemoveSubscription(subscription);
                }
                catch
                {
                }

                subscription.Dispose();
                subscription = null;
            }
        }

        private MonitoredItem сreateMonitoredItem(ReferenceDescription variableReference)
        {
            if (variableReference == null)
                return null;

            if (subscription == null)
            {
                subscription = new Subscription(session.DefaultSubscription);

                subscription.PublishingEnabled = true;
                subscription.PublishingInterval = 1000;
                subscription.KeepAliveCount = 10;
                subscription.LifetimeCount = 10;
                subscription.MaxNotificationsPerPublish = 1000;
                subscription.Priority = 100;

                session.AddSubscription(subscription);

                subscription.Create();
            }

            // add the new monitored item.
            MonitoredItem monitoredItem = new MonitoredItem(subscription.DefaultItem);

            monitoredItem.StartNodeId = (NodeId) variableReference.NodeId;
            monitoredItem.AttributeId = Attributes.Value;
            monitoredItem.DisplayName = variableReference.DisplayName + "Subscription";
            monitoredItem.MonitoringMode = MonitoringMode.Reporting;
            monitoredItem.SamplingInterval = 1000;
            monitoredItem.QueueSize = 0;
            monitoredItem.DiscardOldest = true;

            monitoredItems.Add(monitoredItem);

            monitoredItem.Notification += monitoredItem_Notification;

            subscription.AddItem(monitoredItem);

            subscription.ApplyChanges();

            return monitoredItem;
        }

        private void monitoredItem_Notification(MonitoredItem monitoredItem, MonitoredItemNotificationEventArgs e)
        {
            for (int i = 0; i < programDescriptors.Count; i++)
            {
                if (programDescriptors[i].EnabledMonitoredItem == monitoredItem)
                    programDescriptors[i].Enabled = (bool)((MonitoredItemNotification)e.NotificationValue).Value.Value;
                else if (programDescriptors[i].ConnectedMonitoredItem == monitoredItem)
                    programDescriptors[i].Connected = (bool) ((MonitoredItemNotification)e.NotificationValue).Value.Value;
                else if (programDescriptors[i].ConnectionErrorMonitoredItem == monitoredItem)
                    programDescriptors[i].ConnectionError = ((MonitoredItemNotification)e.NotificationValue).Value?.ToString();
                else
                    continue;

                programDescriptors.ResetItem(i);

                return;
            }
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0 || !(dataGridView.Columns[e.ColumnIndex] is DataGridViewButtonColumn))
                return;

            ConnectorDescriptor connectorDescriptor = programDescriptors[e.RowIndex];

            if (connectorDescriptor == null)
                return;

            try
            {
                switch (dataGridView.Columns[e.ColumnIndex].Name)
                {
                    case nameof(enableColumn):
                        if (connectorDescriptor.EnableMethodNodeId != null)
                            session.Call(connectorDescriptor.NodeId, connectorDescriptor.EnableMethodNodeId);
                        break;

                    case nameof(disableColumn):
                        if (connectorDescriptor.DisableMethodNodeId != null)
                            session.Call(connectorDescriptor.NodeId, connectorDescriptor.DisableMethodNodeId);
                        break;

                    case nameof(testColumn):
                        if (connectorDescriptor.TestMethodNodeId != null)
                        {
                            IList<object> testResults = session.Call(connectorDescriptor.NodeId, connectorDescriptor.TestMethodNodeId);

                            if (testResults.Count == 2 && (bool)testResults[0])
                                MessageBox.Show("Connected");
                            else
                                MessageBox.Show("Not connected: " + testResults[1]);
                        }
                        break;
                }
            }
            catch (ServiceResultException sre)
            {
                MessageBox.Show(sre.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
