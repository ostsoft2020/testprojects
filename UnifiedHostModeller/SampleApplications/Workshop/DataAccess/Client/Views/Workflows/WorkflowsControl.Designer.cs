﻿namespace UnifiedStudio.Controls
{
	partial class WorkflowsControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dataGridView = new System.Windows.Forms.DataGridView();
			this.nameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.autoStartColumt = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.currentStateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.runColumn = new System.Windows.Forms.DataGridViewButtonColumn();
			this.abortColumn = new System.Windows.Forms.DataGridViewButtonColumn();
			this.cancelColumn = new System.Windows.Forms.DataGridViewButtonColumn();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView
			// 
			this.dataGridView.AllowUserToAddRows = false;
			this.dataGridView.AllowUserToDeleteRows = false;
			this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameColumn,
            this.autoStartColumt,
            this.currentStateColumn,
            this.runColumn,
            this.abortColumn,
            this.cancelColumn});
			this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridView.Location = new System.Drawing.Point(0, 0);
			this.dataGridView.Name = "dataGridView";
			this.dataGridView.ReadOnly = true;
			this.dataGridView.RowHeadersVisible = false;
			this.dataGridView.Size = new System.Drawing.Size(779, 459);
			this.dataGridView.TabIndex = 0;
			this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentClick);
			// 
			// nameColumn
			// 
			this.nameColumn.DataPropertyName = "Name";
			this.nameColumn.Frozen = true;
			this.nameColumn.HeaderText = "Name";
			this.nameColumn.Name = "nameColumn";
			this.nameColumn.ReadOnly = true;
			this.nameColumn.Width = 128;
			// 
			// autoStartColumt
			// 
			this.autoStartColumt.DataPropertyName = "AutoStart";
			this.autoStartColumt.HeaderText = "Autostart";
			this.autoStartColumt.Name = "autoStartColumt";
			this.autoStartColumt.ReadOnly = true;
			this.autoStartColumt.Width = 60;
			// 
			// currentStateColumn
			// 
			this.currentStateColumn.DataPropertyName = "CurrentState";
			this.currentStateColumn.HeaderText = "Current state";
			this.currentStateColumn.Name = "currentStateColumn";
			this.currentStateColumn.ReadOnly = true;
			// 
			// runColumn
			// 
			this.runColumn.HeaderText = "Run";
			this.runColumn.Name = "runColumn";
			this.runColumn.ReadOnly = true;
			this.runColumn.Text = "Run";
			this.runColumn.UseColumnTextForButtonValue = true;
			this.runColumn.Width = 50;
			// 
			// abortColumn
			// 
			this.abortColumn.HeaderText = "Abort";
			this.abortColumn.Name = "abortColumn";
			this.abortColumn.ReadOnly = true;
			this.abortColumn.Text = "Abort";
			this.abortColumn.UseColumnTextForButtonValue = true;
			this.abortColumn.Width = 50;
			// 
			// cancelColumn
			// 
			this.cancelColumn.HeaderText = "Cancel";
			this.cancelColumn.Name = "cancelColumn";
			this.cancelColumn.ReadOnly = true;
			this.cancelColumn.Text = "Cancel";
			this.cancelColumn.UseColumnTextForButtonValue = true;
			this.cancelColumn.Width = 50;
			// 
			// WorkflowsControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.dataGridView);
			this.Name = "WorkflowsControl";
			this.Size = new System.Drawing.Size(779, 459);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridView;
		private System.Windows.Forms.DataGridViewTextBoxColumn nameColumn;
		private System.Windows.Forms.DataGridViewCheckBoxColumn autoStartColumt;
		private System.Windows.Forms.DataGridViewTextBoxColumn currentStateColumn;
		private System.Windows.Forms.DataGridViewButtonColumn runColumn;
		private System.Windows.Forms.DataGridViewButtonColumn abortColumn;
		private System.Windows.Forms.DataGridViewButtonColumn cancelColumn;
	}
}
