﻿using System.Windows.Forms;
using Opc.Ua.Client;
using Opc.Ua;
using Quickstarts;
using System.ComponentModel;
using UnifiedStudio.Models;

namespace UnifiedStudio.Controls
{
	public partial class WorkflowParametersControl : UserControl
	{
        public BindingList<ParameterDescriptor> ParameterDescriptors = new BindingList<ParameterDescriptor>();

        public WorkflowParametersControl()
		{
			InitializeComponent();

            //dataGridView.AutoGenerateColumns = false;

            ParameterDescriptors.ListChanged += parameterDescriptors_ListChanged;
        }

        private void parameterDescriptors_ListChanged(object sender, ListChangedEventArgs e)
        {
        }

        public void DisplayParamaters(Session session, NodeId parametersNodeId, bool forType)
		{
            //valueColumn.Visible = !forType;
            //dataGridView.ContextMenuStrip = null;
            ////setValueToolStripMenuItem.Enabled = !forType;

            //ParameterDescriptors.Clear();

            //BrowseDescriptionCollection nodesToBrowse = new BrowseDescriptionCollection 
            //{
            //    new BrowseDescription
            //    {
            //        NodeId = parametersNodeId,
            //        BrowseDirection = BrowseDirection.Forward,
            //        ReferenceTypeId = ReferenceTypeIds.HasComponent,
            //        IncludeSubtypes = true,
            //        NodeClassMask = (uint) NodeClass.Variable,
            //        ResultMask = (uint)BrowseResultMask.All
            //    }
            //};

            //// fetch property references from the server.
            //ReferenceDescriptionCollection references = FormUtils.Browse(session, nodesToBrowse, false);

            //if (references == null)
            //    return;

            //for (int i = 0; i < references.Count; i++)
            //{
            //    // ignore external references.
            //    if (references[i].NodeId.IsAbsolute)
            //        continue;

            //    ReadValueIdCollection nodesToRead = new ReadValueIdCollection();
            //    nodesToRead.Add(new ReadValueId { NodeId = (NodeId)references[i].NodeId, AttributeId = Attributes.Description });
            //    nodesToRead.Add(new ReadValueId { NodeId = (NodeId)references[i].NodeId, AttributeId = Attributes.DataTypeName });
            //    nodesToRead.Add(new ReadValueId { NodeId = (NodeId)references[i].NodeId, AttributeId = Attributes.ValueRank });
            //    nodesToRead.Add(new ReadValueId { NodeId = (NodeId)references[i].NodeId, AttributeId = Attributes.AccessLevel });

            //    // read all values.
            //    DataValueCollection results;
            //    DiagnosticInfoCollection diagnosticInfos;

            //    session.Read(
            //        null,
            //        0,
            //        TimestampsToReturn.Neither,
            //        nodesToRead,
            //        out results,
            //        out diagnosticInfos);

            //    ClientBase.ValidateResponse(results, nodesToRead);
            //    ClientBase.ValidateDiagnosticInfos(diagnosticInfos, nodesToRead);

            //    ParameterDescriptor parameterDescriptor = new ParameterDescriptor
            //    {
            //        Name = references[i].DisplayName.ToString()
            //    };

            //    // process attribute value.
            //    for (int ii = 0; ii < results.Count; ii++)
            //    {
            //        // ignore attributes which are invalid for the node.
            //        if (results[ii].StatusCode == StatusCodes.BadAttributeIdInvalid)
            //            continue;

            //        switch (nodesToRead[ii].AttributeId)
            //        {
            //            case Attributes.Description:
            //                parameterDescriptor.Description = results[ii].ValueAsString?.ToString();
            //                break;

            //            case Attributes.DataTypeName:
            //                parameterDescriptor.DataTypeName = TypeInfo.GetBuiltInType((NodeId)results[ii].ValueAsString);
            //                break;

            //            case Attributes.ValueRank:
            //                parameterDescriptor.IsArray = (int) results[ii].ValueAsString >= ValueRanks.OneOrMoreDimensions;
            //                break;

            //            case Attributes.AccessLevel:
            //                switch ((byte)results[ii].ValueAsString)
            //                {
            //                    case AccessLevels.None:
            //                        parameterDescriptor.AccessLevel = "None";
            //                        break;

            //                    case AccessLevels.CurrentRead:
            //                        parameterDescriptor.AccessLevel = "Read";
            //                        break;

            //                    case AccessLevels.CurrentWrite:
            //                        parameterDescriptor.AccessLevel = "Write";
            //                        break;

            //                    case AccessLevels.CurrentReadOrWrite:
            //                        parameterDescriptor.AccessLevel = "Read or write";
            //                        break;
            //                }
            //                break;
            //        }
            //    }

            //    ParameterDescriptors.Add(parameterDescriptor);
            //}

            //dataGridView.DataSource = ParameterDescriptors;
        }
	}
}
