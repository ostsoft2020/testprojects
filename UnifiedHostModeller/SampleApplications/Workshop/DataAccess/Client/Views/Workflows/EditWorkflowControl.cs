﻿using System.Linq;
using System.Windows.Forms;
using Opc.Ua.Client;
using Opc.Ua;
using System;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Controls
{
	public partial class EditWorkflowControl : UserControl
	{
		private Session session;
		private ReferenceDescription autoStartReference;

		public EditWorkflowControl()
		{
			InitializeComponent();
		}

		public void DisplayWorkflow(Session session, NodeId workflowNodeId)
		{
			this.session = session;

			ReferenceDescriptionCollection childReferences =
				OpcUaNavigationHelper.Browse(session, workflowNodeId, BrowseDirection.Forward, NodeClass.Variable, new NodeId[] { ReferenceTypeIds.HasComponent }, false);

			autoStartReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "AutoStart");
			autoStartCheckBox.Checked = (bool)session.ReadValue((NodeId)autoStartReference.NodeId).Value;
		}

		private void autoStartCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (autoStartReference == null)
				return;

			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)autoStartReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = autoStartCheckBox.Checked };

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);
		}
	}
}
