﻿using System.Windows.Forms;
using Opc.Ua.Client;
using Opc.Ua;
using System.ComponentModel;
using UnifiedStudio.Models;
using System.Collections.Generic;
using System;
using Opc.Ua.Client.Controls;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Controls
{
	public partial class WorkflowsControl : UserControl
	{
        private Session session;
        private Subscription subscription;
        private List<MonitoredItem> monitoredItems = new List<MonitoredItem>();

        private BindingList<WorkflowDescriptor> workflowDescriptors { get; set; } = new BindingList<WorkflowDescriptor>();

        public WorkflowsControl()
		{
			InitializeComponent();

            dataGridView.AutoGenerateColumns = false;
        }

        public void DisplayWorkflows(Session session, NodeId workflowsNodeId, bool forType)
		{
            removeSubscription();

            this.session = session;

            workflowDescriptors.Clear();

            currentStateColumn.Visible = !forType;
            runColumn.Visible = !forType;
            cancelColumn.Visible = !forType;
            abortColumn.Visible = !forType;

            // fetch property references from the server.
            ReferenceDescriptionCollection references =
				OpcUaNavigationHelper.Browse(session, workflowsNodeId, BrowseDirection.Forward, NodeClass.Object, new NodeId[] { ReferenceTypeIds.HasComponent });

            if (references == null)
                return;

            for (int i = 0; i < references.Count; i++)
            {
                // ignore external references.
                if (references[i].NodeId.IsAbsolute)
                    continue;

                WorkflowDescriptor workflowDescriptor = new WorkflowDescriptor()
                {
                    Name = references[i].DisplayName.ToString(),
                    WorkflowNodeId = (NodeId) references[i].NodeId
                };

                ReferenceDescriptionCollection subReferences =
					OpcUaNavigationHelper.Browse(session, (NodeId) references[i].NodeId, BrowseDirection.Forward, NodeClass.Variable | NodeClass.Method, new NodeId[] { ReferenceTypeIds.HasComponent });

                for (int j = 0; j < subReferences.Count; j++)
                    switch (subReferences[j].DisplayName.ToString())
                    {
                        case nameof(workflowDescriptor.AutoStart):
                            workflowDescriptor.AutoStartNodeId = (NodeId)subReferences[j].NodeId;
                            workflowDescriptor.AutoStart = (bool)session.ReadValue(workflowDescriptor.AutoStartNodeId).Value;
                            workflowDescriptor.AutoStartMonitoredItem = сreateMonitoredItem(subReferences[j]);
                            break;

                        case nameof(workflowDescriptor.CurrentState):
                            workflowDescriptor.CurrentState = session.ReadValue((NodeId)subReferences[j].NodeId)?.ToString();

                            MonitoredItem monitoredItem = сreateMonitoredItem((NodeId)subReferences[j].NodeId);

                            if (monitoredItem == null)
                                continue;

                            workflowDescriptor.MonitoredItem = monitoredItem;

                            break;

                        case "Run":
                            workflowDescriptor.RunMethodNodeId = (NodeId)subReferences[j].NodeId;
                            break;

                        case "Abort":
                            workflowDescriptor.AbortMethodNodeId = (NodeId)subReferences[j].NodeId;
                            break;

                        case "Cancel":
                            workflowDescriptor.CancelMethodNodeId = (NodeId)subReferences[j].NodeId;
                            break;
                    }

                workflowDescriptors.Add(workflowDescriptor);
            }

            dataGridView.DataSource = workflowDescriptors;
        }

        private MonitoredItem сreateMonitoredItem(ReferenceDescription variableReference)
        {
            if (variableReference == null)
                return null;

            if (subscription == null)
            {
                subscription = new Subscription(session.DefaultSubscription);

                subscription.PublishingEnabled = true;
                subscription.PublishingInterval = 1000;
                subscription.KeepAliveCount = 10;
                subscription.LifetimeCount = 10;
                subscription.MaxNotificationsPerPublish = 1000;
                subscription.Priority = 100;

                session.AddSubscription(subscription);

                subscription.Create();
            }

            // add the new monitored item.
            MonitoredItem monitoredItem = new MonitoredItem(subscription.DefaultItem);

            monitoredItem.StartNodeId = (NodeId)variableReference.NodeId;
            monitoredItem.AttributeId = Attributes.Value;
            monitoredItem.DisplayName = variableReference.DisplayName + "Subscription";
            monitoredItem.MonitoringMode = MonitoringMode.Reporting;
            monitoredItem.SamplingInterval = 1000;
            monitoredItem.QueueSize = 0;
            monitoredItem.DiscardOldest = true;

            monitoredItems.Add(monitoredItem);

            monitoredItem.Notification += monitoredItem_Notification;

            subscription.AddItem(monitoredItem);

            subscription.ApplyChanges();

            return monitoredItem;
        }

        private void removeSubscription()
        {
            if (subscription != null && subscription != null && monitoredItems != null)
            {
                try
                {
                    foreach (MonitoredItem monitoredItem in monitoredItems)
                    {
                        monitoredItem.Notification -= monitoredItem_Notification;
                        subscription.RemoveItem(monitoredItem);
                    }

                    session.RemoveSubscription(subscription);
                }
                catch
                {
                }

                subscription.Dispose();
                subscription = null;
            }
        }

        private MonitoredItem сreateMonitoredItem(NodeId nodeId)
        {
            if (nodeId == null)
                return null;

            if (subscription == null)
            {
                subscription = new Subscription(session.DefaultSubscription);

                subscription.PublishingEnabled = true;
                subscription.PublishingInterval = 1000;
                subscription.KeepAliveCount = 10;
                subscription.LifetimeCount = 10;
                subscription.MaxNotificationsPerPublish = 1000;
                subscription.Priority = 100;

                session.AddSubscription(subscription);

                subscription.Create();
            }

            // add the new monitored item.
            MonitoredItem monitoredItem = new MonitoredItem(subscription.DefaultItem);

            monitoredItem.StartNodeId = nodeId;
            monitoredItem.AttributeId = Attributes.Value;
            monitoredItem.DisplayName = "CurrentStateSubscription";
            monitoredItem.MonitoringMode = MonitoringMode.Reporting;
            monitoredItem.SamplingInterval = 1000;
            monitoredItem.QueueSize = 0;
            monitoredItem.DiscardOldest = true;

            monitoredItems.Add(monitoredItem);

            monitoredItem.Notification += monitoredItem_Notification;

            subscription.AddItem(monitoredItem);

            subscription.ApplyChanges();

            return monitoredItem;
        }

        private void monitoredItem_Notification(MonitoredItem monitoredItem, MonitoredItemNotificationEventArgs e)
        {
            int i;

            for (i = 0; i < workflowDescriptors.Count; i++)
            {
                if (workflowDescriptors[i].AutoStartMonitoredItem == monitoredItem)
                    workflowDescriptors[i].AutoStart = (bool)((MonitoredItemNotification)e.NotificationValue).Value.Value;
                else if (workflowDescriptors[i].MonitoredItem == monitoredItem)
                    workflowDescriptors[i].CurrentState = ((MonitoredItemNotification)e.NotificationValue).Value?.ToString();
                else
                    continue;

                workflowDescriptors.ResetItem(i);

                return;
            }
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
                return;

            DataGridViewButtonColumn buttonColumn = dataGridView.Columns[e.ColumnIndex] as DataGridViewButtonColumn;

            if (buttonColumn == null)
                return;

            NodeId methodNodeId;
            object[] parameters = null;

            try
            {
                if (buttonColumn == runColumn)
                    methodNodeId = workflowDescriptors[e.RowIndex].RunMethodNodeId;
                else if (buttonColumn == abortColumn)
                {
                    methodNodeId = workflowDescriptors[e.RowIndex].AbortMethodNodeId;
                    parameters = new object[] { "Aborted by user" };
                }
                else if (buttonColumn == cancelColumn)
                    methodNodeId = workflowDescriptors[e.RowIndex].CancelMethodNodeId;
                else
                    return;

                session.Call(workflowDescriptors[e.RowIndex].WorkflowNodeId, methodNodeId, parameters);
            }
            catch (Exception ex)
            {
                ExceptionDlg.Show("UnifiedHostModeler", ex);
                return;
            }
        }
    }
}
