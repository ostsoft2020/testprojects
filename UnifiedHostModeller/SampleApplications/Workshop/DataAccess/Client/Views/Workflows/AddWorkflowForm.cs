﻿using System.Windows.Forms;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Editors
{
	public partial class AddWorkflowForm : Form
	{
		public AddWorkflowForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(out (string Name, string Description) typeDescription)
		{
			if (ShowDialog() == DialogResult.OK)
			{
				typeDescription = (nameTextBox.Text, descriptionTextBox.Text);
				return true;
			}
			else
			{
				typeDescription = ("", "");
				return false;
			}
		}

		private void formClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				return;

			if (!OpcUaHelper.ValidateName(nameTextBox.Text, "Name", out string errorMessage))
			{
				MessageBox.Show(errorMessage);
				e.Cancel = true;
			}
		}
	}
}
