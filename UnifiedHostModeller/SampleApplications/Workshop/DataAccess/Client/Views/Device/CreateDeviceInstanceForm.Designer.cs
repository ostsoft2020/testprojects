﻿namespace UnifiedStudio.Editors
{
	partial class CreateDeviceInstanceForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.nameTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.descriptionTextBox = new System.Windows.Forms.TextBox();
			this.cancelButton = new System.Windows.Forms.Button();
			this.saveButton = new System.Windows.Forms.Button();
			this.softwareRevisionTextBox = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.hardwareRevisionTextBox = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.modelTextBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.serialNumberTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.productCodeTextBox = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.deviceRevisionTextBox = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(35, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Name";
			// 
			// nameTextBox
			// 
			this.nameTextBox.Location = new System.Drawing.Point(109, 17);
			this.nameTextBox.Name = "nameTextBox";
			this.nameTextBox.Size = new System.Drawing.Size(206, 20);
			this.nameTextBox.TabIndex = 1;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 57);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(60, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Description";
			// 
			// descriptionTextBox
			// 
			this.descriptionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.descriptionTextBox.Location = new System.Drawing.Point(109, 54);
			this.descriptionTextBox.Name = "descriptionTextBox";
			this.descriptionTextBox.Size = new System.Drawing.Size(482, 20);
			this.descriptionTextBox.TabIndex = 2;
			// 
			// cancelButton
			// 
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(516, 295);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(75, 23);
			this.cancelButton.TabIndex = 101;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.UseVisualStyleBackColor = true;
			// 
			// saveButton
			// 
			this.saveButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.saveButton.Location = new System.Drawing.Point(434, 295);
			this.saveButton.Name = "saveButton";
			this.saveButton.Size = new System.Drawing.Size(75, 23);
			this.saveButton.TabIndex = 100;
			this.saveButton.Text = "Create";
			this.saveButton.UseVisualStyleBackColor = true;
			// 
			// softwareRevisionTextBox
			// 
			this.softwareRevisionTextBox.Location = new System.Drawing.Point(109, 194);
			this.softwareRevisionTextBox.Name = "softwareRevisionTextBox";
			this.softwareRevisionTextBox.Size = new System.Drawing.Size(206, 20);
			this.softwareRevisionTextBox.TabIndex = 6;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(11, 197);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(88, 13);
			this.label6.TabIndex = 24;
			this.label6.Text = "Software revision";
			// 
			// hardwareRevisionTextBox
			// 
			this.hardwareRevisionTextBox.Location = new System.Drawing.Point(109, 159);
			this.hardwareRevisionTextBox.Name = "hardwareRevisionTextBox";
			this.hardwareRevisionTextBox.Size = new System.Drawing.Size(206, 20);
			this.hardwareRevisionTextBox.TabIndex = 5;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(11, 162);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(92, 13);
			this.label5.TabIndex = 22;
			this.label5.Text = "Hardware revision";
			// 
			// modelTextBox
			// 
			this.modelTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.modelTextBox.Location = new System.Drawing.Point(109, 89);
			this.modelTextBox.Name = "modelTextBox";
			this.modelTextBox.Size = new System.Drawing.Size(482, 20);
			this.modelTextBox.TabIndex = 3;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(11, 92);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(36, 13);
			this.label4.TabIndex = 20;
			this.label4.Text = "Model";
			// 
			// serialNumberTextBox
			// 
			this.serialNumberTextBox.Location = new System.Drawing.Point(109, 124);
			this.serialNumberTextBox.Name = "serialNumberTextBox";
			this.serialNumberTextBox.Size = new System.Drawing.Size(206, 20);
			this.serialNumberTextBox.TabIndex = 4;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(11, 127);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(71, 13);
			this.label2.TabIndex = 26;
			this.label2.Text = "Serial number";
			// 
			// productCodeTextBox
			// 
			this.productCodeTextBox.Location = new System.Drawing.Point(109, 262);
			this.productCodeTextBox.Name = "productCodeTextBox";
			this.productCodeTextBox.Size = new System.Drawing.Size(206, 20);
			this.productCodeTextBox.TabIndex = 8;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(11, 265);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(71, 13);
			this.label9.TabIndex = 31;
			this.label9.Text = "Product code";
			// 
			// deviceRevisionTextBox
			// 
			this.deviceRevisionTextBox.Location = new System.Drawing.Point(109, 227);
			this.deviceRevisionTextBox.Name = "deviceRevisionTextBox";
			this.deviceRevisionTextBox.Size = new System.Drawing.Size(206, 20);
			this.deviceRevisionTextBox.TabIndex = 7;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(11, 230);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(80, 13);
			this.label8.TabIndex = 30;
			this.label8.Text = "Device revision";
			// 
			// CreateDeviceInstanceForm
			// 
			this.AcceptButton = this.saveButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.CancelButton = this.cancelButton;
			this.ClientSize = new System.Drawing.Size(604, 329);
			this.Controls.Add(this.productCodeTextBox);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.deviceRevisionTextBox);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.serialNumberTextBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.softwareRevisionTextBox);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.hardwareRevisionTextBox);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.modelTextBox);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.saveButton);
			this.Controls.Add(this.descriptionTextBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.nameTextBox);
			this.Controls.Add(this.label1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "CreateDeviceInstanceForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Create device instance";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditParametersForm_FormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox nameTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox descriptionTextBox;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button saveButton;
		private System.Windows.Forms.TextBox softwareRevisionTextBox;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox hardwareRevisionTextBox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox modelTextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox serialNumberTextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox productCodeTextBox;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox deviceRevisionTextBox;
		private System.Windows.Forms.Label label8;
	}
}