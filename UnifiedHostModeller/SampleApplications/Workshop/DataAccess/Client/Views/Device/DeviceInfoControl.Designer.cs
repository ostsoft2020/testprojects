﻿namespace UnifiedHostModeler.Views.Device
{
	partial class DeviceInfoControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.deviceClassTextBox = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.deviceRevisionTextBox = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.manufacturerUriTextBox = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.softwareRevisionTextBox = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.hardwareRevisionTextBox = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.modelTextBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.manufacturerTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.productCodeTextBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.serialNumberTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.enabledCheckBox = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// deviceClassTextBox
			// 
			this.deviceClassTextBox.Enabled = false;
			this.deviceClassTextBox.Location = new System.Drawing.Point(114, 261);
			this.deviceClassTextBox.Name = "deviceClassTextBox";
			this.deviceClassTextBox.Size = new System.Drawing.Size(206, 20);
			this.deviceClassTextBox.TabIndex = 8;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(16, 264);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(68, 13);
			this.label9.TabIndex = 38;
			this.label9.Text = "Device class";
			// 
			// deviceRevisionTextBox
			// 
			this.deviceRevisionTextBox.Enabled = false;
			this.deviceRevisionTextBox.Location = new System.Drawing.Point(114, 226);
			this.deviceRevisionTextBox.Name = "deviceRevisionTextBox";
			this.deviceRevisionTextBox.Size = new System.Drawing.Size(206, 20);
			this.deviceRevisionTextBox.TabIndex = 7;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(16, 229);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(80, 13);
			this.label8.TabIndex = 37;
			this.label8.Text = "Device revision";
			// 
			// manufacturerUriTextBox
			// 
			this.manufacturerUriTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.manufacturerUriTextBox.Enabled = false;
			this.manufacturerUriTextBox.Location = new System.Drawing.Point(114, 50);
			this.manufacturerUriTextBox.Name = "manufacturerUriTextBox";
			this.manufacturerUriTextBox.Size = new System.Drawing.Size(520, 20);
			this.manufacturerUriTextBox.TabIndex = 2;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(16, 53);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(92, 13);
			this.label7.TabIndex = 36;
			this.label7.Text = "Manufacturer URI";
			// 
			// softwareRevisionTextBox
			// 
			this.softwareRevisionTextBox.Enabled = false;
			this.softwareRevisionTextBox.Location = new System.Drawing.Point(114, 191);
			this.softwareRevisionTextBox.Name = "softwareRevisionTextBox";
			this.softwareRevisionTextBox.Size = new System.Drawing.Size(206, 20);
			this.softwareRevisionTextBox.TabIndex = 6;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(16, 194);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(88, 13);
			this.label6.TabIndex = 35;
			this.label6.Text = "Software revision";
			// 
			// hardwareRevisionTextBox
			// 
			this.hardwareRevisionTextBox.Enabled = false;
			this.hardwareRevisionTextBox.Location = new System.Drawing.Point(114, 156);
			this.hardwareRevisionTextBox.Name = "hardwareRevisionTextBox";
			this.hardwareRevisionTextBox.Size = new System.Drawing.Size(206, 20);
			this.hardwareRevisionTextBox.TabIndex = 5;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(16, 159);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(92, 13);
			this.label5.TabIndex = 34;
			this.label5.Text = "Hardware revision";
			// 
			// modelTextBox
			// 
			this.modelTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.modelTextBox.Enabled = false;
			this.modelTextBox.Location = new System.Drawing.Point(114, 85);
			this.modelTextBox.Name = "modelTextBox";
			this.modelTextBox.Size = new System.Drawing.Size(520, 20);
			this.modelTextBox.TabIndex = 3;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(16, 88);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(36, 13);
			this.label4.TabIndex = 33;
			this.label4.Text = "Model";
			// 
			// manufacturerTextBox
			// 
			this.manufacturerTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.manufacturerTextBox.Enabled = false;
			this.manufacturerTextBox.Location = new System.Drawing.Point(114, 15);
			this.manufacturerTextBox.Name = "manufacturerTextBox";
			this.manufacturerTextBox.Size = new System.Drawing.Size(520, 20);
			this.manufacturerTextBox.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(16, 18);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(70, 13);
			this.label2.TabIndex = 32;
			this.label2.Text = "Manufacturer";
			// 
			// productCodeTextBox
			// 
			this.productCodeTextBox.Enabled = false;
			this.productCodeTextBox.Location = new System.Drawing.Point(114, 296);
			this.productCodeTextBox.Name = "productCodeTextBox";
			this.productCodeTextBox.Size = new System.Drawing.Size(206, 20);
			this.productCodeTextBox.TabIndex = 9;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(16, 299);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(71, 13);
			this.label1.TabIndex = 40;
			this.label1.Text = "Product code";
			// 
			// serialNumberTextBox
			// 
			this.serialNumberTextBox.Enabled = false;
			this.serialNumberTextBox.Location = new System.Drawing.Point(114, 120);
			this.serialNumberTextBox.Name = "serialNumberTextBox";
			this.serialNumberTextBox.Size = new System.Drawing.Size(206, 20);
			this.serialNumberTextBox.TabIndex = 4;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(16, 123);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(71, 13);
			this.label3.TabIndex = 42;
			this.label3.Text = "Serial number";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(16, 334);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(46, 13);
			this.label10.TabIndex = 43;
			this.label10.Text = "Enabled";
			// 
			// enabledCheckBox
			// 
			this.enabledCheckBox.AutoSize = true;
			this.enabledCheckBox.Location = new System.Drawing.Point(114, 333);
			this.enabledCheckBox.Name = "enabledCheckBox";
			this.enabledCheckBox.Size = new System.Drawing.Size(15, 14);
			this.enabledCheckBox.TabIndex = 44;
			this.enabledCheckBox.UseVisualStyleBackColor = true;
			this.enabledCheckBox.CheckedChanged += new System.EventHandler(this.enabledCheckBox_CheckedChanged);
			// 
			// DeviceInfoControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Controls.Add(this.enabledCheckBox);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.serialNumberTextBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.productCodeTextBox);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.deviceClassTextBox);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.deviceRevisionTextBox);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.manufacturerUriTextBox);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.softwareRevisionTextBox);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.hardwareRevisionTextBox);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.modelTextBox);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.manufacturerTextBox);
			this.Controls.Add(this.label2);
			this.Name = "DeviceInfoControl";
			this.Size = new System.Drawing.Size(650, 511);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox deviceClassTextBox;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox deviceRevisionTextBox;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox manufacturerUriTextBox;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox softwareRevisionTextBox;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox hardwareRevisionTextBox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox modelTextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox manufacturerTextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox productCodeTextBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox serialNumberTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.CheckBox enabledCheckBox;
	}
}
