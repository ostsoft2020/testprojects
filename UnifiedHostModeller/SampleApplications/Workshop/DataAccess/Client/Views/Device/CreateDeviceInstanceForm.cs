﻿using Opc.Ua;
using Opc.Ua.Client;
using System.Windows.Forms;
using UnifiedHostModeler;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Editors
{
	public partial class CreateDeviceInstanceForm : Form
	{
		public CreateDeviceInstanceForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(Session session, ReferenceDescription nodeTypeReference, out (string Name, string Description, string Model, string SerialNumber, string HardwareRevision, string SoftwareRevision, string DeviceRevision, string ProductCode) instanceDescription)
		{
			nameTextBox.Text = nodeTypeReference.DisplayName.ToString();

			if (nameTextBox.Text.EndsWith("Type"))
				nameTextBox.Text = nameTextBox.Text.Remove(nameTextBox.Text.Length - 4, 4);

			ReferenceDescriptionCollection references =
				OpcUaNavigationHelper.Browse(session, (NodeId)nodeTypeReference.NodeId, BrowseDirection.Forward, NodeClass.Variable, new NodeId[] { ReferenceTypeIds.Aggregates });

			foreach (ReferenceDescription reference in references)
				switch (reference.DisplayName.ToString())
				{
					case nameof(IDevice.Model):
						modelTextBox.Text = session.ReadValue((NodeId)reference.NodeId).ToString();
						break;

					case nameof(IDevice.SerialNumber):
						serialNumberTextBox.Text = session.ReadValue((NodeId)reference.NodeId).ToString();
						break;

					case nameof(IDevice.HardwareRevision):
						hardwareRevisionTextBox.Text = session.ReadValue((NodeId)reference.NodeId).ToString();
						break;

					case nameof(IDevice.SoftwareRevision):
						softwareRevisionTextBox.Text = session.ReadValue((NodeId)reference.NodeId).ToString();
						break;

					case nameof(IDevice.DeviceRevision):
						deviceRevisionTextBox.Text = session.ReadValue((NodeId)reference.NodeId).ToString();
						break;

					case nameof(IDevice.ProductCode):
						productCodeTextBox.Text = session.ReadValue((NodeId)reference.NodeId).ToString();
						break;
				}

			if (ShowDialog() == DialogResult.OK)
			{
				instanceDescription = (nameTextBox.Text, descriptionTextBox.Text, modelTextBox.Text, serialNumberTextBox.Text, hardwareRevisionTextBox.Text, softwareRevisionTextBox.Text, deviceRevisionTextBox.Text, productCodeTextBox.Text);
				return true;
			}
			else
			{
				instanceDescription = ("", "", "", "", "", "", "", "");
				return false;
			}
		}

		private void EditParametersForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				return;

			if (!OpcUaHelper.ValidateName(nameTextBox.Text, "Name", out string errorMessage))
			{
				MessageBox.Show(errorMessage);
				e.Cancel = true;
			}
		}
	}
}
