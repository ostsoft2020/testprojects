﻿using System.Windows.Forms;
using Opc.Ua.Client;
using Opc.Ua;
using System.ComponentModel;
using UnifiedStudio.Models;
using System.Collections.Generic;
using System;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Controls
{
	public partial class DevicesControl : UserControl
	{
        private Session session;
        private Subscription subscription;
		private BindingList<DeviceDescriptor> deviceDescriptors = new BindingList<DeviceDescriptor>();
		private List<MonitoredItem> monitoredItems = new List<MonitoredItem>();

        public DevicesControl()
		{
			InitializeComponent();

            dataGridView.AutoGenerateColumns = false;
        }

        public void DisplayDevices(Session session, NodeId devicesNodeId)
		{
            removeSubscription();

            this.session = session;

            deviceDescriptors.Clear();

            // fetch property references from the server.
            ReferenceDescriptionCollection dvicesReferences =
				OpcUaNavigationHelper.Browse(session, devicesNodeId, BrowseDirection.Forward, NodeClass.Object, new NodeId[] { ReferenceTypeIds.Organizes });

            if (dvicesReferences == null)
                return;

            for (int i = 0; i < dvicesReferences.Count; i++)
            {
                // ignore external references.
                if (dvicesReferences[i].NodeId.IsAbsolute || dvicesReferences[i].DisplayName.ToString() == "DeviceFeatures")
                    continue;

                ReferenceDescriptionCollection deviceContentReferences =
					OpcUaNavigationHelper.Browse(session, (NodeId) dvicesReferences[i].NodeId, BrowseDirection.Forward, NodeClass.Variable | NodeClass.Method, new NodeId[] { ReferenceTypeIds.Aggregates }, true);

                DeviceDescriptor deviceDescriptor = new DeviceDescriptor 
                { 
                    Name = dvicesReferences[i].DisplayName.ToString(),
                    NodeId = (NodeId) dvicesReferences[i].NodeId
                };

                for (int j = 0; j < deviceContentReferences.Count; j++)
                    switch (deviceContentReferences[j].DisplayName.ToString())
				    {
                        case nameof(DeviceDescriptor.SerialNumber):
                            deviceDescriptor.SerialNumber = session.ReadValue((NodeId)deviceContentReferences[j].NodeId).Value?.ToString();
                            break;

                        case nameof(DeviceDescriptor.Enabled):
                            deviceDescriptor.Enabled = (bool)session.ReadValue((NodeId)deviceContentReferences[j].NodeId).Value;
                            deviceDescriptor.EnabledNodeId = (NodeId)deviceContentReferences[j].NodeId;
                            deviceDescriptor.EnabledMonitoredItem = сreateMonitoredItem(deviceDescriptor.EnabledNodeId);
                            break;

                        case "Enable":
							deviceDescriptor.EnableMethodNodeId = (NodeId)deviceContentReferences[j].NodeId;
							break;
						
                        case "Disable":
                            deviceDescriptor.DisableMethodNodeId = (NodeId)deviceContentReferences[j].NodeId;
                            break;
					}

				deviceDescriptors.Add(deviceDescriptor);
            }

            dataGridView.DataSource = deviceDescriptors;
        }

        private void removeSubscription()
        {
            if (subscription != null && subscription != null && monitoredItems != null)
            {
                try
                {
                    foreach (MonitoredItem monitoredItem in monitoredItems)
                    {
                        monitoredItem.Notification -= monitoredItem_Notification;
                        subscription.RemoveItem(monitoredItem);
                    }

                    session.RemoveSubscription(subscription);
                }
                catch
                {
                }

                subscription.Dispose();
                subscription = null;
            }
        }

        private MonitoredItem сreateMonitoredItem(NodeId nodeId)
        {
            if (nodeId == null)
                return null;

            if (subscription == null)
            {
                subscription = new Subscription(session.DefaultSubscription);

                subscription.PublishingEnabled = true;
                subscription.PublishingInterval = 1000;
                subscription.KeepAliveCount = 10;
                subscription.LifetimeCount = 10;
                subscription.MaxNotificationsPerPublish = 1000;
                subscription.Priority = 100;

                session.AddSubscription(subscription);

                subscription.Create();
            }

            // add the new monitored item.
            MonitoredItem monitoredItem = new MonitoredItem(subscription.DefaultItem);

            monitoredItem.StartNodeId = nodeId;
            monitoredItem.AttributeId = Attributes.Value;
            monitoredItem.DisplayName = "ValueSubscription";
            monitoredItem.MonitoringMode = MonitoringMode.Reporting;
            monitoredItem.SamplingInterval = 1000;
            monitoredItem.QueueSize = 0;
            monitoredItem.DiscardOldest = true;

            monitoredItems.Add(monitoredItem);

            monitoredItem.Notification += monitoredItem_Notification;

            subscription.AddItem(monitoredItem);

            subscription.ApplyChanges();

            return monitoredItem;
        }

        private void monitoredItem_Notification(MonitoredItem monitoredItem, MonitoredItemNotificationEventArgs e)
        {
            int i;

            for (i = 0; i < deviceDescriptors.Count; i++)
            {
                if (deviceDescriptors[i].EnabledMonitoredItem == monitoredItem)
                    deviceDescriptors[i].Enabled = (bool)((MonitoredItemNotification)e.NotificationValue).Value.Value;
                else
                    continue;

                deviceDescriptors.ResetItem(i);

                return;
            }
        }

		private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
            if (e.RowIndex < 0 || e.ColumnIndex < 0 || !(dataGridView.Columns[e.ColumnIndex] is DataGridViewButtonColumn))
                return;

            DeviceDescriptor deviceDescriptor = deviceDescriptors[e.RowIndex];

            if (deviceDescriptor == null)
                return;

            try
            {
                switch (dataGridView.Columns[e.ColumnIndex].Name)
                {
                    case nameof(enableColumn):
                        if (deviceDescriptor.EnableMethodNodeId != null)
                            session.Call(deviceDescriptor.NodeId, deviceDescriptor.EnableMethodNodeId);
                        break;

					case nameof(disableColumn):
						if (deviceDescriptor.DisableMethodNodeId != null)
							session.Call(deviceDescriptor.NodeId, deviceDescriptor.DisableMethodNodeId);
						break;
				}
			}
            catch (ServiceResultException sre)
            {
                MessageBox.Show(sre.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
	}
}
