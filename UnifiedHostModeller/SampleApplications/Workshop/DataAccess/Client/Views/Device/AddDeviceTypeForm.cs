﻿using System.Windows.Forms;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Editors
{
	public partial class AddDeviceTypeForm : Form
	{
		public AddDeviceTypeForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(out (string Name, string Description, string Manufacturer, string ManufacturerUri, string Model, string HardwareRevision, string SoftwareRevision, string DeviceRevision, string DeviceClass) deviceTypeDescription)
		{
			if (ShowDialog() == DialogResult.OK)
			{
				deviceTypeDescription = (nameTextBox.Text, descriptionTextBox.Text, manufacturerTextBox.Text, manufacturerUriTextBox.Text, modelTextBox.Text, hardwareRevisionTextBox.Text, softwareRevisionTextBox.Text, deviceRevisionTextBox.Text, deviceClassTextBox.Text);
				return true;
			}
			else
			{
				deviceTypeDescription = ("", "", "", "", "", "", "", "", "");
				return false;
			}
		}

		private void formClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				return;

			if (!OpcUaHelper.ValidateName(nameTextBox.Text, "Name", out string errorMessage))
			{
				MessageBox.Show(errorMessage);
				e.Cancel = true;
			}
		}
	}
}
