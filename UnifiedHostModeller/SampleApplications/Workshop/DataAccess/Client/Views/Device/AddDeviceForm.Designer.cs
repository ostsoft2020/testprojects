﻿namespace UnifiedStudio.Editors
{
	partial class AddDeviceForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.productCodeTextBox = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.deviceRevisionTextBox = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.manufacturerTextBox = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.softwareRevisionTextBox = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.hardwareRevisionTextBox = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.modelTextBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.descriptionTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.cancelButton = new System.Windows.Forms.Button();
			this.saveButton = new System.Windows.Forms.Button();
			this.displayNameTextBox = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.browseNameTextBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.deviceTypeComboBox = new System.Windows.Forms.ComboBox();
			this.label10 = new System.Windows.Forms.Label();
			this.deviceClassTextBox = new System.Windows.Forms.TextBox();
			this.label11 = new System.Windows.Forms.Label();
			this.serialNumberTextBox = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// productCodeTextBox
			// 
			this.productCodeTextBox.Location = new System.Drawing.Point(111, 366);
			this.productCodeTextBox.Name = "productCodeTextBox";
			this.productCodeTextBox.Size = new System.Drawing.Size(206, 20);
			this.productCodeTextBox.TabIndex = 10;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(13, 369);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(71, 13);
			this.label9.TabIndex = 119;
			this.label9.Text = "Product code";
			// 
			// deviceRevisionTextBox
			// 
			this.deviceRevisionTextBox.Location = new System.Drawing.Point(111, 331);
			this.deviceRevisionTextBox.Name = "deviceRevisionTextBox";
			this.deviceRevisionTextBox.Size = new System.Drawing.Size(206, 20);
			this.deviceRevisionTextBox.TabIndex = 9;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(13, 334);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(80, 13);
			this.label8.TabIndex = 118;
			this.label8.Text = "Device revision";
			// 
			// manufacturerTextBox
			// 
			this.manufacturerTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.manufacturerTextBox.Location = new System.Drawing.Point(111, 191);
			this.manufacturerTextBox.Name = "manufacturerTextBox";
			this.manufacturerTextBox.Size = new System.Drawing.Size(482, 20);
			this.manufacturerTextBox.TabIndex = 5;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(13, 194);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(70, 13);
			this.label7.TabIndex = 117;
			this.label7.Text = "Manufacturer";
			// 
			// softwareRevisionTextBox
			// 
			this.softwareRevisionTextBox.Location = new System.Drawing.Point(111, 296);
			this.softwareRevisionTextBox.Name = "softwareRevisionTextBox";
			this.softwareRevisionTextBox.Size = new System.Drawing.Size(206, 20);
			this.softwareRevisionTextBox.TabIndex = 8;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(13, 299);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(88, 13);
			this.label6.TabIndex = 116;
			this.label6.Text = "Software revision";
			// 
			// hardwareRevisionTextBox
			// 
			this.hardwareRevisionTextBox.Location = new System.Drawing.Point(111, 261);
			this.hardwareRevisionTextBox.Name = "hardwareRevisionTextBox";
			this.hardwareRevisionTextBox.Size = new System.Drawing.Size(206, 20);
			this.hardwareRevisionTextBox.TabIndex = 7;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(13, 264);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(92, 13);
			this.label5.TabIndex = 115;
			this.label5.Text = "Hardware revision";
			// 
			// modelTextBox
			// 
			this.modelTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.modelTextBox.Location = new System.Drawing.Point(111, 226);
			this.modelTextBox.Name = "modelTextBox";
			this.modelTextBox.Size = new System.Drawing.Size(482, 20);
			this.modelTextBox.TabIndex = 6;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(13, 229);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(36, 13);
			this.label4.TabIndex = 114;
			this.label4.Text = "Model";
			// 
			// descriptionTextBox
			// 
			this.descriptionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.descriptionTextBox.Location = new System.Drawing.Point(111, 119);
			this.descriptionTextBox.Name = "descriptionTextBox";
			this.descriptionTextBox.Size = new System.Drawing.Size(482, 20);
			this.descriptionTextBox.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(13, 122);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(60, 13);
			this.label2.TabIndex = 113;
			this.label2.Text = "Description";
			// 
			// cancelButton
			// 
			this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(518, 433);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(75, 23);
			this.cancelButton.TabIndex = 13;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.UseVisualStyleBackColor = true;
			// 
			// saveButton
			// 
			this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.saveButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.saveButton.Location = new System.Drawing.Point(436, 433);
			this.saveButton.Name = "saveButton";
			this.saveButton.Size = new System.Drawing.Size(75, 23);
			this.saveButton.TabIndex = 12;
			this.saveButton.Text = "Save";
			this.saveButton.UseVisualStyleBackColor = true;
			// 
			// displayNameTextBox
			// 
			this.displayNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.displayNameTextBox.Location = new System.Drawing.Point(111, 83);
			this.displayNameTextBox.Name = "displayNameTextBox";
			this.displayNameTextBox.Size = new System.Drawing.Size(482, 20);
			this.displayNameTextBox.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(13, 86);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(70, 13);
			this.label3.TabIndex = 108;
			this.label3.Text = "Display name";
			// 
			// browseNameTextBox
			// 
			this.browseNameTextBox.Location = new System.Drawing.Point(111, 47);
			this.browseNameTextBox.Name = "browseNameTextBox";
			this.browseNameTextBox.Size = new System.Drawing.Size(206, 20);
			this.browseNameTextBox.TabIndex = 1;
			this.browseNameTextBox.TextChanged += new System.EventHandler(this.browseNameTextBox_TextChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(13, 50);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(71, 13);
			this.label1.TabIndex = 103;
			this.label1.Text = "Browse name";
			// 
			// deviceTypeComboBox
			// 
			this.deviceTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.deviceTypeComboBox.FormattingEnabled = true;
			this.deviceTypeComboBox.Location = new System.Drawing.Point(111, 11);
			this.deviceTypeComboBox.Name = "deviceTypeComboBox";
			this.deviceTypeComboBox.Size = new System.Drawing.Size(482, 21);
			this.deviceTypeComboBox.TabIndex = 0;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(13, 14);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(64, 13);
			this.label10.TabIndex = 122;
			this.label10.Text = "Device type";
			// 
			// deviceClassTextBox
			// 
			this.deviceClassTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.deviceClassTextBox.Location = new System.Drawing.Point(111, 155);
			this.deviceClassTextBox.Name = "deviceClassTextBox";
			this.deviceClassTextBox.Size = new System.Drawing.Size(206, 20);
			this.deviceClassTextBox.TabIndex = 4;
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(13, 158);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(68, 13);
			this.label11.TabIndex = 125;
			this.label11.Text = "Device class";
			// 
			// serialNumberTextBox
			// 
			this.serialNumberTextBox.Location = new System.Drawing.Point(111, 400);
			this.serialNumberTextBox.Name = "serialNumberTextBox";
			this.serialNumberTextBox.Size = new System.Drawing.Size(206, 20);
			this.serialNumberTextBox.TabIndex = 11;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(13, 403);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(71, 13);
			this.label12.TabIndex = 127;
			this.label12.Text = "Serial number";
			// 
			// AddDeviceForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(606, 470);
			this.Controls.Add(this.serialNumberTextBox);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.deviceClassTextBox);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.deviceTypeComboBox);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.productCodeTextBox);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.deviceRevisionTextBox);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.manufacturerTextBox);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.softwareRevisionTextBox);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.hardwareRevisionTextBox);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.modelTextBox);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.descriptionTextBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.saveButton);
			this.Controls.Add(this.displayNameTextBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.browseNameTextBox);
			this.Controls.Add(this.label1);
			this.Name = "AddDeviceForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Add device";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formClosing);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox productCodeTextBox;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox deviceRevisionTextBox;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox manufacturerTextBox;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox softwareRevisionTextBox;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox hardwareRevisionTextBox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox modelTextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox descriptionTextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button saveButton;
		private System.Windows.Forms.TextBox displayNameTextBox;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox browseNameTextBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox deviceTypeComboBox;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox deviceClassTextBox;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox serialNumberTextBox;
		private System.Windows.Forms.Label label12;
	}
}