﻿namespace UnifiedStudio.Controls
{
	partial class DevicesControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dataGridView = new System.Windows.Forms.DataGridView();
			this.nameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.serialNumberColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.enabledColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.enableColumn = new System.Windows.Forms.DataGridViewButtonColumn();
			this.disableColumn = new System.Windows.Forms.DataGridViewButtonColumn();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView
			// 
			this.dataGridView.AllowUserToAddRows = false;
			this.dataGridView.AllowUserToDeleteRows = false;
			this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameColumn,
            this.serialNumberColumn,
            this.enabledColumn,
            this.enableColumn,
            this.disableColumn});
			this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridView.Location = new System.Drawing.Point(0, 0);
			this.dataGridView.Name = "dataGridView";
			this.dataGridView.RowHeadersVisible = false;
			this.dataGridView.Size = new System.Drawing.Size(779, 459);
			this.dataGridView.TabIndex = 0;
			this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentClick);
			// 
			// nameColumn
			// 
			this.nameColumn.DataPropertyName = "Name";
			this.nameColumn.Frozen = true;
			this.nameColumn.HeaderText = "Name";
			this.nameColumn.Name = "nameColumn";
			this.nameColumn.ReadOnly = true;
			this.nameColumn.Width = 150;
			// 
			// serialNumberColumn
			// 
			this.serialNumberColumn.DataPropertyName = "SerialNumber";
			this.serialNumberColumn.HeaderText = "Serial number";
			this.serialNumberColumn.Name = "serialNumberColumn";
			this.serialNumberColumn.ReadOnly = true;
			// 
			// enabledColumn
			// 
			this.enabledColumn.DataPropertyName = "Enabled";
			this.enabledColumn.HeaderText = "Enabled";
			this.enabledColumn.Name = "enabledColumn";
			this.enabledColumn.Width = 55;
			// 
			// enableColumn
			// 
			this.enableColumn.HeaderText = "Enable";
			this.enableColumn.Name = "enableColumn";
			this.enableColumn.Text = "Enable";
			this.enableColumn.UseColumnTextForButtonValue = true;
			this.enableColumn.Width = 50;
			// 
			// disableColumn
			// 
			this.disableColumn.HeaderText = "Disable";
			this.disableColumn.Name = "disableColumn";
			this.disableColumn.Text = "Disable";
			this.disableColumn.UseColumnTextForButtonValue = true;
			this.disableColumn.Width = 50;
			// 
			// DevicesControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.dataGridView);
			this.Name = "DevicesControl";
			this.Size = new System.Drawing.Size(779, 459);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridView;
		private System.Windows.Forms.DataGridViewTextBoxColumn nameColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn serialNumberColumn;
		private System.Windows.Forms.DataGridViewCheckBoxColumn enabledColumn;
		private System.Windows.Forms.DataGridViewButtonColumn enableColumn;
		private System.Windows.Forms.DataGridViewButtonColumn disableColumn;
	}
}
