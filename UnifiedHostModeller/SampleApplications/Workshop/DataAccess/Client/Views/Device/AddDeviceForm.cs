﻿using Opc.Ua;
using Opc.Ua.Client;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Editors
{
	public partial class AddDeviceForm : Form
	{
		private string prevBrowseName = "";
		private readonly Dictionary<string, NodeId> deviceTypes = new Dictionary<string, NodeId>();

		public AddDeviceForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(Session session, out (NodeId deviceType, string browseName, string displayName, string description, string deviceClass, string manufacturer, string model, string hardwareRevision, string softwareRevision, string deviceRevision, string productCode, string serialNumber) deviceDescription)
		{
			ReferenceDescriptionCollection subTypeReferences =
				OpcUaNavigationHelper.Browse(session, session.GetDynamicDeviceTypeNodeId(), BrowseDirection.Forward, NodeClass.ObjectType, new NodeId[] { ReferenceTypes.HasSubtype });

			deviceTypes.Clear();
			deviceTypes.Add("DynamicDevice", session.GetDynamicDeviceTypeNodeId());
			foreach (var item in subTypeReferences)
				deviceTypes.Add(item.DisplayName.ToString().Replace("Type", ""), (NodeId)item.NodeId);

			deviceTypeComboBox.Items.Clear();
			deviceTypeComboBox.Items.AddRange(deviceTypes.Keys.ToArray());
			deviceTypeComboBox.SelectedIndex = 0;

			if (ShowDialog() == DialogResult.OK)
			{
				deviceTypes.TryGetValue(deviceTypeComboBox.Text, out NodeId devicetypeNodeId);
				deviceDescription = (devicetypeNodeId , browseNameTextBox.Text, displayNameTextBox.Text, descriptionTextBox.Text, deviceClassTextBox.Text, manufacturerTextBox.Text, modelTextBox.Text, hardwareRevisionTextBox.Text, softwareRevisionTextBox.Text, deviceRevisionTextBox.Text, productCodeTextBox.Text, serialNumberTextBox.Text);
				return true;
			}
			else
			{
				deviceDescription = (null, "", "", "", "", "", "", "", "", "", "", "");
				return false;
			}
		}

		private void formClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				return;

			if (!OpcUaHelper.ValidateName(browseNameTextBox.Text, "Browse name", out string errorMessage))
			{
				MessageBox.Show(errorMessage);
				e.Cancel = true;
			}

			if (!OpcUaHelper.ValidateName(displayNameTextBox.Text, "Display name", out errorMessage))
			{
				MessageBox.Show(errorMessage);
				e.Cancel = true;
			}
		}

		private void browseNameTextBox_TextChanged(object sender, System.EventArgs e)
		{
			if (prevBrowseName == displayNameTextBox.Text)
				displayNameTextBox.Text = browseNameTextBox.Text;

			prevBrowseName = browseNameTextBox.Text;

		}
	}
}
