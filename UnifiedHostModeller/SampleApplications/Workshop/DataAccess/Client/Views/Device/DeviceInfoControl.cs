﻿using System.Windows.Forms;
using Opc.Ua.Client;
using Opc.Ua;
using System.Threading.Tasks;
using System;
using Opc.Ua.Client.Controls;
using UnifiedHostModeler.Helpers;

namespace UnifiedHostModeler.Views.Device
{
	public partial class DeviceInfoControl : UserControl
	{
		private Session session;
		private bool forType;
		private ReferenceDescription enabledReference;
		private bool loading;

		public DeviceInfoControl()
		{
			InitializeComponent();
		}

		public void DisplayDevice(Session session, NodeId nodeId, bool forType)
		{
			this.session = session;
			this.forType = forType;

			loading = true;

			try
			{
				reset();

				ReferenceDescriptionCollection childReferences =
					OpcUaNavigationHelper.Browse(session, nodeId, BrowseDirection.Forward, NodeClass.Variable, new NodeId[] { ReferenceTypeIds.Aggregates });

				foreach (ReferenceDescription childReference in childReferences)
					switch (childReference.DisplayName.ToString())
					{
						case nameof(IDevice.Manufacturer):
							manufacturerTextBox.Text = session.ReadValue((NodeId)childReference.NodeId)?.ToString();
							break;

						case nameof(IDevice.ManufacturerUri):
							manufacturerUriTextBox.Text = session.ReadValue((NodeId)childReference.NodeId)?.ToString();
							break;

						case nameof(IDevice.Model):
							modelTextBox.Text = session.ReadValue((NodeId)childReference.NodeId)?.ToString();
							break;

						case nameof(IDevice.SerialNumber):
							serialNumberTextBox.Text = session.ReadValue((NodeId)childReference.NodeId)?.ToString();
							break;

						case nameof(IDevice.HardwareRevision):
							hardwareRevisionTextBox.Text = session.ReadValue((NodeId)childReference.NodeId)?.ToString();
							break;

						case nameof(IDevice.SoftwareRevision):
							softwareRevisionTextBox.Text = session.ReadValue((NodeId)childReference.NodeId)?.ToString();
							break;

						case nameof(IDevice.DeviceRevision):
							deviceRevisionTextBox.Text = session.ReadValue((NodeId)childReference.NodeId)?.ToString();
							break;

						case nameof(IDevice.DeviceClass):
							deviceClassTextBox.Text = session.ReadValue((NodeId)childReference.NodeId)?.ToString();
							break;

						case nameof(IDevice.ProductCode):
							productCodeTextBox.Text = session.ReadValue((NodeId)childReference.NodeId)?.ToString();
							break;

						case nameof(IDevice.Enabled):
							enabledReference = childReference;
							enabledCheckBox.Checked = (bool)session.ReadValue((NodeId)childReference.NodeId).Value;
							break;
					}

				enabledCheckBox.Enabled = checkIsEnabledWritable();
			}
			finally
			{
				loading = false;
			}
		}

		private void reset()
		{
			enabledReference = null;

			manufacturerTextBox.Text = "";
			manufacturerUriTextBox.Text = "";
			modelTextBox.Text = "";
			serialNumberTextBox.Text = "";
			hardwareRevisionTextBox.Text = "";
			softwareRevisionTextBox.Text = "";
			deviceRevisionTextBox.Text = "";
			deviceClassTextBox.Text = "";
			productCodeTextBox.Text = "";
			enabledCheckBox.Checked = false;
		}

		private void enabledCheckBox_CheckedChanged(object sender, System.EventArgs e)
		{
			if (enabledReference == null || loading)
				return;

			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)enabledReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = enabledCheckBox.Checked };

			try
			{
				session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);
			}
			catch (Exception exception)
			{
				ClientUtils.HandleException(Text, exception);
			}

			Task.Factory.StartNew(
				() =>
				{
					Task.Delay(500).Wait();

					int timeout = 5000;
					int delay = 1000;

					for (int i = 0; i < timeout / delay; i++)
					{
						if (!checkIsEnabledWritable())
							break;

						Task.Delay(delay).Wait();
					}

					OpcUaHelper.SaveConfiguration(session);
				}
			);
		}

		private bool checkIsEnabledWritable()
		{
			if (forType || enabledReference == null)
				return false;

			ReadValueIdCollection nodesToRead = new ReadValueIdCollection()
			{
				new ReadValueId {NodeId = (NodeId)enabledReference.NodeId, AttributeId = Attributes.AccessLevel},
				new ReadValueId {NodeId = (NodeId)enabledReference.NodeId, AttributeId = Attributes.UserAccessLevel}
			};

			try
			{
				session.Read(null, 0, TimestampsToReturn.Neither, nodesToRead, out DataValueCollection results, out DiagnosticInfoCollection diagnosticInfos);

				byte accessLevel = (byte)results[0].Value;
				byte userAccessLevel = (byte)results[1].Value;

				return (accessLevel == AccessLevels.CurrentWrite || accessLevel == AccessLevels.CurrentReadOrWrite)
					&& (userAccessLevel == AccessLevels.CurrentWrite || userAccessLevel == AccessLevels.CurrentReadOrWrite);
			}
			catch 
			{
				return false;
			}
		}
	}
}
