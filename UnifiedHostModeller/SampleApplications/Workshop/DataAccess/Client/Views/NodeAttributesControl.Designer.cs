﻿namespace UnifiedStudio.Controls
{
	partial class NodeAttributesControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.nameColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.valueColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.attributesControl = new System.Windows.Forms.ListView();
			this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.copyNodeIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.contextMenuStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// nameColumn
			// 
			this.nameColumn.Text = "Name";
			// 
			// valueColumn
			// 
			this.valueColumn.Text = "Value";
			this.valueColumn.Width = 102;
			// 
			// attributesControl
			// 
			this.attributesControl.BackColor = System.Drawing.Color.White;
			this.attributesControl.BackgroundImageTiled = true;
			this.attributesControl.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameColumn,
            this.valueColumn});
			this.attributesControl.ContextMenuStrip = this.contextMenuStrip;
			this.attributesControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.attributesControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.attributesControl.ForeColor = System.Drawing.SystemColors.WindowText;
			this.attributesControl.FullRowSelect = true;
			this.attributesControl.HideSelection = false;
			this.attributesControl.Location = new System.Drawing.Point(0, 0);
			this.attributesControl.Name = "attributesControl";
			this.attributesControl.Size = new System.Drawing.Size(296, 200);
			this.attributesControl.TabIndex = 3;
			this.attributesControl.UseCompatibleStateImageBehavior = false;
			this.attributesControl.View = System.Windows.Forms.View.Details;
			// 
			// contextMenuStrip
			// 
			this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyNodeIDToolStripMenuItem});
			this.contextMenuStrip.Name = "contextMenuStrip";
			this.contextMenuStrip.Size = new System.Drawing.Size(181, 48);
			// 
			// copyNodeIDToolStripMenuItem
			// 
			this.copyNodeIDToolStripMenuItem.Name = "copyNodeIDToolStripMenuItem";
			this.copyNodeIDToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.copyNodeIDToolStripMenuItem.Text = "Copy node ID";
			this.copyNodeIDToolStripMenuItem.Click += new System.EventHandler(this.copyNodeIDToolStripMenuItem_Click);
			// 
			// NodeAttributesControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.Controls.Add(this.attributesControl);
			this.Name = "NodeAttributesControl";
			this.Size = new System.Drawing.Size(296, 200);
			this.contextMenuStrip.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ColumnHeader nameColumn;
		private System.Windows.Forms.ColumnHeader valueColumn;
		private System.Windows.Forms.ListView attributesControl;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
		private System.Windows.Forms.ToolStripMenuItem copyNodeIDToolStripMenuItem;
	}
}
