﻿using System.Windows.Forms;
using Opc.Ua.Client;
using Opc.Ua;
using System.Collections;
using System.Linq;

namespace UnifiedStudio.Controls
{
	public partial class VariableControl : UserControl
	{
		public VariableControl()
		{
			InitializeComponent();
		}

		public void DisplayVariable(Session session, NodeId nodeId)
		{
			DataValue value = session.ReadValue(nodeId);

			if (!(value.Value is IEnumerable))
				valueTextBox.Text = value.Value?.ToString();
			else
				valueTextBox.Text = "[" + string.Join(",", ((IEnumerable) value.Value).Cast<object>().Select(o => o?.ToString()).ToArray()) + "]";
		}
	}
}
