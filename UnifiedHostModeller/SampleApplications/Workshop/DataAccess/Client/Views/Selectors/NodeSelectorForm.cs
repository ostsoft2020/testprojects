﻿using Opc.Ua;
using Opc.Ua.Client;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using UnifiedHostModeler.Helpers;

namespace UnifiedHostModeler.Views.Selectors
{
	public partial class NodeSelectorForm : Form
	{
		public class nodeDescriptor
		{
			public bool Expanded;
			public ReferenceDescription NodeReference;
		}

		private Session session;
		private NodeId originalSelectedNodeId;
		private NodeId selectedNodeId;
		private NodeClass nodeClassesMask;
		private NodeId[] referenceTypeIds;
		private BrowseResultMask browseResultMask;
		private Func<ReferenceDescription, bool> childFilter;
		private Func<Session, IList<ReferenceDescription>, bool> selectButtonChecker;

		public NodeSelectorForm()
		{
			InitializeComponent();
		}

		public NodeId ShowDialog(
			Session session, 
			NodeId rootNodeId, 
			NodeId selectedNodeId, 
			NodeClass nodeClassesMask, 
			NodeId[] referenceTypeIds, 
			BrowseResultMask browseResultMask = BrowseResultMask.All, 
			Func<ReferenceDescription, bool> childFilter = null,
			Func<Session, IList<ReferenceDescription>, bool> selectButtonChecker = null)
		{
			treeView.Nodes.Clear();

			this.session = session;
			originalSelectedNodeId = selectedNodeId;
			this.nodeClassesMask = nodeClassesMask;
			this.referenceTypeIds = referenceTypeIds;
			this.browseResultMask = browseResultMask;
			this.childFilter = childFilter;
			this.selectButtonChecker = selectButtonChecker;

			// fill root nodes
			browseChildren(treeView.Nodes, rootNodeId);

			// fill root subnodes
			for (int i = 0; i < treeView.Nodes.Count; i++)
			{
				nodeDescriptor nodeDescriptor = treeView.Nodes[i].Tag as nodeDescriptor;

				browseChildren(treeView.Nodes[i].Nodes, (NodeId)nodeDescriptor.NodeReference.NodeId);
			}

			if (ShowDialog() == DialogResult.OK)
				return this.selectedNodeId;
			else
				return originalSelectedNodeId;
		}

		private void browseChildren(TreeNodeCollection nodeCollection, NodeId parentNodeId)
		{
			ReferenceDescriptionCollection childReferences =
				OpcUaNavigationHelper.Browse(session, parentNodeId, BrowseDirection.Forward, nodeClassesMask, referenceTypeIds, true, browseResultMask, childFilter);

			for (int i = 0; i < childReferences.Count; i++)
			{
				var treeNode = new TreeNode(childReferences[i].DisplayName.ToString()) { Tag = new nodeDescriptor() { NodeReference = childReferences[i] } };

				nodeCollection.Add(treeNode);
			}
		}

		private List<ReferenceDescription> getParentReferences(TreeNode node)
		{
			List<ReferenceDescription> result = new List<ReferenceDescription>();

			do
			{
				nodeDescriptor nodeDescriptor = node.Tag as nodeDescriptor;

				result.Add(nodeDescriptor.NodeReference);

				node = node.Parent;
			}
			while (node != null);

			return result;
		}

		private void NodeSelectorForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (e.Cancel)
				return;
		}

		private void treeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
		{
			nodeDescriptor nodeDescriptor = e.Node.Tag as nodeDescriptor;

			if (nodeDescriptor == null || nodeDescriptor.Expanded)
				return;

			nodeDescriptor.Expanded = true;

			for (int i = 0; i < e.Node.Nodes.Count; i++)
				browseChildren(e.Node.Nodes[i].Nodes, (NodeId)((nodeDescriptor)e.Node.Nodes[i].Tag).NodeReference.NodeId);
		}

		private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
		{
			if (e.Node == null)
			{
				selectButton.Enabled = false;
				selectedNodeId = null;
				return;
			}

			nodeDescriptor nodeDescriptor = e.Node.Tag as nodeDescriptor;

			if (selectButtonChecker == null)
			{
				selectButton.Enabled = true;
				selectedNodeId = (NodeId)nodeDescriptor.NodeReference.NodeId;
			}
			else
			{
				List<ReferenceDescription> parentReferences = getParentReferences(e.Node);

				if (selectButtonChecker(session, parentReferences))
				{
					selectButton.Enabled = true;
					selectedNodeId = (NodeId)nodeDescriptor.NodeReference.NodeId;
				}
				else
				{
					selectButton.Enabled = false;
					selectedNodeId = null;
				}
			}				
		}
	}
}
