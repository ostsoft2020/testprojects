﻿using Opc.Ua;
using Opc.Ua.Client;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Editors
{
	public partial class AddBindingForm : Form
	{
		private readonly Dictionary<string, NodeId> bindngTypes = new Dictionary<string, NodeId>();

		public AddBindingForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(Session session, NodeId variableNodeId, out (NodeId variableNodeId, NodeId bindingTypeNodeId) bindingDescription)
		{
			variableNodeIdTextBox.Text = variableNodeId.ToString();

			ReferenceDescriptionCollection subTypeReferences =
				OpcUaNavigationHelper.Browse(session, session.GetVariableBindingTypeNodeId(), BrowseDirection.Forward, NodeClass.ObjectType, new NodeId[] { ReferenceTypes.HasSubtype });

			bindngTypes.Clear();
			foreach (var item in subTypeReferences)
				bindngTypes.Add(item.DisplayName.ToString().Replace("Type", ""), (NodeId) item.NodeId);

			bindingTypeComboBox.Items.Clear();
			bindingTypeComboBox.Items.AddRange(bindngTypes.Keys.ToArray());
			bindingTypeComboBox.SelectedIndex = 0;

			if (ShowDialog() == DialogResult.OK)
			{
				bindngTypes.TryGetValue(bindingTypeComboBox.Text, out NodeId bindingTypeNodeId);
				bindingDescription = (NodeId.Parse(variableNodeIdTextBox.Text), bindingTypeNodeId);
				return true;
			}
			else
			{
				bindingDescription = (null, null);
				return false;
			}
		}

		private void formClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				return;

			try
			{
				NodeId.Parse(variableNodeIdTextBox.Text);
			}
			catch
			{
				MessageBox.Show("");
				e.Cancel = true;
			}
		}
	}
}
