﻿using Opc.Ua;
using System.Collections.Generic;
using System.Windows.Forms;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Editors
{
	public partial class AddFunctionaGroupForm : Form
	{
		private readonly Dictionary<string, NodeId> subTypes = new Dictionary<string, NodeId>();

		public AddFunctionaGroupForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(out (string Name, string Description) functionalGroupDescription)
		{
			if (ShowDialog() == DialogResult.OK)
			{
				functionalGroupDescription = (nameTextBox.Text, descriptionTextBox.Text);
				return true;
			}
			else
			{
				functionalGroupDescription = ("", "");
				return false;
			}
		}

		private void formClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				return;

			if (!OpcUaHelper.ValidateName(nameTextBox.Text, "Name", out string errorMessage))
			{
				MessageBox.Show(errorMessage);
				e.Cancel = true;
			}
		}
	}
}
