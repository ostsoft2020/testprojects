﻿using Opc.Ua;
using System.Collections.Generic;
using System.Windows.Forms;


namespace UnifiedStudio.Editors
{
	public partial class AssingParameterToFunctionalGroupForm : Form
	{
		private readonly Dictionary<string, NodeId> subTypes = new Dictionary<string, NodeId>();

		public AssingParameterToFunctionalGroupForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(string functionalGroupName, string[] parameterNames, out string selectedParameterName)
		{
			functionalGroupNameTextBox.Text = functionalGroupName;
			parameterNamesComboBox.Items.AddRange(parameterNames);

			if (ShowDialog() == DialogResult.OK)
			{
				selectedParameterName = parameterNamesComboBox.Text;
				return true;
			}
			else
			{
				selectedParameterName = "";
				return false;
			}
		}

		private void formClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				return;

			if (string.IsNullOrWhiteSpace(parameterNamesComboBox.Text))
			{
				MessageBox.Show("Parameter should be selected");
				e.Cancel = true;
			}
		}
	}
}
