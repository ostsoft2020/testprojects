﻿using Opc.Ua;
using Opc.Ua.Client;
using Opc.Ua.Client.Controls;
using Quickstarts;
using System;
using System.Windows.Forms;

namespace UnifiedStudio.Controls
{
	public partial class NodeAttributesControl : UserControl
	{
        private NodeId nodeId;

		public NodeAttributesControl()
		{
			InitializeComponent();
		}

        /// <summary>
        /// Displays the attributes and properties in the attributes view.
        /// </summary>
        /// <param name="sourceId">The NodeId of the Node to browse.</param>
        public void DisplayAttributes(Session session, NodeId sourceId)
        {
            try
            {
                Clear();

                ReadValueIdCollection nodesToRead = new ReadValueIdCollection()
                {
                    new ReadValueId {NodeId = sourceId, AttributeId = Attributes.NodeId},
                    new ReadValueId {NodeId = sourceId, AttributeId = Attributes.BrowseName},
                    new ReadValueId {NodeId = sourceId, AttributeId = Attributes.DisplayName},
                    new ReadValueId {NodeId = sourceId, AttributeId = Attributes.IsAbstract},
                    new ReadValueId {NodeId = sourceId, AttributeId = Attributes.AccessLevel},
                    new ReadValueId {NodeId = sourceId, AttributeId = Attributes.DataType},
                    new ReadValueId {NodeId = sourceId, AttributeId = Attributes.Value},
                    new ReadValueId {NodeId = sourceId, AttributeId = Attributes.Description},
                };

                // read all values.
                DataValueCollection results = null;
                DiagnosticInfoCollection diagnosticInfos = null;

                session.Read(
                    null,
                    0,
                    TimestampsToReturn.Neither,
                    nodesToRead,
                    out results,
                    out diagnosticInfos);

                ClientBase.ValidateResponse(results, nodesToRead);
                ClientBase.ValidateDiagnosticInfos(diagnosticInfos, nodesToRead);

                // process results
                for (int i = 0; i < results.Count; i++)
                {
                    string name = null;
                    string value = null;

                    // ignore attributes which are invalid for the node.
                    if (results[i].StatusCode == StatusCodes.BadAttributeIdInvalid)
                        continue;

                    // get the name of the attribute.
                    name = Attributes.GetBrowseName(nodesToRead[i].AttributeId);

                    if (name == nameof(NodeId))
                        nodeId = results[i].Value as NodeId;

                    // display any unexpected error.
                    if (StatusCode.IsBad(results[i].StatusCode))
                        value = Utils.Format("{0}", results[i].StatusCode);
                    // display the value.
                    else
                    {
                        TypeInfo typeInfo = TypeInfo.Construct(results[i].Value);

                        switch (name)
                        {
                            case nameof(Attributes.DataType):
                                value = TypeInfo.GetBuiltInType(results[i].Value as NodeId).ToString();
                                break;

                            case nameof(Attributes.AccessLevel):
                                switch ((byte)results[i].Value)
                                {
                                    case AccessLevels.None:
                                        value = "None";
                                        break;
                                    case AccessLevels.CurrentRead:
                                        value = "CurrentRead";
                                        break;
                                    case AccessLevels.CurrentWrite:
                                        value = "CurrentWrite";
                                        break;
                                    case AccessLevels.CurrentReadOrWrite:
                                        value = "CurrentReadOrWrite";
                                        break;
                                }
                                break;

                            default:
                                value = Utils.Format("{0}", results[i].Value);
                                break;
                        }
                    }

                    // add the attribute name/value to the list view.
                    ListViewItem item = new ListViewItem(name);
                    item.SubItems.Add(value);
                    attributesControl.Items.Add(item);
                }

                // adjust width of all columns.
                for (int ii = 0; ii < attributesControl.Columns.Count; ii++)
                    attributesControl.Columns[ii].Width = -2;
            }
            catch (Exception exception)
            {
                ClientUtils.HandleException(Text, exception);
            }
        }

        public void Clear()
		{
			attributesControl.Items.Clear();
		}

		private void copyNodeIDToolStripMenuItem_Click(object sender, EventArgs e)
		{
            Clipboard.SetText(nodeId?.ToString());
        }
	}
}
