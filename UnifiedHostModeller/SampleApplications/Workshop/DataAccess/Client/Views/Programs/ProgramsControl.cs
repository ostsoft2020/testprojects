﻿using System.Windows.Forms;
using Opc.Ua.Client;
using Opc.Ua;
using UnifiedStudio.Models;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Controls
{
	public partial class ProgramsControl : UserControl
	{
        private Session session;
        private Subscription subscription;
        private readonly BindingList<ProgramDescriptor> programDescriptors = new BindingList<ProgramDescriptor>();
        private readonly List<MonitoredItem> monitoredItems = new List<MonitoredItem>();

        public ProgramsControl()
		{
			InitializeComponent();

            dataGridView.AutoGenerateColumns = false;
        }

        public void DisplayPrograms(Session session, NodeId programsNodeId, bool forType)
		{
            removeSubscription();

            this.session = session;

            programDescriptors.Clear();

            startColumn.Visible = !forType;
            haltColumn.Visible = !forType;
            resetColumn.Visible = !forType;

            ReferenceDescriptionCollection references =
				OpcUaNavigationHelper.Browse(session, programsNodeId, BrowseDirection.Forward, NodeClass.Object, new NodeId[] { ReferenceTypeIds.HasComponent });

            if (references == null)
                return;

            for (int i = 0; i < references.Count; i++)
            {
                ProgramDescriptor programDescriptor = new ProgramDescriptor
                {
                    Name = references[i].DisplayName.ToString(),
                    NodeId = (NodeId) references[i].NodeId
                };

                ReferenceDescriptionCollection subReferences =
					OpcUaNavigationHelper.Browse(session, (NodeId) references[i].NodeId, BrowseDirection.Forward, NodeClass.Variable | NodeClass.Object | NodeClass.Method, new NodeId[] { ReferenceTypeIds.HasComponent });

                for (int j = 0; j < subReferences.Count; j++)
                    switch (subReferences[j].DisplayName.ToString())
                    {
                        case nameof(programDescriptor.AutoStart):
                            programDescriptor.AutoStartNodeId = (NodeId)subReferences[j].NodeId;
                            programDescriptor.AutoStart = (bool)session.ReadValue(programDescriptor.AutoStartNodeId).Value;
                            programDescriptor.AutoStartMonitoredItem = сreateMonitoredItem(subReferences[j]);
                            break;

                        case nameof(programDescriptor.CurrentState):
                            programDescriptor.CurrentStateNodeId = (NodeId)subReferences[j].NodeId;
                            programDescriptor.CurrentState = session.ReadValue(programDescriptor.CurrentStateNodeId).ToString();
                            programDescriptor.CurrentStateMonitoredItem = сreateMonitoredItem(subReferences[j]);
                            break;

                        case nameof(programDescriptor.LastTransition):
                            programDescriptor.LastTransitionNodeId = (NodeId)subReferences[j].NodeId;
                            programDescriptor.LastTransition = session.ReadValue(programDescriptor.LastTransitionNodeId).ToString();
                            programDescriptor.LastTransitionMonitoredItem = сreateMonitoredItem(subReferences[j]);
                            break;

                        case "FinalResultData":
                            ReferenceDescription haltReasonReference =
								OpcUaNavigationHelper.Browse(session, (NodeId)subReferences[j].NodeId, BrowseDirection.Forward, NodeClass.Variable, new NodeId[] { ReferenceTypeIds.HasComponent }, childFilter: (rd) => rd.DisplayName.ToString() == nameof(programDescriptor.HaltReason)).FirstOrDefault();
                            
                            if (haltReasonReference != null)
                            {
                                programDescriptor.HaltReasonNodeId = (NodeId)haltReasonReference.NodeId;
                                programDescriptor.HaltReason = session.ReadValue(programDescriptor.HaltReasonNodeId).ToString();
                                programDescriptor.HaltReasonMonitoredItem = сreateMonitoredItem(haltReasonReference);
                            }
                            
                            break;

                        case "Start":
                            programDescriptor.StartMethodNodeId = (NodeId)subReferences[j].NodeId;
                            break;

                        case "Halt":
                            programDescriptor.HaltMethodNodeId = (NodeId)subReferences[j].NodeId;
                            break;

                        case "Reset":
                            programDescriptor.ResetMethodNodeId = (NodeId)subReferences[j].NodeId;
                            break;
                    }

                programDescriptors.Add(programDescriptor);
            }

            dataGridView.DataSource = programDescriptors;
        }

        private void removeSubscription()
        {
            if (subscription != null && subscription != null && monitoredItems != null)
            {
                try
                {
                    foreach (MonitoredItem monitoredItem in monitoredItems)
                    {
                        monitoredItem.Notification -= monitoredItem_Notification;
                        subscription.RemoveItem(monitoredItem);
                    }

                    session.RemoveSubscription(subscription);
                }
                catch
                {
                }

                subscription.Dispose();
                subscription = null;
            }
        }

        private MonitoredItem сreateMonitoredItem(ReferenceDescription variableReference)
        {
            if (variableReference == null)
                return null;

            if (subscription == null)
            {
                subscription = new Subscription(session.DefaultSubscription);

                subscription.PublishingEnabled = true;
                subscription.PublishingInterval = 1000;
                subscription.KeepAliveCount = 10;
                subscription.LifetimeCount = 10;
                subscription.MaxNotificationsPerPublish = 1000;
                subscription.Priority = 100;

                session.AddSubscription(subscription);

                subscription.Create();
            }

            // add the new monitored item.
            MonitoredItem monitoredItem = new MonitoredItem(subscription.DefaultItem);

            monitoredItem.StartNodeId = (NodeId) variableReference.NodeId;
            monitoredItem.AttributeId = Attributes.Value;
            monitoredItem.DisplayName = variableReference.DisplayName + "Subscription";
            monitoredItem.MonitoringMode = MonitoringMode.Reporting;
            monitoredItem.SamplingInterval = 1000;
            monitoredItem.QueueSize = 0;
            monitoredItem.DiscardOldest = true;

            monitoredItems.Add(monitoredItem);

            monitoredItem.Notification += monitoredItem_Notification;

            subscription.AddItem(monitoredItem);

            subscription.ApplyChanges();

            return monitoredItem;
        }

        private void monitoredItem_Notification(MonitoredItem monitoredItem, MonitoredItemNotificationEventArgs e)
        {
            for (int i = 0; i < programDescriptors.Count; i++)
            {
                if (programDescriptors[i].AutoStartMonitoredItem == monitoredItem)
                    programDescriptors[i].AutoStart = (bool) ((MonitoredItemNotification)e.NotificationValue).Value.Value;
                if (programDescriptors[i].CurrentStateMonitoredItem == monitoredItem)
                    programDescriptors[i].CurrentState = ((MonitoredItemNotification)e.NotificationValue).Value?.ToString();
                else if (programDescriptors[i].LastTransitionMonitoredItem == monitoredItem)
                    programDescriptors[i].LastTransition = ((MonitoredItemNotification)e.NotificationValue).Value?.ToString();
                else if (programDescriptors[i].HaltReasonMonitoredItem == monitoredItem)
                    programDescriptors[i].HaltReason = ((MonitoredItemNotification)e.NotificationValue).Value?.ToString();
                else
                    continue;

                programDescriptors.ResetItem(i);

                return;
            }
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0 || !(dataGridView.Columns[e.ColumnIndex] is DataGridViewButtonColumn))
                return;

            ProgramDescriptor programDescriptor = programDescriptors[e.RowIndex];

            if (programDescriptor == null)
                return;

            try
            {
                switch (dataGridView.Columns[e.ColumnIndex].Name)
                {
                    case nameof(startColumn):
                        if (programDescriptor.StartMethodNodeId != null)
                            session.Call(programDescriptor.NodeId, programDescriptor.StartMethodNodeId);
                        break;

                    case nameof(haltColumn):
                        if (programDescriptor.HaltMethodNodeId != null)
                            session.Call(programDescriptor.NodeId, programDescriptor.HaltMethodNodeId);
                        break;

                    case nameof(resetColumn):
                        if (programDescriptor.ResetMethodNodeId != null)
                            session.Call(programDescriptor.NodeId, programDescriptor.ResetMethodNodeId);
                        break;
                }
            }
            catch (ServiceResultException sre)
            {
                MessageBox.Show(sre.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
