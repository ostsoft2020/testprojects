﻿namespace UnifiedStudio.Controls
{
	partial class ConditionMonitoringProgramControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.panel1 = new System.Windows.Forms.Panel();
			this.databaseConnectorComboBox = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.deactivationDelayUpDown = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.activationDelayUpDown = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.autoStartCheckBox = new System.Windows.Forms.CheckBox();
			this.label4 = new System.Windows.Forms.Label();
			this.samplingIntervalUpDown = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.editingIsNotAllowedLabel = new System.Windows.Forms.Label();
			this.conditionSourceConnectorComboBox = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.bindingSource = new System.Windows.Forms.BindingSource(this.components);
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.deactivationDelayUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.activationDelayUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.samplingIntervalUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.databaseConnectorComboBox);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.deactivationDelayUpDown);
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.activationDelayUpDown);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Controls.Add(this.autoStartCheckBox);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.samplingIntervalUpDown);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.editingIsNotAllowedLabel);
			this.panel1.Controls.Add(this.conditionSourceConnectorComboBox);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(818, 519);
			this.panel1.TabIndex = 0;
			// 
			// databaseConnectorComboBox
			// 
			this.databaseConnectorComboBox.FormattingEnabled = true;
			this.databaseConnectorComboBox.Location = new System.Drawing.Point(152, 45);
			this.databaseConnectorComboBox.Name = "databaseConnectorComboBox";
			this.databaseConnectorComboBox.Size = new System.Drawing.Size(200, 21);
			this.databaseConnectorComboBox.TabIndex = 19;
			this.databaseConnectorComboBox.TextChanged += new System.EventHandler(this.databaseConnectorComboBox_TextChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(9, 48);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(104, 13);
			this.label2.TabIndex = 18;
			this.label2.Text = "Database connector";
			// 
			// deactivationDelayUpDown
			// 
			this.deactivationDelayUpDown.DecimalPlaces = 2;
			this.deactivationDelayUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.deactivationDelayUpDown.Location = new System.Drawing.Point(152, 180);
			this.deactivationDelayUpDown.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
			this.deactivationDelayUpDown.Name = "deactivationDelayUpDown";
			this.deactivationDelayUpDown.Size = new System.Drawing.Size(55, 20);
			this.deactivationDelayUpDown.TabIndex = 17;
			this.deactivationDelayUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.deactivationDelayUpDown.ValueChanged += new System.EventHandler(this.deactivationDelayUpDown_ValueChanged);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(9, 182);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(109, 13);
			this.label6.TabIndex = 16;
			this.label6.Text = "Deactivation delay (s)";
			// 
			// activationDelayUpDown
			// 
			this.activationDelayUpDown.DecimalPlaces = 2;
			this.activationDelayUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.activationDelayUpDown.Location = new System.Drawing.Point(152, 145);
			this.activationDelayUpDown.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
			this.activationDelayUpDown.Name = "activationDelayUpDown";
			this.activationDelayUpDown.Size = new System.Drawing.Size(55, 20);
			this.activationDelayUpDown.TabIndex = 15;
			this.activationDelayUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.activationDelayUpDown.ValueChanged += new System.EventHandler(this.activationDelayUpDown_ValueChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(9, 147);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(96, 13);
			this.label5.TabIndex = 14;
			this.label5.Text = "Activation delay (s)";
			// 
			// autoStartCheckBox
			// 
			this.autoStartCheckBox.AutoSize = true;
			this.autoStartCheckBox.Location = new System.Drawing.Point(152, 81);
			this.autoStartCheckBox.Name = "autoStartCheckBox";
			this.autoStartCheckBox.Size = new System.Drawing.Size(15, 14);
			this.autoStartCheckBox.TabIndex = 9;
			this.autoStartCheckBox.UseVisualStyleBackColor = true;
			this.autoStartCheckBox.CheckedChanged += new System.EventHandler(this.autoStartCheckBox_CheckedChanged);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(9, 81);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(52, 13);
			this.label4.TabIndex = 8;
			this.label4.Text = "Auto start";
			// 
			// samplingIntervalUpDown
			// 
			this.samplingIntervalUpDown.DecimalPlaces = 2;
			this.samplingIntervalUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.samplingIntervalUpDown.Location = new System.Drawing.Point(152, 110);
			this.samplingIntervalUpDown.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
			this.samplingIntervalUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.samplingIntervalUpDown.Name = "samplingIntervalUpDown";
			this.samplingIntervalUpDown.Size = new System.Drawing.Size(55, 20);
			this.samplingIntervalUpDown.TabIndex = 7;
			this.samplingIntervalUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.samplingIntervalUpDown.ValueChanged += new System.EventHandler(this.samplingIntervalUpDown_ValueChanged);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(9, 112);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(101, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Sampling interval (s)";
			// 
			// editingIsNotAllowedLabel
			// 
			this.editingIsNotAllowedLabel.AutoSize = true;
			this.editingIsNotAllowedLabel.ForeColor = System.Drawing.Color.DarkOrchid;
			this.editingIsNotAllowedLabel.Location = new System.Drawing.Point(393, 12);
			this.editingIsNotAllowedLabel.Name = "editingIsNotAllowedLabel";
			this.editingIsNotAllowedLabel.Size = new System.Drawing.Size(106, 13);
			this.editingIsNotAllowedLabel.TabIndex = 5;
			this.editingIsNotAllowedLabel.Text = "Editing is not allowed";
			// 
			// conditionSourceConnectorComboBox
			// 
			this.conditionSourceConnectorComboBox.FormattingEnabled = true;
			this.conditionSourceConnectorComboBox.Location = new System.Drawing.Point(152, 9);
			this.conditionSourceConnectorComboBox.Name = "conditionSourceConnectorComboBox";
			this.conditionSourceConnectorComboBox.Size = new System.Drawing.Size(200, 21);
			this.conditionSourceConnectorComboBox.TabIndex = 3;
			this.conditionSourceConnectorComboBox.TextChanged += new System.EventHandler(this.conditionSourceConnectorComboBox_TextChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(9, 12);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(137, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Condition source connector";
			// 
			// bindingSource
			// 
			this.bindingSource.DataSource = typeof(UnifiedStudio.Models.OpcUaConditionMonitoringSettings);
			// 
			// OpcUaConditionMonitoringProgramControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Controls.Add(this.panel1);
			this.Name = "OpcUaConditionMonitoringProgramControl";
			this.Size = new System.Drawing.Size(818, 519);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.deactivationDelayUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.activationDelayUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.samplingIntervalUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.bindingSource)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.ComboBox conditionSourceConnectorComboBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DataGridViewTextBoxColumn modeNameDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn modeIdDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewCheckBoxColumn monitorableDataGridViewCheckBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn targetVariableNameDataGridViewTextBoxColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn sourceVariableIdDataGridViewTextBoxColumn;
		private System.Windows.Forms.BindingSource bindingSource;
		private System.Windows.Forms.Label editingIsNotAllowedLabel;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
		private System.Windows.Forms.NumericUpDown samplingIntervalUpDown;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox autoStartCheckBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.NumericUpDown deactivationDelayUpDown;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.NumericUpDown activationDelayUpDown;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox databaseConnectorComboBox;
		private System.Windows.Forms.Label label2;
	}
}
