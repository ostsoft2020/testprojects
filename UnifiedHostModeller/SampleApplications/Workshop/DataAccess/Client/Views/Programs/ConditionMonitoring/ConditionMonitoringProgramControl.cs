﻿using System.Linq;
using System.Windows.Forms;
using Opc.Ua.Client;
using Opc.Ua;
using System.Collections.Generic;
using System;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Controls
{
	public partial class ConditionMonitoringProgramControl : UserControl
	{
		private Session session;
		private ReferenceDescription[] opcUaConnectorReferences;
		private ReferenceDescription[] databaseConnectorReferences;
		private ReferenceDescription conditionSourceConnectorNameReference;
		private ReferenceDescription databaseConnectorNameReference;
		private ReferenceDescription autoStartReference;
		private ReferenceDescription smaplingIntervalReference;
		private ReferenceDescription activationDelayReference;
		private ReferenceDescription deactivationDelayReference;

		public ConditionMonitoringProgramControl()
		{
			InitializeComponent();
		}

		public void DisplayProgram(Session session, NodeId programNodeId, NodeId connectorSetNodeId, string conditionSourceConnectorTypeName)
		{
			this.session = session;

			opcUaConnectorReferences = getConnectorReferences(connectorSetNodeId, conditionSourceConnectorTypeName);
			databaseConnectorReferences = getConnectorReferences(connectorSetNodeId, "DatabaseConnectorType");

			conditionSourceConnectorComboBox.Items.Clear();
			conditionSourceConnectorComboBox.Items.AddRange(opcUaConnectorReferences.Select(r => r.DisplayName.ToString()).ToArray());

			databaseConnectorComboBox.Items.Clear();
			databaseConnectorComboBox.Items.AddRange(databaseConnectorReferences.Select(r => r.DisplayName.ToString()).ToArray());

			ReferenceDescriptionCollection childReferences =
				OpcUaNavigationHelper.Browse(session, programNodeId, BrowseDirection.Forward, NodeClass.Variable, new NodeId[] { ReferenceTypeIds.HasComponent }, false);

			conditionSourceConnectorNameReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "ConditionSourceConnectorName");
			conditionSourceConnectorComboBox.Text = session.ReadValue((NodeId)conditionSourceConnectorNameReference.NodeId).Value?.ToString();

			databaseConnectorNameReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "DataBaseConnectorName");
			databaseConnectorComboBox.Text = session.ReadValue((NodeId)databaseConnectorNameReference.NodeId).Value?.ToString();

			autoStartReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "AutoStart");
			autoStartCheckBox.Checked = (bool)session.ReadValue((NodeId)autoStartReference.NodeId).Value;

			smaplingIntervalReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "SamplingInterval");
			samplingIntervalUpDown.Value = Convert.ToDecimal(session.ReadValue((NodeId)smaplingIntervalReference.NodeId).Value);

			activationDelayReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "ActivationDelay");
			activationDelayUpDown.Value = Convert.ToDecimal(session.ReadValue((NodeId)activationDelayReference.NodeId).Value);

			deactivationDelayReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "DeactivationDelay");
			deactivationDelayUpDown.Value = Convert.ToDecimal(session.ReadValue((NodeId)deactivationDelayReference.NodeId).Value);

			ReferenceDescription currentStateReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "CurrentState");

			string currentState = session.ReadValue((NodeId)currentStateReference.NodeId).ToString();

			if (currentState != "Ready")
			{
				editingIsNotAllowedLabel.Text = $"Program can't be edited because current state is '{currentState}'";
				editingIsNotAllowedLabel.Visible = true;
				Enabled = false;
			}
			else
			{
				editingIsNotAllowedLabel.Visible = false;
				Enabled = true;
			}
		}

		private ReferenceDescription[] getConnectorReferences(NodeId connectorSetNodeId, string connectorTypeName)
		{
			List<ReferenceDescription> result = new List<ReferenceDescription>();

			ReferenceDescriptionCollection connectorReferences =
				OpcUaNavigationHelper.Browse(session, connectorSetNodeId, BrowseDirection.Forward, NodeClass.Object, new NodeId[] { ReferenceTypeIds.HasComponent });

			foreach (ReferenceDescription connectorReference in connectorReferences)
			{
				ReferenceDescription connectorTypeReference =
					OpcUaNavigationHelper.Browse(session, (NodeId)connectorReference.NodeId, BrowseDirection.Forward, NodeClass.ObjectType, new NodeId[] { ReferenceTypeIds.HasTypeDefinition }).FirstOrDefault();

				if (connectorTypeReference.DisplayName == connectorTypeName)
					result.Add(connectorReference);
			}

			return result.ToArray();
		}

		private void conditionSourceConnectorComboBox_TextChanged(object sender, EventArgs e)
		{
			if (conditionSourceConnectorNameReference == null)
				return;

			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)conditionSourceConnectorNameReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = conditionSourceConnectorComboBox.Text };

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);
		}

		private void databaseConnectorComboBox_TextChanged(object sender, EventArgs e)
		{
			if (databaseConnectorNameReference == null)
				return;

			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)databaseConnectorNameReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = databaseConnectorComboBox.Text };

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);
		}

		private void autoStartCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (autoStartReference == null)
				return;

			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)autoStartReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = autoStartCheckBox.Checked };

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);
		}

		private void samplingIntervalUpDown_ValueChanged(object sender, EventArgs e)
		{
			if (smaplingIntervalReference == null)
				return;

			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)smaplingIntervalReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = Convert.ToDouble(samplingIntervalUpDown.Value) };

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);
		}

		private void activationDelayUpDown_ValueChanged(object sender, EventArgs e)
		{
			if (activationDelayReference == null)
				return;

			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)activationDelayReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = Convert.ToDouble(activationDelayUpDown.Value) };

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);
		}

		private void deactivationDelayUpDown_ValueChanged(object sender, EventArgs e)
		{
			if (deactivationDelayReference == null)
				return;

			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)deactivationDelayReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = Convert.ToDouble(deactivationDelayUpDown.Value) };

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);
		}
	}
}
