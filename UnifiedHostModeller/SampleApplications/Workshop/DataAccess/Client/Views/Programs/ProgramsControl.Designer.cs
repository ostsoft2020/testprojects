﻿namespace UnifiedStudio.Controls
{
	partial class ProgramsControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dataGridView = new System.Windows.Forms.DataGridView();
			this.nameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.currentStateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.lastTransitionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.autostartColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.startColumn = new System.Windows.Forms.DataGridViewButtonColumn();
			this.haltColumn = new System.Windows.Forms.DataGridViewButtonColumn();
			this.resetColumn = new System.Windows.Forms.DataGridViewButtonColumn();
			this.haltReasonColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView
			// 
			this.dataGridView.AllowUserToAddRows = false;
			this.dataGridView.AllowUserToDeleteRows = false;
			this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameColumn,
            this.currentStateColumn,
            this.lastTransitionColumn,
            this.autostartColumn,
            this.startColumn,
            this.haltColumn,
            this.resetColumn,
            this.haltReasonColumn});
			this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridView.Location = new System.Drawing.Point(0, 0);
			this.dataGridView.Name = "dataGridView";
			this.dataGridView.ReadOnly = true;
			this.dataGridView.RowHeadersVisible = false;
			this.dataGridView.Size = new System.Drawing.Size(779, 459);
			this.dataGridView.TabIndex = 0;
			this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentClick);
			// 
			// nameColumn
			// 
			this.nameColumn.DataPropertyName = "Name";
			this.nameColumn.HeaderText = "Name";
			this.nameColumn.Name = "nameColumn";
			this.nameColumn.ReadOnly = true;
			this.nameColumn.Width = 120;
			// 
			// currentStateColumn
			// 
			this.currentStateColumn.DataPropertyName = "CurrentState";
			this.currentStateColumn.HeaderText = "Current state";
			this.currentStateColumn.Name = "currentStateColumn";
			this.currentStateColumn.ReadOnly = true;
			this.currentStateColumn.Width = 90;
			// 
			// lastTransitionColumn
			// 
			this.lastTransitionColumn.DataPropertyName = "LastTransition";
			this.lastTransitionColumn.HeaderText = "Last transition";
			this.lastTransitionColumn.Name = "lastTransitionColumn";
			this.lastTransitionColumn.ReadOnly = true;
			// 
			// autostartColumn
			// 
			this.autostartColumn.DataPropertyName = "AutoStart";
			this.autostartColumn.HeaderText = "Autostart";
			this.autostartColumn.Name = "autostartColumn";
			this.autostartColumn.ReadOnly = true;
			this.autostartColumn.Width = 55;
			// 
			// startColumn
			// 
			this.startColumn.HeaderText = "Start";
			this.startColumn.Name = "startColumn";
			this.startColumn.ReadOnly = true;
			this.startColumn.Text = "Start";
			this.startColumn.UseColumnTextForButtonValue = true;
			this.startColumn.Width = 50;
			// 
			// haltColumn
			// 
			this.haltColumn.HeaderText = "Halt";
			this.haltColumn.Name = "haltColumn";
			this.haltColumn.ReadOnly = true;
			this.haltColumn.Text = "Halt";
			this.haltColumn.UseColumnTextForButtonValue = true;
			this.haltColumn.Width = 50;
			// 
			// resetColumn
			// 
			this.resetColumn.HeaderText = "Reset";
			this.resetColumn.Name = "resetColumn";
			this.resetColumn.ReadOnly = true;
			this.resetColumn.Text = "Reset";
			this.resetColumn.UseColumnTextForButtonValue = true;
			this.resetColumn.Width = 50;
			// 
			// haltReasonColumn
			// 
			this.haltReasonColumn.DataPropertyName = "HaltReason";
			this.haltReasonColumn.HeaderText = "Halt reason";
			this.haltReasonColumn.Name = "haltReasonColumn";
			this.haltReasonColumn.ReadOnly = true;
			this.haltReasonColumn.Width = 300;
			// 
			// ProgramsControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.dataGridView);
			this.Name = "ProgramsControl";
			this.Size = new System.Drawing.Size(779, 459);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridView;
		private System.Windows.Forms.DataGridViewTextBoxColumn nameColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn currentStateColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn lastTransitionColumn;
		private System.Windows.Forms.DataGridViewCheckBoxColumn autostartColumn;
		private System.Windows.Forms.DataGridViewButtonColumn startColumn;
		private System.Windows.Forms.DataGridViewButtonColumn haltColumn;
		private System.Windows.Forms.DataGridViewButtonColumn resetColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn haltReasonColumn;
	}
}
