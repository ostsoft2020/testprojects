﻿using Opc.Ua;
using Opc.Ua.Client;
using Opc.Ua.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using UnifiedHostModeler.Helpers;
using UnifiedHostModeler.Models;
using UnifiedHostModeler.Views.Selectors;
using UnifiedStudio;
using UnifiedStudio.Models;

namespace UnifiedHostModeler.Views.Programs.BindOpcUaVariable
{
	public partial class EditOpcUaBindingItemForm : Form
	{		
		private class opcConnectionSettings
		{
			public int AuthenticationMethod;
			public byte[] Certificate;
			public double ConnectionTimeout;
			public string EndpointUrl;
			public int MessageSecurityMode;
			public double OperationTimeout;
			public byte[] PrivateKey;
			public int SecurityPolicy;
			public string SessionName;
			public string UserName;
			public string UserPassword;
		}

		private Session session;
		private OpcUaBindingSettings bindingSettings;
		private Dictionary<string, string> converterParameterDescriptions;
		private opcConnectionSettings remoteConnectionSettings;

		public EditOpcUaBindingItemForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(
			Session session, 
			OpcUaBindingSettings bindingSettings, 
			ReferenceDescription[] converterReferences, 
			ParameterReferenceDescription[] parameterReferences,
			NodeId connectorNodeId)
		{
			this.session = session;
			this.bindingSettings = bindingSettings;

			modeComboBox.SelectedIndex = 1;

			remoteConnectionSettings = getConnectionSettings(connectorNodeId);

			selectNodeButton.Enabled = remoteConnectionSettings != null;

			converterParameterDescriptions = getConverterParameterDescriptions(converterReferences);

			converterComboBox.Items.Clear();
			converterComboBox.Items.AddRange(converterParameterDescriptions.Keys.ToArray());

			targetParameterNameComboBox.Items.Clear();
			targetParameterNameComboBox.Items.AddRange(getParameters("", parameterReferences).ToArray());

			ReferenceDescription selectedConverterReference = converterReferences.FirstOrDefault(cr => ((NodeId)cr.NodeId).ToString() == bindingSettings.Converter);

			modeComboBox.Text = bindingSettings.ModeName;
			targetParameterNameComboBox.Text = bindingSettings.TargetVariablePath;
			monitorableCheckBox.Checked = bindingSettings.Monitorable;
			converterComboBox.Text = selectedConverterReference != null ? selectedConverterReference.DisplayName.ToString() : "";
			converterParameterTextBox.Text = bindingSettings.ConverterParameter;
			samplingIntervalTextBox.Text = bindingSettings.SamplingInterval.ToString();
			sourceVariableNodeIdTextBox.Text = bindingSettings.SourceVariableId;

			bool result = ShowDialog() == DialogResult.OK;

			bindingSettings.ModeName = modeComboBox.Text;
			bindingSettings.TargetVariablePath = targetParameterNameComboBox.Text;
			bindingSettings.Monitorable = monitorableCheckBox.Checked;
			bindingSettings.Converter = (NodeId)converterReferences.FirstOrDefault(cr => cr.DisplayName.ToString() == converterComboBox.Text)?.NodeId;
			bindingSettings.ConverterName = converterComboBox.Text;
			bindingSettings.ConverterParameter = converterParameterTextBox.Text;
			bindingSettings.SamplingInterval = double.Parse(samplingIntervalTextBox.Text);
			bindingSettings.SourceVariableId = sourceVariableNodeIdTextBox.Text;

			return result;		
		}

		private IList<string> getParameters(string parentPath, ParameterReferenceDescription[] parameterReferences)
		{
			List<string> result = new List<string>();

			foreach (ParameterReferenceDescription parameterReference in parameterReferences)
			{
				string path = parameterReference.ReferenceDescription.DisplayName.ToString();

				if (!string.IsNullOrEmpty(parentPath))
					path = parentPath + "/" + path;

				result.Add(path);

				if (parameterReference.Children.Length > 0)
					result.AddRange(getParameters(path, parameterReference.Children));
			}

			return result;
		}

		private opcConnectionSettings getConnectionSettings(NodeId connectorNodeId)
		{
			if (connectorNodeId == null || connectorNodeId.IsNullNodeId)
				return null;

			ReferenceDescriptionCollection references = OpcUaNavigationHelper.Browse(
				session,
				connectorNodeId,
				BrowseDirection.Forward,
				NodeClass.Variable,
				new NodeId[] { ReferenceTypeIds.HasComponent });

			if (references == null)
				return null;

			opcConnectionSettings connectionSettings = new opcConnectionSettings();

			foreach (ReferenceDescription reference in references)
			{
				DataValue variableValue = session.ReadValue((NodeId)reference.NodeId);

				switch (reference.DisplayName.ToString())
				{
					case nameof(connectionSettings.AuthenticationMethod):
						connectionSettings.AuthenticationMethod = (int) variableValue.Value;
						break;

					case nameof(connectionSettings.Certificate):
						connectionSettings.Certificate = (byte[]) variableValue.Value;
						break;

					case nameof(connectionSettings.ConnectionTimeout):
						connectionSettings.ConnectionTimeout = (double)variableValue.Value * 1000;
						break;

					case nameof(connectionSettings.EndpointUrl):
						connectionSettings.EndpointUrl = (string) variableValue.Value;
						break;

					case nameof(connectionSettings.MessageSecurityMode):
						connectionSettings.MessageSecurityMode = (int) variableValue.Value;
						break;

					case nameof(connectionSettings.OperationTimeout):
						connectionSettings.OperationTimeout = (double)variableValue.Value * 1000;
						break;

					case nameof(connectionSettings.PrivateKey):
						connectionSettings.PrivateKey = (byte[]) variableValue.Value;
						break;

					case nameof(connectionSettings.SecurityPolicy):
						connectionSettings.SecurityPolicy = (int) variableValue.Value;
						break;

					case nameof(connectionSettings.SessionName):
						connectionSettings.SessionName = (string) variableValue.Value;
						break;

					case nameof(connectionSettings.UserName):
						connectionSettings.UserName = (string) variableValue.Value;
						break;

					case nameof(connectionSettings.UserPassword):
						connectionSettings.UserPassword = (string) variableValue.Value;
						break;
				}
			}

			return connectionSettings;
		}

		private Dictionary<string, string> getConverterParameterDescriptions(ReferenceDescription[] converterReferences)
		{
			Dictionary<string, string> result = new Dictionary<string, string>(converterReferences.Length);

			foreach (ReferenceDescription reference in converterReferences)
			{
				string converterName = reference.DisplayName.ToString();

				ReferenceDescription parameterDescriptionReference =
					OpcUaNavigationHelper.Browse(
						session,
						(NodeId)reference.NodeId,
						BrowseDirection.Forward,
						NodeClass.Variable,
						new NodeId[] { ReferenceTypeIds.HasProperty },
						true,
						BrowseResultMask.All,
						(rd) => rd.DisplayName.ToString() == "ConverterParameterDescription"
					).FirstOrDefault();

				string converterParameterDescription = null;

				if (parameterDescriptionReference.NodeId != null)
					converterParameterDescription = session.ReadValue((NodeId)parameterDescriptionReference.NodeId).ToString();

				result[converterName] = converterParameterDescription;
			}

			return result;
		}

		private Session createSessionByRemoteConectionSettings()
		{
			if (remoteConnectionSettings == null)
				return null;

			try
			{
				ApplicationConfiguration configuration = ApplicationConfiguration.Load("RemoteClient", ApplicationType.Client).Result;

				ApplicationInstance application = new ApplicationInstance(configuration);
				//{
				//	ApplicationName = "OPC-UA Client",
				//	ApplicationType = ApplicationType.Client,
				//	ConfigSectionName = "RemoteClient"
				//};

				EndpointConfiguration endpointConfiguration = EndpointConfiguration.Create(application.ApplicationConfiguration);

				// select the best endpoint.
				EndpointDescription endpointDescription = CoreClientUtils.SelectEndpoint(remoteConnectionSettings.EndpointUrl, remoteConnectionSettings.SecurityPolicy != 0, Convert.ToInt32(remoteConnectionSettings.OperationTimeout));

				ConfiguredEndpoint endpoint = new ConfiguredEndpoint(null, endpointDescription, endpointConfiguration);

				Session remoteSession = Session.Create(
					application.ApplicationConfiguration,
					endpoint,
					endpoint.UpdateBeforeConnect,
					false,
					application.ApplicationName,
					60000,
					new UserIdentity(),
					new List<string>()
				).Result;

				return remoteSession;
			}
			catch (Exception e)
			{
				MessageBox.Show(e.GetBaseException().Message, "Connecting error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return null;
			}
		}

		private void editOpcUaBindingItemForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				return;

			string message = null;

			if (!BindingSettingsBase.ValidateModeName(modeComboBox.Text))
				message = "Invalid mode specified";
			else if (string.IsNullOrWhiteSpace(targetParameterNameComboBox.Text))
				message = "Empty target parameter name specified";

			if (!string.IsNullOrWhiteSpace(converterComboBox.Text))
				try
				{
					NodeId.Parse(converterComboBox.Text);
				}
				catch
				{
					message = "Invalid converter node id specified";
				}

			if (string.IsNullOrWhiteSpace(sourceVariableNodeIdTextBox.Text))
				message = "Source variable node id is not specified";
			else
				try
				{
					NodeId.Parse(sourceVariableNodeIdTextBox.Text);
				}
				catch
				{
					message = "Invalid source variable node id specified";
				}

			if (message != null)
			{
				MessageBox.Show(message);
				e.Cancel = true;
			}
		}

		private void selectNodeButton_Click(object sender, System.EventArgs e)
		{
			NodeSelectorForm nodeSelector = new NodeSelectorForm();

			bool checkNode(Session session, IList<ReferenceDescription> referenceDescriptions)
			{
				return referenceDescriptions[0].NodeClass == NodeClass.Variable;
			}

			NodeId originalSelectedNodeId;

			try
			{
				originalSelectedNodeId = sourceVariableNodeIdTextBox.Text;
			}
			catch
			{
				originalSelectedNodeId = new NodeId();
			}

			Session remoteSettion = createSessionByRemoteConectionSettings();

			if (remoteSettion == null)
				return;

			NodeId selectedNodeId =	nodeSelector.ShowDialog(
				remoteSettion,
				ObjectIds.RootFolder,
				originalSelectedNodeId,
				NodeClass.Object | NodeClass.Variable,
				new NodeId[] { ReferenceTypeIds.Aggregates, ReferenceTypeIds.Organizes },
				BrowseResultMask.All,
				(rd) => !Equals(rd.NodeId.Identifier, (uint)86) || rd.NodeId.NamespaceIndex != 0,
				checkNode);

			sourceVariableNodeIdTextBox.Text = selectedNodeId?.ToString();
		}

		private void converterComboBox_TextChanged(object sender, System.EventArgs e)
		{
			converterParameterDescriptions.TryGetValue(converterComboBox.Text, out string parameterDescription);

			if (string.IsNullOrWhiteSpace(parameterDescription))
				converterParameterLabel.Text = "Converter parameter";
			else
				converterParameterLabel.Text = parameterDescription;
		}
	}
}
