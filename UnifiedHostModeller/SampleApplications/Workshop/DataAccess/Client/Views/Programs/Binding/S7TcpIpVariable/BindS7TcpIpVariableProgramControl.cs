﻿using System.Linq;
using System.Windows.Forms;
using Opc.Ua.Client;
using Opc.Ua;
using System.Collections.Generic;
using UnifiedStudio.Models;
using System.Reflection;
using System;
using System.ComponentModel;
using UnifiedHostModeler.Views.Programs.BindOpcUaVariable;
using UnifiedHostModeler.Models;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Controls
{
	public partial class BindS7TcpIpVariableProgramControl : UserControl
	{
		private Session session;
		private ReferenceDescription[] connectorReferences;
		private ReferenceDescription[] converterReferences;
		private ParameterReferenceDescription[] parameterReferences;
		private ReferenceDescription connectorNameReference;
		private ReferenceDescription autoStartReference;
		private ReferenceDescription bindingItemsReference;
		private readonly List<S7TcpIpBindingSettings> bindingSettingsList = new List<S7TcpIpBindingSettings>();

		public BindS7TcpIpVariableProgramControl()
		{
			InitializeComponent();
			dataGridView.AutoGenerateColumns = false;
		}

		public void DisplayProgram(Session session, NodeId programNodeId, NodeId connectorSetNodeId, NodeId parameterSetNodeId)
		{
			this.session = session;

			connectorReferences = getConnectorReferences(connectorSetNodeId);
			converterReferences = getConverterReferences();
			parameterReferences = getParameterReferences(parameterSetNodeId);

			connectorNameComboBox.Items.Clear();
			connectorNameComboBox.Items.AddRange(connectorReferences.Select(r => r.DisplayName.ToString()).ToArray());

			ReferenceDescriptionCollection childReferences =
				OpcUaNavigationHelper.Browse(session, programNodeId, BrowseDirection.Forward, NodeClass.Variable, new NodeId[] { ReferenceTypeIds.HasComponent }, false);

			connectorNameReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "ConnectorName");
			connectorNameComboBox.Text = session.ReadValue((NodeId)connectorNameReference.NodeId).Value?.ToString();

			autoStartReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "AutoStart");
			autoStartCheckBox.Checked = (bool)session.ReadValue((NodeId)autoStartReference.NodeId).Value;

			bindingItemsReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "BindingItems");

			refreshBindingItems();

			ReferenceDescription currentStateReference = childReferences.FirstOrDefault(r => r.DisplayName.ToString() == "CurrentState");

			string currentState = session.ReadValue((NodeId)currentStateReference.NodeId).ToString();

			if (currentState != "Ready")
			{
				editingIsNotAllowedLabel.Text = $"Program can't be edited because current state is '{currentState}'";
				editingIsNotAllowedLabel.Visible = true;
				Enabled = false;
			}
			else
			{
				editingIsNotAllowedLabel.Visible = false;
				Enabled = true;
			}
		}

		private ReferenceDescription[] getConnectorReferences(NodeId connectorSetNodeId)
		{
			List<ReferenceDescription> result = new List<ReferenceDescription>();

			ReferenceDescriptionCollection connectorReferences =
				OpcUaNavigationHelper.Browse(session, connectorSetNodeId, BrowseDirection.Forward, NodeClass.Object, new NodeId[] { ReferenceTypeIds.HasComponent });

			foreach (ReferenceDescription connectorReference in connectorReferences)
			{
				ReferenceDescription connectorTypeReference =
					OpcUaNavigationHelper.Browse(session, (NodeId)connectorReference.NodeId, BrowseDirection.Forward, NodeClass.ObjectType, new NodeId[] { ReferenceTypeIds.HasTypeDefinition }).FirstOrDefault();

				if (connectorTypeReference.DisplayName == "S7TcpIpConnectorType")
					result.Add(connectorReference);
			}

			return result.ToArray();
		}

		private ReferenceDescription[] getConverterReferences()
		{
			ReferenceDescription dataConverterSetReference =
				OpcUaNavigationHelper.Browse(session, ObjectIds.ObjectsFolder, BrowseDirection.Forward, NodeClass.Object, new NodeId[] { ReferenceTypeIds.Organizes }, false, childFilter: (ch) => ch.DisplayName == "DataConverterSet").FirstOrDefault();

			if (dataConverterSetReference == null)
				return new ReferenceDescription[0];

			return OpcUaNavigationHelper.Browse(session, ExpandedNodeId.ToNodeId(dataConverterSetReference.NodeId, session.NamespaceUris), BrowseDirection.Forward, NodeClass.Object, new NodeId[] { ReferenceTypeIds.Organizes }, false).ToArray();
		}

		private ParameterReferenceDescription[] getParameterReferences(NodeId parameterSetNodeId)
		{
			List<ParameterReferenceDescription> getReferences(NodeId rootNodeId)
			{
				List<ParameterReferenceDescription> result = new List<ParameterReferenceDescription>();

				ReferenceDescriptionCollection references =
					OpcUaNavigationHelper.Browse(session, rootNodeId, BrowseDirection.Forward, NodeClass.Variable, new NodeId[] { ReferenceTypeIds.Aggregates, ReferenceTypeIds.Organizes });

				foreach (ReferenceDescription refrence in references)
				{
					NodeId typeNodeId = (NodeId)refrence.TypeDefinition;

					List<ParameterReferenceDescription> children = new List<ParameterReferenceDescription>();

					if (typeNodeId != VariableTypeIds.BaseVariableType
						&& typeNodeId != VariableTypeIds.BaseDataVariableType
						&& typeNodeId != VariableTypeIds.PropertyType
					)
						children.AddRange(getReferences((NodeId)refrence.NodeId));

					result.Add(new ParameterReferenceDescription(refrence, children.ToArray()));
				}

				return result;
			}

			return getReferences(parameterSetNodeId).ToArray();
		}

		private void connectorNameComboBox_TextChanged(object sender, System.EventArgs e)
		{
			if (connectorNameReference == null)
				return;

			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)connectorNameReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = connectorNameComboBox.Text };

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);
		}

		private void autoStartCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (autoStartReference == null)
				return;

			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)autoStartReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue() { Value = autoStartCheckBox.Checked };

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);
		}

		private void refreshBindingItems()
		{
			List<int> selectedRowsIndicies = new List<int>();

			if (dataGridView.SelectedRows.Count > 0)
				foreach (DataGridViewRow row in dataGridView.SelectedRows)
					selectedRowsIndicies.Add(row.Index);
			
			object bindingItemsValue = session.ReadValue((NodeId)bindingItemsReference.NodeId).WrappedValue;

			if (bindingSource.Count > 0)
				bindingSource.Clear();

			ExtensionObject[] encodedValues = (ExtensionObject[])((Variant)bindingItemsValue).Value;

			bindingSettingsList.Clear();

			foreach (object item in encodedValues.Select(eo => eo.Body))
			{
				PropertyInfo[] properties = item.GetType().GetProperties();

				S7TcpIpBindingSettings bindingSettings = new S7TcpIpBindingSettings();

				PropertyInfo modeProperty = properties.FirstOrDefault(p => p.Name == "Mode");
				if (modeProperty != null)
					bindingSettings.Mode = Convert.ToInt32(modeProperty.GetValue(item));

				PropertyInfo monitorableProperty = properties.FirstOrDefault(p => p.Name == nameof(bindingSettings.Monitorable));
				if (monitorableProperty != null)
					bindingSettings.Monitorable = Convert.ToBoolean(monitorableProperty.GetValue(item));

				PropertyInfo targetVariableNameProperty = properties.FirstOrDefault(p => p.Name == nameof(bindingSettings.TargetVariablePath));
				if (targetVariableNameProperty != null)
					bindingSettings.TargetVariablePath = Convert.ToString(targetVariableNameProperty.GetValue(item));

				PropertyInfo converterProperty = properties.FirstOrDefault(p => p.Name == nameof(BindingSettingsBase.Converter));
				if (converterProperty != null)
				{
					NodeId nodeId = converterProperty.GetValue(item) as NodeId;

					bindingSettings.Converter = nodeId;

					ReferenceDescription converterReference = converterReferences.FirstOrDefault(cr => Equals((NodeId)cr.NodeId, nodeId));

					if (converterReference != null)
						bindingSettings.ConverterName = converterReference.DisplayName.ToString();
				}

				PropertyInfo converterParameterProperty = properties.FirstOrDefault(p => p.Name == nameof(bindingSettings.ConverterParameter));
				if (converterParameterProperty != null)
					bindingSettings.ConverterParameter = Convert.ToString(converterParameterProperty.GetValue(item));

				PropertyInfo s7DataTypeProperty = properties.FirstOrDefault(p => p.Name == nameof(bindingSettings.S7DataType));
				if (s7DataTypeProperty != null)
					bindingSettings.S7DataType = Convert.ToString(s7DataTypeProperty.GetValue(item));

				PropertyInfo dbNumberProperty = properties.FirstOrDefault(p => p.Name == nameof(bindingSettings.DBNumber));
				if (dbNumberProperty != null)
					bindingSettings.DBNumber = Convert.ToUInt16(dbNumberProperty.GetValue(item));

				PropertyInfo addressProperty = properties.FirstOrDefault(p => p.Name == nameof(bindingSettings.Address));
				if (addressProperty != null)
					bindingSettings.Address = Convert.ToUInt16(addressProperty.GetValue(item));

				PropertyInfo bitAddressProperty = properties.FirstOrDefault(p => p.Name == nameof(bindingSettings.BitAddress));
				if (bitAddressProperty != null)
					bindingSettings.BitAddress = Convert.ToByte(bitAddressProperty.GetValue(item));

				PropertyInfo samplingIntervalProperty = properties.FirstOrDefault(p => p.Name == nameof(bindingSettings.SamplingInterval));
				if (samplingIntervalProperty != null)
					bindingSettings.SamplingInterval = Convert.ToDouble(samplingIntervalProperty.GetValue(item));

				bindingSettingsList.Add(bindingSettings);
			}

			foreach (S7TcpIpBindingSettings settings in bindingSettingsList)
				bindingSource.Add(settings);

			if (selectedRowsIndicies.Count > 0 && dataGridView.Rows.Count > 0)
			{
				dataGridView.Rows[0].Selected = false;

				foreach (int rowIndex in selectedRowsIndicies)
					if (rowIndex < dataGridView.Rows.Count)
						dataGridView.Rows[rowIndex].Selected = true;
			}
		}

		private void saveBindingSettingsList()
		{
			ExtensionObject[] extensionObjects = new ExtensionObject[bindingSettingsList.Count];

			for (int i = 0; i < bindingSettingsList.Count; i++)
			{
				object settingsInstance = Activator.CreateInstance(session.Factory.GetSystemType(new ExpandedNodeId("S7TcpIpBindingSettings", "http://Gimecs.com/Siemens.S7.Client/")));

				Type settingsInstanceType = settingsInstance.GetType();

				ExtensionObject extensionObject = new ExtensionObject(settingsInstance);

				settingsInstanceType.GetProperty(nameof(BindingSettingsBase.Mode)).SetValue(settingsInstance, bindingSettingsList[i].Mode);
				settingsInstanceType.GetProperty(nameof(BindingSettingsBase.Monitorable)).SetValue(settingsInstance, bindingSettingsList[i].Monitorable);
				settingsInstanceType.GetProperty(nameof(BindingSettingsBase.TargetVariablePath)).SetValue(settingsInstance, bindingSettingsList[i].TargetVariablePath);
				settingsInstanceType.GetProperty(nameof(BindingSettingsBase.Converter)).SetValue(settingsInstance, bindingSettingsList[i].Converter);
				settingsInstanceType.GetProperty(nameof(BindingSettingsBase.ConverterParameter)).SetValue(settingsInstance, bindingSettingsList[i].ConverterParameter);
				settingsInstanceType.GetProperty(nameof(BindingSettingsBase.SamplingInterval)).SetValue(settingsInstance, bindingSettingsList[i].SamplingInterval);
				settingsInstanceType.GetProperty(nameof(S7TcpIpBindingSettings.S7DataType)).SetValue(settingsInstance, bindingSettingsList[i].S7DataType);
				settingsInstanceType.GetProperty(nameof(S7TcpIpBindingSettings.DBNumber)).SetValue(settingsInstance, bindingSettingsList[i].DBNumber);
				settingsInstanceType.GetProperty(nameof(S7TcpIpBindingSettings.Address)).SetValue(settingsInstance, bindingSettingsList[i].Address);
				settingsInstanceType.GetProperty(nameof(S7TcpIpBindingSettings.BitAddress)).SetValue(settingsInstance, bindingSettingsList[i].BitAddress);

				extensionObjects[i] = extensionObject;
			}

			WriteValue writeValue = new WriteValue();

			writeValue.NodeId = (NodeId)bindingItemsReference.NodeId;
			writeValue.AttributeId = Attributes.Value;
			writeValue.Value = new DataValue(new Variant(extensionObjects));

			session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);
		}

		private void bindingSource_ListChanged(object sender, ListChangedEventArgs e)
		{			
			if (!Visible || e.ListChangedType == ListChangedType.Reset)
				return;

			//MessageBox.Show("Need to update");
		}

		private void addButton_Click(object sender, EventArgs e)
		{
			S7TcpIpBindingSettings bindingSettings = new S7TcpIpBindingSettings();

			EditS7TcpIpBindingItemForm bindingItemEditor = new EditS7TcpIpBindingItemForm();

			if (bindingItemEditor.ShowDialog(session, bindingSettings, converterReferences, parameterReferences))
			{
				bindingSettingsList.Add(bindingSettings);

				saveBindingSettingsList();

				refreshBindingItems();
			}
		}

		private void editButton_Click(object sender, EventArgs e)
		{
			if (dataGridView.SelectedRows.Count == 1)
				editBindingSettingsItem(dataGridView.SelectedRows[0].Index);
		}

		private void dataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			editBindingSettingsItem(e.RowIndex);
		}

		private void editBindingSettingsItem(int rowIndex)
		{
			if (rowIndex < 0)
				return;

			EditS7TcpIpBindingItemForm bindingItemEditor = new EditS7TcpIpBindingItemForm();

			if (bindingItemEditor.ShowDialog(session, bindingSettingsList[rowIndex], converterReferences, parameterReferences))
			{
				saveBindingSettingsList();

				refreshBindingItems();
			}
		}

		private void removeButton_Click(object sender, EventArgs e)
		{
			if (dataGridView.SelectedRows.Count == 0)
				return;

			for (int i = dataGridView.SelectedRows.Count - 1; i >= 0; i--)
				bindingSettingsList.RemoveAt(dataGridView.SelectedRows[i].Index);

			saveBindingSettingsList();

			refreshBindingItems();
		}


		private void dataGridView_SelectionChanged(object sender, EventArgs e)
		{
			editButton.Enabled = dataGridView.SelectedRows.Count == 1;
			removeButton.Enabled = dataGridView.SelectedRows.Count > 0;
		}
	}
}
