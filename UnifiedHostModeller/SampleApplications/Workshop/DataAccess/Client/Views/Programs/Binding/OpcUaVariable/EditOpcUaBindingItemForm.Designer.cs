﻿namespace UnifiedHostModeler.Views.Programs.BindOpcUaVariable
{
	partial class EditOpcUaBindingItemForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.modeComboBox = new System.Windows.Forms.ComboBox();
			this.targetParameterNameComboBox = new System.Windows.Forms.ComboBox();
			this.monitorableCheckBox = new System.Windows.Forms.CheckBox();
			this.converterComboBox = new System.Windows.Forms.ComboBox();
			this.sourceVariableNodeIdTextBox = new System.Windows.Forms.TextBox();
			this.cancelButton = new System.Windows.Forms.Button();
			this.saveButton = new System.Windows.Forms.Button();
			this.toolTip = new System.Windows.Forms.ToolTip(this.components);
			this.samplingIntervalTextBox = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.converterParameterTextBox = new System.Windows.Forms.TextBox();
			this.converterParameterLabel = new System.Windows.Forms.Label();
			this.selectNodeButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 17);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(34, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Mode";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 52);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(88, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Target parameter";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 84);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(62, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "Monitorable";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 115);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(53, 13);
			this.label4.TabIndex = 3;
			this.label4.Text = "Converter";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 222);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(108, 13);
			this.label5.TabIndex = 4;
			this.label5.Text = "Source variable node";
			// 
			// modeComboBox
			// 
			this.modeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.modeComboBox.FormattingEnabled = true;
			this.modeComboBox.Items.AddRange(new object[] {
            "TwoWay",
            "OneWay",
            "OneWayToSource"});
			this.modeComboBox.Location = new System.Drawing.Point(135, 14);
			this.modeComboBox.MaxDropDownItems = 3;
			this.modeComboBox.Name = "modeComboBox";
			this.modeComboBox.Size = new System.Drawing.Size(135, 21);
			this.modeComboBox.TabIndex = 0;
			// 
			// targetParameterNameComboBox
			// 
			this.targetParameterNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.targetParameterNameComboBox.FormattingEnabled = true;
			this.targetParameterNameComboBox.Location = new System.Drawing.Point(135, 49);
			this.targetParameterNameComboBox.Name = "targetParameterNameComboBox";
			this.targetParameterNameComboBox.Size = new System.Drawing.Size(371, 21);
			this.targetParameterNameComboBox.TabIndex = 1;
			// 
			// monitorableCheckBox
			// 
			this.monitorableCheckBox.AutoSize = true;
			this.monitorableCheckBox.Location = new System.Drawing.Point(135, 83);
			this.monitorableCheckBox.Name = "monitorableCheckBox";
			this.monitorableCheckBox.Size = new System.Drawing.Size(15, 14);
			this.monitorableCheckBox.TabIndex = 2;
			this.monitorableCheckBox.UseVisualStyleBackColor = true;
			// 
			// converterComboBox
			// 
			this.converterComboBox.FormattingEnabled = true;
			this.converterComboBox.Location = new System.Drawing.Point(135, 112);
			this.converterComboBox.Name = "converterComboBox";
			this.converterComboBox.Size = new System.Drawing.Size(371, 21);
			this.converterComboBox.TabIndex = 3;
			this.converterComboBox.TextChanged += new System.EventHandler(this.converterComboBox_TextChanged);
			// 
			// sourceVariableNodeIdTextBox
			// 
			this.sourceVariableNodeIdTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.sourceVariableNodeIdTextBox.Location = new System.Drawing.Point(135, 219);
			this.sourceVariableNodeIdTextBox.Name = "sourceVariableNodeIdTextBox";
			this.sourceVariableNodeIdTextBox.Size = new System.Drawing.Size(331, 20);
			this.sourceVariableNodeIdTextBox.TabIndex = 7;
			this.toolTip.SetToolTip(this.sourceVariableNodeIdTextBox, "In string format like this: ns=7;s=NodeStringIdentifier");
			// 
			// cancelButton
			// 
			this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(431, 252);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(75, 23);
			this.cancelButton.TabIndex = 102;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.UseVisualStyleBackColor = true;
			// 
			// saveButton
			// 
			this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.saveButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.saveButton.Location = new System.Drawing.Point(349, 252);
			this.saveButton.Name = "saveButton";
			this.saveButton.Size = new System.Drawing.Size(75, 23);
			this.saveButton.TabIndex = 101;
			this.saveButton.Text = "Save";
			this.saveButton.UseVisualStyleBackColor = true;
			// 
			// samplingIntervalTextBox
			// 
			this.samplingIntervalTextBox.Location = new System.Drawing.Point(135, 184);
			this.samplingIntervalTextBox.Name = "samplingIntervalTextBox";
			this.samplingIntervalTextBox.Size = new System.Drawing.Size(135, 20);
			this.samplingIntervalTextBox.TabIndex = 5;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(12, 187);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(101, 13);
			this.label9.TabIndex = 26;
			this.label9.Text = "Sampling interval (s)";
			// 
			// converterParameterTextBox
			// 
			this.converterParameterTextBox.Location = new System.Drawing.Point(135, 149);
			this.converterParameterTextBox.Name = "converterParameterTextBox";
			this.converterParameterTextBox.Size = new System.Drawing.Size(371, 20);
			this.converterParameterTextBox.TabIndex = 4;
			// 
			// converterParameterLabel
			// 
			this.converterParameterLabel.AutoSize = true;
			this.converterParameterLabel.Location = new System.Drawing.Point(12, 152);
			this.converterParameterLabel.Name = "converterParameterLabel";
			this.converterParameterLabel.Size = new System.Drawing.Size(103, 13);
			this.converterParameterLabel.TabIndex = 105;
			this.converterParameterLabel.Text = "Converter parameter";
			// 
			// selectNodeButton
			// 
			this.selectNodeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.selectNodeButton.Location = new System.Drawing.Point(472, 218);
			this.selectNodeButton.Name = "selectNodeButton";
			this.selectNodeButton.Size = new System.Drawing.Size(34, 22);
			this.selectNodeButton.TabIndex = 106;
			this.selectNodeButton.Text = "...";
			this.selectNodeButton.UseVisualStyleBackColor = true;
			this.selectNodeButton.Click += new System.EventHandler(this.selectNodeButton_Click);
			// 
			// EditOpcUaBindingItemForm
			// 
			this.AcceptButton = this.saveButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.cancelButton;
			this.ClientSize = new System.Drawing.Size(518, 285);
			this.Controls.Add(this.selectNodeButton);
			this.Controls.Add(this.converterParameterTextBox);
			this.Controls.Add(this.converterParameterLabel);
			this.Controls.Add(this.samplingIntervalTextBox);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.saveButton);
			this.Controls.Add(this.sourceVariableNodeIdTextBox);
			this.Controls.Add(this.converterComboBox);
			this.Controls.Add(this.monitorableCheckBox);
			this.Controls.Add(this.targetParameterNameComboBox);
			this.Controls.Add(this.modeComboBox);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "EditOpcUaBindingItemForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Edit OPC-UA binding settings item";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.editOpcUaBindingItemForm_FormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox modeComboBox;
		private System.Windows.Forms.ComboBox targetParameterNameComboBox;
		private System.Windows.Forms.CheckBox monitorableCheckBox;
		private System.Windows.Forms.ComboBox converterComboBox;
		private System.Windows.Forms.TextBox sourceVariableNodeIdTextBox;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button saveButton;
		private System.Windows.Forms.ToolTip toolTip;
		private System.Windows.Forms.TextBox samplingIntervalTextBox;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox converterParameterTextBox;
		private System.Windows.Forms.Label converterParameterLabel;
		private System.Windows.Forms.Button selectNodeButton;
	}
}