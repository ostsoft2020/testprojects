﻿namespace UnifiedHostModeler.Views.Programs.BindOpcUaVariable
{
	partial class EditS7TcpIpBindingItemForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.modeComboBox = new System.Windows.Forms.ComboBox();
			this.targetVariablePathComboBox = new System.Windows.Forms.ComboBox();
			this.monitorableCheckBox = new System.Windows.Forms.CheckBox();
			this.converterComboBox = new System.Windows.Forms.ComboBox();
			this.cancelButton = new System.Windows.Forms.Button();
			this.saveButton = new System.Windows.Forms.Button();
			this.toolTip = new System.Windows.Forms.ToolTip(this.components);
			this.s7DataTypeComboBox = new System.Windows.Forms.ComboBox();
			this.label6 = new System.Windows.Forms.Label();
			this.dbNumberTextBox = new System.Windows.Forms.TextBox();
			this.addressTextBox = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.bitAddressTextBox = new System.Windows.Forms.TextBox();
			this.bitAddressLabel = new System.Windows.Forms.Label();
			this.samplingIntervalTextBox = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.converterParameterTextBox = new System.Windows.Forms.TextBox();
			this.converterParameterLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 17);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(34, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Mode";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 52);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(88, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Target parameter";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 84);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(62, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "Monitorable";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 115);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(53, 13);
			this.label4.TabIndex = 3;
			this.label4.Text = "Converter";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 221);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(67, 13);
			this.label5.TabIndex = 4;
			this.label5.Text = "S7 data type";
			// 
			// modeComboBox
			// 
			this.modeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.modeComboBox.FormattingEnabled = true;
			this.modeComboBox.Items.AddRange(new object[] {
            "TwoWay",
            "OneWay",
            "OneWayToSource"});
			this.modeComboBox.Location = new System.Drawing.Point(133, 14);
			this.modeComboBox.MaxDropDownItems = 3;
			this.modeComboBox.Name = "modeComboBox";
			this.modeComboBox.Size = new System.Drawing.Size(158, 21);
			this.modeComboBox.TabIndex = 1;
			// 
			// targetVariablePathComboBox
			// 
			this.targetVariablePathComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.targetVariablePathComboBox.FormattingEnabled = true;
			this.targetVariablePathComboBox.Location = new System.Drawing.Point(133, 49);
			this.targetVariablePathComboBox.Name = "targetVariablePathComboBox";
			this.targetVariablePathComboBox.Size = new System.Drawing.Size(333, 21);
			this.targetVariablePathComboBox.TabIndex = 2;
			// 
			// monitorableCheckBox
			// 
			this.monitorableCheckBox.AutoSize = true;
			this.monitorableCheckBox.Location = new System.Drawing.Point(133, 84);
			this.monitorableCheckBox.Name = "monitorableCheckBox";
			this.monitorableCheckBox.Size = new System.Drawing.Size(15, 14);
			this.monitorableCheckBox.TabIndex = 3;
			this.monitorableCheckBox.UseVisualStyleBackColor = true;
			// 
			// converterComboBox
			// 
			this.converterComboBox.FormattingEnabled = true;
			this.converterComboBox.Location = new System.Drawing.Point(133, 112);
			this.converterComboBox.Name = "converterComboBox";
			this.converterComboBox.Size = new System.Drawing.Size(333, 21);
			this.converterComboBox.TabIndex = 4;
			this.converterComboBox.TextChanged += new System.EventHandler(this.converterComboBox_TextChanged);
			// 
			// cancelButton
			// 
			this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(391, 354);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(75, 23);
			this.cancelButton.TabIndex = 101;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.UseVisualStyleBackColor = true;
			// 
			// saveButton
			// 
			this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.saveButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.saveButton.Location = new System.Drawing.Point(309, 354);
			this.saveButton.Name = "saveButton";
			this.saveButton.Size = new System.Drawing.Size(75, 23);
			this.saveButton.TabIndex = 100;
			this.saveButton.Text = "Save";
			this.saveButton.UseVisualStyleBackColor = true;
			// 
			// s7DataTypeComboBox
			// 
			this.s7DataTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.s7DataTypeComboBox.FormattingEnabled = true;
			this.s7DataTypeComboBox.Location = new System.Drawing.Point(133, 218);
			this.s7DataTypeComboBox.Name = "s7DataTypeComboBox";
			this.s7DataTypeComboBox.Size = new System.Drawing.Size(158, 21);
			this.s7DataTypeComboBox.TabIndex = 8;
			this.s7DataTypeComboBox.TextChanged += new System.EventHandler(this.s7DataTypeComboBox_TextChanged);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(12, 256);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(60, 13);
			this.label6.TabIndex = 15;
			this.label6.Text = "DB number";
			// 
			// dbNumberTextBox
			// 
			this.dbNumberTextBox.Location = new System.Drawing.Point(133, 253);
			this.dbNumberTextBox.Name = "dbNumberTextBox";
			this.dbNumberTextBox.Size = new System.Drawing.Size(158, 20);
			this.dbNumberTextBox.TabIndex = 9;
			// 
			// addressTextBox
			// 
			this.addressTextBox.Location = new System.Drawing.Point(133, 288);
			this.addressTextBox.Name = "addressTextBox";
			this.addressTextBox.Size = new System.Drawing.Size(158, 20);
			this.addressTextBox.TabIndex = 10;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(12, 291);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(45, 13);
			this.label7.TabIndex = 17;
			this.label7.Text = "Address";
			// 
			// bitAddressTextBox
			// 
			this.bitAddressTextBox.Location = new System.Drawing.Point(133, 323);
			this.bitAddressTextBox.Name = "bitAddressTextBox";
			this.bitAddressTextBox.Size = new System.Drawing.Size(158, 20);
			this.bitAddressTextBox.TabIndex = 11;
			// 
			// bitAdddressLabel
			// 
			this.bitAddressLabel.AutoSize = true;
			this.bitAddressLabel.Location = new System.Drawing.Point(12, 326);
			this.bitAddressLabel.Name = "bitAdddressLabel";
			this.bitAddressLabel.Size = new System.Drawing.Size(89, 13);
			this.bitAddressLabel.TabIndex = 19;
			this.bitAddressLabel.Text = "Bit address, [0..7]";
			// 
			// samplingIntervalTextBox
			// 
			this.samplingIntervalTextBox.Location = new System.Drawing.Point(133, 183);
			this.samplingIntervalTextBox.Name = "samplingIntervalTextBox";
			this.samplingIntervalTextBox.Size = new System.Drawing.Size(158, 20);
			this.samplingIntervalTextBox.TabIndex = 6;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(12, 186);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(101, 13);
			this.label9.TabIndex = 21;
			this.label9.Text = "Sampling interval (s)";
			// 
			// converterParameterTextBox
			// 
			this.converterParameterTextBox.Location = new System.Drawing.Point(133, 148);
			this.converterParameterTextBox.Name = "converterParameterTextBox";
			this.converterParameterTextBox.Size = new System.Drawing.Size(333, 20);
			this.converterParameterTextBox.TabIndex = 5;
			// 
			// converterParameterLabel
			// 
			this.converterParameterLabel.AutoSize = true;
			this.converterParameterLabel.Location = new System.Drawing.Point(12, 151);
			this.converterParameterLabel.Name = "converterParameterLabel";
			this.converterParameterLabel.Size = new System.Drawing.Size(103, 13);
			this.converterParameterLabel.TabIndex = 103;
			this.converterParameterLabel.Text = "Converter parameter";
			// 
			// EditS7TcpIpBindingItemForm
			// 
			this.AcceptButton = this.saveButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.cancelButton;
			this.ClientSize = new System.Drawing.Size(478, 387);
			this.Controls.Add(this.converterParameterTextBox);
			this.Controls.Add(this.converterParameterLabel);
			this.Controls.Add(this.samplingIntervalTextBox);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.bitAddressTextBox);
			this.Controls.Add(this.bitAddressLabel);
			this.Controls.Add(this.addressTextBox);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.dbNumberTextBox);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.s7DataTypeComboBox);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.saveButton);
			this.Controls.Add(this.converterComboBox);
			this.Controls.Add(this.monitorableCheckBox);
			this.Controls.Add(this.targetVariablePathComboBox);
			this.Controls.Add(this.modeComboBox);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "EditS7TcpIpBindingItemForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Edit S7 TCP/IP binding settings item";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.editOpcUaBindingItemForm_FormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox modeComboBox;
		private System.Windows.Forms.ComboBox targetVariablePathComboBox;
		private System.Windows.Forms.CheckBox monitorableCheckBox;
		private System.Windows.Forms.ComboBox converterComboBox;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button saveButton;
		private System.Windows.Forms.ToolTip toolTip;
		private System.Windows.Forms.ComboBox s7DataTypeComboBox;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox dbNumberTextBox;
		private System.Windows.Forms.TextBox addressTextBox;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox bitAddressTextBox;
		private System.Windows.Forms.Label bitAddressLabel;
		private System.Windows.Forms.TextBox samplingIntervalTextBox;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox converterParameterTextBox;
		private System.Windows.Forms.Label converterParameterLabel;
	}
}