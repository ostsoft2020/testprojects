﻿using Opc.Ua;
using Opc.Ua.Client;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using UnifiedHostModeler.Helpers;
using UnifiedHostModeler.Models;
using UnifiedStudio;
using UnifiedStudio.Models;

namespace UnifiedHostModeler.Views.Programs.BindOpcUaVariable
{
	public partial class EditS7TcpIpBindingItemForm : Form
	{
		private readonly string[] availableS7DataTypes =
		{
			"Bool",
			"Byte",
			"SByte",
			"Word",
			"Int",
			"DWord",
			"DInt",
			"Real"
		};

		private Session session;
		private S7TcpIpBindingSettings bindingSettings;
		private Dictionary<string, string> converterParameterDescriptions;

		public EditS7TcpIpBindingItemForm()
		{
			InitializeComponent();

			s7DataTypeComboBox.Items.AddRange(availableS7DataTypes);
		}

		public bool ShowDialog(
			Session session, 
			S7TcpIpBindingSettings bindingSettings, 
			ReferenceDescription[] converterReferences,
			ParameterReferenceDescription[] parameterReferences)
		{
			this.session = session;
			this.bindingSettings = bindingSettings;

			modeComboBox.SelectedIndex = 1;

			converterParameterDescriptions = getConverterParameterDescriptions(converterReferences);

			converterComboBox.Items.Clear();
			converterComboBox.Items.AddRange(converterReferences.Select(r => r.DisplayName.ToString()).ToArray());

			targetVariablePathComboBox.Items.Clear();
			targetVariablePathComboBox.Items.AddRange(getParameters("", parameterReferences).ToArray());

			modeComboBox.Text = bindingSettings.ModeName;
			targetVariablePathComboBox.Text = bindingSettings.TargetVariablePath;
			monitorableCheckBox.Checked = bindingSettings.Monitorable;
			converterComboBox.Text = bindingSettings.ConverterName;
			converterParameterTextBox.Text = bindingSettings.ConverterParameter;
			samplingIntervalTextBox.Text = bindingSettings.SamplingInterval.ToString();
			s7DataTypeComboBox.Text = bindingSettings.S7DataType;
			dbNumberTextBox.Text = bindingSettings.DBNumber.ToString();
			addressTextBox.Text = bindingSettings.Address.ToString();
			bitAddressTextBox.Text = bindingSettings.BitAddress.ToString();

			bool result = ShowDialog() == DialogResult.OK;

			bindingSettings.ModeName = modeComboBox.Text;
			bindingSettings.TargetVariablePath = targetVariablePathComboBox.Text;
			bindingSettings.Monitorable = monitorableCheckBox.Checked;
			bindingSettings.Converter = (NodeId) converterReferences.FirstOrDefault(cr => cr.DisplayName.ToString() == converterComboBox.Text)?.NodeId;
			bindingSettings.ConverterName = converterComboBox.Text;
			bindingSettings.ConverterParameter = converterParameterTextBox.Text;
			bindingSettings.SamplingInterval = double.Parse(samplingIntervalTextBox.Text);
			bindingSettings.S7DataType = s7DataTypeComboBox.Text;
			bindingSettings.DBNumber = ushort.Parse(dbNumberTextBox.Text);
			bindingSettings.Address = ushort.Parse(addressTextBox.Text);
			bindingSettings.BitAddress = byte.Parse(bitAddressTextBox.Text);

			return result;		
		}
		
		private IList<string> getParameters(string parentPath, ParameterReferenceDescription[] parameterReferences)
		{
			List<string> result = new List<string>();

			foreach (ParameterReferenceDescription parameterReference in parameterReferences)
			{
				string path = parameterReference.ReferenceDescription.DisplayName.ToString();

				if (!string.IsNullOrEmpty(parentPath))
					path = parentPath + "/" + path;

				result.Add(path);

				if (parameterReference.Children.Length > 0)
					result.AddRange(getParameters(path, parameterReference.Children));
			}

			return result;
		}

		private void editOpcUaBindingItemForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				return;

			if (s7DataTypeComboBox.Text != availableS7DataTypes[0])
				bitAddressTextBox.Text = "0";

			string message = null;

			if (!BindingSettingsBase.ValidateModeName(modeComboBox.Text))
				message = "Invalid mode specified";
			else if (string.IsNullOrWhiteSpace(targetVariablePathComboBox.Text))
				message = "Empty target parameter name specified";
			else if (!availableS7DataTypes.Contains(s7DataTypeComboBox.Text))
				message = "Invalid S7 data type specified";
			else if (!ushort.TryParse(dbNumberTextBox.Text, out _))
				message = "Invalid DB number specified";
			else if (!ushort.TryParse(addressTextBox.Text, out _))
				message = "Invalid address specified";
			else if (!byte.TryParse(bitAddressTextBox.Text, out byte bitAddress))
				message = "Invalid bit address specified";
			else if (bitAddress > 7)
				message = "Bit address is out of range [0..7]";
			else if (!double.TryParse(samplingIntervalTextBox.Text, out _))
				message = "Invalid sampling interval specified";
			else if (!string.IsNullOrWhiteSpace(converterComboBox.Text)) // should be the last check
				try
				{
					NodeId.Parse(converterComboBox.Text);
				}
				catch
				{
					message = "Invalid converter node id specified";
				}

			if (message != null)
			{
				MessageBox.Show(message);
				e.Cancel = true;
			}
		}

		private Dictionary<string, string> getConverterParameterDescriptions(ReferenceDescription[] converterReferences)
		{
			Dictionary<string, string> result = new Dictionary<string, string>(converterReferences.Length);

			foreach (ReferenceDescription reference in converterReferences)
			{
				string converterName = reference.DisplayName.ToString();

				ReferenceDescription parameterDescriptionReference =
					OpcUaNavigationHelper.Browse(
						session,
						(NodeId)reference.NodeId,
						BrowseDirection.Forward,
						NodeClass.Variable,
						new NodeId[] { ReferenceTypeIds.HasProperty },
						true,
						BrowseResultMask.All,
						(rd) => rd.DisplayName.ToString() == "ConverterParameterDescription"
					).FirstOrDefault();

				string converterParameterDescription = null;

				if (parameterDescriptionReference.NodeId != null)
					converterParameterDescription = session.ReadValue((NodeId)parameterDescriptionReference.NodeId).ToString();

				result[converterName] = converterParameterDescription;
			}

			return result;
		}

		private void converterComboBox_TextChanged(object sender, System.EventArgs e)
		{
			converterParameterDescriptions.TryGetValue(converterComboBox.Text, out string parameterDescription);

			if (string.IsNullOrWhiteSpace(parameterDescription))
				converterParameterLabel.Text = "Converter parameter";
			else
				converterParameterLabel.Text = parameterDescription;
		}

		private void s7DataTypeComboBox_TextChanged(object sender, System.EventArgs e)
		{
			bitAddressLabel.Enabled = s7DataTypeComboBox.Text == availableS7DataTypes[0];
			bitAddressTextBox.Enabled = bitAddressLabel.Enabled;
		}
	}
}
