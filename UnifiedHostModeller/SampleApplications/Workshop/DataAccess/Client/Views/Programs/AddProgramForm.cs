﻿using Opc.Ua;
using Opc.Ua.Client;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Editors
{
	public partial class AddProgramForm : Form
	{
		private readonly Dictionary<string, NodeId> subTypes = new Dictionary<string, NodeId>();

		public AddProgramForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(Session session, out (string Name, string Description, NodeId Type) typeDescription)
		{
			subTypes.Clear();

			fillSubTypes(session, Constants.UnifiedProgramTypeNodeId);

			typeComboBox.Items.Clear();
			typeComboBox.Items.AddRange(subTypes.Keys.ToArray());

			if (ShowDialog() == DialogResult.OK)
			{
				typeDescription = (nameTextBox.Text, descriptionTextBox.Text, subTypes[typeComboBox.Text]);
				return true;
			}
			else
			{
				typeDescription = ("", "", null);
				return false;
			}
		}

		private void fillSubTypes(Session session, NodeId parentType)
		{
			ReferenceDescriptionCollection subTypeReferences =
				OpcUaNavigationHelper.Browse(session, parentType, BrowseDirection.Forward, NodeClass.ObjectType, new NodeId[] { ReferenceTypes.HasSubtype });

			foreach (ReferenceDescription item in subTypeReferences)
			{
				ObjectTypeNode typeNode = session.NodeCache.Find(item.NodeId) as ObjectTypeNode;

				if (typeNode == null)
					continue;

				if (!typeNode.IsAbstract)
					subTypes.Add(item.DisplayName.ToString().Replace("Type", ""), (NodeId)item.NodeId);

				fillSubTypes(session, typeNode.NodeId);
			}
		}

		private void formClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				return;

			if (!OpcUaHelper.ValidateName(nameTextBox.Text, "Name", out string errorMessage))
			{
				MessageBox.Show(errorMessage);
				e.Cancel = true;
			}
			else if (!subTypes.ContainsKey(typeComboBox.Text))
			{
				MessageBox.Show("Invalid type specified");
				e.Cancel = true;
			}
		}
	}
}
