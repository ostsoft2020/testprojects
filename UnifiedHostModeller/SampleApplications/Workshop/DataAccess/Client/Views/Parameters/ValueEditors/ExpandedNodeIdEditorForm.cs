﻿using Opc.Ua;
using System.Windows.Forms;

namespace UnifiedHostModeler.Editors.Values
{
	public partial class ExpandedNodeIdEditorForm : Form
	{
		public ExpandedNodeIdEditorForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(string parameterName, ExpandedNodeId defaultValue, out ExpandedNodeId value)
		{
			label.Text = parameterName + " (format is 'svr=<server index>;nsu=<namespace uri>;<identifier type (n/s/g/o)>=<identifier>')";
			textBox.Text = defaultValue?.ToString();

			if (ShowDialog() == DialogResult.OK)
			{
				value = NodeId.Parse(textBox.Text);
				return true;
			}
			else
			{
				value = new NodeId();
				return false;
			}
		}

		private void CommonEditorForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult == DialogResult.OK)
				try
				{
					ExpandedNodeId.Parse(textBox.Text);
				}
				catch
				{
					MessageBox.Show("Invalid value");
					e.Cancel = true;
				}
		}
	}
}
