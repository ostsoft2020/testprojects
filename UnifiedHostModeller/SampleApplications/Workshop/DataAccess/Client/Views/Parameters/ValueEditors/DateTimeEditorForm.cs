﻿using System;
using System.Windows.Forms;

namespace UnifiedHostModeler.Editors.Values
{
	public partial class DateTimeEditorForm : Form
	{
		public DateTimeEditorForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(string parameterName, DateTime defaultValue, out DateTime value)
		{
			label.Text = parameterName;
			textBox.Text = 
				(defaultValue.Kind == DateTimeKind.Utc)
					? defaultValue.ToString("yyyy-MM-dd HH:mm:ss.FFzz")
					: defaultValue.ToString("yyyy-MM-dd HH:mm:ss.FF");

			if (ShowDialog() == DialogResult.OK)
			{
				value = DateTime.Parse(textBox.Text);
				return true;
			}
			else
			{
				value = DateTime.MinValue;
				return false;
			}
		}

		private void DateTimeEditorForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult == DialogResult.OK && !DateTime.TryParse(textBox.Text, out _))
			{
				MessageBox.Show("Invalid date/time format");
				e.Cancel = true;
			}
		}
	}
}
