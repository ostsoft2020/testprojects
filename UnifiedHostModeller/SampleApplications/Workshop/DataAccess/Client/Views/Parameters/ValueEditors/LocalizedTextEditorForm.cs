﻿using Opc.Ua;
using System.Windows.Forms;

namespace UnifiedHostModeler.Editors.Values
{
	public partial class LocalizedTextEditorForm : Form
	{
		public LocalizedTextEditorForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(string parameterName, LocalizedText defaultValue, out LocalizedText value)
		{
			label.Text = parameterName + " (format is  'locale name'_'text')";
			localeTextBox.Text = defaultValue?.Locale;
			textBox.Text = defaultValue?.Text;

			if (ShowDialog() == DialogResult.OK)
			{
				value = new LocalizedText(localeTextBox.Text, textBox.Text);
				return true;
			}
			else
			{
				value = new LocalizedText("", "");
				return false;
			}
		}
	}
}