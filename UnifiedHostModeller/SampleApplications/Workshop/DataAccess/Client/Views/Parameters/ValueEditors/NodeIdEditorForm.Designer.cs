﻿namespace UnifiedHostModeler.Editors.Values
{
	partial class NodeIdEditorForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.okButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.label = new System.Windows.Forms.Label();
			this.textBox = new System.Windows.Forms.TextBox();
			this.selectNodeButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// okButton
			// 
			this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.okButton.Location = new System.Drawing.Point(355, 74);
			this.okButton.Name = "okButton";
			this.okButton.Size = new System.Drawing.Size(75, 23);
			this.okButton.TabIndex = 2;
			this.okButton.Text = "OK";
			this.okButton.UseVisualStyleBackColor = true;
			// 
			// cancelButton
			// 
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(436, 74);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(75, 23);
			this.cancelButton.TabIndex = 3;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.UseVisualStyleBackColor = true;
			// 
			// label
			// 
			this.label.AutoSize = true;
			this.label.Location = new System.Drawing.Point(16, 19);
			this.label.Name = "label";
			this.label.Size = new System.Drawing.Size(0, 13);
			this.label.TabIndex = 3;
			// 
			// textBox
			// 
			this.textBox.Location = new System.Drawing.Point(19, 39);
			this.textBox.Name = "textBox";
			this.textBox.Size = new System.Drawing.Size(464, 20);
			this.textBox.TabIndex = 0;
			// 
			// selectNodeButton
			// 
			this.selectNodeButton.Location = new System.Drawing.Point(486, 38);
			this.selectNodeButton.Name = "selectNodeButton";
			this.selectNodeButton.Size = new System.Drawing.Size(25, 22);
			this.selectNodeButton.TabIndex = 1;
			this.selectNodeButton.Text = "...";
			this.selectNodeButton.UseVisualStyleBackColor = true;
			this.selectNodeButton.Click += new System.EventHandler(this.selectNodeButton_Click);
			// 
			// NodeIdEditorForm
			// 
			this.AcceptButton = this.okButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.cancelButton;
			this.ClientSize = new System.Drawing.Size(527, 110);
			this.Controls.Add(this.selectNodeButton);
			this.Controls.Add(this.textBox);
			this.Controls.Add(this.label);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.okButton);
			this.Name = "NodeIdEditorForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Set value";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CommonEditorForm_FormClosing);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Button okButton;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Label label;
		private System.Windows.Forms.TextBox textBox;
		private System.Windows.Forms.Button selectNodeButton;
	}
}