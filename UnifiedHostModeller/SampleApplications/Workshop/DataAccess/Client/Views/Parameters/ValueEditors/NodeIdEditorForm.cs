﻿using Opc.Ua;
using Opc.Ua.Client;
using System.Windows.Forms;
using UnifiedHostModeler.Views.Selectors;

namespace UnifiedHostModeler.Editors.Values
{
	public partial class NodeIdEditorForm : Form
	{
		private Session session;

		public NodeIdEditorForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(Session session, string parameterName, NodeId defaultValue, out NodeId value)
		{
			this.session = session;

			label.Text = parameterName + " (format is 'ns=<namespace index>;<identifier type (n/s/g/o)>=<identifier>')";
			textBox.Text = defaultValue?.ToString();

			if (ShowDialog() == DialogResult.OK)
			{
				value = NodeId.Parse(textBox.Text);
				return true;
			}
			else
			{
				value = new NodeId();
				return false;
			}
		}

		private void CommonEditorForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult == DialogResult.OK)
				try
				{
					NodeId.Parse(textBox.Text);
				}
				catch
				{
					MessageBox.Show("Invalid value");
					e.Cancel = true;
				}
		}

		private void selectNodeButton_Click(object sender, System.EventArgs e)
		{
			NodeId rootNodeId = new NodeId("i=84");
			NodeId nodeId = null;

			try
			{
				nodeId = NodeId.Parse(textBox.Text);
			}
			catch
			{
			}

			NodeSelectorForm nodeSelectorForm = new NodeSelectorForm();

			NodeClass nodeClass = NodeClass.Object | NodeClass.Variable | NodeClass.Method | NodeClass.ObjectType | NodeClass.VariableType | NodeClass.ReferenceType | NodeClass.DataType | NodeClass.View;
			NodeId[] referenceTypeIds = new NodeId[] { ReferenceTypeIds.References };

			nodeId = nodeSelectorForm.ShowDialog(session, rootNodeId, nodeId, nodeClass, referenceTypeIds, BrowseResultMask.All);

			textBox.Text = nodeId?.ToString();
		}
	}
}
