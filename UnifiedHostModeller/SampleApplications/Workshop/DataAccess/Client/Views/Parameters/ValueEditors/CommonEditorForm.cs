﻿using System;
using System.Windows.Forms;

namespace UnifiedHostModeler.Editors.Values
{
	public partial class CommonEditorForm : Form
	{
		private Func<string, bool> valueChecker;

		public CommonEditorForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog<T>(string parameterName, T defaultValue, Func<string, bool> valueChecker, out T value)
		{
			label.Text = $"{parameterName} (type is {typeof(T).Name})";
			textBox.Text = defaultValue?.ToString();
			this.valueChecker = valueChecker;

			if (ShowDialog() == DialogResult.OK)
			{
				value = (T) Convert.ChangeType(textBox.Text, typeof(T));
				return true;
			}
			else
			{
				value = default;
				return false;
			}
		}

		private void CommonEditorForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult == DialogResult.OK && valueChecker != null && !valueChecker.Invoke(textBox.Text))
			{
				MessageBox.Show("Invalid value");
				e.Cancel = true;
			}
		}
	}
}
