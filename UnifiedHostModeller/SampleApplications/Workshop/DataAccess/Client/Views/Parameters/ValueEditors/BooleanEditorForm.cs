﻿using System.Windows.Forms;

namespace UnifiedHostModeler.Editors.Values
{
	public partial class BooleanEditorForm : Form
	{
		public BooleanEditorForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(string parameterName, bool defaultValue, out bool value)
		{
			valueCheckBox.Text = parameterName;
			valueCheckBox.Checked = defaultValue;

			if (ShowDialog() == DialogResult.OK)
			{
				value = valueCheckBox.Checked;
				return true;
			}
			else
			{
				value = false;
				return false;
			}
		}
	}
}
