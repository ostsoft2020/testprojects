﻿using System;
using System.Windows.Forms;

namespace UnifiedHostModeler.Editors.Values
{
	public partial class NumberEditorForm : Form
	{
		public NumberEditorForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog<T>(string parameterName, T defaultValue, T minimum, T maximum, out T value)
			where T : struct
		{
			bool checkTypeIsNumber(Type t)
			{
				switch (Type.GetTypeCode(t))
				{
					case TypeCode.Byte:
					case TypeCode.SByte:
					case TypeCode.UInt16:
					case TypeCode.UInt32:
					case TypeCode.UInt64:
					case TypeCode.Int16:
					case TypeCode.Int32:
					case TypeCode.Int64:
					case TypeCode.Decimal:
					case TypeCode.Double:
					case TypeCode.Single:
						return true;

					case TypeCode.Object:
						if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>))
							return checkTypeIsNumber(Nullable.GetUnderlyingType(t));
						else
							return false;

					default:
						return false;
				}
			}

			if (!checkTypeIsNumber(typeof(T)))
				throw new Exception("Generic argument T is of type " + typeof(T).FullName + ". Only numeric type is accepted");

			label.Text = $"{parameterName} (type is {typeof(T).Name})";
			numericUpDown.Minimum = Convert.ToDecimal(minimum);
			numericUpDown.Maximum = Convert.ToDecimal(maximum);
			numericUpDown.Value = Convert.ToDecimal(defaultValue);

			if (ShowDialog() == DialogResult.OK)
			{
				value = (T) Convert.ChangeType(numericUpDown.Value, typeof(T));
				return true;
			}
			else
			{
				value = default;
				return false;
			}
		}
	}
}
