﻿using System;
using System.Windows.Forms;

namespace UnifiedHostModeler.Editors.Values
{
	public partial class DateEditorForm : Form
	{
		public DateEditorForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(string parameterName, DateTime defaultValue, out DateTime value)
		{
			label.Text = parameterName;
			monthCalendar.SetDate(defaultValue);

			if (ShowDialog() == DialogResult.OK)
			{
				value = monthCalendar.SelectionRange.Start;
				return true;
			}
			else
			{
				value = DateTime.MinValue;
				return false;
			}
		}
	}
}
