﻿namespace UnifiedStudio.Controls
{
	partial class ParametersControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.dataGridView = new System.Windows.Forms.DataGridView();
			this.nameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.accessLevelColumt = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.variableTypeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataTypeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.valueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.setValueColumn = new UnifiedStudio.Controls.DataGridViewDisableButtonColumn();
			this.historizingColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// dataGridView
			// 
			this.dataGridView.AllowUserToAddRows = false;
			this.dataGridView.AllowUserToDeleteRows = false;
			this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameColumn,
            this.accessLevelColumt,
            this.variableTypeColumn,
            this.dataTypeColumn,
            this.valueColumn,
            this.setValueColumn,
            this.historizingColumn});
			this.dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridView.Location = new System.Drawing.Point(0, 0);
			this.dataGridView.Name = "dataGridView";
			this.dataGridView.ReadOnly = true;
			this.dataGridView.RowHeadersVisible = false;
			this.dataGridView.Size = new System.Drawing.Size(779, 459);
			this.dataGridView.TabIndex = 0;
			this.dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_CellContentClick);
			// 
			// nameColumn
			// 
			this.nameColumn.DataPropertyName = "Name";
			this.nameColumn.Frozen = true;
			this.nameColumn.HeaderText = "Name";
			this.nameColumn.Name = "nameColumn";
			this.nameColumn.ReadOnly = true;
			this.nameColumn.Width = 128;
			// 
			// accessLevelColumt
			// 
			this.accessLevelColumt.DataPropertyName = "AccessLevel";
			this.accessLevelColumt.HeaderText = "Access";
			this.accessLevelColumt.Name = "accessLevelColumt";
			this.accessLevelColumt.ReadOnly = true;
			this.accessLevelColumt.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.accessLevelColumt.Width = 50;
			// 
			// variableTypeColumn
			// 
			this.variableTypeColumn.DataPropertyName = "VariableType";
			this.variableTypeColumn.HeaderText = "Variable type";
			this.variableTypeColumn.Name = "variableTypeColumn";
			this.variableTypeColumn.ReadOnly = true;
			this.variableTypeColumn.Width = 130;
			// 
			// dataTypeColumn
			// 
			this.dataTypeColumn.DataPropertyName = "DataTypeName";
			this.dataTypeColumn.HeaderText = "Data type";
			this.dataTypeColumn.Name = "dataTypeColumn";
			this.dataTypeColumn.ReadOnly = true;
			this.dataTypeColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.dataTypeColumn.Width = 85;
			// 
			// valueColumn
			// 
			this.valueColumn.DataPropertyName = "ValueAsString";
			this.valueColumn.HeaderText = "Value";
			this.valueColumn.Name = "valueColumn";
			this.valueColumn.ReadOnly = true;
			// 
			// setValueColumn
			// 
			this.setValueColumn.HeaderText = "Set";
			this.setValueColumn.Name = "setValueColumn";
			this.setValueColumn.ReadOnly = true;
			this.setValueColumn.Text = "Set";
			this.setValueColumn.UseColumnTextForButtonValue = true;
			this.setValueColumn.Width = 32;
			// 
			// historizingColumn
			// 
			this.historizingColumn.DataPropertyName = "Historizing";
			this.historizingColumn.HeaderText = "Historizing";
			this.historizingColumn.Name = "historizingColumn";
			this.historizingColumn.ReadOnly = true;
			this.historizingColumn.Width = 60;
			// 
			// ParametersControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.dataGridView);
			this.Name = "ParametersControl";
			this.Size = new System.Drawing.Size(779, 459);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.DataGridView dataGridView;
		private System.Windows.Forms.DataGridViewTextBoxColumn nameColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn accessLevelColumt;
		private System.Windows.Forms.DataGridViewTextBoxColumn variableTypeColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataTypeColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn valueColumn;
		private System.Windows.Forms.DataGridViewCheckBoxColumn historizingColumn;
		private DataGridViewDisableButtonColumn setValueColumn;
	}
}
