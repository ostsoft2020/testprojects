﻿namespace UnifiedStudio.Editors
{
	partial class AddParameterForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.browseNameTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.descriptionTextBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.isArrayCheckBox = new System.Windows.Forms.CheckBox();
			this.label5 = new System.Windows.Forms.Label();
			this.readOnlyCheckBox = new System.Windows.Forms.CheckBox();
			this.cancelButton = new System.Windows.Forms.Button();
			this.saveButton = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.dataTypeComboBox = new System.Windows.Forms.ComboBox();
			this.variableTypeComboBox = new System.Windows.Forms.ComboBox();
			this.displayNameTextBox = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.arraySizeUpDown = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.arraySizeUpDown)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(71, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Browse name";
			// 
			// browseNameTextBox
			// 
			this.browseNameTextBox.Location = new System.Drawing.Point(85, 17);
			this.browseNameTextBox.Name = "browseNameTextBox";
			this.browseNameTextBox.Size = new System.Drawing.Size(158, 20);
			this.browseNameTextBox.TabIndex = 0;
			this.browseNameTextBox.TextChanged += new System.EventHandler(this.browseNameTextBox_TextChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 128);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(68, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "Variable type";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 92);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(60, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Description";
			// 
			// descriptionTextBox
			// 
			this.descriptionTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.descriptionTextBox.Location = new System.Drawing.Point(85, 89);
			this.descriptionTextBox.Name = "descriptionTextBox";
			this.descriptionTextBox.Size = new System.Drawing.Size(434, 20);
			this.descriptionTextBox.TabIndex = 2;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 224);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(31, 13);
			this.label4.TabIndex = 15;
			this.label4.Text = "Array";
			// 
			// isArrayCheckBox
			// 
			this.isArrayCheckBox.AutoSize = true;
			this.isArrayCheckBox.Location = new System.Drawing.Point(85, 224);
			this.isArrayCheckBox.Name = "isArrayCheckBox";
			this.isArrayCheckBox.Size = new System.Drawing.Size(15, 14);
			this.isArrayCheckBox.TabIndex = 6;
			this.isArrayCheckBox.UseVisualStyleBackColor = true;
			this.isArrayCheckBox.CheckedChanged += new System.EventHandler(this.isArrayCheckBox_CheckedChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 196);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(55, 13);
			this.label5.TabIndex = 13;
			this.label5.Text = "Read only";
			// 
			// readOnlyCheckBox
			// 
			this.readOnlyCheckBox.AutoSize = true;
			this.readOnlyCheckBox.Location = new System.Drawing.Point(85, 196);
			this.readOnlyCheckBox.Name = "readOnlyCheckBox";
			this.readOnlyCheckBox.Size = new System.Drawing.Size(15, 14);
			this.readOnlyCheckBox.TabIndex = 5;
			this.readOnlyCheckBox.UseVisualStyleBackColor = true;
			// 
			// cancelButton
			// 
			this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(444, 282);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(75, 23);
			this.cancelButton.TabIndex = 9;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.UseVisualStyleBackColor = true;
			// 
			// saveButton
			// 
			this.saveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.saveButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.saveButton.Location = new System.Drawing.Point(362, 282);
			this.saveButton.Name = "saveButton";
			this.saveButton.Size = new System.Drawing.Size(75, 23);
			this.saveButton.TabIndex = 8;
			this.saveButton.Text = "Save";
			this.saveButton.UseVisualStyleBackColor = true;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(12, 164);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(53, 13);
			this.label6.TabIndex = 17;
			this.label6.Text = "Data type";
			// 
			// dataTypeComboBox
			// 
			this.dataTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.dataTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dataTypeComboBox.FormattingEnabled = true;
			this.dataTypeComboBox.Location = new System.Drawing.Point(85, 161);
			this.dataTypeComboBox.Name = "dataTypeComboBox";
			this.dataTypeComboBox.Size = new System.Drawing.Size(434, 21);
			this.dataTypeComboBox.TabIndex = 4;
			// 
			// variableTypeComboBox
			// 
			this.variableTypeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.variableTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.variableTypeComboBox.FormattingEnabled = true;
			this.variableTypeComboBox.Location = new System.Drawing.Point(85, 125);
			this.variableTypeComboBox.Name = "variableTypeComboBox";
			this.variableTypeComboBox.Size = new System.Drawing.Size(434, 21);
			this.variableTypeComboBox.TabIndex = 3;
			this.variableTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.variableTypeComboBox_SelectedIndexChanged);
			// 
			// displayNameTextBox
			// 
			this.displayNameTextBox.Location = new System.Drawing.Point(85, 53);
			this.displayNameTextBox.Name = "displayNameTextBox";
			this.displayNameTextBox.Size = new System.Drawing.Size(158, 20);
			this.displayNameTextBox.TabIndex = 1;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(12, 56);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(70, 13);
			this.label7.TabIndex = 19;
			this.label7.Text = "Display name";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(12, 253);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(52, 13);
			this.label8.TabIndex = 21;
			this.label8.Text = "Array size";
			// 
			// arraySizeUpDown
			// 
			this.arraySizeUpDown.Location = new System.Drawing.Point(85, 251);
			this.arraySizeUpDown.Name = "arraySizeUpDown";
			this.arraySizeUpDown.Size = new System.Drawing.Size(81, 20);
			this.arraySizeUpDown.TabIndex = 7;
			// 
			// AddParameterForm
			// 
			this.AcceptButton = this.saveButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.CancelButton = this.cancelButton;
			this.ClientSize = new System.Drawing.Size(534, 317);
			this.Controls.Add(this.arraySizeUpDown);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.displayNameTextBox);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.variableTypeComboBox);
			this.Controls.Add(this.dataTypeComboBox);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.isArrayCheckBox);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.readOnlyCheckBox);
			this.Controls.Add(this.cancelButton);
			this.Controls.Add(this.saveButton);
			this.Controls.Add(this.descriptionTextBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.browseNameTextBox);
			this.Controls.Add(this.label1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "AddParameterForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Add parameter";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.formClosing);
			((System.ComponentModel.ISupportInitialize)(this.arraySizeUpDown)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox browseNameTextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox descriptionTextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.CheckBox isArrayCheckBox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.CheckBox readOnlyCheckBox;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button saveButton;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ComboBox dataTypeComboBox;
		private System.Windows.Forms.ComboBox variableTypeComboBox;
		private System.Windows.Forms.TextBox displayNameTextBox;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.NumericUpDown arraySizeUpDown;
	}
}