﻿using System;
using System.Windows.Forms;
using System.Linq;
using UnifiedHostModeler;
using Opc.Ua;
using System.Collections.Generic;
using UnifiedHostModeler.Helpers;

namespace UnifiedStudio.Editors
{
	public partial class AddParameterForm : Form
	{
		private string prevBrowseName = "";
		private readonly Dictionary<string, NodeId> variableTypes = new Dictionary<string, NodeId>();
		private readonly Dictionary<string, NodeId> dataTypes = new Dictionary<string, NodeId>();

		public AddParameterForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(out (string BrowseName, string DisplayName, string Description, NodeId DataType, bool IsArray, uint ArraySize, bool ReadOnly, NodeId VariableType) parameterDescriptor)
		{
			variableTypes.Clear();
			foreach (var vt in TypeTreeHelper.VariableTypeList)
				variableTypes.Add(vt.Value.Name, vt.Key);

			variableTypeComboBox.Items.Clear();
			variableTypeComboBox.Items.AddRange(variableTypes.Keys.OrderBy(vt => vt).ToArray());
			if (variableTypes.ContainsKey("BaseDataVariableType"))
				variableTypeComboBox.SelectedIndex = variableTypeComboBox.Items.IndexOf("BaseDataVariableType");
			else
				variableTypeComboBox.SelectedIndex = 0;

			dataTypes.Clear();
			foreach (var dt in TypeTreeHelper.DataTypeList)
				dataTypes.Add(dt.Value.Name, dt.Key);

			if (ShowDialog() == DialogResult.OK)
			{
				variableTypes.TryGetValue(variableTypeComboBox.Text, out NodeId variableTypeNodeId);
				dataTypes.TryGetValue(dataTypeComboBox.Text, out NodeId dataTypeNodeId);

				parameterDescriptor = (browseNameTextBox.Text, displayNameTextBox.Text, descriptionTextBox.Text, dataTypeNodeId, isArrayCheckBox.Checked, (uint)arraySizeUpDown.Value, readOnlyCheckBox.Checked, variableTypeNodeId);

				return true;
			}
			else
			{
				parameterDescriptor = ("", "", "", null, false, 0, false, null);
				return false;
			}
		}

		private void formClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				return;

			if (!OpcUaHelper.ValidateName(browseNameTextBox.Text, "Name", out string errorMessage))
			{
				MessageBox.Show(errorMessage);
				e.Cancel = true;
			}
			else if (string.IsNullOrWhiteSpace(dataTypeComboBox.Text))
			{
				MessageBox.Show("Correct data type should be specified");
				e.Cancel = true;
			}
		}

		private void variableTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			string selectedDataType = dataTypeComboBox.Text;
			dataTypeComboBox.Items.Clear();

			if (TypeTreeHelper.SupportedVariableTypes.TryGetValue(variableTypeComboBox.Text, out var variableDescriptor))
			{
				dataTypeComboBox.Items.AddRange(variableDescriptor.DataTypes.OrderBy(dt => dt).ToArray());

				if (!string.IsNullOrWhiteSpace(selectedDataType) && variableDescriptor.DataTypes.Contains(selectedDataType))
					dataTypeComboBox.SelectedItem = selectedDataType;

				if (string.IsNullOrWhiteSpace(dataTypeComboBox.Text) && variableDescriptor.DataTypes.Length > 0)
					dataTypeComboBox.SelectedItem = variableDescriptor.DataTypes[0];
			}
		}

		private void isArrayCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			arraySizeUpDown.Enabled = isArrayCheckBox.Checked;
		}

		private void browseNameTextBox_TextChanged(object sender, EventArgs e)
		{
			if (prevBrowseName == displayNameTextBox.Text)
				displayNameTextBox.Text = browseNameTextBox.Text;

			prevBrowseName = browseNameTextBox.Text;
		}
	}
}
