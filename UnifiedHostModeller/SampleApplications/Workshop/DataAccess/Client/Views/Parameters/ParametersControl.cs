﻿using Opc.Ua;
using Opc.Ua.Client;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using UnifiedHostModeler.Editors.Values;
using UnifiedHostModeler.Helpers;
using UnifiedStudio.Models;

namespace UnifiedStudio.Controls
{
	public partial class ParametersControl : UserControl
	{
        private Session session;
        private NodeManager nodeManager;
        private Action onAfterValueChanged;
        private Subscription subscription;
        private List<MonitoredItem> monitoredItems = new List<MonitoredItem>();

        public BindingList<ParameterDescriptor> ParameterDescriptors = new BindingList<ParameterDescriptor>();

        public ParametersControl()
		{
			InitializeComponent();

            dataGridView.AutoGenerateColumns = false;
        }

        public void DisplayParameters(Session session, NodeManager nodeManager, NodeId parametersNodeId, Action onAfterValueChanged)
		{
            removeSubscription();

            this.session = session;
            this.nodeManager = nodeManager;
            this.onAfterValueChanged = onAfterValueChanged;

            ParameterDescriptors.Clear();

            // fetch property references from the server.
            ReferenceDescriptionCollection references =
				OpcUaNavigationHelper.Browse(session, parametersNodeId, BrowseDirection.Forward, NodeClass.Variable, new NodeId[] { ReferenceTypeIds.Aggregates });

            if (references == null)
                return;

            foreach (ReferenceDescription rd in references.OrderBy(rd => rd.DisplayName.ToString()))
            {
                // ignore external references.
                if (rd.NodeId.IsAbsolute)
                    continue;

                ReferenceDescription reference =
					OpcUaNavigationHelper.Browse(session, (NodeId) rd.NodeId, BrowseDirection.Forward, NodeClass.VariableType, new NodeId[] { ReferenceTypeIds.HasTypeDefinition }, false).FirstOrDefault();

				ReadValueIdCollection nodesToRead = new ReadValueIdCollection
				{
					new ReadValueId { NodeId = (NodeId)rd.NodeId, AttributeId = Attributes.Description },
					new ReadValueId { NodeId = (NodeId)rd.NodeId, AttributeId = Attributes.DataType },
					new ReadValueId { NodeId = (NodeId)rd.NodeId, AttributeId = Attributes.ValueRank },
					new ReadValueId { NodeId = (NodeId)rd.NodeId, AttributeId = Attributes.AccessLevel },
					new ReadValueId { NodeId = (NodeId)rd.NodeId, AttributeId = Attributes.Value },
					new ReadValueId { NodeId = (NodeId)rd.NodeId, AttributeId = Attributes.Historizing }
				};

				// read all values.
				session.Read(null, 0, TimestampsToReturn.Neither, nodesToRead, out DataValueCollection results, out _);

                ParameterDescriptor parameterDescriptor = new ParameterDescriptor
                {
                    Name = rd.DisplayName.ToString(),
                    NodeId = (NodeId)rd.NodeId,
                    VariableType = reference?.DisplayName.ToString() ?? ""
                };

                parameterDescriptor.ValueMonitoredItem = сreateValueMonitoredItem((NodeId)rd.NodeId);
				parameterDescriptor.HistorizingMonitoredItem = сreateHistorizingMonitoredItem((NodeId)rd.NodeId);

				// process attribute value.
				for (int j = 0; j < results.Count; j++)
                {
                    // ignore attributes which are invalid for the node.
                    if (results[j].StatusCode == StatusCodes.BadAttributeIdInvalid)
                        continue;

                    switch (nodesToRead[j].AttributeId)
                    {
                        case Attributes.Description:
                            parameterDescriptor.Description = results[j].Value?.ToString();
                            break;

                        case Attributes.DataType:
                            parameterDescriptor.DataType = (NodeId)results[j].Value;
							parameterDescriptor.DataTypeName = (session.TypeTree as NodeCache).GetDisplayText(parameterDescriptor.DataType);
							if (parameterDescriptor.IsArray == true)
                                parameterDescriptor.DataTypeName += "[]";
							break;

                        case Attributes.ValueRank:
                            parameterDescriptor.IsArray = (int)results[j].Value >= ValueRanks.OneOrMoreDimensions;
                            if (parameterDescriptor.IsArray == true && !string.IsNullOrEmpty(parameterDescriptor.DataTypeName))
								parameterDescriptor.DataTypeName += "[]";
							break;

                        case Attributes.AccessLevel:
                            parameterDescriptor.AccessLevel = "";
                            var al = (byte)results[j].Value;
                            if ((al & AccessLevels.CurrentRead) > 0)
								parameterDescriptor.AccessLevel += "R";
                            if ((al & AccessLevels.CurrentWrite) > 0)
                            {
                                parameterDescriptor.AccessLevel += "W";
                                parameterDescriptor.IsWritable = true;
                            }
							if ((al & AccessLevels.HistoryRead) > 0)
								parameterDescriptor.AccessLevel += "H";
							//if ((al & AccessLevels.HistoryWrite) > 0)
							//	parameterDescriptor.AccessLevel += "Hw";
                            if (parameterDescriptor.AccessLevel == "")
								parameterDescriptor.AccessLevel = "-";
                            break;

                        case Attributes.Value:
                            parameterDescriptor.OriginalValue = results[j].Value;
							parameterDescriptor.ValueAsString = convertValueToString(results[j].Value);
                            break;

                        case Attributes.Historizing:
							parameterDescriptor.Historizing = (bool)results[j].Value;
                            break;
					}
				}

                ParameterDescriptors.Add(parameterDescriptor);
            }

            dataGridView.DataSource = ParameterDescriptors;

			dataGridView.Invalidate();

		}

        private void removeSubscription()
        {
            if (subscription != null && subscription != null && monitoredItems != null)
            {
                try
                {
                    foreach (MonitoredItem monitoredItem in monitoredItems)
                    {
                        monitoredItem.Notification -= monitoredItem_Notification;
                        subscription.RemoveItem(monitoredItem);
                    }

                    session.RemoveSubscription(subscription);
                }
                catch
                {
                }

                subscription.Dispose();
                subscription = null;
            }
        }

        private MonitoredItem сreateValueMonitoredItem(NodeId nodeId)
        {
            if (nodeId == null)
                return null;

            if (subscription == null)
            {
                subscription = new Subscription(session.DefaultSubscription);

                subscription.PublishingEnabled = true;
                subscription.PublishingInterval = 1000;
                subscription.KeepAliveCount = 10;
                subscription.LifetimeCount = 10;
                subscription.MaxNotificationsPerPublish = 1000;
                subscription.Priority = 100;

                session.AddSubscription(subscription);

                subscription.Create();
            }

            // add the new monitored item.
            MonitoredItem monitoredItem = new MonitoredItem(subscription.DefaultItem);

            monitoredItem.StartNodeId = nodeId;
            monitoredItem.AttributeId = Attributes.Value;
            monitoredItem.DisplayName = "ValueSubscription";
            monitoredItem.MonitoringMode = MonitoringMode.Reporting;
            monitoredItem.SamplingInterval = 1000;
            monitoredItem.QueueSize = 0;
            monitoredItem.DiscardOldest = true;

            monitoredItems.Add(monitoredItem);

            monitoredItem.Notification += monitoredItem_Notification;

            subscription.AddItem(monitoredItem);

            subscription.ApplyChanges();

            return monitoredItem;
        }

		private MonitoredItem сreateHistorizingMonitoredItem(NodeId nodeId)
		{
			if (nodeId == null)
				return null;

			if (subscription == null)
			{
				subscription = new Subscription(session.DefaultSubscription);

				subscription.PublishingEnabled = true;
				subscription.PublishingInterval = 1000;
				subscription.KeepAliveCount = 10;
				subscription.LifetimeCount = 10;
				subscription.MaxNotificationsPerPublish = 1000;
				subscription.Priority = 100;

				session.AddSubscription(subscription);

				subscription.Create();
			}

			// add the new monitored item.
			MonitoredItem monitoredItem = new MonitoredItem(subscription.DefaultItem);

			monitoredItem.StartNodeId = nodeId;
			monitoredItem.AttributeId = Attributes.Historizing;
			monitoredItem.DisplayName = "HistorizingSubscription";
			monitoredItem.MonitoringMode = MonitoringMode.Reporting;
			monitoredItem.SamplingInterval = 1000;
			monitoredItem.QueueSize = 0;
			monitoredItem.DiscardOldest = true;

			monitoredItems.Add(monitoredItem);

			monitoredItem.Notification += monitoredItem_Notification;

			subscription.AddItem(monitoredItem);

			subscription.ApplyChanges();

			return monitoredItem;
		}

		private void monitoredItem_Notification(MonitoredItem monitoredItem, MonitoredItemNotificationEventArgs e)
        {
            int i;

            for (i = 0; i < ParameterDescriptors.Count; i++)
            {
                if (ParameterDescriptors[i].ValueMonitoredItem == monitoredItem)
                {
                    var dv = ((MonitoredItemNotification)e.NotificationValue).Value;
                    ParameterDescriptors[i].OriginalValue = dv.Value;
                    ParameterDescriptors[i].ValueAsString = convertValueToString(dv.Value);

                    ParameterDescriptors.ResetItem(i);

                    return;
                }
                else if (ParameterDescriptors[i].HistorizingMonitoredItem == monitoredItem)
                {
                    var dv = ((MonitoredItemNotification)e.NotificationValue).Value;
                    ParameterDescriptors[i].Historizing = (bool)dv.Value;

                    ParameterDescriptors.ResetItem(i);

                    return;
                }
            }
		}

		private string convertValueToString(object value)
        {
            if (value is string)
                return (string)value;
            else if (value is IEnumerable)
                return "[" + string.Join(" ", ((IEnumerable)value).Cast<object>().Select(v => ((v is byte) ? ((byte)v).ToString("X") : v?.ToString())).ToArray()) + "]";
            else if (value is DateTime)
            {
                var dt = (DateTime)value;
                return (dt.Kind == DateTimeKind.Utc) ? dt.ToString("yyyy-MM-dd HH:mm:ss.FF", CultureInfo.InvariantCulture) : dt.ToString("yyyy-MM-dd HH:mm:ss.FFzz");
			}
            else
                return value?.ToString();
        }

        private void dataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            try
            {
                if (e.ColumnIndex == setValueColumn.Index)
                {
                    if (!ParameterDescriptors[e.RowIndex].IsWritable)
                        return;

                    if (ParameterDescriptors[e.RowIndex].IsArray == true)
                        throw new Exception("Array editing is not supported");

                    object value = null;

                    var dt = ParameterDescriptors[e.RowIndex].DataType;

                    if (session.TypeTree.IsTypeOf(dt, DataTypeIds.Boolean))
                    {
                        if (new BooleanEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name, Convert.ToBoolean(ParameterDescriptors[e.RowIndex].OriginalValue ?? false), out bool newBoolValue))
                            value = newBoolValue;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.SByte))
                    {
                        if (new NumberEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name, Convert.ToSByte(ParameterDescriptors[e.RowIndex].OriginalValue ?? 0), sbyte.MinValue, sbyte.MaxValue, out sbyte newSByteValue))
                            value = newSByteValue;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.Byte))
                    {
                        if (new NumberEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name, Convert.ToByte(ParameterDescriptors[e.RowIndex].OriginalValue ?? 0), byte.MinValue, byte.MaxValue, out byte newByteValue))
                            value = newByteValue;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.Int16))
                    {
                        if (new NumberEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name, Convert.ToInt16(ParameterDescriptors[e.RowIndex].OriginalValue ?? 0), short.MinValue, short.MaxValue, out short newInt16Value))
                            value = newInt16Value;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.UInt16))
                    {
                        if (new NumberEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name, Convert.ToUInt16(ParameterDescriptors[e.RowIndex].OriginalValue ?? 0), ushort.MinValue, ushort.MaxValue, out ushort newUInt16Value))
                            value = newUInt16Value;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.Int32))
                    {
                        if (new NumberEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name, Convert.ToInt32(ParameterDescriptors[e.RowIndex].OriginalValue ?? 0), int.MinValue, int.MaxValue, out int newInt32Value))
                            value = newInt32Value;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.UInt32))
                    {
                        if (new NumberEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name, Convert.ToUInt32(ParameterDescriptors[e.RowIndex].OriginalValue ?? 0), uint.MinValue, uint.MaxValue, out uint newUInt32Value))
                            value = newUInt32Value;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.UInt64))
                    {
                        if (new NumberEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name, Convert.ToInt64(ParameterDescriptors[e.RowIndex].OriginalValue ?? 0), long.MinValue, long.MaxValue, out long newInt64Value))
                            value = newInt64Value;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.Float))
                    {
                        if (new CommonEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name, Convert.ToSingle(ParameterDescriptors[e.RowIndex].OriginalValue ?? 0), (s) => float.TryParse(s, out _), out float newFloatValue))
                            value = newFloatValue;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.Duration))
                    {
                        if (new CommonEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name + ", ms", Convert.ToDouble(ParameterDescriptors[e.RowIndex].OriginalValue ?? 0), (s) => double.TryParse(s, out _), out double newDoubleValue))
                            value = newDoubleValue;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.Double))
                    {
                        if (new CommonEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name, Convert.ToDouble(ParameterDescriptors[e.RowIndex].OriginalValue ?? 0), (s) => double.TryParse(s, out _), out double newDoubleValue))
                            value = newDoubleValue;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.String))
                    {
                        if (new CommonEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name, ParameterDescriptors[e.RowIndex].ValueAsString, null, out string newStringValue))
                            value = newStringValue;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.LocalizedText))
                    {
                        if (new LocalizedTextEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name, (LocalizedText)ParameterDescriptors[e.RowIndex].OriginalValue, out LocalizedText newLoclizedtextValue))
                            value = newLoclizedtextValue;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.NodeId))
                    {
                        if (new NodeIdEditorForm().ShowDialog(session, ParameterDescriptors[e.RowIndex].Name, (NodeId)ParameterDescriptors[e.RowIndex].OriginalValue, out NodeId newNodeIdValue))
                            value = newNodeIdValue;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.ExpandedNodeId))
                    {
                        if (new ExpandedNodeIdEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name, (ExpandedNodeId)ParameterDescriptors[e.RowIndex].OriginalValue, out ExpandedNodeId newExpandedNodeIdValue))
                            value = newExpandedNodeIdValue;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.DateString))
                    {
                        if (new DateEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name, Convert.ToDateTime(ParameterDescriptors[e.RowIndex].OriginalValue ?? DateTime.Today), out DateTime newDateValue))
                            value = newDateValue;
                    }
                    else if (session.TypeTree.IsTypeOf(dt, DataTypeIds.DateTime))
                    {
                        if (new DateTimeEditorForm().ShowDialog(ParameterDescriptors[e.RowIndex].Name, Convert.ToDateTime(ParameterDescriptors[e.RowIndex].OriginalValue ?? DateTime.Now), out DateTime newDateTimeValue))
                            value = newDateTimeValue;
                    }
                    else
                    {
                        MessageBox.Show("Editing is not supported for type");
                        return;
                    }

                    if (value != null)
                    {
                        WriteValue writeValue = new WriteValue();

                        writeValue.NodeId = ParameterDescriptors[e.RowIndex].NodeId;
                        writeValue.AttributeId = Attributes.Value;
                        writeValue.Value = new DataValue() { Value = value };

                        session.Write(null, new WriteValueCollection(new WriteValue[] { writeValue }), out StatusCodeCollection results, out DiagnosticInfoCollection diagnosticInfos);

                        onAfterValueChanged?.Invoke();
                    }
                }
                else if (e.ColumnIndex == historizingColumn.Index)
                    nodeManager.ChangeHistorizing(ParameterDescriptors[e.RowIndex].NodeId, !ParameterDescriptors[e.RowIndex].Historizing);
            }
			catch (Exception ex)
			{
				MessageBox.Show(ex.GetBaseException().Message, "Error");
			}
		}
	}

	public class DataGridViewDisableButtonColumn : DataGridViewButtonColumn
	{
		public DataGridViewDisableButtonColumn()
		{
			CellTemplate = new DataGridViewDisableButtonCell();
		}
	}

	public class DataGridViewDisableButtonCell : DataGridViewButtonCell
	{
		// Override the Clone method so that the Enabled property is copied.
		public override object Clone()
		{
			DataGridViewDisableButtonCell cell =
				(DataGridViewDisableButtonCell)base.Clone();
			return cell;
		}

		// By default, enable the button cell.
		public DataGridViewDisableButtonCell()
		{
		}

		protected override void Paint(Graphics graphics,
			Rectangle clipBounds, Rectangle cellBounds, int rowIndex,
			DataGridViewElementStates elementState, object value,
			object formattedValue, string errorText,
			DataGridViewCellStyle cellStyle,
			DataGridViewAdvancedBorderStyle advancedBorderStyle,
			DataGridViewPaintParts paintParts)
		{
            bool enabled = true;

            ParametersControl parametersControl = DataGridView.Parent as ParametersControl;

            if (parametersControl != null && rowIndex >= 0)
                enabled = parametersControl.ParameterDescriptors[rowIndex].IsWritable;

			// The button cell is disabled, so paint the border,
			// background, and disabled button for the cell.
			if (!enabled)
			{
				// Draw the cell background, if specified.
				if ((paintParts & DataGridViewPaintParts.Background) ==
					DataGridViewPaintParts.Background)
				{
					SolidBrush cellBackground =
						new SolidBrush(cellStyle.BackColor);
					graphics.FillRectangle(cellBackground, cellBounds);
					cellBackground.Dispose();
				}

				// Draw the cell borders, if specified.
				if ((paintParts & DataGridViewPaintParts.Border) ==
					DataGridViewPaintParts.Border)
				{
					PaintBorder(graphics, clipBounds, cellBounds, cellStyle,
						advancedBorderStyle);
				}

				// Calculate the area in which to draw the button.
				Rectangle buttonArea = cellBounds;
				Rectangle buttonAdjustment =
					this.BorderWidths(advancedBorderStyle);
				buttonArea.X += buttonAdjustment.X;
				buttonArea.Y += buttonAdjustment.Y;
				buttonArea.Height -= buttonAdjustment.Height;
				buttonArea.Width -= buttonAdjustment.Width;

				// Draw the disabled button.
				ButtonRenderer.DrawButton(graphics, buttonArea,
					PushButtonState.Disabled);

				TextRenderer.DrawText(graphics,
					"",
					this.DataGridView.Font,
					buttonArea, SystemColors.GrayText);
			}
			else
			{
				// The button cell is enabled, so let the base class
				// handle the painting.
				base.Paint(graphics, clipBounds, cellBounds, rowIndex,
					elementState, value, formattedValue, errorText,
					cellStyle, advancedBorderStyle, paintParts);
			}
		}
	}
}
