﻿using Opc.Ua;

namespace UnifiedStudio
{
    public static class Constants
    {
        public const int IntegrationHostCommonNamespaceIndex = 5;
        public const int IntegrationHostNamespaceIndex = 7;

        public static NodeId UnifiedVariableTypeNodeId = new NodeId("UnifiedVariableType", IntegrationHostCommonNamespaceIndex);
        public static NodeId UnifiedVariableTypeManager = new NodeId("UnifiedVariableTypeManager", IntegrationHostCommonNamespaceIndex);

        public static NodeId UnifiedProgramTypeNodeId = new NodeId("UnifiedProgramType", IntegrationHostCommonNamespaceIndex);
        public static NodeId UnifiedConnectorTypeNodeId = new NodeId("UnifiedConnectorType", IntegrationHostCommonNamespaceIndex);

        public static NodeId FunctionalGroupTypeNodeId = new NodeId(1005, 6);

        public static NodeId AddVariableTypeMethodNodeId = new NodeId(188, IntegrationHostCommonNamespaceIndex);
        public static NodeId RemoveVariableTypeMethodNodeId = new NodeId(191, IntegrationHostCommonNamespaceIndex);
        public static NodeId AddVariablePropertyMethodNodeId = new NodeId(196, IntegrationHostCommonNamespaceIndex);
        public static NodeId AddVariableVariableMethodNodeId = new NodeId(193, IntegrationHostCommonNamespaceIndex);
        public static NodeId RemoveVariableChildMethodNodeId = new NodeId(199, IntegrationHostCommonNamespaceIndex);

        public const string DevicesTreeNodeName = "Devices";
        public const string TypesTreeNodeName = "Types";
        public const string DeviceTypesTreeNodeName = "Devices";
        public const string VarableTypesTreeNodeName = "Variables";

        public const string CreateDeviceInstanceMethodName = "CreateInstance";
        public const string DeleteDeviceInstanceMethodName = "DeleteInstance";

        public const string AddDeviceTypeMethodName = "AddSubType";
        public const string RemoveDeviceTypeMethodName = "RemoveSubType";

		public const string CreateDeviceMethodName = "CreateDevice";
		public const string DeleteDeviceMethodName = "DeleteDevice";

		public const string AddVariableTypeMethodName = "AddVariableType";
        public const string RemoveVariableTypeMethodName = "RemoveVariableType";

        public const string ParametersNodeName = "ParameterSet";
        public const string AddParameterMethodName = "AddParameter";
        public const string RemoveParameterMethodName = "RemoveParameter";

        public const string MethodsNodeName = "MethodSet";
        public const string AddMethodMethodName = "AddMethod";
        public const string RemoveMethodMethodName = "RemoveMethod";

        public const string ProgramsNodeName = "ProgramSet";
        public const string AddProgramMethodName = "AddProgram";
        public const string RemoveProgramMethodName = "RemoveProgram";

        public const string ConnectorsNodeName = "ConnectorSet";
        public const string AddConnectorMethodName = "AddConnector";
        public const string RemoveConnectorMethodName = "RemoveConnector";

        public const string WorkflowsNodeName = "WorkflowSet";
        public const string AddWorkflowMethodName = "AddWorkflow";
        public const string RemoveWorkflowMethodName = "RemoveWorkflow";

        public const string AddFunctionalGroupMethodName = "AddFunctionalGroup";
        public const string RemoveFunctionalGroupMethodName = "RemoveFunctionalGroup";

        public const string AssignParameterMethodName = "AssignParameter";
        public const string DeassignParameterMethodName = "DeassignParameter";

		public const string VariableBindingNodeName = "Binding";
	}
}
