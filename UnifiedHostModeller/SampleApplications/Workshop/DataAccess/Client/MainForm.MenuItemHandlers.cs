﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Opc.Ua;
using Opc.Ua.Client;
using Opc.Ua.Client.Controls;
using UnifiedHostModeler.Helpers;
using UnifiedHostModeler;
using UnifiedStudio.Editors;
using UnifiedStudio.Models;
using UnifiedStudio;
using System.Configuration;


namespace Quickstarts.DataAccessClient
{
	public partial class MainForm
	{
		/// <summary>
		/// Handles the Click event of the Browse_MonitorMI control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		private void browse_MonitorMI_Click(object sender, EventArgs e)
		{
			try
			{
				// check if operation is currently allowed.
				if (session == null || browseNodesTreeView.SelectedNode == null)
					return;

				// can only subscribe to local variables. 
				NodeDescriptor nodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Tag as NodeDescriptor;
				ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

				if (reference.NodeId.IsAbsolute || reference.NodeClass != NodeClass.Variable)
					return;
			}
			catch (Exception exception)
			{
				ClientUtils.HandleException(this.Text, exception);
			}
		}

		private void server_SetUserMI_Click(object sender, EventArgs e)
		{
			try
			{
			}
			catch (Exception exception)
			{
				ClientUtils.HandleException(this.Text, exception);
			}
		}

		private void exitToolStripMenuItem_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Exit the application?", "UA Sample Client", MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
				Application.Exit();
		}

		private void addDeviceTypeStripMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null)
				return;

			AddDeviceTypeForm addTypeForm = new AddDeviceTypeForm();

			if (addTypeForm.ShowDialog(out var typeDescriptor))
			{
				ReferenceDescriptionCollection children =
					OpcUaNavigationHelper.Browse(session, session.GetDeviceMangerNodeId(), BrowseDirection.Forward, NodeClass.Method, new NodeId[] { ReferenceTypeIds.HasComponent }, false);

				ReferenceDescription methodReference = children.First(ch => ch.DisplayName == Constants.AddDeviceTypeMethodName);

				try
				{
					session.Call(
						session.GetDeviceMangerNodeId(),
						(NodeId)methodReference.NodeId,
						typeDescriptor.Name,
						typeDescriptor.Name,
						typeDescriptor.Description,
						typeDescriptor.DeviceClass,
						typeDescriptor.Manufacturer,
						typeDescriptor.ManufacturerUri,
						typeDescriptor.Model,
						typeDescriptor.HardwareRevision,
						typeDescriptor.SoftwareRevision,
						typeDescriptor.DeviceRevision
					);

					OpcUaHelper.SaveConfiguration(session);

					refreshDeviceTypeSubNodes();
				}
				catch (Exception exception)
				{
					ClientUtils.HandleException(Text, exception);
				}
			}
		}

		private void deleteDeviceTypeMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null)
				return;

			if (MessageBox.Show($"Are you shure you want to remove device type '{browseNodesTreeView.SelectedNode.Text}'", "Attention!", MessageBoxButtons.YesNo) == DialogResult.No)
				return;

			ReferenceDescriptionCollection children =
				OpcUaNavigationHelper.Browse(session, session.GetDeviceMangerNodeId(), BrowseDirection.Forward, NodeClass.Method, new NodeId[] { ReferenceTypeIds.HasComponent }, true);

			ReferenceDescription methodReference = children.First(ch => ch.DisplayName == Constants.RemoveDeviceTypeMethodName);

			try
			{
				session.Call(
					session.GetDeviceMangerNodeId(),
					(NodeId)methodReference.NodeId,
					browseNodesTreeView.SelectedNode.Text);

				OpcUaHelper.SaveConfiguration(session);

				refreshDeviceTypeSubNodes();
			}
			catch (Exception exception)
			{
				ClientUtils.HandleException(Text, exception);
			}
		}

		private void addDeviceMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null)
				return;

			AddDeviceForm addDeviceForm = new AddDeviceForm();

			if (addDeviceForm.ShowDialog(session, out var deviceDescriptor))
			{
				try
				{
					nodeManager.CreateDevice(deviceDescriptor);

					refreshDevicesSubNodes();
				}
				catch (Exception exception)
				{
					ClientUtils.HandleException(Text, exception);
				}
			}
		}

		private void deleteDeviceMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null || browseNodesTreeView.SelectedNode.Parent == null)
				return;

			if (MessageBox.Show($"Are you shure you want to delete connector '{browseNodesTreeView.SelectedNode.Text}'", "Attention!", MessageBoxButtons.YesNo) == DialogResult.No)
				return;

			try
			{
				nodeManager.DeleteDevice((browseNodesTreeView.SelectedNode.Tag as NodeDescriptor).NodeId);

				TreeNode devicesNode = browseNodesTreeView.SelectedNode.Parent;
				devicesNode.Nodes.Clear();
				fillNodeChildren(devicesNode);
			}
			catch (Exception exception)
			{
				ClientUtils.HandleException(Text, exception);
			}
		}

		private void createInstanceMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null)
				return;

			NodeDescriptor nodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			if (reference == null)
				return;

			CreateDeviceInstanceForm createInstanceForm = new CreateDeviceInstanceForm();

			if (createInstanceForm.ShowDialog(session, reference, out var instanceDescriptor))
			{
				ReferenceDescriptionCollection children =
					OpcUaNavigationHelper.Browse(session, (NodeId)reference.NodeId, BrowseDirection.Forward, NodeClass.Method, new NodeId[] { ReferenceTypeIds.HasComponent }, false);

				ReferenceDescription methodReference = children.First(ch => ch.DisplayName == Constants.CreateDeviceInstanceMethodName);

				try
				{
					session.Call(
						(NodeId)reference.NodeId,
						(NodeId)methodReference.NodeId,
						instanceDescriptor.Name,
						instanceDescriptor.Name,
						instanceDescriptor.Description,
						instanceDescriptor.Model,
						instanceDescriptor.HardwareRevision,
						instanceDescriptor.SoftwareRevision,
						instanceDescriptor.DeviceRevision,
						instanceDescriptor.ProductCode,
						instanceDescriptor.SerialNumber
					);

					OpcUaHelper.SaveConfiguration(session);

					refreshDevicesSubNodes();
				}
				catch (Exception exception)
				{
					ClientUtils.HandleException(Text, exception);
				}
			}
		}

		private void addWorkflowMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null)
				return;

			NodeDescriptor nodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			if (reference == null)
				return;

			AddWorkflowForm addWorkflowForm = new AddWorkflowForm();

			if (addWorkflowForm.ShowDialog(out (string Name, string Description) typeDescriptor))
			{
				ReferenceDescriptionCollection children =
					OpcUaNavigationHelper.Browse(session, (NodeId)reference.NodeId, BrowseDirection.Forward, NodeClass.Method, new NodeId[] { ReferenceTypeIds.HasComponent }, false);

				ReferenceDescription methodReference = children.First(ch => ch.DisplayName == Constants.AddWorkflowMethodName);

				try
				{
					session.Call(
						(NodeId)reference.NodeId,
						(NodeId)methodReference.NodeId,
						typeDescriptor.Name,
						typeDescriptor.Name,
						typeDescriptor.Description
					);

					OpcUaHelper.SaveConfiguration(session);

					browseNodesTreeView.SelectedNode.Nodes.Clear();
					fillNodeChildren(browseNodesTreeView.SelectedNode);

					workflowsControl.DisplayWorkflows(session, (NodeId)reference.NodeId, nodeReferenceDescriptor.InTypeHierarchy);
				}
				catch (Exception exception)
				{
					ClientUtils.HandleException(Text, exception);
				}
			}
		}

		private void deleteWorkflowMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null || browseNodesTreeView.SelectedNode.Parent == null)
				return;

			if (MessageBox.Show($"Are you shure you want to remove workflow '{browseNodesTreeView.SelectedNode.Text}'", "Attention!", MessageBoxButtons.YesNo) == DialogResult.No)
				return;

			TreeNode workflowSetNode = browseNodesTreeView.SelectedNode.Parent;

			NodeDescriptor nodeReferenceDescriptor = workflowSetNode.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			if (reference == null)
				return;

			ReferenceDescriptionCollection children =
				OpcUaNavigationHelper.Browse(session, (NodeId)reference.NodeId, BrowseDirection.Forward, NodeClass.Method, new NodeId[] { ReferenceTypeIds.HasComponent }, false);

			ReferenceDescription methodReference = children.First(ch => ch.DisplayName == Constants.RemoveWorkflowMethodName);

			try
			{
				session.Call(
					(NodeId)reference.NodeId,
					(NodeId)methodReference.NodeId,
					browseNodesTreeView.SelectedNode.Text);

				OpcUaHelper.SaveConfiguration(session);

				workflowSetNode.Nodes.Clear();
				fillNodeChildren(workflowSetNode);
			}
			catch (Exception exception)
			{
				ClientUtils.HandleException(Text, exception);
			}
		}

		private void addProgramMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null)
				return;

			NodeDescriptor nodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			if (reference == null)
				return;

			AddProgramForm addProgramForm = new AddProgramForm();

			if (addProgramForm.ShowDialog(session, out (string Name, string Description, NodeId Type) typeDescriptor))
			{
				ReferenceDescriptionCollection children =
					OpcUaNavigationHelper.Browse(session, (NodeId)reference.NodeId, BrowseDirection.Forward, NodeClass.Method, new NodeId[] { ReferenceTypeIds.HasComponent }, false);

				ReferenceDescription methodReference = children.First(ch => ch.DisplayName == Constants.AddProgramMethodName);

				try
				{
					session.Call(
						(NodeId)reference.NodeId,
						(NodeId)methodReference.NodeId,
						typeDescriptor.Name,
						typeDescriptor.Name,
						typeDescriptor.Description,
						typeDescriptor.Type
					);

					OpcUaHelper.SaveConfiguration(session);

					browseNodesTreeView.SelectedNode.Nodes.Clear();
					fillNodeChildren(browseNodesTreeView.SelectedNode);

					programsControl.DisplayPrograms(session, (NodeId)reference.NodeId, nodeReferenceDescriptor.InTypeHierarchy);
				}
				catch (Exception exception)
				{
					ClientUtils.HandleException(Text, exception);
				}
			}
		}

		private void deleteProgramMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null || browseNodesTreeView.SelectedNode.Parent == null)
				return;

			if (MessageBox.Show($"Are you shure you want to remove program '{browseNodesTreeView.SelectedNode.Text}'", "Attention!", MessageBoxButtons.YesNo) == DialogResult.No)
				return;

			TreeNode programSetNode = browseNodesTreeView.SelectedNode.Parent;

			NodeDescriptor nodeReferenceDescriptor = programSetNode.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			if (reference == null)
				return;

			ReferenceDescriptionCollection children =
				OpcUaNavigationHelper.Browse(session, (NodeId)reference.NodeId, BrowseDirection.Forward, NodeClass.Method, new NodeId[] { ReferenceTypeIds.HasComponent }, false);

			ReferenceDescription methodReference = children.First(ch => ch.DisplayName == Constants.RemoveProgramMethodName);

			try
			{
				session.Call(
					(NodeId)reference.NodeId,
					(NodeId)methodReference.NodeId,
					browseNodesTreeView.SelectedNode.Text);

				OpcUaHelper.SaveConfiguration(session);

				programSetNode.Nodes.Clear();
				fillNodeChildren(programSetNode);
			}
			catch (Exception exception)
			{
				ClientUtils.HandleException(Text, exception);
			}
		}

		private void addConnectorMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null)
				return;

			NodeDescriptor parentNodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Parent.Tag as NodeDescriptor;
			ReferenceDescription parentReference = parentNodeReferenceDescriptor?.ReferenceDescription;

			NodeDescriptor nodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			if (reference == null)
				return;

			AddConnectorForm addConnectorForm = new AddConnectorForm();

			if (addConnectorForm.ShowDialog(session, out (string BrowseName, string DisplayName, string Description, NodeId Type) connectorDescriptor))
			{
				try
				{
					var deviceRef =
						OpcUaNavigationHelper.GetAncestorReference(session, reference, r => session.TypeTree.IsTypeOf(r.TypeDefinition.ToNodeId(session), session.GetDynamicDeviceTypeNodeId()))
						?? throw new Exception("Can't find antcestor device for node " + reference.NodeId);

					nodeManager.CreateConnector(deviceRef.NodeId.ToNodeId(session), connectorDescriptor);

					browseNodesTreeView.SelectedNode.Nodes.Clear();
					fillNodeChildren(browseNodesTreeView.SelectedNode);

					connectorsControl.DisplayConnectors(session, (NodeId)reference.NodeId, nodeReferenceDescriptor.InTypeHierarchy);
				}
				catch (Exception exception)
				{
					ClientUtils.HandleException(Text, exception);
				}
			}
		}

		private void deleteConnectorMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null || browseNodesTreeView.SelectedNode.Parent == null)
				return;

			if (MessageBox.Show($"Are you shure you want to delete connector '{browseNodesTreeView.SelectedNode.Text}'", "Attention!", MessageBoxButtons.YesNo) == DialogResult.No)
				return;

			try
			{
				nodeManager.DeleteConnector((browseNodesTreeView.SelectedNode.Tag as NodeDescriptor).NodeId);

				TreeNode parent = browseNodesTreeView.SelectedNode.Parent;
				parent.Nodes.Clear();
				fillNodeChildren(parent);
			}
			catch (Exception exception)
			{
				ClientUtils.HandleException(Text, exception);
			}
		}

		private void addFunctionaGroupMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null)
				return;

			NodeDescriptor nodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			if (reference == null)
				return;

			AddFunctionaGroupForm addFunctionalGroup = new AddFunctionaGroupForm();

			if (addFunctionalGroup.ShowDialog(out (string Name, string Description) functionalGroupDescriptor))
			{
				ReferenceDescription methodReference = OpcUaNavigationHelper.Browse(
					session,
					(NodeId)reference.NodeId,
					BrowseDirection.Forward,
					NodeClass.Method,
					new NodeId[] { ReferenceTypeIds.HasComponent },
					false,
					childFilter: (rd) => rd.DisplayName == Constants.AddFunctionalGroupMethodName).FirstOrDefault();

				try
				{
					session.Call(
						(NodeId)reference.NodeId,
						(NodeId)methodReference.NodeId,
						functionalGroupDescriptor.Name,
						functionalGroupDescriptor.Name,
						functionalGroupDescriptor.Description
					);

					OpcUaHelper.SaveConfiguration(session);

					browseNodesTreeView.SelectedNode.Nodes.Clear();
					fillNodeChildren(browseNodesTreeView.SelectedNode);
				}
				catch (Exception exception)
				{
					ClientUtils.HandleException(Text, exception);
				}
			}
		}

		private void removeFunctionalGroupMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null || browseNodesTreeView.SelectedNode.Parent == null)
				return;

			if (MessageBox.Show($"Are you shure you want to remove functional group '{browseNodesTreeView.SelectedNode.Text}'", "Attention!", MessageBoxButtons.YesNo) == DialogResult.No)
				return;

			TreeNode parent = browseNodesTreeView.SelectedNode.Parent;

			NodeDescriptor nodeReferenceDescriptor = parent.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			if (reference == null)
				return;

			ReferenceDescriptionCollection children =
				OpcUaNavigationHelper.Browse(session, (NodeId)reference.NodeId, BrowseDirection.Forward, NodeClass.Method, new NodeId[] { ReferenceTypeIds.HasComponent }, false);

			ReferenceDescription methodReference = children.First(ch => ch.DisplayName == Constants.RemoveFunctionalGroupMethodName);

			try
			{
				session.Call(
					(NodeId)reference.NodeId,
					(NodeId)methodReference.NodeId,
					browseNodesTreeView.SelectedNode.Text);

				OpcUaHelper.SaveConfiguration(session);

				parent.Nodes.Clear();
				fillNodeChildren(parent);
			}
			catch (Exception exception)
			{
				ClientUtils.HandleException(Text, exception);
			}
		}

		private void copyNodeIdMenuItem_Click(object sender, EventArgs e)
		{
			NodeDescriptor nodeDescriptor = browseNodesTreeView.SelectedNode?.Tag as NodeDescriptor;

			Clipboard.SetText(nodeDescriptor?.NodeId?.ToString());
		}

		private void addVariableTypeMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null)
				return;

			AddVariableTypeForm addTypeForm = new AddVariableTypeForm();

			if (addTypeForm.ShowDialog(out VariableTypeDescriptor typeDescriptor))
			{
				try
				{
					session.Call(
						Constants.UnifiedVariableTypeManager,
						Constants.AddVariableTypeMethodNodeId,
						typeDescriptor.Name,
						typeDescriptor.Name,
						typeDescriptor.Description,
						getDataTypeNodeIdByName(typeDescriptor.DataType),
						typeDescriptor.IsArray
					);

					OpcUaHelper.SaveConfiguration(session);

					TypeTreeHelper.ReloadTypeTree(session);

					refreshVariableTypeSubNodes();
				}
				catch (Exception exception)
				{
					ClientUtils.HandleException(Text, exception);
				}
			}
		}

		private void removeVariableTypeMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null || browseNodesTreeView.SelectedNode.Parent == null)
				return;

			if (MessageBox.Show($"Are you shure you want to remove variable type '{browseNodesTreeView.SelectedNode.Text}'", "Attention!", MessageBoxButtons.YesNo) == DialogResult.No)
				return;

			TreeNode unifiedVariablesTreeNode = browseNodesTreeView.SelectedNode.Parent;

			NodeDescriptor nodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			if (reference == null)
				return;

			try
			{
				session.Call(
					Constants.UnifiedVariableTypeManager,
					Constants.RemoveVariableTypeMethodNodeId,
					(NodeId)reference.NodeId);

				OpcUaHelper.SaveConfiguration(session);

				TypeTreeHelper.ReloadTypeTree(session);

				unifiedVariablesTreeNode.Nodes.Clear();
				fillNodeChildren(unifiedVariablesTreeNode);
			}
			catch (Exception exception)
			{
				ClientUtils.HandleException(Text, exception);
			}
		}

		private void addPropertyMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null || browseNodesTreeView.SelectedNode.Parent == null)
				return;

			NodeDescriptor nodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			AddVariablePropertyForm addPropertyForm = new AddVariablePropertyForm();

			if (addPropertyForm.ShowDialog(out VariablePropertyDescriptor propertyDescriptor))
			{
				try
				{
					session.Call(
						Constants.UnifiedVariableTypeManager,
						Constants.AddVariablePropertyMethodNodeId,
						(NodeId)reference.NodeId,
						propertyDescriptor.Name,
						propertyDescriptor.Name,
						propertyDescriptor.Description,
						getDataTypeNodeIdByName(propertyDescriptor.DataType),
						propertyDescriptor.IsArray,
						propertyDescriptor.ReadOnly
					);

					OpcUaHelper.SaveConfiguration(session);

					browseNodesTreeView.SelectedNode.Nodes.Clear();
					fillNodeChildren(browseNodesTreeView.SelectedNode);
				}
				catch (Exception exception)
				{
					ClientUtils.HandleException(Text, exception);
				}
			}
		}

		private void addVariableMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null || browseNodesTreeView.SelectedNode.Parent == null)
				return;

			NodeDescriptor nodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			AddVariableVariableForm addPropertyForm = new AddVariableVariableForm();

			if (addPropertyForm.ShowDialog(out VariableVariableDescriptor propertyDescriptor))
			{
				try
				{
					session.Call(
						Constants.UnifiedVariableTypeManager,
						Constants.AddVariableVariableMethodNodeId,
						(NodeId)reference.NodeId,
						propertyDescriptor.Name,
						propertyDescriptor.Name,
						propertyDescriptor.Description,
						getDataTypeNodeIdByName(propertyDescriptor.DataType),
						propertyDescriptor.IsArray,
						propertyDescriptor.ReadOnly,
						getVariableTypeNodeIdByName(propertyDescriptor.VariableType)
					);

					OpcUaHelper.SaveConfiguration(session);

					browseNodesTreeView.SelectedNode.Nodes.Clear();
					fillNodeChildren(browseNodesTreeView.SelectedNode);
				}
				catch (Exception exception)
				{
					ClientUtils.HandleException(Text, exception);
				}
			}
		}

		private void removeVariableChildMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null || browseNodesTreeView.SelectedNode.Parent == null)
				return;

			if (MessageBox.Show($"Are you shure you want to remove variable type child '{browseNodesTreeView.SelectedNode.Text}'", "Attention!", MessageBoxButtons.YesNo) == DialogResult.No)
				return;

			TreeNode variableTypeTreeNode = browseNodesTreeView.SelectedNode.Parent;

			NodeDescriptor nodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			if (reference == null)
				return;

			try
			{
				session.Call(
					Constants.UnifiedVariableTypeManager,
					Constants.RemoveVariableChildMethodNodeId,
					(NodeId)reference.NodeId);

				OpcUaHelper.SaveConfiguration(session);

				variableTypeTreeNode.Nodes.Clear();
				fillNodeChildren(variableTypeTreeNode);
			}
			catch (Exception exception)
			{
				ClientUtils.HandleException(Text, exception);
			}
		}

		private void refreshMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == devicesTreeNode)
				refreshDevicesSubNodes();
			else if (browseNodesTreeView.SelectedNode == deviceTypesTreeNode)
				refreshDeviceTypeSubNodes();
			else if (browseNodesTreeView.SelectedNode == variableTypesTreeNode)
				refreshVariableTypeSubNodes();
			else
			{
				NodeDescriptor nodeDescriptor = browseNodesTreeView.SelectedNode?.Tag as NodeDescriptor;

				if (nodeDescriptor == null)
					return;

				browseNodesTreeView.SelectedNode.Nodes.Clear();
				browseNodesTreeView.SelectedNode.Nodes.Add(new TreeNode());

				fillNodeChildren(browseNodesTreeView.SelectedNode);
			}
		}

		private void variableContextMenu_Opening(object sender, CancelEventArgs e)
		{
			NodeDescriptor nodeDescriptor = (NodeDescriptor)browseNodesTreeView.SelectedNode.Tag;
			NodeKind nk = nodeDescriptor.NodeKind;

			var children =
				OpcUaNavigationHelper.Browse(
					session,
					nodeDescriptor.NodeId,
					BrowseDirection.Forward,
					NodeClass.Object,
					new[] { session.GetHasVariableBindingNodeId(), ReferenceTypeIds.HasHistoricalConfiguration },
					browseResultMask: BrowseResultMask.ReferenceTypeId
					);

			bool hasHistory = children.Exists(rd => rd.ReferenceTypeId == ReferenceTypeIds.HasHistoricalConfiguration);
			addVariableHistoryMenuItem.Enabled = !hasHistory;
			removeVariableHistoryMenuItem.Enabled = hasHistory;

			var isBindingVisible = (nk == NodeKind.SimpleVariable || nk == NodeKind.SimpleParameter || nk == NodeKind.SimpleParameterReference);
			addVariableBindingSeparator.Visible = isBindingVisible;
			addVariableBindingMenuItem.Visible = isBindingVisible;
			if (isBindingVisible)
				addVariableBindingMenuItem.Enabled = !children.Exists(rd => rd.ReferenceTypeId == session.GetHasVariableBindingNodeId());

			bool isParam = (nk == NodeKind.SimpleParameter || nk == NodeKind.ComplexParameter);
			bool isParamRef = (nk == NodeKind.SimpleParameterReference || nk == NodeKind.ComplexParameterReference);
			parameterSpecificMenuItemSeparator.Visible = isParam || isParamRef;
			//editParameterMenuItem.Visible = isParam || isParamRef;
			deassignParameterMenuItem.Visible = isParamRef;
			deleteParameterMenuItem.Visible = isParam;
		}

		private void addBindingMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null)
				return;

			NodeDescriptor nodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			if (reference == null)
				return;

			AddBindingForm addBindingForm = new AddBindingForm();

			if (addBindingForm.ShowDialog(session, reference.NodeId.ToNodeId(session), out (NodeId variableNodeId, NodeId bindingTypeNodeId) connectorDescriptor))
			{
				try
				{
					nodeManager.CreateBinding(connectorDescriptor.variableNodeId, connectorDescriptor.bindingTypeNodeId);

					browseNodesTreeView.SelectedNode.Nodes.Clear();
					fillNodeChildren(browseNodesTreeView.SelectedNode);
				}
				catch (Exception exception)
				{
					ClientUtils.HandleException(Text, exception);
				}
			}
		}

		private void deleteBindingMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null || browseNodesTreeView.SelectedNode.Parent == null)
				return;

			if (MessageBox.Show($"Are you shure you want to delete binding '{browseNodesTreeView.SelectedNode.Text}'", "Attention!", MessageBoxButtons.YesNo) == DialogResult.No)
				return;

			try
			{
				nodeManager.DeleteBinding((browseNodesTreeView.SelectedNode.Tag as NodeDescriptor).NodeId);

				TreeNode parent = browseNodesTreeView.SelectedNode.Parent;
				parent.Nodes.Clear();
				fillNodeChildren(parent);
			}
			catch (Exception exception)
			{
				ClientUtils.HandleException(Text, exception);
			}
		}

		private void addVariableHistoryMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null || browseNodesTreeView.SelectedNode.Parent == null)
				return;

			if (MessageBox.Show($"Are you shure you want to enable Historical Access for '{browseNodesTreeView.SelectedNode.Text}'", "Attention!", MessageBoxButtons.YesNo) == DialogResult.No)
				return;

			try
			{
				nodeManager.AddVariableHistory((browseNodesTreeView.SelectedNode.Tag as NodeDescriptor).NodeId);

				TreeNode parent = browseNodesTreeView.SelectedNode.Parent;
				parent.Nodes.Clear();
				fillNodeChildren(parent);
			}
			catch (Exception exception)
			{
				ClientUtils.HandleException(Text, exception);
			}
		}

		private void removeVariableHistoryMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null || browseNodesTreeView.SelectedNode.Parent == null)
				return;

			if (MessageBox.Show($"Are you shure you want to disable Historical Access for '{browseNodesTreeView.SelectedNode.Text}'", "Attention!", MessageBoxButtons.YesNo) == DialogResult.No)
				return;

			try
			{
				var nodeId = (browseNodesTreeView.SelectedNode.Tag as NodeDescriptor).NodeId;
				nodeManager.ChangeHistorizing(nodeId, false);
				nodeManager.RemoveVariableHistory(nodeId);

				TreeNode parent = browseNodesTreeView.SelectedNode.Parent;
				parent.Nodes.Clear();
				fillNodeChildren(parent);
			}
			catch (Exception exception)
			{
				ClientUtils.HandleException(Text, exception);
			}
		}

		private void addParameterMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null)
				return;

			NodeDescriptor parentNodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Parent.Tag as NodeDescriptor;
			ReferenceDescription parentReference = parentNodeReferenceDescriptor?.ReferenceDescription;

			NodeDescriptor nodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			if (reference == null)
				return;

			AddParameterForm addParameterForm = new AddParameterForm();

			if (addParameterForm.ShowDialog(out (string BrowseName, string DisplayName, string Description, NodeId DataType, bool IsArray, uint ArraySize, bool ReadOnly, NodeId VariableType) parameterDescriptor))
			{
				try
				{
					var deviceRef =
						OpcUaNavigationHelper.GetAncestorReference(session, reference, r => session.TypeTree.IsTypeOf(r.TypeDefinition.ToNodeId(session), session.GetDynamicDeviceTypeNodeId()))
						?? throw new Exception("Can't find antcestor device for node " + reference.NodeId);

					nodeManager.CreateParameter(deviceRef.NodeId.ToNodeId(session), parameterDescriptor);

					browseNodesTreeView.SelectedNode.Nodes.Clear();
					fillNodeChildren(browseNodesTreeView.SelectedNode);

					void refreshPropertyGrid()
					{
						// right panel
						nodeAttributesControl.DisplayAttributes(session, (NodeId)reference.NodeId);
						setRightPanelControl(nodeAttributesControl);
					}

					parametersControl.DisplayParameters(session, nodeManager, (NodeId)reference.NodeId, refreshPropertyGrid);
				}
				catch (Exception exception)
				{
					ClientUtils.HandleException(Text, exception);
				}
			}
		}

		private void editParameterMenuItem_Click(object sender, EventArgs e)
		{
			MessageBox.Show("Not implemented");
			/*return;

            if (browseNodesTreeView.SelectedNode == null)
                return;

            NodeDescriptor nodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Tag as NodeDescriptor;
            ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

            if (reference == null)
                return;

            void refreshPropertyGrid()
            {
                // right panel
                nodeAttributesControl.DisplayAttributes(session, (NodeId)reference.NodeId);
                setRightPanelControl(nodeAttributesControl);
            }

            AddParameterForm addParameterForm = new AddParameterForm();

            if (addParameterForm.ShowDialog(out ParameterDescriptor parameterDescriptor))
            {
                ReferenceDescriptionCollection children =
                    OpcUaHelper.Browse(session, (NodeId)reference.NodeId, BrowseDirection.Forward, NodeClass.Method, new NodeId[] { ReferenceTypeIds.HasComponent }, false);

                ReferenceDescription methodReference = children.First(ch => ch.DisplayName == Constants.AddParameterMethodName);

                try
                {
                    session.Call(
                        (NodeId)reference.NodeId,
                        (NodeId)methodReference.NodeId,
                        parameterDescriptor.Name,
                        parameterDescriptor.Name,
                        parameterDescriptor.Description,
                        getDataTypeNodeIdByName(parameterDescriptor.DataTypeName),
                        parameterDescriptor.IsArray,
                        parameterDescriptor.AccessLevel == "Read",
                        getVariableTypeNodeIdByName(parameterDescriptor.VariableType)
                    );

                    OpcUaHelper.SaveConfiguration(session);

                    browseNodesTreeView.SelectedNode.Nodes.Clear();
                    fillNodeChildren(browseNodesTreeView.SelectedNode);

                    parametersControl.DisplayParameters(session, (NodeId)reference.NodeId, refreshPropertyGrid);
                }
                catch (Exception exception)
                {
                    ClientUtils.HandleException(Text, exception);
                }
            }
            */
		}

		private void deleteParameterMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null || browseNodesTreeView.SelectedNode.Parent == null)
				return;

			if (MessageBox.Show($"Are you shure you want to delete parameter '{browseNodesTreeView.SelectedNode.Text}'", "Attention!", MessageBoxButtons.YesNo) == DialogResult.No)
				return;

			try
			{
				nodeManager.DeleteParameter((browseNodesTreeView.SelectedNode.Tag as NodeDescriptor).NodeId);

				TreeNode parent = browseNodesTreeView.SelectedNode.Parent;
				parent.Nodes.Clear();
				fillNodeChildren(parent);
			}
			catch (Exception exception)
			{
				ClientUtils.HandleException(Text, exception);
			}
		}

		private void assignParameterMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null)
				return;

			NodeDescriptor nodeReferenceDescriptor = browseNodesTreeView.SelectedNode.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			if (reference == null)
				return;

			TreeNode deviceNode = browseNodesTreeView.SelectedNode.Parent;

			while (deviceNode != null && deviceNode.Tag != null)
			{
				NodeDescriptor parentNodeReferenceDescriptor = deviceNode.Tag as NodeDescriptor;

				if (parentNodeReferenceDescriptor.NodeKind == NodeKind.Device || parentNodeReferenceDescriptor.NodeKind == NodeKind.DeviceType)
					break;

				deviceNode = deviceNode.Parent;
			}

			if (deviceNode == null)
				return;

			TreeNode parameterSetNode = null;

			foreach (TreeNode node in deviceNode.Nodes)
				if (node.Text == Constants.ParametersNodeName)
				{
					parameterSetNode = node;
					break;
				}

			if (parameterSetNode == null)
				return;

			ReferenceDescriptionCollection variables =
				OpcUaNavigationHelper.Browse(session, ((NodeDescriptor)parameterSetNode.Tag).NodeId, BrowseDirection.Forward, NodeClass.Variable, new NodeId[] { ReferenceTypeIds.HasComponent });

			if (variables.Count == 0)
			{
				MessageBox.Show("No parameters to assing.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}

			List<string> parameters = new List<string>();

			foreach (ReferenceDescription variable in variables)
				parameters.Add(variable.DisplayName.ToString());

			AssingParameterToFunctionalGroupForm assignParameterForm = new AssingParameterToFunctionalGroupForm();

			if (assignParameterForm.ShowDialog(browseNodesTreeView.SelectedNode.Text, parameters.ToArray(), out string selectedParameterName))
			{
				ReferenceDescription methodReference = OpcUaNavigationHelper.Browse(
					session,
					(NodeId)reference.NodeId,
					BrowseDirection.Forward,
					NodeClass.Method,
					new NodeId[] { ReferenceTypeIds.HasComponent },
					false,
					childFilter: (rd) => rd.DisplayName == Constants.AssignParameterMethodName).FirstOrDefault();

				try
				{
					session.Call(
						(NodeId)reference.NodeId,
						(NodeId)methodReference.NodeId,
						selectedParameterName
					);

					OpcUaHelper.SaveConfiguration(session);

					browseNodesTreeView.SelectedNode.Nodes.Clear();
					fillNodeChildren(browseNodesTreeView.SelectedNode);
				}
				catch (Exception exception)
				{
					ClientUtils.HandleException(Text, exception);
				}
			}
		}

		private void deassignParameterMenuItem_Click(object sender, EventArgs e)
		{
			if (browseNodesTreeView.SelectedNode == null)
				return;

			TreeNode functionalGroupNode = browseNodesTreeView.SelectedNode.Parent;

			NodeDescriptor nodeReferenceDescriptor = functionalGroupNode.Tag as NodeDescriptor;
			ReferenceDescription reference = nodeReferenceDescriptor?.ReferenceDescription;

			if (reference == null)
				return;

			ReferenceDescription methodReference = OpcUaNavigationHelper.Browse(
				session,
				(NodeId)reference.NodeId,
				BrowseDirection.Forward,
				NodeClass.Method,
				new NodeId[] { ReferenceTypeIds.HasComponent },
				false,
				childFilter: (rd) => rd.DisplayName == Constants.DeassignParameterMethodName).FirstOrDefault();

			if (MessageBox.Show($"Deassign parameter '{browseNodesTreeView.SelectedNode.Text}' from functional group '{browseNodesTreeView.SelectedNode.Parent.Text}'?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				try
				{
					session.Call(
						(NodeId)reference.NodeId,
						(NodeId)methodReference.NodeId,
						browseNodesTreeView.SelectedNode.Text);

					OpcUaHelper.SaveConfiguration(session);

					functionalGroupNode.Nodes.Clear();
					fillNodeChildren(functionalGroupNode);
				}
				catch (Exception exception)
				{
					ClientUtils.HandleException(Text, exception);
				}
		}
	}
}
