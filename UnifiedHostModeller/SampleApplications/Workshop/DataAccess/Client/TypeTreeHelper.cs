﻿using Opc.Ua;
using Opc.Ua.Client;
using System.Collections.Generic;
using System.Linq;
using UnifiedHostModeler.Helpers;
using UnifiedHostModeler.Models;
using UnifiedStudio;

namespace UnifiedHostModeler
{
	public static class TypeTreeHelper
	{
        public static DataTypeInfo DataTypeTree { get; private set; }
        public static Dictionary<NodeId, DataTypeInfo> DataTypeList = new Dictionary<NodeId, DataTypeInfo>();

        public static VariableTypeInfo VariableTypeTree { get; private set; }
        public static Dictionary<NodeId, VariableTypeInfo> VariableTypeList = new Dictionary<NodeId, VariableTypeInfo>();

        public static Dictionary<string, (NodeId NodeId, string[] DataTypes)> SupportedVariableTypes = new Dictionary<string, (NodeId NodeId, string[] DataTypes)>();

        public static void ReloadTypeTree(Session session)
        {
            DataTypeList.Clear();

            DataTypeTree = new DataTypeInfo(DataTypeIds.BaseDataType, nameof(DataTypeIds.BaseDataType), null, true);
            DataTypeList.Add(DataTypeTree.NodeId, DataTypeTree);

            loadSubDataTypes(session, DataTypeTree);

            VariableTypeList.Clear();

            VariableTypeTree = new VariableTypeInfo(VariableTypeIds.BaseVariableType, nameof(DataTypeIds.BaseDataType), null, DataTypeTree);
            VariableTypeList.Add(VariableTypeTree.NodeId, VariableTypeTree);

            loadSubVariableTypes(session, VariableTypeTree);

            SupportedVariableTypes.Clear();
            loadSupportedVariableTypes();
        }

        private static void loadSubDataTypes(Session session, DataTypeInfo parentType)
        {
            ReferenceDescriptionCollection references
                = OpcUaNavigationHelper.Browse(session, parentType.NodeId, BrowseDirection.Forward, NodeClass.DataType, new NodeId[] { ReferenceTypeIds.HasSubtype }, true);

            foreach (ReferenceDescription reference in references)
            {
                ReadValueIdCollection nodesToRead = new ReadValueIdCollection()
                {
                    new ReadValueId {NodeId = (NodeId)reference.NodeId, AttributeId = Attributes.IsAbstract}
                };

                session.Read(null, 0, TimestampsToReturn.Neither, nodesToRead, out DataValueCollection results, out _);

                bool isAbstract = (bool) results[0].Value;

                DataTypeInfo childType = new DataTypeInfo((NodeId)reference.NodeId, reference.DisplayName.ToString(), parentType, isAbstract);

                DataTypeList.Add(childType.NodeId, childType);

                parentType.Children.Add(childType);

                loadSubDataTypes(session, childType);
            }
        }

        private static void loadSubVariableTypes(Session session, VariableTypeInfo parentType)
        {
            ReferenceDescriptionCollection references
                = OpcUaNavigationHelper.Browse(session, parentType.NodeId, BrowseDirection.Forward, NodeClass.VariableType, new NodeId[] { ReferenceTypeIds.HasSubtype }, true);

            foreach (ReferenceDescription reference in references)
            {
                ReadValueIdCollection nodesToRead = new ReadValueIdCollection()
                {
                    new ReadValueId {NodeId = (NodeId)reference.NodeId, AttributeId = Attributes.DataType}
                };

                session.Read(null, 0, TimestampsToReturn.Neither, nodesToRead, out DataValueCollection results, out _);

                NodeId dataType = (NodeId)results[0].Value;

                VariableTypeInfo childType = new VariableTypeInfo((NodeId)reference.NodeId, reference.DisplayName.ToString(), parentType, DataTypeList[dataType]);

                VariableTypeList.Add(childType.NodeId, childType);

                parentType.Children.Add(childType);

                loadSubVariableTypes(session, childType);
            }
        }

        private static void loadSupportedVariableTypes()
        {
            HashSet<string> supportedDataTypeNames = new HashSet<string>
            {
                "Boolean",
                "SByte",
                "Byte",
                "Int16",
                "UInt16",
                "Int32",
                "UInt32",
                "Int64",
                "UInt64",
                "Float",
                "Double",
                "String",
                "DateTime",
                "Guid",
                "ByteString",
                "XmlElement",
                "NodeId",
                "ExpandedNodeId",
                "StatusCode",
                "DiagnosticInfo",
                "QualifiedName",
                "LocalizedText",
                "UtcTime"
             };

            // load predefined variable types
            SupportedVariableTypes["BaseDataVariableType"] = (VariableTypeIds.BaseDataVariableType, supportedDataTypeNames.ToArray());

            // load special variable types

            HashSet<string> specialVariableTypeNames = new HashSet<string>
            {
                "OperatingCondition",
                "TNTUnwinderData",
                "Handshake"
            };

            VariableTypeInfo unifiedVariableType;
            foreach (string typeName in specialVariableTypeNames)
            {
                unifiedVariableType = VariableTypeList.Values.FirstOrDefault(vt => vt.Name == typeName);

                if (unifiedVariableType != null)
                    SupportedVariableTypes[unifiedVariableType.Name] = (unifiedVariableType.NodeId, unifiedVariableType.DataType.GetFinalTypes().Select(d => d.Name).Where(n => supportedDataTypeNames.Contains(n)).ToArray());
            }

            // load unified controller types
            if (!VariableTypeList.TryGetValue(Constants.UnifiedVariableTypeNodeId, out unifiedVariableType))
                return;

            foreach (VariableTypeInfo variableType in unifiedVariableType.Children)
                SupportedVariableTypes[variableType.Name] = (variableType.NodeId, variableType.DataType.GetFinalTypes().Select(d => d.Name).Where(n => supportedDataTypeNames.Contains(n)).ToArray());
        }
    }
}
