﻿using Opc.Ua;

namespace UnifiedHostModeler.Models
{
	public class ParameterReferenceDescription
	{
		public readonly ReferenceDescription ReferenceDescription;

		public readonly ParameterReferenceDescription[] Children;

		public ParameterReferenceDescription(ReferenceDescription referenceDescription, ParameterReferenceDescription[] children)
		{
			ReferenceDescription = referenceDescription;
			Children = children;
		}
	}
}
