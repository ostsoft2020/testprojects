﻿using Opc.Ua;
using Opc.Ua.Client;

namespace UnifiedStudio.Models
{
    public class VariableTypeDescriptor
    {
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public string DataType { get; set; }
        public bool IsArray { get; set; }

        public NodeId NodeId;

        public MonitoredItem MonitoredItem;
    }
}
