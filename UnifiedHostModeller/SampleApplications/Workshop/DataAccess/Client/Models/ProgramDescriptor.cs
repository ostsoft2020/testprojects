﻿using Opc.Ua;
using Opc.Ua.Client;

namespace UnifiedStudio.Models
{
    public class ProgramDescriptor
    {
        public string Name { get; set; }
        public NodeId NodeId { get; set; }

        public bool AutoStart { get; set; }
        public NodeId AutoStartNodeId { get; set; }
        public MonitoredItem AutoStartMonitoredItem { get; set; }

        public string CurrentState { get; set; }
        public NodeId CurrentStateNodeId { get; set; }
        public MonitoredItem CurrentStateMonitoredItem { get; set; }

        public string LastTransition { get; set; }
        public NodeId LastTransitionNodeId { get; set; }
        public MonitoredItem LastTransitionMonitoredItem { get; set; }

        public string HaltReason { get; set; }
        public NodeId HaltReasonNodeId { get; set; }
        public MonitoredItem HaltReasonMonitoredItem { get; set; }

        public NodeId StartMethodNodeId { get; set; }
        public NodeId HaltMethodNodeId { get; set; }
        public NodeId ResetMethodNodeId { get; set; }
    }
}
