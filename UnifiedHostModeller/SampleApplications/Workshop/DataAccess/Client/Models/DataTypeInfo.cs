﻿using Opc.Ua;
using System.Collections.Generic;

namespace UnifiedHostModeler.Models
{
	public class DataTypeInfo
	{
		public readonly NodeId NodeId;
		public readonly string Name;
		public readonly DataTypeInfo ParentType;
		public readonly bool IsAbstract;

		public readonly List<DataTypeInfo> Children = new List<DataTypeInfo>();

		public DataTypeInfo(NodeId nodeId, string name, DataTypeInfo parentType, bool isAbstract)
		{
			NodeId = nodeId;
			Name = name;
			ParentType = parentType;
			IsAbstract = isAbstract;
		}

		public IList<DataTypeInfo> GetFinalTypes()
		{
			List<DataTypeInfo> result = new List<DataTypeInfo>();

			if (!IsAbstract)
				result.Add(this);

			foreach (DataTypeInfo child in Children)
				result.AddRange(child.GetFinalTypes());

			return result;
		}
	}
}
