﻿using Opc.Ua;
using Opc.Ua.Client;

namespace UnifiedStudio.Models
{
	public class DeviceDescriptor
	{
		public NodeId NodeId { get; set; }

		public string Name { get; set; } = "";
		public string SerialNumber { get; set; } = "";

		public bool Enabled { get; set; }
		public NodeId EnabledNodeId;
		public MonitoredItem EnabledMonitoredItem;

		public NodeId EnableMethodNodeId;
		public NodeId DisableMethodNodeId;
	}
}
