﻿namespace UnifiedStudio.Models
{
    public class VariablePropertyDescriptor
    {
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public string DataType { get; set; }
        public bool IsArray { get; set; }
        public bool ReadOnly { get; set; }
    }
}
