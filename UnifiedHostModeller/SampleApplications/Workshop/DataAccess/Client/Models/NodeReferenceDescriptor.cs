﻿using Opc.Ua;

namespace UnifiedStudio
{
	public class NodeDescriptor
	{
		public bool InTypeHierarchy;
		public ReferenceDescription ReferenceDescription;
		public NodeKind NodeKind;

		public NodeId NodeId => (NodeId)ReferenceDescription.NodeId;

		public NodeDescriptor(bool inTypeHierarchy, ReferenceDescription referenceDescription)
		{
			InTypeHierarchy = inTypeHierarchy;
			ReferenceDescription = referenceDescription;
		}
	}
}
