﻿using Opc.Ua;
using System.Collections.Generic;
using System.Linq;

namespace UnifiedStudio.Models
{
	public class OpcUaConditionMonitoringSettings : ConditionMonitoringSettingsBase
	{
		public List<string> Sources { get; } = new List<string>();

		public override string SourcesAsString => string.Join(", ", Sources.Select(s => s?.ToString()));
	}
}
