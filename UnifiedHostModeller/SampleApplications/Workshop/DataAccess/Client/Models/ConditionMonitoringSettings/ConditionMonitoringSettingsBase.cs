﻿namespace UnifiedStudio.Models
{
	public abstract class ConditionMonitoringSettingsBase
	{
		public int ConditionCode { get; set; }

		public abstract string SourcesAsString { get; }
	}
}
