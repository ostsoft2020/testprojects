﻿using System.Collections.Generic;
using System.Linq;

namespace UnifiedStudio.Models
{
	public class S7TcpIpConditionMonitoringSettings : ConditionMonitoringSettingsBase
	{
		public List<(uint BlockNumber, uint Address, byte BitAddress)> Sources { get; } = new List<(uint BlockNumber, uint Address, byte BitAddress)>();

		public override string SourcesAsString => 
			string.Join(
				", ", 
				Sources.Select(s => $"[{s.BlockNumber}->{s.Address + (s.BitAddress > 0 ? "." + s.BitAddress : "")}]"));
	}
}
