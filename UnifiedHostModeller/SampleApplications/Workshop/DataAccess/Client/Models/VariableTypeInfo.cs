﻿using Opc.Ua;
using System.Collections.Generic;

namespace UnifiedHostModeler.Models
{
	public class VariableTypeInfo
	{
		public readonly NodeId NodeId;
		public readonly string Name;
		public readonly VariableTypeInfo ParentType;
		public readonly DataTypeInfo DataType;

		public readonly List<VariableTypeInfo> Children = new List<VariableTypeInfo>();

		public VariableTypeInfo(NodeId nodeId, string name, VariableTypeInfo parentType, DataTypeInfo dataType)
		{
			NodeId = nodeId;
			Name = name;
			ParentType = parentType;
			DataType = dataType;
		}
	}
}
