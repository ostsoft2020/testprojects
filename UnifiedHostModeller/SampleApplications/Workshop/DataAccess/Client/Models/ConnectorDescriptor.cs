﻿using Opc.Ua;
using Opc.Ua.Client;

namespace UnifiedStudio.Models
{
    public class ConnectorDescriptor
    {
        public string Name { get; set; }
        public NodeId NodeId { get; set; }

        public bool Enabled { get; set; }
        public NodeId EnabledNodeId { get; set; }
        public MonitoredItem EnabledMonitoredItem { get; set; }

        public bool Connected { get; set; }
        public NodeId ConnectedNodeId { get; set; }
        public MonitoredItem ConnectedMonitoredItem { get; set; }

        public string ConnectionError { get; set; }
        public NodeId ConnectionErrorNodeId { get; set; }
        public MonitoredItem ConnectionErrorMonitoredItem { get; set; }

        public NodeId EnableMethodNodeId { get; set; }
        public NodeId DisableMethodNodeId { get; set; }
        public NodeId TestMethodNodeId { get; set; }
    }
}
