﻿using Opc.Ua;
using Opc.Ua.Client;

namespace UnifiedStudio.Models
{
    public class WorkflowDescriptor
    {
        public string Name { get; set; } = "";
        public string CurrentState { get; set; } = "";

        public bool AutoStart { get; set; }
        public NodeId AutoStartNodeId { get; set; }
        public MonitoredItem AutoStartMonitoredItem { get; set; }

        public NodeId WorkflowNodeId { get; set; }
        public NodeId RunMethodNodeId { get; set; }
        public NodeId AbortMethodNodeId { get; set; }
        public NodeId CancelMethodNodeId { get; set; }

        public MonitoredItem MonitoredItem { get; set; }
    }
}
