﻿namespace UnifiedStudio.Models
{
	public class S7TcpIpBindingSettings : BindingSettingsBase
	{
		public string S7DataType { get; set; }
		public ushort DBNumber { get; set; }
		public ushort Address { get; set; }
		public byte BitAddress { get; set; }

		public string FullAddress
		{
			get
			{
				if (S7DataType != "Bool")
					return Address.ToString();
				else
					return $"{Address}.{BitAddress}";
			}
		}
	}
}
