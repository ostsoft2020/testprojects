﻿using Opc.Ua;

namespace UnifiedStudio.Models
{
	public abstract class BindingSettingsBase
	{
		public string ModeName 
		{ 
			get 
			{
				switch (Mode)
				{
					case 0:
						return "TwoWay";
					case 1:
						return "OneWay";
					case 2:
						return "OneWayToSource";
					default:
						return "TwoWay";
				}
			}
			set
			{
				switch (value)
				{
					case "TwoWay":
						Mode = 0;
						break;
					case "OneWay":
						Mode = 1;
						break;
					case "OneWayToSource":
						Mode = 2;
						break;
					default:
						Mode = 0;
						break;
				}
			}
		}

		public int Mode { get; set; }

		public bool Monitorable { get; set; }

		public string TargetVariablePath { get; set; }

		public NodeId Converter { get; set; }

		public string ConverterName { get; set; }

		public string ConverterParameter { get; set; }

		public double SamplingInterval { get; set; }


		public static bool ValidateModeName(string modeName)
		{
			switch (modeName)
			{
				case "TwoWay":
				case "OneWay":
				case "OneWayToSource":
					return true;
				default:
					return false;
			}
		}
	}
}
