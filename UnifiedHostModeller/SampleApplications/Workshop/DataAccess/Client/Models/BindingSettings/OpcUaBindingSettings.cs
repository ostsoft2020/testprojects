﻿namespace UnifiedStudio.Models
{
	public class OpcUaBindingSettings : BindingSettingsBase
	{
		public string SourceVariableId { get; set; }
	}
}
