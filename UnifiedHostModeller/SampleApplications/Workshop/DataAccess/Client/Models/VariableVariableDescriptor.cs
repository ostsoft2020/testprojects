﻿namespace UnifiedStudio.Models
{
    public class VariableVariableDescriptor
    {
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public string VariableType { get; set; }
        public string DataType { get; set; }
        public bool IsArray { get; set; }
        public bool ReadOnly { get; set; }
    }
}
