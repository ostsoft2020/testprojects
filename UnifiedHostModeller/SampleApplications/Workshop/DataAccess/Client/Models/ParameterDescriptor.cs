﻿using Opc.Ua;
using Opc.Ua.Client;

namespace UnifiedStudio.Models
{
    public class ParameterDescriptor
    {
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public string AccessLevel { get; set; } = "";
        public string VariableType { get; set; }
        public NodeId DataType { get; set; }
        public string DataTypeName { get; set; }
        public bool? IsArray { get; set; }
		public object OriginalValue { get; set; }
		public string ValueAsString { get; set; }
		public bool IsWritable { get; set; }
        public bool Historizing { get; set; }

        public NodeId NodeId;

        public MonitoredItem ValueMonitoredItem;
		public MonitoredItem HistorizingMonitoredItem;
	}
}
