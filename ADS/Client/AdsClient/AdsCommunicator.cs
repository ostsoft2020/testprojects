﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using TwinCAT;
using TwinCAT.Ads;
using TwinCAT.TypeSystem;

namespace AdsClient
{
  public class AdsCommunicator : IDisposable
  {
    #region Fields

    private bool connected;
    private AdsSession session;
    
    private readonly Dictionary<string, (ITcAdsSymbol SymbolInfo, ITcAdsDataType DataType)> symbolInfoCache = new Dictionary<string, (ITcAdsSymbol SymbolInfo, ITcAdsDataType DataType)>();

    private readonly ConcurrentDictionary<int, (string SymbolPath, BinaryReader BinaryReader, ITcAdsDataType DataType)> subscriptions = new ConcurrentDictionary<int, (string SymbolPath, BinaryReader BinaryReader, ITcAdsDataType DataType)>();

    #endregion

    #region Properties

    public AmsAddress AmsAddress { get; private set; }

    public SessionSettings SessionSettings { get; private set; }

    public bool Connected
    {
      get => connected;
      private set
      {
        if (connected != value)
        {
          connected = value;
          ConnectionStateChanged?.Invoke(this, Connected);
        }
      }
    }

    #endregion

    #region Events 

    public event EventHandler<bool> ConnectionStateChanged;

    public event EventHandler<(string SymbolPath, object Value)> SubscriptionNotification;

    #endregion

    #region Constructors

    /// <summary>
    /// AdsCommunicator constructor 
    /// </summary>
    /// <param name="amsAddress">Ads server address</param>
    /// <param name="settings">Default settings are Async access with Timeout 5 sec</param>
    public AdsCommunicator()
    {
    }

    #endregion

    #region Interface methods

    public void Connect(AmsAddress amsAddress, SessionSettings settings)
    {
      AmsAddress = amsAddress;
      SessionSettings = settings ?? SessionSettings.Default;

      // Async access is necessary for Console applications!
      session = new AdsSession(AmsAddress, SessionSettings);

      AdsConnection connection = (AdsConnection)session.Connect(); // Establish the connection
      connection.ConnectionStateChanged += delegate (object sender, ConnectionStateChangedEventArgs e)
      {
        Connected = session?.ConnectionState == ConnectionState.Connected;
      };

      Connected = connection.ConnectionState == ConnectionState.Connected;

      connection.AdsNotification += onAdsNotification;

      /*
      // Read the identification and version number of the device
      DeviceInfo deviceInfo = connection.ReadDeviceInfo();
      Version version = deviceInfo.Version.ConvertToStandard();
      Console.WriteLine(string.Format("DeviceName: {0}", deviceInfo.Name));
      Console.WriteLine(string.Format("DeviceVersion: {0}", version.ToString(3)));

      /// Read the state of the device
      StateInfo stateInfo = connection.ReadState();
      AdsState adsState = stateInfo.AdsState;

      short deviceState = stateInfo.DeviceState;
      Console.WriteLine(string.Format("DeviceState: {0}", deviceState));
      Console.WriteLine(string.Format("AdsState   : {0}", adsState));

      // Other ADS methods (as formerly used on TcAdsClient) can be used also on connection object:

      // connection.Read(...)
      // connection.Write(...)
      // connection.AddDeviceNotificationEx += ...


      // Session communication Diagnostic:

      int resurrectionTries = connection.ResurrectingTries;
      int succeededResurrections = connection.Resurrections;

      AdsCommunicationStatistics statistics = session.Statistics; // The communication statistics

      // Symbol access:
      // The Session holds and Caches the Symbolic data information
      ReadOnlyDataTypeCollection types = session.SymbolServer.DataTypes;
      ReadOnlySymbolCollection symbols = session.SymbolServer.Symbols;

      dynamic projectNameSymbol = symbols["TwinCAT_SystemInfoVarList._AppInfo.ProjectName"];
      string projectName = (string)projectNameSymbol.ReadValue();

      // Or use dynamic objects
      dynamic appInfo = symbols["TwinCAT_SystemInfoVarList._AppInfo"];
      string projectName2 = appInfo.ProjectName.ReadValue();*/
    }

    public void Dispose()
    {
      Disconnect();
    }

    public void Disconnect()
    {
      symbolInfoCache.Clear();

      session?.Dispose();
    }

    /// <summary>
    /// Read value with automatic type detection based on SymbolInfo.DataType
    /// </summary>
    public object ReadValue(string symbolPath)
    {
      checkConnection();

      int variableHandle = session.Connection.CreateVariableHandle(symbolPath);

      if (variableHandle == 0)
        throw new Exception($"Invalid symbol path '{symbolPath}'");

      try
      {
        (ITcAdsSymbol SymbolInfo, ITcAdsDataType DataType) symbolData = getSymbolData(symbolPath);

        switch (symbolData.DataType.DataTypeId)
        {
          case AdsDatatypeId.ADST_STRING:
            return session.Connection.ReadAnyString(variableHandle, symbolData.DataType.ByteSize - 1, Encoding.Default);

          case AdsDatatypeId.ADST_WSTRING:
            return session.Connection.ReadAnyString(variableHandle, (symbolData.DataType.ByteSize / 2) - 1, Encoding.Unicode);

          default:
            return session.Connection.ReadAny(variableHandle, symbolData.DataType.ManagedType);
        }
      }
      finally
      {
        session.Connection.DeleteVariableHandle(variableHandle);
      }
    }

    /// <summary>
    /// Read value of simle type except string
    /// </summary>
    public object ReadSimpleValue(string symbolPath, Type variableType)
    {
      checkConnection();

      int variableHandle = session.Connection.CreateVariableHandle(symbolPath);

      if (variableHandle == 0)
        throw new Exception($"Invalid symbol path '{symbolPath}'");

      return session.Connection.ReadAny(variableHandle, variableType);
    }

    /// <summary>
    /// Read value of string type
    /// </summary>
    public string ReadStringValue(string symbolPath, int maxCharactersCount, bool isUnicode)
    {
      checkConnection();

      int variableHandle = session.Connection.CreateVariableHandle(symbolPath);

      if (variableHandle == 0)
        throw new Exception($"Invalid symbol path '{symbolPath}'");

      try
      {
        return session.Connection.ReadAnyString(variableHandle, maxCharactersCount, isUnicode ? Encoding.Unicode : Encoding.Default);
      }
      finally
      {
        session.Connection.DeleteVariableHandle(variableHandle);
      }
    }

    /// <summary>
    /// Write value with automatic type detection based on SymbolInfo.DataType
    /// </summary>
    public void WriteValue(string symbolPath, object value)
    {
      checkConnection();

      if (value == null)
        throw new NullReferenceException("Parameter 'value' is empty");

      int variableHandle = session.Connection.CreateVariableHandle(symbolPath);

      if (variableHandle == 0)
        throw new Exception($"Invalid symbol path '{symbolPath}'");

      try
      {
        (ITcAdsSymbol SymbolInfo, ITcAdsDataType DataType) symbolData = getSymbolData(symbolPath);

        switch (symbolData.DataType.DataTypeId)
        {
          case AdsDatatypeId.ADST_STRING:
            AdsStream stringWriteStream = new AdsStream(symbolData.DataType.ByteSize);
            AdsBinaryWriter stringWriter = new AdsBinaryWriter(stringWriteStream);
            stringWriter.WritePlcString(value.ToString(), symbolData.DataType.ByteSize - 1, Encoding.Default);
            session.Connection.Write(variableHandle, stringWriteStream);
            break;

          case AdsDatatypeId.ADST_WSTRING:
            AdsStream wStringWriteStream = new AdsStream(symbolData.DataType.ByteSize);
            AdsBinaryWriter wStringWriter = new AdsBinaryWriter(wStringWriteStream);
            wStringWriter.WritePlcString(value.ToString(), (symbolData.DataType.ByteSize / 2) - 1, Encoding.Unicode);
            session.Connection.Write(variableHandle, wStringWriteStream);
            break;

          default:
            session.Connection.WriteAny(variableHandle, value);
            break;
        }
      }
      finally
      {
        session.Connection.DeleteVariableHandle(variableHandle);
      }
    }

    /// <summary>
    /// Write value of simle type except string
    /// </summary>
    public void WriteSimpleValue(string symbolPath, object value)
    {
      checkConnection();

      int variableHandle = session.Connection.CreateVariableHandle(symbolPath);

      if (variableHandle == 0)
        throw new Exception($"Invalid symbol path '{symbolPath}'");

      try
      { 
        session.Connection.WriteAny(variableHandle, value);
      }
      finally
      {
        session.Connection.DeleteVariableHandle(variableHandle);
      }
    }

    /// <summary>
    /// Read value of string type
    /// </summary>
    public void WriteStringValue(string symbolPath, int maxCharactersCount, bool isUnicode, string value)
    {
      checkConnection();

      int variableHandle = session.Connection.CreateVariableHandle(symbolPath);

      if (variableHandle == 0)
        throw new Exception($"Invalid symbol path '{symbolPath}'");

      try
      {
        if (isUnicode)
        {
          AdsStream writeStream = new AdsStream(maxCharactersCount + 1);
          AdsBinaryWriter writer = new AdsBinaryWriter(writeStream);
          writer.WritePlcString(value, maxCharactersCount, Encoding.Default);
          session.Connection.Write(variableHandle, writeStream);
        }
        else
        {
          AdsStream writeStream = new AdsStream((maxCharactersCount + 1) * 2);
          AdsBinaryWriter writer = new AdsBinaryWriter(writeStream);
          writer.WritePlcString(value, maxCharactersCount, Encoding.Default);
          session.Connection.Write(variableHandle, writeStream);
        }
      }
      finally
      {
        session.Connection.DeleteVariableHandle(variableHandle);
      }
    }

    public void Subscribe(string symbolPath, AdsTransMode transitionMode = AdsTransMode.OnChange, int cycleTime = 100, int maxDelay = 0)
    {
      foreach (var value in subscriptions.Values)
        if (value.SymbolPath == symbolPath)
          return;

      checkConnection();

      int variableHandle = session.Connection.CreateVariableHandle(symbolPath);

      if (variableHandle == 0)
        throw new Exception($"Invalid symbol path '{symbolPath}'");

      try
      {
        (ITcAdsSymbol SymbolInfo, ITcAdsDataType DataType) symbolData = getSymbolData(symbolPath);

        AdsStream dataStream = new AdsStream(symbolData.DataType.ByteSize);

        BinaryReader binaryReader;

        switch (symbolData.DataType.DataTypeId)
        {
          case AdsDatatypeId.ADST_STRING:
            binaryReader = new BinaryReader(dataStream, Encoding.Default);
            break;

          case AdsDatatypeId.ADST_WSTRING:
            binaryReader = new BinaryReader(dataStream, Encoding.Unicode);
            break;

          default:
            binaryReader = new BinaryReader(dataStream);
            break;
        }

        int notificationHandler = session.Connection.AddDeviceNotification(symbolPath, dataStream, transitionMode, cycleTime, maxDelay, null);

        subscriptions.TryAdd(notificationHandler, (symbolPath, binaryReader, symbolData.DataType));
      }
      finally
      {
        session.Connection.DeleteVariableHandle(variableHandle);
      }
    }

    public void Unsubscribe(string symbolPath)
    {

    }

    #endregion

    #region Service methods

    private void checkConnection()
    {
      if (session == null)
        throw new Exception("Session was not created (Connect method has not been called)");

      if (session.Connection == null)
        throw new Exception("Session connection was not created");

      if (session.Connection.State != ConnectionState.Connected)
        throw new Exception("Session cannestion is not connected");
    }

    private (ITcAdsSymbol SymbolInfo, ITcAdsDataType DataType) getSymbolData(string symbolPath)
    {
      if (!symbolInfoCache.TryGetValue(symbolPath, out (ITcAdsSymbol SymbolInfo, ITcAdsDataType DataType) symbolData))
      {
        ITcAdsSymbol symbolInfo = session.Connection.ReadSymbolInfo(symbolPath);
        ITcAdsDataType dataType = (ITcAdsDataType)symbolInfo.GetType().GetProperty("DataType").GetValue(symbolInfo);

        symbolData = (symbolInfo, dataType);

        symbolInfoCache.Add(symbolPath, symbolData);
      }

      return symbolData;
    }

    private void onAdsNotification(object sender, AdsNotificationEventArgs e)
    {
      if (subscriptions.TryGetValue(e.NotificationHandle, out (string SymbolPath, BinaryReader BinaryReader, ITcAdsDataType DataType) subscriptionData))
      {
        e.DataStream.Position = e.Offset;

        object value = null;

        switch (subscriptionData.DataType.DataTypeId)
        {
          case AdsDatatypeId.ADST_INT8:
            value = subscriptionData.BinaryReader.ReadSByte();
            break;
          case AdsDatatypeId.ADST_UINT8:
            value = subscriptionData.BinaryReader.ReadByte();
            break;
          case AdsDatatypeId.ADST_INT16:
            value = subscriptionData.BinaryReader.ReadInt16();
            break;
          case AdsDatatypeId.ADST_UINT16:
            value = subscriptionData.BinaryReader.ReadUInt16();
            break;
          case AdsDatatypeId.ADST_INT32:
            value = subscriptionData.BinaryReader.ReadInt32();
            break;
          case AdsDatatypeId.ADST_UINT32:
            value = subscriptionData.BinaryReader.ReadUInt32();
            break;
          case AdsDatatypeId.ADST_INT64:
            value = subscriptionData.BinaryReader.ReadInt64();
            break;
          case AdsDatatypeId.ADST_UINT64:
            value = subscriptionData.BinaryReader.ReadUInt64();
            break;
          case AdsDatatypeId.ADST_REAL32:
            value = subscriptionData.BinaryReader.ReadSingle();
            break;
          case AdsDatatypeId.ADST_REAL64:
            value = subscriptionData.BinaryReader.ReadDouble();
            break;
          //case AdsDatatypeId.ADST_BIGTYPE:
            //value = subscriptionData.BinaryReader.ReadBytes();
            //break;
          case AdsDatatypeId.ADST_STRING:
          case AdsDatatypeId.ADST_WSTRING:
            value = subscriptionData.BinaryReader.ReadString();
            break;
          //case AdsDatatypeId.ADST_REAL80:
            //value = subscriptionData.BinaryReader.ReadDecimal();
            //break;
          case AdsDatatypeId.ADST_BIT:
            value = subscriptionData.BinaryReader.ReadBoolean();
            break;
          //case AdsDatatypeId.ADST_MAXTYPES:
            //break;
        }

        if (value != null)
          SubscriptionNotification?.Invoke(this, (subscriptionData.SymbolPath, value));
      }
    }

    #endregion
  }
}