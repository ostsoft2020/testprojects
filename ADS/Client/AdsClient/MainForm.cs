﻿using System;
using System.IO;
using System.Windows.Forms;
using TwinCAT.Ads;
using TwinCAT.Ads.TypeSystem;
using TwinCAT.TypeSystem;

namespace AdsClient
{
  public partial class MainForm : Form
  {
    private AdsCommunicator communicator;

    public MainForm()
    {
      InitializeComponent();
    }

    private void addSubnodes(TreeNode parentNode, ISymbol parentSymbol)
    {
      foreach (ISymbol symbol in parentSymbol.SubSymbols)
      {
        TreeNode node = parentNode.Nodes.Add(symbol.InstancePath);
        addSubnodes(node, symbol);
      }
    }

    private void testButton_Click(object sender, EventArgs e)
    {
      try
      {
        if (communicator == null)
        {
          if (!int.TryParse(portTextBox.Text, out int portNumber))
            throw new Exception("AMS port is in incorrect format");

          AmsAddress address = new AmsAddress(netIdTextBox.Text, portNumber);
          //AmsAddress addressLocal = new AmsAddress("Local", portNumber);

          communicator = new AdsCommunicator();

          communicator.Connect(address, SessionSettings.Default);

          communicator.SubscriptionNotification += communicator_SubscriptionNotification;
        }

        short intValue = (short)communicator.ReadValue("MAIN.Object.VarInt");
        string stringValue = (string)communicator.ReadValue("MAIN.StringVariable");
        string wstringValue = (string)communicator.ReadValue("MAIN.WStringVariable");

        communicator.WriteValue("MAIN.Object.VarInt", (ushort)(intValue + 1));
        communicator.WriteValue("MAIN.StringVariable", stringValue + " +");
        communicator.WriteValue("MAIN.WStringVariable", wstringValue + " +");

        communicator.Subscribe("MAIN.Counter");
        communicator.Subscribe("MAIN.Flag");
      }
      catch (Exception err)
      {
        MessageBox.Show("Error occured: " + err.Message);
      }


      treeView.Nodes.Clear();

      try
      {
        if (!int.TryParse(portTextBox.Text, out int portNumber))
          throw new Exception("AMS port is in incorrect format");

        using (TcAdsClient adsClient = new TcAdsClient())
        {
          // PLC1 Port - TwinCAT 3 = 851
          adsClient.Connect(netIdTextBox.Text, portNumber);

          var deviceInfo = adsClient.ReadDeviceInfo();

          ISymbolLoader loader = SymbolLoaderFactory.Create(adsClient, new SymbolLoaderSettings(TwinCAT.SymbolsLoadMode.VirtualTree));

          foreach (ISymbol symbol in loader.Symbols)
          {
            TreeNode node = treeView.Nodes.Add(symbol.InstancePath);
            addSubnodes(node, symbol);
          }

          //adsClient.Read()

          /*int varHandle = adsClient.CreateVariableHandle("MAIN.StringVariable");

          //length of the stream = length of string in sps + 1
          AdsStream adsStream = new AdsStream(1001);
          BinaryReader reader = new BinaryReader(adsStream, System.Text.Encoding.ASCII);

          int length = adsClient.Read(varHandle, adsStream);

          char[] chars = reader.ReadChars(length);

          string text = new string(chars);

          //necessary if you want to compare the string to other strings
          //text = text.Substring(0,text.IndexOf('\0'));

          MessageBox.Show("MAIN.StringVariable = " + text);*/
        }
      }
      catch (Exception err)
      {
        MessageBox.Show("Error occured: " + err.Message);
      }
    }

    private void communicator_SubscriptionNotification(object sender, (string SymbolPath, object Value) e)
    {
      Invoke(
        (MethodInvoker) delegate()
        {
          switch (e.SymbolPath)
          {
            case "MAIN.Counter":
              counterTextBox.Text = e.Value.ToString();
              break;

            case "MAIN.Flag":
              flagCheckBox.Checked = (bool)e.Value;
              break;
          }
        }
      );
    }
  }
}
