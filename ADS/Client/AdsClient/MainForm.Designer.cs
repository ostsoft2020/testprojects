﻿namespace AdsClient
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.testButton = new System.Windows.Forms.Button();
			this.portTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.netIdTextBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.treeView = new System.Windows.Forms.TreeView();
			this.label3 = new System.Windows.Forms.Label();
			this.counterTextBox = new System.Windows.Forms.TextBox();
			this.flagCheckBox = new System.Windows.Forms.CheckBox();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.flagCheckBox);
			this.panel1.Controls.Add(this.counterTextBox);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.testButton);
			this.panel1.Controls.Add(this.portTextBox);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.netIdTextBox);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(800, 41);
			this.panel1.TabIndex = 1;
			// 
			// testButton
			// 
			this.testButton.Location = new System.Drawing.Point(366, 10);
			this.testButton.Name = "testButton";
			this.testButton.Size = new System.Drawing.Size(75, 20);
			this.testButton.TabIndex = 4;
			this.testButton.Text = "Connect";
			this.testButton.UseVisualStyleBackColor = true;
			this.testButton.Click += new System.EventHandler(this.testButton_Click);
			// 
			// portTextBox
			// 
			this.portTextBox.Location = new System.Drawing.Point(295, 10);
			this.portTextBox.Name = "portTextBox";
			this.portTextBox.Size = new System.Drawing.Size(34, 20);
			this.portTextBox.TabIndex = 3;
			this.portTextBox.Text = "851";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(234, 13);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(55, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "AMS Port:";
			// 
			// netIdTextBox
			// 
			this.netIdTextBox.Location = new System.Drawing.Point(83, 10);
			this.netIdTextBox.Name = "netIdTextBox";
			this.netIdTextBox.Size = new System.Drawing.Size(124, 20);
			this.netIdTextBox.TabIndex = 1;
			this.netIdTextBox.Text = "Local";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(13, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(69, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "AMS NET id:";
			// 
			// treeView
			// 
			this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeView.Location = new System.Drawing.Point(0, 41);
			this.treeView.Name = "treeView";
			this.treeView.Size = new System.Drawing.Size(800, 409);
			this.treeView.TabIndex = 2;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(499, 13);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(47, 13);
			this.label3.TabIndex = 5;
			this.label3.Text = "Counter:";
			// 
			// counterTextBox
			// 
			this.counterTextBox.Location = new System.Drawing.Point(548, 10);
			this.counterTextBox.Name = "counterTextBox";
			this.counterTextBox.Size = new System.Drawing.Size(57, 20);
			this.counterTextBox.TabIndex = 6;
			// 
			// flagCheckBox
			// 
			this.flagCheckBox.AutoSize = true;
			this.flagCheckBox.Location = new System.Drawing.Point(632, 12);
			this.flagCheckBox.Name = "flagCheckBox";
			this.flagCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
			this.flagCheckBox.Size = new System.Drawing.Size(49, 17);
			this.flagCheckBox.TabIndex = 7;
			this.flagCheckBox.Text = ":Flag";
			this.flagCheckBox.UseVisualStyleBackColor = true;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.treeView);
			this.Controls.Add(this.panel1);
			this.Name = "MainForm";
			this.Text = "Form1";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TreeView treeView;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox netIdTextBox;
		private System.Windows.Forms.TextBox portTextBox;
		private System.Windows.Forms.Button testButton;
		private System.Windows.Forms.CheckBox flagCheckBox;
		private System.Windows.Forms.TextBox counterTextBox;
		private System.Windows.Forms.Label label3;
	}
}

