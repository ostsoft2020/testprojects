﻿using TCatSysManagerLib;
using TestTwinCATAutomation;

internal class Program
{
    private static void Main(string[] args)
    {
		MessageFilter.Register();

		//bool showHelp = false;
		string visualStudioFilePath = "D:\\PLC\\TestProjects\\OpcUaTest\\OpcUaTest.sln";
        string amsNetId = "127.0.0.1.1.1";

        /*OptionSet options = new OptionSet()
          .Add("h|?|help", delegate (string v) { showHelp = v != null; })
          .Add("f|vsFilePath", delegate (string v) { visualStudioFilePath = v; })
          .Add("a|amsNetId", delegate (string v) { amsNetId = v; }
        );

        try
        {
            options.Parse(args);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.GetBaseException().Message);
            Environment.Exit(1);
        }

        if (showHelp)
        {
            Console.WriteLine("Usage: TestTwinCATAutomation [OPTIONS]");
            options.WriteOptionDescriptions(Console.Out);
            Environment.Exit(0);
        }

        if (!File.Exists(visualStudioFilePath))
        {
            Console.WriteLine("Visual Studio solution does not exists");
            Environment.Exit(1);
        }

        if (string.IsNullOrWhiteSpace(visualStudioFilePath))
        {
            Console.WriteLine("No AmsNetId provides, assuming local one");
            amsNetId = "127.0.0.1.1.1";
        }*/

        Type vsType = Type.GetTypeFromProgID("TcXaeShell.DTE.15.0");

        if (vsType == null)
        {
            Console.WriteLine("TcXaeShell.DTE.15.0 was not found");
			terminate(1);
		}

		EnvDTE.DTE dte = (EnvDTE.DTE) Activator.CreateInstance(vsType);

        if (dte == null)
        {
            Console.WriteLine("DTE object was not created");
            terminate(1);
        }

        dte.SuppressUI = true;// false;
        dte.MainWindow.Visible = true;

        EnvDTE.Solution solution = dte.Solution;
        solution.Open(visualStudioFilePath);

        EnvDTE.Project project = solution.Projects.Item(1);

        ITcSysManager15 sysManager = (ITcSysManager15) project.Object;

        //$plcPrjProjectName = PathToPlcProjFile
        solution.SolutionBuild.Clean(true);
        solution.SolutionBuild.BuildProject("Release|TwinCAT RT (x64)", project.FullName, true);
        //solution.SolutionBuild.BuildProject("Release|TwinCAT RT (x64)", $plcPrjProjectName, true);

        sysManager.ActivateConfiguration();
        sysManager.StartRestartTwinCAT();

        Console.WriteLine("Press any cay to exit...");

        terminate(0);
    }

    private static void terminate(int exitCode)
    {
		MessageFilter.Revoke();

		Environment.Exit(exitCode);
	}
}