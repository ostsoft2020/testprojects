/* ========================================================================
 * Copyright (c) 2005-2019 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 * 
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

namespace Quickstarts.DataAccessClient
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.MenuBar = new System.Windows.Forms.MenuStrip();
			this.fILEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
			this.buildToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
			this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ServerMI = new System.Windows.Forms.ToolStripMenuItem();
			this.Server_DiscoverMI = new System.Windows.Forms.ToolStripMenuItem();
			this.Server_ConnectMI = new System.Windows.Forms.ToolStripMenuItem();
			this.Server_DisconnectMI = new System.Windows.Forms.ToolStripMenuItem();
			this.Server_SetLocaleMI = new System.Windows.Forms.ToolStripMenuItem();
			this.Server_SetUserMI = new System.Windows.Forms.ToolStripMenuItem();
			this.HelpMI = new System.Windows.Forms.ToolStripMenuItem();
			this.Help_ContentsMI = new System.Windows.Forms.ToolStripMenuItem();
			this.StatusBar = new System.Windows.Forms.StatusStrip();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.ConnectServerCTRL = new Opc.Ua.Client.Controls.ConnectServerCtrl();
			this.applySelectionButton = new System.Windows.Forms.Button();
			this.MainPN = new System.Windows.Forms.SplitContainer();
			this.topSplitContainer = new System.Windows.Forms.SplitContainer();
			this.browseNodesTreeView = new System.Windows.Forms.TreeView();
			this.selectedNodesTreeView = new System.Windows.Forms.TreeView();
			this.AttributesLV = new System.Windows.Forms.ListView();
			this.AttributeNameCH = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.AttributeDataTypeCH = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.AttributeValueCH = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.MenuBar.SuspendLayout();
			this.tableLayoutPanel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.MainPN)).BeginInit();
			this.MainPN.Panel1.SuspendLayout();
			this.MainPN.Panel2.SuspendLayout();
			this.MainPN.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.topSplitContainer)).BeginInit();
			this.topSplitContainer.Panel1.SuspendLayout();
			this.topSplitContainer.Panel2.SuspendLayout();
			this.topSplitContainer.SuspendLayout();
			this.SuspendLayout();
			// 
			// MenuBar
			// 
			this.MenuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fILEToolStripMenuItem,
            this.ServerMI,
            this.HelpMI});
			this.MenuBar.Location = new System.Drawing.Point(0, 0);
			this.MenuBar.Name = "MenuBar";
			this.MenuBar.Size = new System.Drawing.Size(884, 24);
			this.MenuBar.TabIndex = 1;
			this.MenuBar.Text = "menuStrip1";
			// 
			// fILEToolStripMenuItem
			// 
			this.fILEToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openProjectToolStripMenuItem,
            this.saveProjectToolStripMenuItem,
            this.toolStripMenuItem2,
            this.buildToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
			this.fILEToolStripMenuItem.Name = "fILEToolStripMenuItem";
			this.fILEToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fILEToolStripMenuItem.Text = "&File";
			// 
			// openProjectToolStripMenuItem
			// 
			this.openProjectToolStripMenuItem.Name = "openProjectToolStripMenuItem";
			this.openProjectToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
			this.openProjectToolStripMenuItem.Text = "Open Project";
			// 
			// saveProjectToolStripMenuItem
			// 
			this.saveProjectToolStripMenuItem.Name = "saveProjectToolStripMenuItem";
			this.saveProjectToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
			this.saveProjectToolStripMenuItem.Text = "Save Project";
			// 
			// toolStripMenuItem2
			// 
			this.toolStripMenuItem2.Name = "toolStripMenuItem2";
			this.toolStripMenuItem2.Size = new System.Drawing.Size(140, 6);
			// 
			// buildToolStripMenuItem
			// 
			this.buildToolStripMenuItem.Name = "buildToolStripMenuItem";
			this.buildToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
			this.buildToolStripMenuItem.Text = "Build";
			// 
			// toolStripMenuItem1
			// 
			this.toolStripMenuItem1.Name = "toolStripMenuItem1";
			this.toolStripMenuItem1.Size = new System.Drawing.Size(140, 6);
			// 
			// exitToolStripMenuItem
			// 
			this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
			this.exitToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
			this.exitToolStripMenuItem.Text = "E&xit";
			this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
			// 
			// ServerMI
			// 
			this.ServerMI.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Server_DiscoverMI,
            this.Server_ConnectMI,
            this.Server_DisconnectMI,
            this.Server_SetLocaleMI,
            this.Server_SetUserMI});
			this.ServerMI.Name = "ServerMI";
			this.ServerMI.Size = new System.Drawing.Size(51, 20);
			this.ServerMI.Text = "Server";
			// 
			// Server_DiscoverMI
			// 
			this.Server_DiscoverMI.Name = "Server_DiscoverMI";
			this.Server_DiscoverMI.Size = new System.Drawing.Size(151, 22);
			this.Server_DiscoverMI.Text = "Discover...";
			this.Server_DiscoverMI.Click += new System.EventHandler(this.Server_DiscoverMI_Click);
			// 
			// Server_ConnectMI
			// 
			this.Server_ConnectMI.Name = "Server_ConnectMI";
			this.Server_ConnectMI.Size = new System.Drawing.Size(151, 22);
			this.Server_ConnectMI.Text = "Connect";
			this.Server_ConnectMI.Click += new System.EventHandler(this.Server_ConnectMI_ClickAsync);
			// 
			// Server_DisconnectMI
			// 
			this.Server_DisconnectMI.Name = "Server_DisconnectMI";
			this.Server_DisconnectMI.Size = new System.Drawing.Size(151, 22);
			this.Server_DisconnectMI.Text = "Disconnect";
			this.Server_DisconnectMI.Click += new System.EventHandler(this.Server_DisconnectMI_Click);
			// 
			// Server_SetLocaleMI
			// 
			this.Server_SetLocaleMI.Name = "Server_SetLocaleMI";
			this.Server_SetLocaleMI.Size = new System.Drawing.Size(151, 22);
			this.Server_SetLocaleMI.Text = "Select Locale...";
			this.Server_SetLocaleMI.Click += new System.EventHandler(this.Server_SetLocaleMI_Click);
			// 
			// Server_SetUserMI
			// 
			this.Server_SetUserMI.Name = "Server_SetUserMI";
			this.Server_SetUserMI.Size = new System.Drawing.Size(151, 22);
			this.Server_SetUserMI.Text = "Set User...";
			this.Server_SetUserMI.Click += new System.EventHandler(this.Server_SetUserMI_Click);
			// 
			// HelpMI
			// 
			this.HelpMI.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Help_ContentsMI});
			this.HelpMI.Name = "HelpMI";
			this.HelpMI.Size = new System.Drawing.Size(44, 20);
			this.HelpMI.Text = "Help";
			// 
			// Help_ContentsMI
			// 
			this.Help_ContentsMI.Name = "Help_ContentsMI";
			this.Help_ContentsMI.Size = new System.Drawing.Size(122, 22);
			this.Help_ContentsMI.Text = "Contents";
			this.Help_ContentsMI.Click += new System.EventHandler(this.Help_ContentsMI_Click);
			// 
			// StatusBar
			// 
			this.StatusBar.Location = new System.Drawing.Point(0, 524);
			this.StatusBar.Name = "StatusBar";
			this.StatusBar.Size = new System.Drawing.Size(884, 22);
			this.StatusBar.TabIndex = 2;
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 89.70588F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.29412F));
			this.tableLayoutPanel1.Controls.Add(this.ConnectServerCTRL, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.applySelectionButton, 1, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 24);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 1;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(884, 29);
			this.tableLayoutPanel1.TabIndex = 5;
			// 
			// ConnectServerCTRL
			// 
			this.ConnectServerCTRL.Configuration = null;
			this.ConnectServerCTRL.DisableDomainCheck = false;
			this.ConnectServerCTRL.Dock = System.Windows.Forms.DockStyle.Top;
			this.ConnectServerCTRL.Location = new System.Drawing.Point(3, 3);
			this.ConnectServerCTRL.MaximumSize = new System.Drawing.Size(2048, 23);
			this.ConnectServerCTRL.MinimumSize = new System.Drawing.Size(500, 23);
			this.ConnectServerCTRL.Name = "ConnectServerCTRL";
			this.ConnectServerCTRL.PreferredLocales = null;
			this.ConnectServerCTRL.ServerUrl = "";
			this.ConnectServerCTRL.SessionName = null;
			this.ConnectServerCTRL.Size = new System.Drawing.Size(787, 23);
			this.ConnectServerCTRL.StatusStrip = this.StatusBar;
			this.ConnectServerCTRL.TabIndex = 5;
			this.ConnectServerCTRL.UserIdentity = null;
			this.ConnectServerCTRL.UseSecurity = true;
			this.ConnectServerCTRL.ReconnectStarting += new System.EventHandler(this.Server_ReconnectStarting);
			this.ConnectServerCTRL.ReconnectComplete += new System.EventHandler(this.Server_ReconnectComplete);
			this.ConnectServerCTRL.ConnectComplete += new System.EventHandler(this.Server_ConnectComplete);
			// 
			// applySelectionButton
			// 
			this.applySelectionButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.applySelectionButton.Location = new System.Drawing.Point(796, 3);
			this.applySelectionButton.Name = "applySelectionButton";
			this.applySelectionButton.Size = new System.Drawing.Size(85, 23);
			this.applySelectionButton.TabIndex = 6;
			this.applySelectionButton.Text = "Apply";
			this.applySelectionButton.UseVisualStyleBackColor = true;
			this.applySelectionButton.Click += new System.EventHandler(this.applySelectionButton_Click);
			// 
			// MainPN
			// 
			this.MainPN.Dock = System.Windows.Forms.DockStyle.Fill;
			this.MainPN.Location = new System.Drawing.Point(0, 53);
			this.MainPN.Name = "MainPN";
			this.MainPN.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// MainPN.Panel1
			// 
			this.MainPN.Panel1.Controls.Add(this.topSplitContainer);
			// 
			// MainPN.Panel2
			// 
			this.MainPN.Panel2.Controls.Add(this.AttributesLV);
			this.MainPN.Size = new System.Drawing.Size(884, 471);
			this.MainPN.SplitterDistance = 324;
			this.MainPN.TabIndex = 6;
			this.MainPN.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BrowseNodesTV_MouseDown);
			// 
			// topSplitContainer
			// 
			this.topSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.topSplitContainer.Location = new System.Drawing.Point(0, 0);
			this.topSplitContainer.Name = "topSplitContainer";
			// 
			// topSplitContainer.Panel1
			// 
			this.topSplitContainer.Panel1.Controls.Add(this.browseNodesTreeView);
			this.topSplitContainer.Panel1MinSize = 150;
			// 
			// topSplitContainer.Panel2
			// 
			this.topSplitContainer.Panel2.Controls.Add(this.selectedNodesTreeView);
			this.topSplitContainer.Panel2MinSize = 150;
			this.topSplitContainer.Size = new System.Drawing.Size(884, 324);
			this.topSplitContainer.SplitterDistance = 469;
			this.topSplitContainer.TabIndex = 0;
			this.topSplitContainer.Layout += new System.Windows.Forms.LayoutEventHandler(this.topSplitContainer_Layout);
			this.topSplitContainer.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BrowseNodesTV_MouseDown);
			this.topSplitContainer.Resize += new System.EventHandler(this.TopPN_Resize);
			// 
			// browseNodesTreeView
			// 
			this.browseNodesTreeView.CheckBoxes = true;
			this.browseNodesTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.browseNodesTreeView.Location = new System.Drawing.Point(0, 0);
			this.browseNodesTreeView.Name = "browseNodesTreeView";
			this.browseNodesTreeView.Size = new System.Drawing.Size(469, 324);
			this.browseNodesTreeView.TabIndex = 0;
			this.browseNodesTreeView.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.BrowseNodesTV_BeforeExpand);
			this.browseNodesTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.BrowseNodesTV_AfterSelect);
			this.browseNodesTreeView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BrowseNodesTV_MouseDown);
			// 
			// selectedNodesTreeView
			// 
			this.selectedNodesTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.selectedNodesTreeView.Location = new System.Drawing.Point(0, 0);
			this.selectedNodesTreeView.Name = "selectedNodesTreeView";
			this.selectedNodesTreeView.Size = new System.Drawing.Size(411, 324);
			this.selectedNodesTreeView.TabIndex = 0;
			// 
			// AttributesLV
			// 
			this.AttributesLV.BackColor = System.Drawing.Color.White;
			this.AttributesLV.BackgroundImageTiled = true;
			this.AttributesLV.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.AttributeNameCH,
            this.AttributeDataTypeCH,
            this.AttributeValueCH});
			this.AttributesLV.Dock = System.Windows.Forms.DockStyle.Fill;
			this.AttributesLV.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.AttributesLV.ForeColor = System.Drawing.SystemColors.WindowText;
			this.AttributesLV.FullRowSelect = true;
			this.AttributesLV.HideSelection = false;
			this.AttributesLV.Location = new System.Drawing.Point(0, 0);
			this.AttributesLV.Name = "AttributesLV";
			this.AttributesLV.Size = new System.Drawing.Size(884, 143);
			this.AttributesLV.TabIndex = 1;
			this.AttributesLV.UseCompatibleStateImageBehavior = false;
			this.AttributesLV.View = System.Windows.Forms.View.Details;
			this.AttributesLV.MouseDown += new System.Windows.Forms.MouseEventHandler(this.BrowseNodesTV_MouseDown);
			// 
			// AttributeNameCH
			// 
			this.AttributeNameCH.Text = "Name";
			// 
			// AttributeDataTypeCH
			// 
			this.AttributeDataTypeCH.DisplayIndex = 2;
			this.AttributeDataTypeCH.Text = "Data Type";
			this.AttributeDataTypeCH.Width = 102;
			// 
			// AttributeValueCH
			// 
			this.AttributeValueCH.DisplayIndex = 1;
			this.AttributeValueCH.Text = "Value";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(884, 546);
			this.Controls.Add(this.MainPN);
			this.Controls.Add(this.tableLayoutPanel1);
			this.Controls.Add(this.StatusBar);
			this.Controls.Add(this.MenuBar);
			this.MainMenuStrip = this.MenuBar;
			this.Name = "MainForm";
			this.Text = "OPC-UA Client Modeler";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.MenuBar.ResumeLayout(false);
			this.MenuBar.PerformLayout();
			this.tableLayoutPanel1.ResumeLayout(false);
			this.MainPN.Panel1.ResumeLayout(false);
			this.MainPN.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.MainPN)).EndInit();
			this.MainPN.ResumeLayout(false);
			this.topSplitContainer.Panel1.ResumeLayout(false);
			this.topSplitContainer.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.topSplitContainer)).EndInit();
			this.topSplitContainer.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuBar;
        private System.Windows.Forms.StatusStrip StatusBar;
        private System.Windows.Forms.ToolStripMenuItem ServerMI;
        private System.Windows.Forms.ToolStripMenuItem Server_DiscoverMI;
        private System.Windows.Forms.ToolStripMenuItem Server_ConnectMI;
        private System.Windows.Forms.ToolStripMenuItem Server_DisconnectMI;
        private System.Windows.Forms.ToolStripMenuItem HelpMI;
        private System.Windows.Forms.ToolStripMenuItem Help_ContentsMI;
        private System.Windows.Forms.ToolStripMenuItem Server_SetLocaleMI;
        private System.Windows.Forms.ToolStripMenuItem Server_SetUserMI;
        private System.Windows.Forms.ToolStripMenuItem fILEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private Opc.Ua.Client.Controls.ConnectServerCtrl ConnectServerCTRL;
		private System.Windows.Forms.Button applySelectionButton;
		private System.Windows.Forms.SplitContainer MainPN;
		private System.Windows.Forms.SplitContainer topSplitContainer;
		private System.Windows.Forms.TreeView browseNodesTreeView;
		private System.Windows.Forms.TreeView selectedNodesTreeView;
		private System.Windows.Forms.ListView AttributesLV;
		private System.Windows.Forms.ColumnHeader AttributeNameCH;
		private System.Windows.Forms.ColumnHeader AttributeDataTypeCH;
		private System.Windows.Forms.ColumnHeader AttributeValueCH;
		private System.Windows.Forms.ToolStripMenuItem openProjectToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveProjectToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
		private System.Windows.Forms.ToolStripMenuItem buildToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
	}
}
