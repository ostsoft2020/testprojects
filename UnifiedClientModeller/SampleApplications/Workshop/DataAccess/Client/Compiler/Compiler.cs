﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;

namespace UnifiedStudio
{
	public static class Compiler
	{
        public const string TestSourceCode = @"
using System;

namespace UnifiedStudio.Compiler
{
	public class TestSourceCode
	{
        class Program
        {
            public static void Main(string[] args)
            {
                Console.WriteLine(""Hello from dynamically build assembly! Press Enter key to exit"");
                Console.ReadLine();
            }
        }
    }
}";

        public static string[] CompileWithCodeDom(string sourceCode, string assemblyName, bool generateExecutable)
        {
            var csc = new CSharpCodeProvider(new Dictionary<string, string>() { { "CompilerVersion", "v3.5" } });

            var parameters = new CompilerParameters(new[] { "mscorlib.dll", "System.Core.dll" }, assemblyName + (generateExecutable ? ".exe" : ".dll"), true) 
            { 
                GenerateExecutable = generateExecutable 
            };

            CompilerResults results = csc.CompileAssemblyFromSource(parameters, sourceCode);

            return results.Errors.Cast<CompilerError>().Select(ce => ce.ErrorText).ToArray();
        }

        public static string[] CompileWithRolsyn(string sourceCode, string assemblyName, bool generateExecutable)
		{
            var tree = CSharpSyntaxTree.ParseText(sourceCode);

            var mscorlib = MetadataReference.CreateFromFile(typeof(object).Assembly.Location);

            var compilation = CSharpCompilation.Create(assemblyName, syntaxTrees: new[] { tree }, references: new[] { mscorlib });

            //Emitting to file is available through an extension method in the Microsoft.CodeAnalysis namespace
            var emitResult = compilation.Emit(assemblyName + (generateExecutable ? ".exe" : ".dll"), assemblyName + ".pdb");

            //If our compilation failed, we can discover exactly why.
            if (!emitResult.Success)
                return emitResult.Diagnostics.Select(d => d.ToString()).ToArray();
            else
                return new string[0];
        }
	}
}
