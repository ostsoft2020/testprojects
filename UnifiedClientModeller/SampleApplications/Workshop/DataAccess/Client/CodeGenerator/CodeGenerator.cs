﻿using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using System.Text;

namespace UnifiedStudio
{
	public class CodeGenerator
	{
		public readonly string Name;

		public readonly NodeType NodeType;

		public readonly List<CodeGenerator> Children = new List<CodeGenerator>();

		public CodeGenerator(TreeView treeView)
		{
			Name = "Root";
			NodeType = NodeType.Class;

			foreach (TreeNode subNode in treeView.Nodes)
				Children.Add(new CodeGenerator(subNode));
		}

		protected CodeGenerator(TreeNode node)
		{
			Name = node.Text;

			if (node.Nodes.Count > 0)
			{
				NodeType = NodeType.Class;

				foreach (TreeNode subNode in node.Nodes)
					Children.Add(new CodeGenerator(subNode));
			}
			else
				NodeType = NodeType.Property;
		}

		public string GetDefinition()
		{
			if (NodeType == NodeType.Property)
				return null;

			StringBuilder result = new StringBuilder();

			result.AppendLine(string.Join("\n\n", Children.Select(c => c.GetDefinition()).ToArray()));

			result.AppendLine("public class " + Name);
			result.AppendLine("{");
			result.AppendLine(string.Join("\n\n", Children.Select(c => "\t" + c.GetInstanceDeclaration(false)).ToArray()));
			result.AppendLine("}");

			return result.ToString();
		}

		public string GetInstanceDeclaration(bool isStatic)
		{
			string typeName = (NodeType == NodeType.Class) ? Name : "object";

			return "public " + (isStatic ? " static " : "") + typeName + " " + Name + " { get; set; }";
		}
	}
}
