﻿using Microsoft.Win32;

internal class Program
{
	static void Main()
	{
		string[] searchStrings = ["Beckhoff", "TwinCAT"];

		try
		{
			uint removedKeys = SearchRegistry(Registry.LocalMachine, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\S-1-5-18\\Components", searchStrings)
				+ SearchRegistry(Registry.LocalMachine, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Installer\\UserData\\S-1-5-21-2413138205-2987972585-2517883383-3144\\Components", searchStrings)
				+ SearchRegistry(Registry.LocalMachine, "SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall", searchStrings);


            Console.WriteLine(removedKeys + " keys removed");
		}
		catch (Exception ex)  
		{
			Console.WriteLine(ex.GetBaseException().Message);
		}
    }

  //  
//    

    static uint SearchRegistry(RegistryKey root, string pathToAnalyse, params string[] searchStrings)
	{
		uint result = 0;

		string[] pathParts = pathToAnalyse?.Split("\\");

		RegistryKey componentsKey = root;

        if (pathParts != null)
			for (int i = 0; i < pathParts.Length; i++)
				try
				{
                    componentsKey = componentsKey.OpenSubKey(pathParts[i], i == pathParts.Length - 1);
                }
				catch
				{
					return 0;
				}

        foreach (string componentsSubkeyName in componentsKey?.GetSubKeyNames())
		{
			try
			{
				using (RegistryKey subKey = componentsKey.OpenSubKey(componentsSubkeyName))
				{
					if (subKey == null)
						continue;

					foreach (string subkeyValueName in subKey.GetValueNames())
						if (RegistryValueKind.String == subKey.GetValueKind(subkeyValueName))
						{
							string stringValue = (string)subKey.GetValue(subkeyValueName);

							foreach (string searchString in searchStrings)
								if (stringValue.Contains(searchString))
								{
									subKey.Close();

									try
									{
										componentsKey.DeleteSubKeyTree(componentsSubkeyName);
										result++;

									}
									catch
									{
									}

									goto nextCycle;
								}

						}

					subKey.Close();
				}
			}
			catch { }

			nextCycle:;
        }

		return result;
	}
}