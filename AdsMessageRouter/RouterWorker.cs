﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;
using TwinCAT.Ads.TcpRouter;

namespace AdsMessageRouter
{
    public class RouterWorker : BackgroundService
    {
        private readonly ILogger<RouterWorker> logger;
        private IConfiguration configuration;

        public RouterWorker(IConfiguration configuration, ILogger<RouterWorker> logger)
        {
            this.configuration = configuration;
            this.logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken cancel)
        {
            AmsTcpIpRouter router = new AmsTcpIpRouter(logger, configuration);

            //Use this overload to instantiate a Router without support of StaticRoutes.xml and parametrize by code
            //AmsTcpIpRouter router = new AmsTcpIpRouter(new AmsNetId("1.2.3.4.5.6"), AmsTcpIpRouter.DEFAULT_TCP_PORT, _logger);
            //router.AddRoute(...);

            await router.StartAsync(cancel); // Start the router
        }
    }
}
