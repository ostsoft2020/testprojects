﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using TwinCAT.Ads.TcpRouter;

namespace AdsMessageRouter
{
	class Program
	{
		static void Main(string[] args)
		{
			CreateHostBuilder(args).Build().Run();
		}

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<RouterWorker>();
                })
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    // Uncomment to overwrite configuration
                    //config.Sources.Clear(); // Clear all default config sources 
                    //config.AddEnvironmentVariables("AmsRouter"); // Use Environment variables
                    //config.AddCommandLine(args); // Use Command Line
                    //config.AddJsonFile("appSettings.json"); // Use Appsettings
                    config.AddStaticRoutesXmlConfiguration(); // Overriding settings with StaticRoutes.Xml 
                })
                .ConfigureLogging(logging =>
                {
                    // Uncomment to overwrite logging
                    // Microsoft.Extensions.Logging.Console Nuget package
                    // Namespace Microsoft.Extensions.Logging;
                    //logging.ClearProviders();
                    //logging.AddConsole();
                })
            ;
    }
}
