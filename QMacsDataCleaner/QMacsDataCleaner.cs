﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QMacsDataCleaner
{
	public partial class QMacsDataCleaner : Form
	{
		public QMacsDataCleaner()
		{
			InitializeComponent();

			authenticationComboBox.SelectedIndex = 0;
		}

		private void authenticationComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			userTextBox.Enabled = authenticationComboBox.SelectedIndex == 1;
			passwordTextBox.Enabled = authenticationComboBox.SelectedIndex == 1;
		}

		private async void sweepButton_Click(object sender, EventArgs e)
		{
			string connectionString = buildConnectionstring();

			if (DialogResult.Yes != MessageBox.Show("Are you sure you want to clear old data from database with connection string: \r\n" + connectionString, "Attention!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
				return;

			progressPanel.BringToFront();
			progressPanel.Visible = true;

			int leaveDays = (int)leaveDaysNumericUpDown.Value;

			Task<string> task = new Task<string>(
				() =>
				{
					try
					{
						using (SqlConnection connection = new SqlConnection(connectionString))
						{
							connection.Open();

							sweepDatabase(connection, leaveDays);

							connection.Close();
						}

						return "Data cleaning is complete";
					}
					catch (Exception ex)
					{
						return ex.GetBaseException().Message;
					}
				}
			);

			task.Start();

			do
			{
				progressBar.Value = progressBar.Value < progressBar.Maximum - 1 ? progressBar.Value + 1 : progressBar.Minimum;

				await Task.Delay(100);
			}
			while (!task.IsCompleted);

			progressPanel.Visible = false;
			progressPanel.SendToBack();

			MessageBox.Show(task.Result);
		}

		private void sweepDatabase(SqlConnection connection, int daysToLeave)
		{
			DateTime date = (DateTime) new SqlCommand("SELECT CAST(COALESCE(MAX(Finish), GETDATE()) AS DATE) FROM WeldingRecord", connection).ExecuteScalar();

			date = date.AddDays(-daysToLeave);

			int rowCount;

			do
				using (SqlTransaction transaction = connection.BeginTransaction())
				{
					using (var command = new SqlCommand("DELETE FROM MeasurementBlock WHERE Id IN (SELECT TOP 10000 B.Id FROM MeasurementBlock B JOIN WeldingRecord R ON B.WeldingRecordId = R.Id WHERE R.Finish < @Date); SELECT @@ROWCOUNT", connection, transaction))
					{
						command.Parameters.Add(new SqlParameter("@Date", date));
						rowCount = Convert.ToInt32(command.ExecuteScalar());
					}

					transaction.Commit();
				}
			while (rowCount > 0);

			do
				using (SqlTransaction transaction = connection.BeginTransaction())
				{
					using (var command = new SqlCommand("DELETE FROM WeldingRecord WHERE Id IN (SELECT TOP 10000 R.Id FROM WeldingRecord R WHERE R.Finish < @Date OR (Finish IS NULL AND Start < @Date)); SELECT @@ROWCOUNT", connection, transaction))
					{
						command.Parameters.Add(new SqlParameter("@Date", date));
						rowCount = Convert.ToInt32(command.ExecuteScalar());
					}

					transaction.Commit();
				}
			while (rowCount > 0);

			using (SqlTransaction transaction = connection.BeginTransaction())
			{
				using (var command = new SqlCommand("DELETE FROM R FROM WeldingRecord R LEFT JOIN MeasurementBlock B ON B.WeldingRecordId = R.Id WHERE B.Id IS NULL", connection, transaction))
					command.ExecuteNonQuery();

				transaction.Commit();
			}

			using (SqlTransaction transaction = connection.BeginTransaction())
			{
				using (var command = new SqlCommand("DELETE FROM MeasurementBlock WHERE WeldingRecordId IS NULL OR (Finish IS NULL AND Start < @Date)", connection, transaction))
				{
					command.Parameters.Add(new SqlParameter("@Date", date));
					command.ExecuteNonQuery();
				}

				transaction.Commit();
			}

			using (SqlTransaction transaction = connection.BeginTransaction())
			{
				using (var command = new SqlCommand(
					"DELETE FROM WRWeldPass WHERE Id <> 0 AND NOT EXISTS(SELECT * FROM WeldingRecord WR WHERE WR.WeldPassId = WRWeldPass.Id);" +
					"DELETE FROM WRSheduledTask WHERE Id <> 0 AND NOT EXISTS(SELECT * FROM WeldingRecord WR WHERE WR.SheduledTaskId = WRSheduledTask.Id);" +
					"DELETE FROM WRWire WHERE Id <> 0 AND NOT EXISTS(SELECT * FROM WeldingRecord WR WHERE WR.WireId = WRWire.Id);" +
					"DELETE FROM WRWorkpieceJoint WHERE Id <> 0 AND NOT EXISTS(SELECT * FROM WeldingRecord WR WHERE WR.WorkpieceJointId = WRWorkpieceJoint.Id);", 
					connection, 
					transaction)
				)
					command.ExecuteNonQuery();

				transaction.Commit();
			}

			using (var command = new SqlCommand("DECLARE @DbName VARCHAR(32) = DB_NAME(); DBCC SHRINKDATABASE(@DbName, 0)",	connection))
				command.ExecuteNonQuery();
		}

		private string buildConnectionstring()
		{
			SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();

			connectionStringBuilder.DataSource = serverTextBox.Text;
			connectionStringBuilder.TrustServerCertificate = true;

			if (authenticationComboBox.SelectedIndex == 0)
			{
				connectionStringBuilder.Authentication = SqlAuthenticationMethod.ActiveDirectoryIntegrated;
			}
			else
			{
				connectionStringBuilder.Authentication = SqlAuthenticationMethod.ActiveDirectoryIntegrated;
				connectionStringBuilder.UserID = userTextBox.Text;
				connectionStringBuilder.Password = passwordTextBox.Text;
			}

			connectionStringBuilder.InitialCatalog = databaseNameTextBox.Text;

			return connectionStringBuilder.ConnectionString;
		}
	}
}
