﻿namespace QMacsDataCleaner
{
	partial class QMacsDataCleaner
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.serverTextBox = new System.Windows.Forms.TextBox();
			this.databaseNameTextBox = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.authenticationComboBox = new System.Windows.Forms.ComboBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.userTextBox = new System.Windows.Forms.TextBox();
			this.passwordTextBox = new System.Windows.Forms.TextBox();
			this.sweepButton = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.leaveDaysNumericUpDown = new System.Windows.Forms.NumericUpDown();
			this.progressPanel = new System.Windows.Forms.Panel();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			((System.ComponentModel.ISupportInitialize)(this.leaveDaysNumericUpDown)).BeginInit();
			this.progressPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 20);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "SQL server address";
			// 
			// serverTextBox
			// 
			this.serverTextBox.Location = new System.Drawing.Point(129, 17);
			this.serverTextBox.Name = "serverTextBox";
			this.serverTextBox.Size = new System.Drawing.Size(213, 20);
			this.serverTextBox.TabIndex = 1;
			this.serverTextBox.Text = "(local)\\SqlExpress";
			// 
			// databaseNameTextBox
			// 
			this.databaseNameTextBox.Location = new System.Drawing.Point(130, 122);
			this.databaseNameTextBox.Name = "databaseNameTextBox";
			this.databaseNameTextBox.Size = new System.Drawing.Size(213, 20);
			this.databaseNameTextBox.TabIndex = 5;
			this.databaseNameTextBox.Text = "QMacs";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 125);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(82, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Database name";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 46);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(75, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Authentication";
			// 
			// authenticationComboBox
			// 
			this.authenticationComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.authenticationComboBox.FormattingEnabled = true;
			this.authenticationComboBox.Items.AddRange(new object[] {
            "Windows",
            "SQL Server"});
			this.authenticationComboBox.Location = new System.Drawing.Point(129, 43);
			this.authenticationComboBox.Name = "authenticationComboBox";
			this.authenticationComboBox.Size = new System.Drawing.Size(213, 21);
			this.authenticationComboBox.TabIndex = 2;
			this.authenticationComboBox.SelectedIndexChanged += new System.EventHandler(this.authenticationComboBox_SelectedIndexChanged);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(34, 73);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(29, 13);
			this.label4.TabIndex = 6;
			this.label4.Text = "User";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(35, 99);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(53, 13);
			this.label5.TabIndex = 7;
			this.label5.Text = "Password";
			// 
			// userTextBox
			// 
			this.userTextBox.Location = new System.Drawing.Point(161, 70);
			this.userTextBox.Name = "userTextBox";
			this.userTextBox.Size = new System.Drawing.Size(181, 20);
			this.userTextBox.TabIndex = 3;
			this.userTextBox.Text = "sa";
			// 
			// passwordTextBox
			// 
			this.passwordTextBox.Location = new System.Drawing.Point(161, 96);
			this.passwordTextBox.Name = "passwordTextBox";
			this.passwordTextBox.PasswordChar = '*';
			this.passwordTextBox.Size = new System.Drawing.Size(182, 20);
			this.passwordTextBox.TabIndex = 4;
			this.passwordTextBox.Text = "qm@cs_15";
			// 
			// sweepButton
			// 
			this.sweepButton.Location = new System.Drawing.Point(257, 183);
			this.sweepButton.Name = "sweepButton";
			this.sweepButton.Size = new System.Drawing.Size(85, 33);
			this.sweepButton.TabIndex = 7;
			this.sweepButton.Text = "Sweep";
			this.sweepButton.UseVisualStyleBackColor = true;
			this.sweepButton.Click += new System.EventHandler(this.sweepButton_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(12, 151);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(110, 13);
			this.label6.TabIndex = 8;
			this.label6.Text = "Leave data for n days";
			// 
			// leaveDaysNumericUpDown
			// 
			this.leaveDaysNumericUpDown.Location = new System.Drawing.Point(129, 149);
			this.leaveDaysNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
			this.leaveDaysNumericUpDown.Name = "leaveDaysNumericUpDown";
			this.leaveDaysNumericUpDown.Size = new System.Drawing.Size(213, 20);
			this.leaveDaysNumericUpDown.TabIndex = 6;
			this.leaveDaysNumericUpDown.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
			// 
			// progressPanel
			// 
			this.progressPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.progressPanel.Controls.Add(this.progressBar);
			this.progressPanel.Location = new System.Drawing.Point(0, 0);
			this.progressPanel.Name = "progressPanel";
			this.progressPanel.Size = new System.Drawing.Size(355, 234);
			this.progressPanel.TabIndex = 9;
			this.progressPanel.Visible = false;
			// 
			// progressBar
			// 
			this.progressBar.Location = new System.Drawing.Point(12, 99);
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(331, 23);
			this.progressBar.Step = 1;
			this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
			this.progressBar.TabIndex = 0;
			// 
			// QMacsDataCleaner
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(355, 230);
			this.Controls.Add(this.leaveDaysNumericUpDown);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.sweepButton);
			this.Controls.Add(this.passwordTextBox);
			this.Controls.Add(this.userTextBox);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.authenticationComboBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.databaseNameTextBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.serverTextBox);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.progressPanel);
			this.Name = "QMacsDataCleaner";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "QMacs data cleaner";
			((System.ComponentModel.ISupportInitialize)(this.leaveDaysNumericUpDown)).EndInit();
			this.progressPanel.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox serverTextBox;
		private System.Windows.Forms.TextBox databaseNameTextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox authenticationComboBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox userTextBox;
		private System.Windows.Forms.TextBox passwordTextBox;
		private System.Windows.Forms.Button sweepButton;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.NumericUpDown leaveDaysNumericUpDown;
		private System.Windows.Forms.Panel progressPanel;
		private System.Windows.Forms.ProgressBar progressBar;
	}
}

