﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace EntityFramework.Models;

public partial class ProductionMachine
{
    public long Id { get; set; }

    public string SerialNumber { get; set; }

    public string Designation { get; set; }

    public long? LocationId { get; set; }

    public virtual ICollection<FilterType> FilterTypes { get; set; } = new List<FilterType>();

    public virtual ICollection<HotMeltGranulat> HotMeltGranulats { get; set; } = new List<HotMeltGranulat>();

    public virtual ICollection<Irimager> Irimagers { get; set; } = new List<Irimager>();

    public virtual Location Location { get; set; }

    public virtual ICollection<ProcessDatum> ProcessData { get; set; } = new List<ProcessDatum>();

    public virtual ICollection<ProcessDataSetPoint> ProcessDataSetPoints { get; set; } = new List<ProcessDataSetPoint>();

    public virtual ICollection<StatisticalDatum> StatisticalData { get; set; } = new List<StatisticalDatum>();
}