﻿using Opc.Ua;
using OpcUaDeviceClient.Models;
using System;
using System.Windows.Forms;
using UnifiedHostClient.Models;

namespace UnifiedHostClient.View
{
	public partial class CommonValueEditorForm : Form
	{
		private Device device;
		private DeviceParameterDescriptor parameter;

		public CommonValueEditorForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(Device device, DeviceParameterDescriptor parameter)
		{
			this.device = device;
			this.parameter = parameter;

			label.Text = parameter.DisplayName;
			textBox.Text = parameter.Value?.ToString();

			if (ShowDialog() == DialogResult.OK)
				return true;
			else
				return false;
		}

		private void CommonEditorForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			object value = null;

			try
			{
				switch (Enum.Parse(typeof(BuiltInType), parameter.DataType))
				{
					case BuiltInType.Boolean:
						value = bool.Parse(textBox.Text);
						break;

					case BuiltInType.SByte:
						value = sbyte.Parse(textBox.Text);
						break;

					case BuiltInType.Byte:
						value = byte.Parse(textBox.Text);
						break;

					case BuiltInType.Int16:
						value = short.Parse(textBox.Text);
						break;

					case BuiltInType.UInt16:
						value = ushort.Parse(textBox.Text);
						break;

					case BuiltInType.Int32:
						value = int.Parse(textBox.Text);
						break;

					case BuiltInType.UInt32:
						value = uint.Parse(textBox.Text);
						break;

					case BuiltInType.Int64:
						value = long.Parse(textBox.Text);
						break;

					case BuiltInType.UInt64:
						value = ulong.Parse(textBox.Text);
						break;

					case BuiltInType.Float:
						value = float.Parse(textBox.Text);
						break;

					case BuiltInType.Double:
						value = double.Parse(textBox.Text);
						break;

					case BuiltInType.String:
						value = textBox.Text;
						break;

					default:
						MessageBox.Show("Editing is not supported for type");
						break;
				}

				device.SetParameterValue(parameter.NodeId, value);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.GetBaseException().Message, "Error");
			}
		}
	}
}
