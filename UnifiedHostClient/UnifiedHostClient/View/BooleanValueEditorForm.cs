﻿using OpcUaDeviceClient.Models;
using System;
using System.Windows.Forms;
using UnifiedHostClient.Models;

namespace UnifiedHostClient.View
{
	public partial class BooleanValueEditorForm : Form
	{
		private Device device;
		private DeviceParameterDescriptor parameter;

		public BooleanValueEditorForm()
		{
			InitializeComponent();
		}

		public bool ShowDialog(Device device, DeviceParameterDescriptor parameter)
		{
			this.device = device;
			this.parameter = parameter;

			label.Text = parameter.DisplayName;

			try
			{
				valueCheckBox.Checked = bool.Parse(parameter.Value);
			}
			catch
			{
				MessageBox.Show("Value of the parameter " + parameter.DisplayName + " is not a boolean value");
			}

			if (ShowDialog() == DialogResult.OK)
				return true;
			else
				return false;
		}

		private void BooleanValueEditorForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			try
			{
				device.SetParameterValue(parameter.NodeId, valueCheckBox.Checked);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.GetBaseException().Message, "Error");
			}
		}
	}
}
