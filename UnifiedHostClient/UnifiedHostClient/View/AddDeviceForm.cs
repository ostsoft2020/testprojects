﻿using OpcUaDeviceClient;
using OpcUaDeviceClient.Models;
using System.Windows.Forms;
using UnifiedHostClient.Models;
using System.Linq;
using Opc.Ua;

namespace UnifiedHostClient.View
{
	public partial class AddDeviceForm : Form
	{
		private IntegrationHostContext context;
		private DeviceType[] deviceTypes = null;
		private NodeId createdInstanceNodeId;

		public AddDeviceForm()
		{
			InitializeComponent();
		}

		public DeviceDescriptor Execute(IntegrationHostContext context)
		{
			this.context = context;
			
			if (!ExecutionHelper.ExecuteInTryCatch(() => deviceTypes = context.GetUnifiedDeviceSubTypes()))
				return null;

			deviceTypeComboBox.Items.AddRange(deviceTypes.Select(dt => dt.DisplayName).ToArray());

			if (ShowDialog() == DialogResult.OK)
			{
				Device device = context.GetDevice(createdInstanceNodeId);

				return DeviceHelper.CreateDeviceDescriptor(device);
			}
			else
				return null;
		}

		private void AddDeviceForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				return;

			DeviceType deviceType = deviceTypes.FirstOrDefault(dt => dt.DisplayName == deviceTypeComboBox.Text);

			if (deviceType == null)
			{
				MessageBox.Show("Unified device type is not selected");
				e.Cancel = true;
				return;
			}

			if (!ExecutionHelper.VerifyName(nameTextBox.Text, out string errorMessage))
			{
				MessageBox.Show(errorMessage);
				e.Cancel = true;
				return;
			}

			if (!ExecutionHelper.ExecuteInTryCatch(
				() => createdInstanceNodeId = context.CreateUnifiedDevice(
					deviceType.Id, 
					nameTextBox.Text, 
					descriptionTextBox.Text, 
					modelTextBox.Text, 
					hardwareRevisionTextBox.Text, 
					softwareRevisionTextBox.Text, 
					deviceRevisionTextBox.Text, 
					productCodeTextBox.Text, 
					serialNumberTextBox.Text
				)
			))
				e.Cancel = true;
		}

		private void deviceTypeComboBox_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			DeviceType deviceType = deviceTypes.FirstOrDefault(dt => dt.DisplayName == deviceTypeComboBox.Text);

			if (deviceType == null)
				return;

			modelTextBox.Text = deviceType.Model.ToString();
			descriptionTextBox.Text = deviceType.Description;
			hardwareRevisionTextBox.Text = deviceType.HardwareRevision;
			softwareRevisionTextBox.Text = deviceType.SoftwareRevision;
			deviceRevisionTextBox.Text = deviceType.DeviceRevision;
			productCodeTextBox.Text = deviceType.ProductCode;
		}
	}
}
