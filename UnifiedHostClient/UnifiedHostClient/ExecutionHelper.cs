﻿using System;
using System.Windows.Forms;
using System.Xml;

namespace UnifiedHostClient
{
	public static class ExecutionHelper
	{
		/// <summary>
		/// Executes specified action in try..catch block and shows message box with error message in case if exception occured
		/// </summary>
		public static bool ExecuteInTryCatch(Action action)
		{
			try
			{
				action();

				return true;
			}
			catch (Exception e)
			{
				MessageBox.Show(e.GetBaseException().Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

				return false;
			}
		}

		/// <summary>
		/// Verifies that the name is a valid name according to the W3C Extended Markup Language recommendation.
		/// </summary>
		public static bool VerifyName(string name, out string errorMessage)
		{
			try
			{
				XmlConvert.VerifyName(name);
			}
			catch (XmlException e)
			{
				errorMessage = "Name is not valid: " + e.Message;
				return false;
			}
			catch (ArgumentNullException)
			{
				errorMessage = "Name is null or empty";
				return false;
			}

			errorMessage = "";
			return true;
		}
	}
}
