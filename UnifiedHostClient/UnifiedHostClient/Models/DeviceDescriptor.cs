﻿using OpcUaDeviceClient.Models;
using System;
using System.ComponentModel;

namespace UnifiedHostClient.Models
{
	public class DeviceDescriptor : IDisposable
	{
		public Device Device { get; set; }
		public BindingList<DevicePropertyDescriptor> Properties { get; set; }
		public BindingList<DeviceParameterDescriptor> Parameters { get; set; }

		public void Dispose()
		{
			foreach (var p in Parameters)
				p.Dispose();
		}
	}
}
