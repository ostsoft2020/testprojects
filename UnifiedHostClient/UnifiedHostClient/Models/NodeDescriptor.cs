﻿using OpcUaDeviceClient.Models;

namespace UnifiedHostClient.Models
{
	public enum NodeType
	{
		DeviceSet,
		Device,
		ParameterSet,
		MethodSet
	}

	public class NodeDescriptor
	{
		public DeviceDescriptor DeviceDescriptor;
		public NodeType NodeType;
	}
}
