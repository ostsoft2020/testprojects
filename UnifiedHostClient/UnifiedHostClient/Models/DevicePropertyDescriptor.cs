﻿namespace UnifiedHostClient.Models
{
	public class DevicePropertyDescriptor
	{
		public string Name { get; set; }
		public string Value { get; set; }
	}
}
