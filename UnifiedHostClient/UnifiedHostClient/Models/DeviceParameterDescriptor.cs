﻿using Opc.Ua;
using OpcUaDeviceClient.Enums;
using System;
using System.ComponentModel;

namespace UnifiedHostClient.Models
{
	public class DeviceParameterDescriptor : INotifyPropertyChanged, IDisposable
	{
		#region Properties

		public NodeId NodeId;
		public string DisplayName { get; set; }
		public string DataType { get; set; }
		public bool IsArray { get; set; }
		public AccessLevel AccessLevel { get; set; }
		public string Value { get; private set; }
		public bool IsWritable => AccessLevel == AccessLevel.ReadWrite || AccessLevel == AccessLevel.WriteOnly;

		#endregion

		#region Events

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion

		#region Methods

		public void SetValue(object newValue)
		{
			string newVal = newValue != null ? newValue.ToString() : "<null>";

			if (Value != newVal)
			{
				Value = newVal;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Value)));
			}
		}

		public void Dispose()
		{
			// Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
			Dispose(disposing: true);
			GC.SuppressFinalize(this);
		}

		#endregion

		#region Internal members

		// for IDisposable implementation pattern
		private bool disposedValue;

		// stores action which should be called to unsubscribe from monitoring
		internal Action UnsubscribeMonitoringAction;

		internal void OnValueChanged(NodeId nodeId, object newValue)
		{
			SetValue(newValue);
		}

		internal void OnValueError(NodeId nodeId, StatusCode statusCode)
		{
			SetValue("Error: " + statusCode);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					// unsubscribe from data monitoring
					UnsubscribeMonitoringAction?.Invoke();
				}

				// TODO: free unmanaged resources (unmanaged objects) and override finalizer
				// TODO: set large fields to null
				disposedValue = true;
			}
		}

		// // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
		// ~DeviceParameterDescriptor()
		// {
		//     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
		//     Dispose(disposing: false);
		// }

		#endregion
	}
}
