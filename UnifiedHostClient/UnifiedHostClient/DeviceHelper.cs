﻿using OpcUaDeviceClient;
using OpcUaDeviceClient.Models;
using System.Collections.Generic;
using UnifiedHostClient.Models;
using Opc.Ua;
using OpcUaDeviceClient.Classes;
using System.ComponentModel;

namespace UnifiedHostClient
{
	public static class DeviceHelper
	{
		/// <summary>
		/// Get all device instances from OPC-UA server and creates device descriptors for each device instance
		/// </summary>
		public static List<DeviceDescriptor> CreateDeviceDescriptors(IntegrationHostContext context)
		{
			List<DeviceDescriptor> result = new List<DeviceDescriptor>();

			Device[] devices = context.GetDevices();

			for (int i = 0; i < devices.Length; i++)
				result.Add(CreateDeviceDescriptor(devices[i]));

			return result;
		}

		/// <summary>
		/// Creaates device ddescriptor for specified device
		/// </summary>
		public static DeviceDescriptor CreateDeviceDescriptor(Device device)
		{
			DeviceDescriptor deviceDescriptor = new DeviceDescriptor();

			// save original device in the descriptor
			deviceDescriptor.Device = device;

			// save device properties as a collection of values with name and value. 
			// this needed for presenting values in a DataGridView control			
			deviceDescriptor.Properties = new BindingList<DevicePropertyDescriptor>();
			deviceDescriptor.Properties.Add(new DevicePropertyDescriptor { Name = nameof(Device.Description), Value = device.Description });
			deviceDescriptor.Properties.Add(new DevicePropertyDescriptor { Name = nameof(Device.DeviceClass), Value = device.DeviceClass });
			deviceDescriptor.Properties.Add(new DevicePropertyDescriptor { Name = nameof(Device.DeviceTypeName), Value = device.DeviceTypeName });
			deviceDescriptor.Properties.Add(new DevicePropertyDescriptor { Name = nameof(Device.Manufacturer), Value = device.Manufacturer.ToString() });
			deviceDescriptor.Properties.Add(new DevicePropertyDescriptor { Name = nameof(Device.Model), Value = device.Model.ToString() });
			deviceDescriptor.Properties.Add(new DevicePropertyDescriptor { Name = nameof(Device.HardwareRevision), Value = device.HardwareRevision });
			deviceDescriptor.Properties.Add(new DevicePropertyDescriptor { Name = nameof(Device.SoftwareRevision), Value = device.SoftwareRevision });
			deviceDescriptor.Properties.Add(new DevicePropertyDescriptor { Name = nameof(Device.DeviceRevision), Value = device.ProductCode });
			deviceDescriptor.Properties.Add(new DevicePropertyDescriptor { Name = nameof(Device.SerialNumber), Value = device.SerialNumber });
			deviceDescriptor.Properties.Add(new DevicePropertyDescriptor { Name = nameof(Device.ProductCode), Value = device.ProductCode });

			// save device parameters (OPC_UA properties and variables from Device.ParameterSet folder)
			DeviceParameter[] parameters = device.GetParameters();
			deviceDescriptor.Parameters = new BindingList<DeviceParameterDescriptor>();
			for (int i = 0; i < parameters.Length; i++)
			{
				// fill parameter attributes
				DeviceParameterDescriptor deviceParameterDescriptor = new DeviceParameterDescriptor
				{
					NodeId = parameters[i].NodeId,
					DisplayName = parameters[i].DisplayName,
					DataType = TypeInfo.GetBuiltInType(parameters[i].DataTypeId).ToString(),
					IsArray = parameters[i].IsArray,
					AccessLevel = parameters[i].CombinedAccessLevel
				};

				// get parameter initial value
				deviceParameterDescriptor.SetValue(device.GetParameterValue(parameters[i].NodeId));

				// subscribe to parameter value changing (data monitoring)
				deviceParameterDescriptor.UnsubscribeMonitoringAction =
					parameters[i].SubscribeToValueChanges(
						new VariableValueChangedConfig(
							500, // sampling interval
							deviceParameterDescriptor.OnValueChanged, // good value handler
							deviceParameterDescriptor.OnValueError // bad value handler
						)
					);

				deviceDescriptor.Parameters.Add(deviceParameterDescriptor);
			}

			return deviceDescriptor;
		}
	}
}
