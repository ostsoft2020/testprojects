﻿namespace UnifiedHostClient
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.panel1 = new System.Windows.Forms.Panel();
			this.connectButton = new System.Windows.Forms.Button();
			this.serverAddressTextBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
			this.treeView = new System.Windows.Forms.TreeView();
			this.tabControl = new System.Windows.Forms.TabControl();
			this.noDataPage = new System.Windows.Forms.TabPage();
			this.deviceTabPage = new System.Windows.Forms.TabPage();
			this.devicePropertiesGrid = new System.Windows.Forms.DataGridView();
			this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.parametersTabPage = new System.Windows.Forms.TabPage();
			this.deviceParametersGrid = new System.Windows.Forms.DataGridView();
			this.nameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.VariableTypeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.DataTypeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.valueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.turnMonitoringColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.setValueColumn = new System.Windows.Forms.DataGridViewButtonColumn();
			this.methodsTabPage = new System.Windows.Forms.TabPage();
			this.deviceMethodsGrid = new System.Windows.Forms.DataGridView();
			this.deviceSetContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.addDeviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.deviceContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
			this.removeDeviceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
			this.mainSplitContainer.Panel1.SuspendLayout();
			this.mainSplitContainer.Panel2.SuspendLayout();
			this.mainSplitContainer.SuspendLayout();
			this.tabControl.SuspendLayout();
			this.deviceTabPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.devicePropertiesGrid)).BeginInit();
			this.parametersTabPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.deviceParametersGrid)).BeginInit();
			this.methodsTabPage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.deviceMethodsGrid)).BeginInit();
			this.deviceSetContextMenu.SuspendLayout();
			this.deviceContextMenu.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.connectButton);
			this.panel1.Controls.Add(this.serverAddressTextBox);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(990, 32);
			this.panel1.TabIndex = 0;
			// 
			// connectButton
			// 
			this.connectButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.connectButton.Location = new System.Drawing.Point(910, 5);
			this.connectButton.Name = "connectButton";
			this.connectButton.Size = new System.Drawing.Size(75, 22);
			this.connectButton.TabIndex = 2;
			this.connectButton.Text = "Connect";
			this.connectButton.UseVisualStyleBackColor = true;
			this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
			// 
			// serverAddressTextBox
			// 
			this.serverAddressTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.serverAddressTextBox.Location = new System.Drawing.Point(125, 6);
			this.serverAddressTextBox.Name = "serverAddressTextBox";
			this.serverAddressTextBox.Size = new System.Drawing.Size(781, 20);
			this.serverAddressTextBox.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(122, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "OPC-UA server address:";
			// 
			// mainSplitContainer
			// 
			this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mainSplitContainer.Location = new System.Drawing.Point(0, 32);
			this.mainSplitContainer.Name = "mainSplitContainer";
			// 
			// mainSplitContainer.Panel1
			// 
			this.mainSplitContainer.Panel1.Controls.Add(this.treeView);
			// 
			// mainSplitContainer.Panel2
			// 
			this.mainSplitContainer.Panel2.Controls.Add(this.tabControl);
			this.mainSplitContainer.Size = new System.Drawing.Size(990, 553);
			this.mainSplitContainer.SplitterDistance = 328;
			this.mainSplitContainer.TabIndex = 1;
			// 
			// treeView
			// 
			this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeView.Location = new System.Drawing.Point(0, 0);
			this.treeView.Name = "treeView";
			this.treeView.Size = new System.Drawing.Size(328, 553);
			this.treeView.TabIndex = 0;
			this.treeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView_AfterSelect);
			this.treeView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.treeView_MouseDown);
			// 
			// tabControl
			// 
			this.tabControl.Controls.Add(this.noDataPage);
			this.tabControl.Controls.Add(this.deviceTabPage);
			this.tabControl.Controls.Add(this.parametersTabPage);
			this.tabControl.Controls.Add(this.methodsTabPage);
			this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl.Location = new System.Drawing.Point(0, 0);
			this.tabControl.Name = "tabControl";
			this.tabControl.SelectedIndex = 0;
			this.tabControl.Size = new System.Drawing.Size(658, 553);
			this.tabControl.TabIndex = 1;
			// 
			// noDataPage
			// 
			this.noDataPage.Location = new System.Drawing.Point(4, 22);
			this.noDataPage.Name = "noDataPage";
			this.noDataPage.Padding = new System.Windows.Forms.Padding(3);
			this.noDataPage.Size = new System.Drawing.Size(650, 527);
			this.noDataPage.TabIndex = 0;
			this.noDataPage.Text = "No data";
			this.noDataPage.UseVisualStyleBackColor = true;
			// 
			// deviceTabPage
			// 
			this.deviceTabPage.Controls.Add(this.devicePropertiesGrid);
			this.deviceTabPage.Location = new System.Drawing.Point(4, 22);
			this.deviceTabPage.Name = "deviceTabPage";
			this.deviceTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.deviceTabPage.Size = new System.Drawing.Size(650, 527);
			this.deviceTabPage.TabIndex = 1;
			this.deviceTabPage.Text = "Device";
			this.deviceTabPage.UseVisualStyleBackColor = true;
			// 
			// devicePropertiesGrid
			// 
			this.devicePropertiesGrid.AllowUserToAddRows = false;
			this.devicePropertiesGrid.AllowUserToDeleteRows = false;
			this.devicePropertiesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.devicePropertiesGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn8});
			this.devicePropertiesGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.devicePropertiesGrid.Location = new System.Drawing.Point(3, 3);
			this.devicePropertiesGrid.MultiSelect = false;
			this.devicePropertiesGrid.Name = "devicePropertiesGrid";
			this.devicePropertiesGrid.ReadOnly = true;
			this.devicePropertiesGrid.RowHeadersVisible = false;
			this.devicePropertiesGrid.ShowEditingIcon = false;
			this.devicePropertiesGrid.Size = new System.Drawing.Size(644, 521);
			this.devicePropertiesGrid.TabIndex = 2;
			// 
			// dataGridViewTextBoxColumn5
			// 
			this.dataGridViewTextBoxColumn5.DataPropertyName = "Name";
			this.dataGridViewTextBoxColumn5.HeaderText = "Name";
			this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
			this.dataGridViewTextBoxColumn5.ReadOnly = true;
			this.dataGridViewTextBoxColumn5.Width = 120;
			// 
			// dataGridViewTextBoxColumn8
			// 
			this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.dataGridViewTextBoxColumn8.DataPropertyName = "Value";
			this.dataGridViewTextBoxColumn8.HeaderText = "Value";
			this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
			this.dataGridViewTextBoxColumn8.ReadOnly = true;
			// 
			// parametersTabPage
			// 
			this.parametersTabPage.Controls.Add(this.deviceParametersGrid);
			this.parametersTabPage.Location = new System.Drawing.Point(4, 22);
			this.parametersTabPage.Name = "parametersTabPage";
			this.parametersTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.parametersTabPage.Size = new System.Drawing.Size(650, 527);
			this.parametersTabPage.TabIndex = 2;
			this.parametersTabPage.Text = "Parameters";
			this.parametersTabPage.UseVisualStyleBackColor = true;
			// 
			// deviceParametersGrid
			// 
			this.deviceParametersGrid.AllowUserToAddRows = false;
			this.deviceParametersGrid.AllowUserToDeleteRows = false;
			this.deviceParametersGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.deviceParametersGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameColumn,
            this.VariableTypeColumn,
            this.DataTypeColumn,
            this.valueColumn,
            this.turnMonitoringColumn,
            this.setValueColumn});
			this.deviceParametersGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.deviceParametersGrid.Location = new System.Drawing.Point(3, 3);
			this.deviceParametersGrid.MultiSelect = false;
			this.deviceParametersGrid.Name = "deviceParametersGrid";
			this.deviceParametersGrid.RowHeadersVisible = false;
			this.deviceParametersGrid.ShowEditingIcon = false;
			this.deviceParametersGrid.Size = new System.Drawing.Size(644, 521);
			this.deviceParametersGrid.TabIndex = 1;
			this.deviceParametersGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.deviceParametersGrid_CellContentClick);
			// 
			// nameColumn
			// 
			this.nameColumn.DataPropertyName = "DisplayName";
			this.nameColumn.HeaderText = "Name";
			this.nameColumn.Name = "nameColumn";
			this.nameColumn.ReadOnly = true;
			this.nameColumn.Width = 120;
			// 
			// VariableTypeColumn
			// 
			this.VariableTypeColumn.DataPropertyName = "VariableType";
			this.VariableTypeColumn.HeaderText = "Variable type";
			this.VariableTypeColumn.Name = "VariableTypeColumn";
			this.VariableTypeColumn.ReadOnly = true;
			this.VariableTypeColumn.Width = 120;
			// 
			// DataTypeColumn
			// 
			this.DataTypeColumn.DataPropertyName = "DataType";
			this.DataTypeColumn.HeaderText = "Data type";
			this.DataTypeColumn.Name = "DataTypeColumn";
			this.DataTypeColumn.ReadOnly = true;
			this.DataTypeColumn.Width = 120;
			// 
			// valueColumn
			// 
			this.valueColumn.DataPropertyName = "Value";
			this.valueColumn.HeaderText = "Value";
			this.valueColumn.Name = "valueColumn";
			this.valueColumn.ReadOnly = true;
			// 
			// turnMonitoringColumn
			// 
			this.turnMonitoringColumn.HeaderText = "Moitoring";
			this.turnMonitoringColumn.Name = "turnMonitoringColumn";
			this.turnMonitoringColumn.Width = 70;
			// 
			// setValueColumn
			// 
			this.setValueColumn.HeaderText = "";
			this.setValueColumn.Name = "setValueColumn";
			this.setValueColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.setValueColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.setValueColumn.Text = "Set";
			this.setValueColumn.UseColumnTextForButtonValue = true;
			this.setValueColumn.Width = 30;
			// 
			// methodsTabPage
			// 
			this.methodsTabPage.Controls.Add(this.deviceMethodsGrid);
			this.methodsTabPage.Location = new System.Drawing.Point(4, 22);
			this.methodsTabPage.Name = "methodsTabPage";
			this.methodsTabPage.Padding = new System.Windows.Forms.Padding(3);
			this.methodsTabPage.Size = new System.Drawing.Size(650, 527);
			this.methodsTabPage.TabIndex = 3;
			this.methodsTabPage.Text = "Methods";
			this.methodsTabPage.UseVisualStyleBackColor = true;
			// 
			// deviceMethodsGrid
			// 
			this.deviceMethodsGrid.AllowUserToAddRows = false;
			this.deviceMethodsGrid.AllowUserToDeleteRows = false;
			this.deviceMethodsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.deviceMethodsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.deviceMethodsGrid.Location = new System.Drawing.Point(3, 3);
			this.deviceMethodsGrid.MultiSelect = false;
			this.deviceMethodsGrid.Name = "deviceMethodsGrid";
			this.deviceMethodsGrid.ReadOnly = true;
			this.deviceMethodsGrid.RowHeadersVisible = false;
			this.deviceMethodsGrid.ShowEditingIcon = false;
			this.deviceMethodsGrid.Size = new System.Drawing.Size(644, 521);
			this.deviceMethodsGrid.TabIndex = 2;
			// 
			// deviceSetContextMenu
			// 
			this.deviceSetContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addDeviceToolStripMenuItem});
			this.deviceSetContextMenu.Name = "deviceSetContextMenu";
			this.deviceSetContextMenu.Size = new System.Drawing.Size(134, 26);
			// 
			// addDeviceToolStripMenuItem
			// 
			this.addDeviceToolStripMenuItem.Name = "addDeviceToolStripMenuItem";
			this.addDeviceToolStripMenuItem.Size = new System.Drawing.Size(133, 22);
			this.addDeviceToolStripMenuItem.Text = "Add device";
			this.addDeviceToolStripMenuItem.Click += new System.EventHandler(this.addDeviceToolStripMenuItem_Click);
			// 
			// deviceContextMenu
			// 
			this.deviceContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeDeviceToolStripMenuItem});
			this.deviceContextMenu.Name = "deviceContextMenu";
			this.deviceContextMenu.Size = new System.Drawing.Size(145, 26);
			// 
			// removeDeviceToolStripMenuItem
			// 
			this.removeDeviceToolStripMenuItem.Name = "removeDeviceToolStripMenuItem";
			this.removeDeviceToolStripMenuItem.Size = new System.Drawing.Size(144, 22);
			this.removeDeviceToolStripMenuItem.Text = "Delete device";
			this.removeDeviceToolStripMenuItem.Click += new System.EventHandler(this.removeDeviceToolStripMenuItem_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(990, 585);
			this.Controls.Add(this.mainSplitContainer);
			this.Controls.Add(this.panel1);
			this.Name = "MainForm";
			this.Text = "Unified host client";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.mainForm_FormClosed);
			this.Load += new System.EventHandler(this.mainForm_Load);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.mainSplitContainer.Panel1.ResumeLayout(false);
			this.mainSplitContainer.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
			this.mainSplitContainer.ResumeLayout(false);
			this.tabControl.ResumeLayout(false);
			this.deviceTabPage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.devicePropertiesGrid)).EndInit();
			this.parametersTabPage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.deviceParametersGrid)).EndInit();
			this.methodsTabPage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.deviceMethodsGrid)).EndInit();
			this.deviceSetContextMenu.ResumeLayout(false);
			this.deviceContextMenu.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button connectButton;
		private System.Windows.Forms.TextBox serverAddressTextBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.SplitContainer mainSplitContainer;
		private System.Windows.Forms.TreeView treeView;
		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.TabPage noDataPage;
		private System.Windows.Forms.TabPage deviceTabPage;
		private System.Windows.Forms.DataGridView devicePropertiesGrid;
		private System.Windows.Forms.TabPage parametersTabPage;
		private System.Windows.Forms.DataGridView deviceParametersGrid;
		private System.Windows.Forms.TabPage methodsTabPage;
		private System.Windows.Forms.DataGridView deviceMethodsGrid;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
		private System.Windows.Forms.ContextMenuStrip deviceSetContextMenu;
		private System.Windows.Forms.ToolStripMenuItem addDeviceToolStripMenuItem;
		private System.Windows.Forms.ContextMenuStrip deviceContextMenu;
		private System.Windows.Forms.ToolStripMenuItem removeDeviceToolStripMenuItem;
		private System.Windows.Forms.DataGridViewTextBoxColumn nameColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn VariableTypeColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn DataTypeColumn;
		private System.Windows.Forms.DataGridViewTextBoxColumn valueColumn;
		private System.Windows.Forms.DataGridViewCheckBoxColumn turnMonitoringColumn;
		private System.Windows.Forms.DataGridViewButtonColumn setValueColumn;
	}
}

