﻿using Opc.Ua;
using OpcUaDeviceClient;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using UnifiedHostClient.Models;
using UnifiedHostClient.View;

namespace UnifiedHostClient
{
	public partial class MainForm : Form
	{
		private IntegrationHostContext context;

		// cached devices and related data
		private List<DeviceDescriptor> deviceDescriptors;

		public MainForm()
		{
			InitializeComponent();

			serverAddressTextBox.Text = "opc.tcp://127.0.0.1:62541/OpcServer";

			devicePropertiesGrid.AutoGenerateColumns = false;
			deviceParametersGrid.AutoGenerateColumns = false;
			deviceMethodsGrid.AutoGenerateColumns = false;

			NLog.LogManager.LoadConfiguration("NLog.config");
			OpcUaClient.Init("Opc.Ua.Client.Config.xml");
		}

		private void addDeviceNodes(int deviceIndex)
		{
			TreeNode deviceNode = new TreeNode(deviceDescriptors[deviceIndex].Device.DisplayName)
			{
				Tag = new NodeDescriptor { DeviceDescriptor = deviceDescriptors[deviceIndex], NodeType = NodeType.Device },
				ContextMenuStrip = deviceContextMenu
			};
			treeView.Nodes[0].Nodes.Add(deviceNode);

			deviceNode.Nodes.Add(
				new TreeNode(nameof(NodeType.ParameterSet))
				{
					Tag = new NodeDescriptor { DeviceDescriptor = deviceDescriptors[deviceIndex], NodeType = NodeType.ParameterSet }
				}
			);

			deviceNode.Nodes.Add(
				new TreeNode(nameof(NodeType.MethodSet))
				{
					Tag = new NodeDescriptor { DeviceDescriptor = deviceDescriptors[deviceIndex], NodeType = NodeType.MethodSet }
				}
			);
		}

		private void disposeExistingData()
		{
			treeView.Nodes.Clear();

			if (deviceDescriptors != null)
			{
				foreach (var dd in deviceDescriptors)
					dd.Dispose();

				deviceDescriptors = null;
			}

			context?.Dispose();
		}

		private void selectTabPage(TabPage tabPage)
		{
			tabControl.TabPages.Clear();
			tabControl.TabPages.Add(tabPage);
		}

		private void mainForm_Load(object sender, EventArgs e)
		{
			selectTabPage(noDataPage);
		}

		private void mainForm_FormClosed(object sender, FormClosedEventArgs e)
		{
			disposeExistingData();
		}

		private async void connectButton_Click(object sender, EventArgs e)
		{
			try
			{
				disposeExistingData();

				context = new IntegrationHostContext();

				await context.ConnectAsync(serverAddressTextBox.Text);

				deviceDescriptors = DeviceHelper.CreateDeviceDescriptors(context);

				TreeNode devicesNode = new TreeNode("Devices")
				{
					Tag = new NodeDescriptor { NodeType = NodeType.DeviceSet },
					ContextMenuStrip = deviceSetContextMenu
				};

				treeView.Nodes.Add(devicesNode);

				for (int i = 0; i < deviceDescriptors.Count; i++)
					addDeviceNodes(i);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
		{			
			if (e.Node == null)
			{
				selectTabPage(noDataPage);
				return;
			}

			NodeDescriptor nodeDescriptor = (NodeDescriptor) e.Node.Tag;

			switch (nodeDescriptor.NodeType)
			{
				case NodeType.DeviceSet:
					selectTabPage(noDataPage);
					break;

				case NodeType.Device:
					devicePropertiesGrid.DataSource = nodeDescriptor.DeviceDescriptor.Properties;
					devicePropertiesGrid.Tag = nodeDescriptor.DeviceDescriptor;
					selectTabPage(deviceTabPage);
					break;

				case NodeType.ParameterSet:
					deviceParametersGrid.DataSource = nodeDescriptor.DeviceDescriptor.Parameters;
					deviceParametersGrid.Tag = nodeDescriptor.DeviceDescriptor;
					selectTabPage(parametersTabPage);
					break;

				case NodeType.MethodSet:
					deviceMethodsGrid.DataSource = null;
					deviceMethodsGrid.Tag = nodeDescriptor.DeviceDescriptor;
					selectTabPage(methodsTabPage);
					break;
			}
		}

		private void treeView_MouseDown(object sender, MouseEventArgs e)
		{
			treeView.SelectedNode = treeView.GetNodeAt(e.X, e.Y);
		}

		private void addDeviceToolStripMenuItem_Click(object sender, EventArgs e)
		{
			AddDeviceForm addDeviceForm = new AddDeviceForm();

			DeviceDescriptor deviceDescriptor = addDeviceForm.Execute(context);

			if (deviceDescriptor != null)
			{
				deviceDescriptors.Add(deviceDescriptor);

				addDeviceNodes(deviceDescriptors.Count - 1);
			}
		}

		private void removeDeviceToolStripMenuItem_Click(object sender, EventArgs e)
		{
			NodeDescriptor nodeDescriptor = treeView.SelectedNode.Tag as NodeDescriptor;

			if (nodeDescriptor == null || nodeDescriptor.DeviceDescriptor == null)
				return;

			string message = "Are you shure you want to remove device " + nodeDescriptor.DeviceDescriptor.Device.DisplayName + "?";

			if (MessageBox.Show(message, "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
				return;

			ExecutionHelper.ExecuteInTryCatch(
				() =>
				{
					context.DeleteUnifiedDevice(nodeDescriptor.DeviceDescriptor.Device.Id);
					treeView.Nodes.Remove(treeView.SelectedNode);
					deviceDescriptors.Remove(nodeDescriptor.DeviceDescriptor);
				}
			);
		}

		private void deviceParametersGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.ColumnIndex != setValueColumn.Index || e.RowIndex < 0 || deviceParametersGrid.Tag == null)
				return;

			DeviceDescriptor device = (DeviceDescriptor)deviceParametersGrid.Tag;

			if (!device.Parameters[e.RowIndex].IsWritable)
			{
				MessageBox.Show("Editing is not allowed because access level is " + device.Parameters[e.RowIndex].AccessLevel);
				return;
			}

			if (device.Parameters[e.RowIndex].IsArray)
			{
				MessageBox.Show("Array editing is not supported");
				return;
			}

			switch (Enum.Parse(typeof(BuiltInType), device.Parameters[e.RowIndex].DataType))
			{
				case BuiltInType.Boolean:
					new BooleanValueEditorForm().ShowDialog(device.Device, device.Parameters[e.RowIndex]);
					break;

				default:
					new CommonValueEditorForm().ShowDialog(device.Device, device.Parameters[e.RowIndex]);
					break;
			}
		}
	}
}
