﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using JetBrains.Annotations;
using Opc.Ua;
using OpcUaDeviceClient;
using OpcUaDeviceClient.Classes;
using OpcUaDeviceClient.Models;

namespace SAPB1DemoMonitor
{
	public class WeldingMachine : IDisposable, INotifyPropertyChanged
	{
		#region · Constructors ·

		public WeldingMachine([NotNull] DeviceContext context, [NotNull] string browseNameNamespace, [NotNull] string browseNameName)
		{
			this.context = context;

			device =
				context.GetDevice(context.GetBrowseName(browseNameNamespace, browseNameName))
				?? throw new Exception($"No Device {browseNameNamespace}:{browseNameName} at the OPC-UA Server");
		}

		#endregion

		#region · Properties ·

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool IsDisposed => 1 == isDisposed;

		public int OperationCondition
		{
			get => _OperationCondition;
			private set
			{
				_OperationCondition = value;
				RaisePropertyChanged();
			}
		}

		public double Current
		{
			get => _Current;
			private set
			{
				_Current = value;
				RaisePropertyChanged();
			}
		}

		public double Voltage
		{
			get => _Voltage;
			private set
			{
				_Voltage = value;
				RaisePropertyChanged();
			}
		}

		public double WireFeed
		{
			get => _WireFeed;
			private set
			{
				_WireFeed = value;
				RaisePropertyChanged();
			}
		}

		public double GasFlow
		{
			get => _GasFlow;
			private set
			{
				_GasFlow = value;
				RaisePropertyChanged();
			}
		}

		public double Velocity
		{
			get => _Velocity;
			private set
			{
				_Velocity = value;
				RaisePropertyChanged();
			}
		}

		public double HeatInput
		{
			get => _HeatInput;
			private set
			{
				_HeatInput = value;
				RaisePropertyChanged();
			}
		}

		#endregion

		#region · Methods ·

		public void StartMonitoring()
		{
			unsubscribeAction =
				device.SubscribeToParameterValueChanges(
					new Dictionary<string, VariableValueChangedConfig>()
					{
						{ "OperationCondition", new VariableValueChangedConfig(1, handleOperationConditionValue, null) },
						{ "Current", new VariableValueChangedConfig(1, handleCurrentValue, null) },
						{ "Voltage", new VariableValueChangedConfig(1, handleVoltageValue, null) },
						{ "WireFeed", new VariableValueChangedConfig(1, handleWireFeedValue, null) },
						{ "GasFlow", new VariableValueChangedConfig(1, handleGasFlowValue, null) },
						{ "TravelSpeed", new VariableValueChangedConfig(1, handleVelocityValue, null) },
						{ "HeatInput", new VariableValueChangedConfig(1, handleHeatInputValue, null) },
					}
					);
		}

		public void StopMonitoring()
		{
			try
			{
				unsubscribeAction?.Invoke();
				unsubscribeAction = null;
			}
			catch (Exception e)
			{
				throw new Exception("Can't stop monitoring", e);
			}
		}

		#endregion

		#region · IDisposable implementation ·

		public void Dispose()
		{
			dispose(true);

			// This object will be cleaned up by the Dispose method.
			// Therefore, you should call GC.SupressFinalize to
			// take this object off the finalization queue
			// and prevent finalization code for this object
			// from executing a second time.
			GC.SuppressFinalize(this);
		}

		#endregion

		#region · INotifyPropertyChanged implementation ·

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion

		#region · non-public members ·

		#region · fields ·

		private volatile int isDisposed;
 
		[NotNull]
		private readonly DeviceContext context;

		[NotNull]
		private readonly Device device;

		private Action unsubscribeAction;


		private int _OperationCondition;

		private double _Current;

		private double _Voltage;

		private double _WireFeed;

		private double _GasFlow;

		private double _Velocity;

		private double _HeatInput;

		#endregion

		#region · Properties ·


		#endregion

		#region · methods ·

		#region · raise methods ·

		/// <summary>
		/// Raises this object's PropertyChanged event.
		/// </summary>
		/// <param name="propertyName">The property that has a new value.</param>
		[NotifyPropertyChangedInvocator]
		protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = "") =>
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

		#endregion

		#region · Dispose background ·

		~WeldingMachine()
		{
			dispose(false);
		}

		private void dispose(bool disposing)
		{
			if (1 == Interlocked.CompareExchange(ref isDisposed, 1, 0))
				return;

			if (disposing)
				DisposeManagedResources();

			DisposeUnmanagedResources();

			RaisePropertyChanged(nameof(IsDisposed));
		}

		/// <summary>
		/// Child classes can override this method to perform 
		/// managed resources clean-up logic, such as removing event handlers.
		/// </summary>
		protected virtual void DisposeManagedResources()
		{
			try { unsubscribeAction?.Invoke(); } catch { /* ignore */ }
		}

		/// <summary>
		/// Child classes can override this method to perform 
		/// unmanaged (Windows native) resources clean-up logic.
		/// </summary>
		protected virtual void DisposeUnmanagedResources()
		{
		}

		#endregion

		#region · Variable value changed handlers ·

		private void handleOperationConditionValue(NodeId varId, object value)
		{
			if (value is int v)
				OperationCondition = v;
		}

		private void handleCurrentValue(NodeId varId, object value)
		{
			if (value is double v)
				Current = v;
		}

		private void handleVoltageValue(NodeId varId, object value)
		{
			if (value is double v)
				Voltage = v;
		}

		private void handleWireFeedValue(NodeId varId, object value)
		{
			if (value is double v)
				WireFeed = v;
		}

		private void handleGasFlowValue(NodeId varId, object value)
		{
			if (value is double v)
				GasFlow = v;
		}

		private void handleVelocityValue(NodeId varId, object value)
		{
			if (value is double v)
				Velocity = v;
		}

		private void handleHeatInputValue(NodeId varId, object value)
		{
			if (value is double v)
				HeatInput = v;
		}

		#endregion


		#endregion

		#endregion
	}
}
