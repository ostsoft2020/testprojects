﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpcUaDeviceClient;

namespace SAPB1DemoMonitor
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();

			NLog.LogManager.LoadConfiguration("NLog.config");
			OpcUaClient.Init("Opc.Ua.Client.Config.xml");

			mainSyncContext = SynchronizationContext.Current;
		}

		protected override async void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			context = new DeviceContext();
			await context.ConnectAsync("127.0.0.1:62541");
			
			Machine = new WeldingMachine(context, "http://Gimecs.com/UnifiedFactory.IntegrationHost/", "WeldingMachine1");
			Machine.StartMonitoring();

			Machine.PropertyChanged += machine_PropertyChanged;
		}

		private void machine_PropertyChanged(object sender, PropertyChangedEventArgs eventArgs)
		{
			try
			{
				mainSyncContext.Post(
					_ => 
					{
						switch (eventArgs.PropertyName)
						{
							case nameof(Machine.Current):
								CurrentTextBox.Text = Machine.Current.ToString("N2");
								break;
							case nameof(Machine.Voltage):
								VoltageTextBox.Text = Machine.Voltage.ToString("N2");
								break;
							case nameof(Machine.WireFeed):
								WireFeedTextBox.Text = Machine.WireFeed.ToString("N2");
								break;
							case nameof(Machine.GasFlow):
								GasFlowTextBox.Text = Machine.GasFlow.ToString("N2");
								break;
							case nameof(Machine.Velocity):
								VelocityTextBox.Text = Machine.Velocity.ToString("N2");
								break;
							case nameof(Machine.HeatInput):
								HeatInputTextBox.Text = Machine.HeatInput.ToString("N2");
								break;
							case nameof(Machine.OperationCondition):
								updateOperationalState(Machine.OperationCondition);
								break;
						}
					},
					null
					);
			}
			catch (Exception e)
			{
				/* ignore */
			}
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			Machine.StopMonitoring();
			context.Dispose();

			base.OnClosing(e);
		}

		private DeviceContext context;
		public WeldingMachine Machine;
		
		private SynchronizationContext mainSyncContext;

		private void updateOperationalState(int operationalState)
		{
			switch (operationalState)
			{
				case 0:
					OperationConditionTextBox.BackColor = Color.White;
					OperationConditionTextBox.Text = "Off";
					break;

				case 1:
					OperationConditionTextBox.BackColor = Color.Green;
					OperationConditionTextBox.Text = "Working";
					break;

				case 2:
					OperationConditionTextBox.BackColor = Color.Yellow;
					OperationConditionTextBox.Text = "Alarm";
					break;

				default:
					OperationConditionTextBox.BackColor = Color.Red;
					OperationConditionTextBox.Text = "Faulted";
					break;
			}
		}
	}
}
