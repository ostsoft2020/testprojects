﻿namespace SAPB1DemoMonitor
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.pictureBox = new System.Windows.Forms.PictureBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.CurrentTextBox = new System.Windows.Forms.TextBox();
			this.VoltageTextBox = new System.Windows.Forms.TextBox();
			this.VelocityTextBox = new System.Windows.Forms.TextBox();
			this.GasFlowTextBox = new System.Windows.Forms.TextBox();
			this.WireFeedTextBox = new System.Windows.Forms.TextBox();
			this.HeatInputTextBox = new System.Windows.Forms.TextBox();
			this.OperationConditionTextBox = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBox
			// 
			this.pictureBox.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox.Image")));
			this.pictureBox.Location = new System.Drawing.Point(12, 12);
			this.pictureBox.Name = "pictureBox";
			this.pictureBox.Size = new System.Drawing.Size(410, 512);
			this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox.TabIndex = 0;
			this.pictureBox.TabStop = false;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(463, 122);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(110, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Operational condition:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(463, 185);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(60, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Current (A):";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(463, 220);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(62, 13);
			this.label3.TabIndex = 3;
			this.label3.Text = "Voltage (V):";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(463, 256);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(91, 13);
			this.label4.TabIndex = 4;
			this.label4.Text = "Velocity (cm/min):";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(463, 291);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(83, 13);
			this.label5.TabIndex = 5;
			this.label5.Text = "Gas flow (l/min):";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(463, 327);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(100, 13);
			this.label6.TabIndex = 6;
			this.label6.Text = "Wire feed (cm/min):";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(463, 362);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(100, 13);
			this.label7.TabIndex = 7;
			this.label7.Text = "Heat input (kJ/mm):";
			// 
			// CurrentTextBox
			// 
			this.CurrentTextBox.Location = new System.Drawing.Point(596, 182);
			this.CurrentTextBox.Name = "CurrentTextBox";
			this.CurrentTextBox.ReadOnly = true;
			this.CurrentTextBox.Size = new System.Drawing.Size(69, 20);
			this.CurrentTextBox.TabIndex = 8;
			// 
			// VoltageTextBox
			// 
			this.VoltageTextBox.Location = new System.Drawing.Point(596, 217);
			this.VoltageTextBox.Name = "VoltageTextBox";
			this.VoltageTextBox.ReadOnly = true;
			this.VoltageTextBox.Size = new System.Drawing.Size(69, 20);
			this.VoltageTextBox.TabIndex = 9;
			// 
			// VelocityTextBox
			// 
			this.VelocityTextBox.Location = new System.Drawing.Point(596, 253);
			this.VelocityTextBox.Name = "VelocityTextBox";
			this.VelocityTextBox.ReadOnly = true;
			this.VelocityTextBox.Size = new System.Drawing.Size(69, 20);
			this.VelocityTextBox.TabIndex = 10;
			// 
			// GasFlowTextBox
			// 
			this.GasFlowTextBox.Location = new System.Drawing.Point(596, 288);
			this.GasFlowTextBox.Name = "GasFlowTextBox";
			this.GasFlowTextBox.ReadOnly = true;
			this.GasFlowTextBox.Size = new System.Drawing.Size(69, 20);
			this.GasFlowTextBox.TabIndex = 11;
			// 
			// WireFeedTextBox
			// 
			this.WireFeedTextBox.Location = new System.Drawing.Point(596, 324);
			this.WireFeedTextBox.Name = "WireFeedTextBox";
			this.WireFeedTextBox.ReadOnly = true;
			this.WireFeedTextBox.Size = new System.Drawing.Size(69, 20);
			this.WireFeedTextBox.TabIndex = 12;
			// 
			// HeatInputTextBox
			// 
			this.HeatInputTextBox.Location = new System.Drawing.Point(596, 359);
			this.HeatInputTextBox.Name = "HeatInputTextBox";
			this.HeatInputTextBox.ReadOnly = true;
			this.HeatInputTextBox.Size = new System.Drawing.Size(69, 20);
			this.HeatInputTextBox.TabIndex = 13;
			// 
			// OperationConditionTextBox
			// 
			this.OperationConditionTextBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
			this.OperationConditionTextBox.Location = new System.Drawing.Point(596, 119);
			this.OperationConditionTextBox.Name = "OperationConditionTextBox";
			this.OperationConditionTextBox.ReadOnly = true;
			this.OperationConditionTextBox.Size = new System.Drawing.Size(69, 20);
			this.OperationConditionTextBox.TabIndex = 7;
			this.OperationConditionTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(713, 534);
			this.Controls.Add(this.OperationConditionTextBox);
			this.Controls.Add(this.HeatInputTextBox);
			this.Controls.Add(this.WireFeedTextBox);
			this.Controls.Add(this.GasFlowTextBox);
			this.Controls.Add(this.VelocityTextBox);
			this.Controls.Add(this.VoltageTextBox);
			this.Controls.Add(this.CurrentTextBox);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.pictureBox);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Power source";
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pictureBox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox CurrentTextBox;
		private System.Windows.Forms.TextBox VoltageTextBox;
		private System.Windows.Forms.TextBox VelocityTextBox;
		private System.Windows.Forms.TextBox GasFlowTextBox;
		private System.Windows.Forms.TextBox WireFeedTextBox;
		private System.Windows.Forms.TextBox HeatInputTextBox;
		private System.Windows.Forms.TextBox OperationConditionTextBox;
	}
}

