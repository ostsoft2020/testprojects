﻿using Opc.Ua;
using OpcUaDeviceClient;
using OpcUaDeviceClient.Classes;
using OpcUaDeviceClient.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Timer = System.Timers.Timer;

namespace WeldControllerClient
{
	internal class WeldController : INotifyPropertyChanged, IDisposable
	{
		#region Contructor and destructor

		public WeldController()
        {
            historyCollectingTimer = new Timer(100)
            {
                AutoReset = true
            };

            historyCollectingTimer.Elapsed += historyCollectingTimer_Elapsed;

            savingTask = Task.Factory.StartNew(saveHistory, TaskCreationOptions.LongRunning);
        }

        ~WeldController()
		{
			Dispose();
		}

		#endregion

		#region Public members

		public bool Ready
		{
			get => ready;
			private set
			{
				if (ready != value)
				{
					ready = value;
					raisePropertyChanged();
				}
			}
		}

		public bool ArcOn
		{
			get => arcOn;
			private set
			{
				if (arcOn != value)
				{
					arcOn = value;

					if (value)
						startHistorizing();

					raisePropertyChanged();
				}
			}
		}

		public bool MainPhase
		{
			get => mainPhase;
			private set
			{
				if (mainPhase != value)
				{  
					mainPhase = value; 
					raisePropertyChanged(); 
				}
			}
		}

		public bool ProcessActive
		{
			get => processActive;
			private set
			{
				if (processActive != value)
				{
					processActive = value;

					if (!value)
					{
						stopHisorizing();
					}

					raisePropertyChanged();
				}
			}
		}

		public double Current
		{
			get => current;
			private set
			{
				if (current != value)
				{
					current = value;
					raisePropertyChanged();
				}
			}
		}

		public double Voltage
		{
			get => voltage;
			private set
			{
				if (voltage != value)
				{
					voltage = value;
					raisePropertyChanged();
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public event EventHandler<string> MessageOccured;

		public async Task ConnectAsync(string serverAddress, string browsNameNamespace = @"http://Gimecs.com/UnifiedFactory.IntegrationHost.Devices.Instances/", string browseName = "WeldingMachine")
		{
			context = new DeviceContext();

			await context.ConnectAsync(serverAddress);

			device = context.GetDevice(context.GetBrowseName(browsNameNamespace, browseName))
				?? throw new Exception($"No Device {browsNameNamespace}:{browseName} at the OPC-UA Server");

			unsubscribeAction =
				device.SubscribeToParameterValueChanges(
					new Dictionary<string, VariableValueChangedConfig>()
					{
						{ ParameterNames.Ready, new VariableValueChangedConfig(1, handleReadyValue, null) },
						{ ParameterNames.ArcOn, new VariableValueChangedConfig(1, handleArcOnValue, null) },
						{ ParameterNames.IsMainPhase, new VariableValueChangedConfig(1, handleMainPhaseValue, null) },
						{ ParameterNames.ProcessActive, new VariableValueChangedConfig(1, handleProcessActiveValue, null) },
						{ ParameterNames.Current, new VariableValueChangedConfig(1, handleCurrentValue, null) },
						{ ParameterNames.Voltage, new VariableValueChangedConfig(1, handleVoltageValue, null) },
					}
				);
		}

		public async Task DisconnectAsync()
		{
			try
			{
				stopHisorizing();

				await Task.Run(() => unsubscribeAction?.Invoke());

				unsubscribeAction = null;
			}
			catch (Exception e)
			{
				throw new Exception("Can't stop monitoring", e);
			}
		}

		#endregion

		#region Non public members

		private DeviceContext context;
		private Device device;
		private Action unsubscribeAction;
		private object syncObject = new object();

		private volatile int isDisposed;

		private bool ready;
		private bool arcOn;
		private bool mainPhase;
		private bool processActive;
		private double voltage;
		private double current;

		private readonly Timer historyCollectingTimer;
		private HistoryCollector historyCollector;
		private readonly Task savingTask;
		private readonly BlockingCollection<HistorySaver> historySavers = new BlockingCollection<HistorySaver>();

		private void startHistorizing()
		{
			lock (syncObject)
			{
				historyCollector = new HistoryCollector();

				historyCollector.StartHistorizing();
				historyCollector.AddMeasurement(new HistoryMeasurement(Current, Voltage, MainPhase));

				Task.Factory.StartNew(
					() => 
					{
						try
						{
							object[] parameterValues = device.GetParameterValues(
								new[]
								{
									ParameterNames.WorkpieceSerialNumber,
									ParameterNames.WpsNumber,
									ParameterNames.WpsCaption,
									ParameterNames.WpsRawData,
									ParameterNames.WpsPassName,
									ParameterNames.WpsPassIndex,
									ParameterNames.WeldingMachineSerialNumber,
									ParameterNames.WeldingMachineModel,
									ParameterNames.WeldingMachineProductCode,
									ParameterNames.WelderName,
									ParameterNames.WelderQualification,
									ParameterNames.JobName,
									ParameterNames.JobRawData
								}
							);

							historyCollector.WorkpieceSerialNumber = (string)parameterValues[0];
							historyCollector.WpsNumber = (string)parameterValues[1];
							historyCollector.WpsCaption = (string)parameterValues[2];
							historyCollector.WpsRawData = (string)parameterValues[3];
							historyCollector.WpsPassName = (string)parameterValues[4];
							historyCollector.WpsPassIndex = (short)parameterValues[5];
							historyCollector.WeldingMachineSerialNumber = (string)parameterValues[6];
							historyCollector.WeldingMachineModel = (string)parameterValues[7];
							historyCollector.WeldingMachineProductCode = (string)parameterValues[8];
							historyCollector.WelderName = (string)parameterValues[9];
							historyCollector.WelderQualification = (string)parameterValues[10];
							historyCollector.JobName = (string)parameterValues[11];
							historyCollector.JobRawData = (string)parameterValues[12];
						}
						catch {}
					}
				);

				historyCollectingTimer.Start();
			}
		}

		private void stopHisorizing()
		{
			lock (syncObject)
			{
				historyCollectingTimer.Stop();

				if (historyCollector != null && historyCollector.Started)
				{
					historyCollector.AddMeasurement(new HistoryMeasurement(Current, Voltage, MainPhase));

					historyCollector.StopHistorizing();

					historySavers.Add(new HistorySaver(historyCollector));
				}

				historyCollector = null;
			}
		}

        private void saveHistory()
        {
            foreach (HistorySaver historySaver in historySavers.GetConsumingEnumerable())
                try 
				{ 
					historySaver.Save();
                    MessageOccured?.Invoke(this, "Weld history saved");
                }
                catch (Exception ex) 
				{
					MessageOccured?.Invoke(this, "Error: " + ex.GetBaseException().Message);
                }

			try { historySavers.Dispose(); } catch { }
        }

        private void historyCollectingTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
		{
			historyCollector.AddMeasurement(new HistoryMeasurement(Current, Voltage, MainPhase));
		}

		private void raisePropertyChanged([CallerMemberName] string propertyName = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

		private void handleReadyValue(NodeId varId, object value)
		{
			if (value is bool v)
				Ready = v;
		}

		private void handleArcOnValue(NodeId varId, object value)
		{
			if (value is bool v)
				ArcOn = v;
		}

		private void handleMainPhaseValue(NodeId varId, object value)
		{
			if (value is bool v)
				MainPhase = v;
		}

		private void handleProcessActiveValue(NodeId varId, object value)
		{
			if (value is bool v)
				ProcessActive = v;
		}

		private void handleCurrentValue(NodeId varId, object value)
		{
			if (value is double v)
				Current = v;
		}

		private void handleVoltageValue(NodeId varId, object value)
		{
			if (value is double v)
				Voltage = v;
		}

		public void Dispose()
		{
			if (1 == Interlocked.CompareExchange(ref isDisposed, 1, 0))
				return;

			historySavers.CompleteAdding();
            try { savingTask.Dispose(); } catch { /* ignore */ }
            try { unsubscribeAction?.Invoke(); } catch { /* ignore */ }
		}

		#endregion
	}
}
