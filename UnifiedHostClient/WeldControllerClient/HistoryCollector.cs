﻿using System;
using System.Collections.Generic;

namespace WeldControllerClient
{
	internal class HistoryCollector
	{
		public HistoryCollector() 
		{
		}

		public bool Started { get; private set; }

		public bool JointStart;
		public DateTime? WeldingStart;
		public DateTime? MainPahseStart;
		public DateTime? MainPahseFinish;
		public DateTime? Finish;
		public string WorkpieceSerialNumber = null;
		public string WpsNumber = null;
		public string WpsCaption = null;
		public string WpsRawData = null;
		public string WpsPassName = null;
		public short WpsPassIndex;
		public string WeldingMachineSerialNumber = null;
		public string WeldingMachineModel = null;
		public string WeldingMachineProductCode = null;
		public string WelderName = null;
		public string WelderQualification = null;
		public string JobName = null;
		public string JobRawData = null;

		public readonly List<HistoryMeasurement> Measurements = new List<HistoryMeasurement>();

		internal void StartHistorizing()
		{
			Started = true;
			WeldingStart = DateTime.Now;
		}

		internal void StopHistorizing()
		{
			Started = false;
			Finish = DateTime.Now;

			if (MainPahseStart.HasValue && !MainPahseFinish.HasValue)
				MainPahseFinish = Finish;
		}

		internal void AddMeasurement(HistoryMeasurement measurement)
		{
			if (!Started)
				return;

			if (Measurements.Count > 0 && Measurements[Measurements.Count - 1].MainPhase != measurement.MainPhase)
			{
				HistoryMeasurement fakeMeasurement = measurement;

				if (measurement.MainPhase)
				{
					fakeMeasurement.MainPhase = false;

					if (!MainPahseStart.HasValue)
						MainPahseStart = measurement.Timestamp;
				}
				else
				{
					fakeMeasurement.MainPhase = true;
					MainPahseFinish = measurement.Timestamp;
				}

				Measurements.Add(fakeMeasurement);
			}

			Measurements.Add(measurement);
		}
	}
}
