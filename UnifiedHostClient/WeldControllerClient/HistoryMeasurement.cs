﻿using System;

namespace WeldControllerClient
{
	internal class HistoryMeasurement
	{
		internal HistoryMeasurement(double current, double voltage, bool mainPhase) 
		{
			Current = current;
			Voltage = voltage;
			MainPhase = mainPhase;
		}

		internal DateTime Timestamp = DateTime.Now;
		internal double Current;
		internal double Voltage;
		internal bool MainPhase;
	}
}
