﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;

namespace WeldControllerClient
{
	internal class HistorySaver
	{
		public HistorySaver(HistoryCollector historyCollector) 
		{
			this.historyCollector = historyCollector;
		}
		
		public void Save()
		{
			if (historyCollector.Measurements.Count == 0)
				return;

			string connectionString = ConfigurationManager.ConnectionStrings[0].ConnectionString;

			using(SqlConnection connection = new SqlConnection(connectionString))
			{
				connection.Open();

				SqlTransaction transaction = connection.BeginTransaction();

				try
				{

					SqlCommand command = createAddWeldHistoryCommand(connection, transaction);
					long weldHistoryId = (long)command.ExecuteScalar();
					command.Dispose();

					command = createAddWeldParameterHistoryCommand(connection, transaction, weldHistoryId, "Current");
					command.ExecuteNonQuery();
					command.Dispose();

					command = createAddWeldParameterHistoryCommand(connection, transaction, weldHistoryId, "Voltage");
					command.ExecuteNonQuery();
					command.Dispose();

					transaction.Commit();
				}
				catch
				{
					transaction.Rollback();
					throw;
				}

				connection.Close();
			}
		}

		private SqlCommand createAddWeldHistoryCommand(SqlConnection connection, SqlTransaction transaction)
		{
			string command = @"
				DECLARE @Id Id;

				EXECUTE AddWeldHistoryRecord
				  @JointStart,
				  @WeldingStart,
				  @Duration,
				  @WorkpieceSerialNumber,  
				  @WpsNumber,
				  @WpsCaption,
				  @WpsRawData,
				  @WpsPassName,
				  @WpsPassIndex,
				  @WeldingMachineSerialNumber,
				  @WeldingMachineModel,
				  @WeldingMachineProductCode,
				  @WelderName,
				  @WelderQualification,
				  @JobName,
				  @JobRawData, 
				  @MainPhaseStart, 
				  @MainPhaseFinish,
				  @Id Id OUTPUT

				SELECT @Id";

			SqlCommand result = new SqlCommand(command, connection, transaction);

			result.Parameters.AddWithValue("@JointStart", historyCollector.JointStart);
			result.Parameters.AddWithValue("@WeldingStart", historyCollector.WeldingStart.Value);
			result.Parameters.AddWithValue("@Duration", (historyCollector.Finish.Value - historyCollector.WeldingStart.Value).TotalMilliseconds);
			result.Parameters.AddWithValue("@WorkpieceSerialNumber", historyCollector.WorkpieceSerialNumber);
			result.Parameters.AddWithValue("@WpsNumber", historyCollector.WpsNumber);
			result.Parameters.AddWithValue("@WpsCaption", historyCollector.WpsCaption);
			result.Parameters.AddWithValue("@WpsRawData", historyCollector.WpsRawData);
			result.Parameters.AddWithValue("@WpsPassName", historyCollector.WpsPassName);
			result.Parameters.AddWithValue("@WpsPassIndex", historyCollector.WpsPassIndex);
			result.Parameters.AddWithValue("@WeldingMachineSerialNumber", historyCollector.WeldingMachineSerialNumber);
			result.Parameters.AddWithValue("@WeldingMachineModel", historyCollector.WeldingMachineModel);
			result.Parameters.AddWithValue("@WeldingMachineProductCode", historyCollector.WeldingMachineProductCode);
			result.Parameters.AddWithValue("@WelderName", historyCollector.WelderName);
			result.Parameters.AddWithValue("@WelderQualification", historyCollector.WelderQualification);
			result.Parameters.AddWithValue("@JobName", historyCollector.JobName);
			result.Parameters.AddWithValue("@JobRawData", historyCollector.JobRawData);
			result.Parameters.AddWithValue("@MainPhaseStart", historyCollector.MainPahseStart.Value);
			result.Parameters.AddWithValue("@MainPhaseFinish", historyCollector.MainPahseFinish.Value);

			return result;
		}

		private SqlCommand createAddWeldParameterHistoryCommand(SqlConnection connection, SqlTransaction transaction, long weldHistoryId, string parameterName)
		{
			string command = @"
				DECLARE @Id Id

				EXECUTE AddWeldParameterHistoryRecord
					@WeldHistoryId,
					@ParameterName,
					@HistoryData,
					@NormalValue,
					@ActualValue,
					@Setpoint,
					@LowLimit,
					@HighLimit,
					@Id OUTPUT

				SELECT @Id";

			//Compresser varchar(max) in next format: DelayFromStart0:Value0|...|DelayFromStartN:ValueN, DelayFromStartX in deciseconds
			StringBuilder historyData = new StringBuilder();

			foreach (HistoryMeasurement measurement in historyCollector.Measurements)
			{
				if (historyData.Length > 0)
					historyData.Append("|");

				historyData.Append(((measurement.Timestamp - historyCollector.WeldingStart.Value).TotalSeconds * 10) + ":" + typeof(HistoryMeasurement).GetProperty(parameterName).GetValue(measurement, null));
			}

			SqlCommand result = new SqlCommand(command, connection, transaction);

			result.Parameters.AddWithValue("@WeldHistoryId", weldHistoryId);
			result.Parameters.AddWithValue("@ParameterName", parameterName);
			result.Parameters.AddWithValue("@HistoryData", historyData.ToString());
			result.Parameters.AddWithValue("@NormalValue", DBNull.Value);
			result.Parameters.AddWithValue("@ActualValue", DBNull.Value);
			result.Parameters.AddWithValue("@Setpoint", DBNull.Value);
			result.Parameters.AddWithValue("@LowLimit", DBNull.Value);
			result.Parameters.AddWithValue("@HighLimit", DBNull.Value);

			return result;
		}

		private readonly HistoryCollector historyCollector;
	}
}
