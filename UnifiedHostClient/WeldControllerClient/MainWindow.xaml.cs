﻿using OpcUaDeviceClient;
using System;
using System.Windows;

namespace WeldControllerClient
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			weldController = new WeldController();

            weldController.MessageOccured += weldController_MessageOccured;

			InitializeComponent();
		}

        private readonly WeldController weldController;
		private bool inited;

        private void weldController_MessageOccured(object sender, string e)
        {
			Dispatcher.Invoke(() => logTextBox.Text = e + "\n" + logTextBox.Text);
        }

        private async void connectButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (!inited)
				{
					OpcUaClient.Init("Opc.Ua.Client.Config.xml");
					inited = true;
				}

				await weldController.ConnectAsync("127.0.0.1:62541");
				connectButton.IsEnabled = false;
				disconnectButton.IsEnabled = true;
            
				logTextBox.Text = "Successfully connected\n" + logTextBox.Text;
            }
            catch (Exception ex)
			{
				logTextBox.Text = "Connect error: " + ex.GetBaseException().Message + "\n" + logTextBox.Text;
			}

		}

		private async void disconnectButton_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				await weldController.DisconnectAsync();
				disconnectButton.IsEnabled = false;
				connectButton.IsEnabled = true;

                logTextBox.Text = "Successfully disconnected\n" + logTextBox.Text;
            }
            catch (Exception ex)
			{
				logTextBox.Text = "Disconnect error: " + ex.GetBaseException().Message + "\n" + logTextBox.Text;
			}
		}
	}
}
