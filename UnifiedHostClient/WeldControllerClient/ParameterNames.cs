﻿namespace WeldControllerClient
{
	internal static class ParameterNames
	{
		public static string Ready = "Ready";
		public static string ArcOn = "ArcOn";
		public static string IsMainPhase = "IsMainPhase";
		public static string ProcessActive = "ProcessActive";
		public static string Current = "Current";
		public static string Voltage = "Voltage";

		public static string WorkpieceSerialNumber = "WorkpieceSerialNumber";
		public static string WpsNumber = "WpsNumber";
		public static string WpsCaption = "WpsCaption";
		public static string WpsRawData = "WpsRawData";
		public static string WpsPassName = "WpsPassName";
		public static string WpsPassIndex = "WpsPassIndex";
		public static string WeldingMachineSerialNumber = "WeldingMachineSerialNumber";
		public static string WeldingMachineModel = "WeldingMachineModel";
		public static string WeldingMachineProductCode = "WeldingMachineProductCode";
		public static string WelderName = "WelderName";
		public static string WelderQualification = "WelderQualification";
		public static string JobName = "JobName";
		public static string JobRawData = "JobRawData";
	}
}
