﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TwinCAT.Ads.TcpRouter;
using Microsoft.Extensions.Logging;

namespace AdsRouterConsole
{
    public static class Progam
    {
        public static IHostBuilder HostBuilder;

        public static void Main(string[] args)
        {
            HostBuilder = CreateHostBuilder(args);
            
            HostBuilder.Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args).
                ConfigureServices(
                    (hostContext, services) =>
                    {
                        services.AddHostedService<RouterWorker>();
                    }
                ).
                ConfigureAppConfiguration(
                    (hostingContext, config) =>
                    {
                        // Uncomment to overwrite configuration
                        config.Sources.Clear(); // Clear all default config sources 
                        //config.AddEnvironmentVariables("AmsRouter"); // Use Environment variables
                        //config.AddCommandLine(args); // Use Command Line
                        //config.AddJsonFile("appSettings.json"); // Use Appsettings
                        config.AddStaticRoutesXmlConfiguration(); // Overriding settings with StaticRoutes.Xml 
                    }
                ).
                ConfigureLogging(
                    logging =>
                    {
                        // Uncomment to overwrite logging
                        // Microsoft.Extensions.Logging.Console Nuget package
                        // Namespace Microsoft.Extensions.Logging;
                        //logging.ClearProviders();
                        //logging.AddConsole();
                    }
                );
        }
    }
}
