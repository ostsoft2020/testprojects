﻿using Merkle.TIG4WeldingMachine;
using System;
using System.Collections.Generic;
using System.Text;
using UnifiedAutomation.UaBase;
using UnifiedAutomation.UaServer;

namespace Merkle.TIG4
{
	public class Tig4InstanceModel : BaseObjectModel, IMethodDispatcher
	{
		public override DataValue GetValue(IMapperContext context, ModelMapping mapping)
		{
			return base.GetValue(context, mapping);
		}

		public override bool SetValue(IMapperContext context, ModelMapping mapping, DataValue value)
		{
			return base.SetValue(context, mapping, value);
		}
		public CallMethodEventHandler GetMethodDispatcher(RequestContext context, NodeId objectId, NodeId methodId)
		{
			return CallMethodEventHandler;
		}

		public StatusCode CallMethodEventHandler(RequestContext context, MethodHandle methodHandle, IList<Variant> inputArguments, List<StatusCode> inputArgumentResults, List<Variant> outputArguments)
		{
			ushort operationId = ++operationCounter;
			double duration = 3;

			if (methodHandle.MethodId == MethodIds.DeviceSet_TIG4WeldingMachine_BeginWriteJob.ToNodeId(context.NamespaceUris))
			{				
			}

			ushort namespaceIndex = (ushort) Program.Server.NamespaceUris.IndexOf(Namespaces.TIG4);

			// create the event.
			GenericEvent e = new GenericEvent(Program.Server.FilterManager);

			// initializ base event type fields
			e.Initialize(
				null, // EventId created by SDK if null
				new NodeId(ObjectTypes.OperationCompletedEventType, namespaceIndex), // EventType
				methodHandle.NodeId, // SourceNode
				"TIG4WeldingMachine", // SourceName
				EventSeverity.Medium, // Severity
				"Operation completed."); // Message
												
			// set additional event field State
			e.Set(e.ToPath(new QualifiedName("ErrorCode", namespaceIndex)), (long) 0);
			e.Set(e.ToPath(new QualifiedName("ErrorMessage", namespaceIndex)), "");
			e.Set(e.ToPath(new QualifiedName("OperationId", namespaceIndex)), operationId);

			// report the event.
			Program.Server.ReportEvent(e);

			outputArguments[0] = operationId;
			outputArguments[1] = duration;

			return StatusCodes.Good;
		}

		private ushort operationCounter;
	}
}
