using System.Reflection;
using System.Runtime.InteropServices;

// In SDK-style projects such as this one, several assembly attributes that were historically
// defined in this file are now automatically added during build and populated with
// values defined in project properties. For details of which attributes are included
// and how to customise this process see: https://aka.ms/assembly-info-properties
[assembly: AssemblyTitle("SomeDemoCode.Std")]
[assembly: AssemblyDescription("SomeDemoCode .NET Standard")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SAS")]
[assembly: AssemblyProduct("SAS 1.0")]
[assembly: AssemblyCopyright("Copyright � SAS 2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible to COM
// components.  If you need to access a type in this assembly from COM, set the ComVisible
// attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM.

[assembly: Guid("760d1286-7d95-46f3-8ba3-bbb915d3ef2c")]

// Version information for an assembly consists of the following four values:
//
// Major Version
// Minor Version
// Build Number
// Revision
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.*")]