using Gimecs.TestStateMachine;
using System;
using System.Collections.Generic;
using UnifiedAutomation.UaBase;
using UnifiedAutomation.UaServer;

namespace Gimecs1.TestStateMachine1
{
    public partial class TestStateMachineModel 
    {
        protected enum StateNumbers : uint
        {
            Ready = 0,
            Running = 1
        }
        protected enum TransitionNumbers : uint
        {
            ReadyToRunning = 0,
            RunningToReady = 1
        }

        protected readonly ObjectNode Node;

        protected readonly NodeId ReadyStateNodeId = null;
        protected readonly NodeId RunnigStateNodeId = null;

        protected readonly NodeId ReadyToRunningTransitionNodeId = null;
        protected readonly NodeId RunningToReadyTransitionNodeId = null;

        public TestStateMachineModel(ObjectNode node, TestStateMachineModel template) : this(template)
        {
            Node = node;

            Ready.StateNumber = (uint) StateNumbers.Ready;
            Running.StateNumber = (uint) StateNumbers.Running;

            ReadyToRunning.TransitionNumber = (uint)TransitionNumbers.ReadyToRunning;
            RunningToReady.TransitionNumber = (uint)TransitionNumbers.RunningToReady;

            //UnifiedAutomation.UaServer.ProgramStateMachineModel

            List<ReferenceNode> children = node.FindReferences(UnifiedAutomation.UaBase.ReferenceTypeIds.HasComponent, false);

            foreach (var child in children)
            {
                if (Equals(child.TargetId.Identifier, "TestStateMachine." + BrowseNames.Ready))
                    ReadyStateNodeId = child.TargetId.ToNodeId(Program.Server.NamespaceUris);
                else if (Equals(child.TargetId.Identifier, "TestStateMachine." + BrowseNames.Running))
                    RunnigStateNodeId = child.TargetId.ToNodeId(Program.Server.NamespaceUris);
                else if (Equals(child.TargetId.Identifier, "TestStateMachine." + BrowseNames.ReadyToRunning))
                    ReadyToRunningTransitionNodeId = child.TargetId.ToNodeId(Program.Server.NamespaceUris);
                else if (Equals(child.TargetId.Identifier, "TestStateMachine." + BrowseNames.RunningToReady))
                    RunningToReadyTransitionNodeId = child.TargetId.ToNodeId(Program.Server.NamespaceUris);
            }

            AvailableStates = new NodeId[] { ReadyStateNodeId, RunnigStateNodeId };
            AvailableTransitions = new NodeId[] { ReadyToRunningTransitionNodeId, RunningToReadyTransitionNodeId };

            SetLastTransition(TransitionNumbers.RunningToReady);
            SetCurrentState(StateNumbers.Ready);
        }

        protected void DoTransition(StateModel targetState)
        {
            if (CurrentState.Number == targetState.StateNumber)
                return;

            StateNumbers stateNumber = (StateNumbers)targetState.StateNumber;

            TransitionNumbers transitionNumber = stateNumber == StateNumbers.Running ? TransitionNumbers.ReadyToRunning : TransitionNumbers.RunningToReady;

            SetCurrentState(stateNumber);
            SetLastTransition(transitionNumber);
        }

        protected void SetCurrentState(StateNumbers number)
        {
            CurrentState.Value = number.ToString();
            CurrentState.EffectiveDisplayName = number.ToString();
            CurrentState.Id = number == StateNumbers.Ready ? ReadyStateNodeId : RunnigStateNodeId;
            CurrentState.Name = number.ToString();
            CurrentState.Number = (uint) number;
        }

        protected void SetLastTransition(TransitionNumbers trasition)
        {
            DateTime transitionTime = DateTime.Now;

            LastTransition.Value = trasition.ToString();
            LastTransition.EffectiveTransitionTime = transitionTime;
            LastTransition.Id = trasition == TransitionNumbers.ReadyToRunning ? ReadyToRunningTransitionNodeId : RunningToReadyTransitionNodeId;
            LastTransition.Name = trasition.ToString();
            LastTransition.Number = (uint) trasition;
            LastTransition.TransitionTime = transitionTime;
        }
    }
}

