﻿using System;
using System.Windows;
using TwinCAT;
using TwinCAT.Ads;
using TwinCAT.Ads.TypeSystem;
using TwinCAT.TypeSystem;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Linq;
using System.Configuration;
using System.Threading;
using System.Dynamic;

namespace AdsClient
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private TwinCAT.Ads.AdsClient adsClient;

		private readonly Dictionary<DynamicSymbol, IAdsSymbol> symbolInfos = new Dictionary<DynamicSymbol, IAdsSymbol>();

		public MainWindow()
		{
			InitializeComponent();

			adsClient = new TwinCAT.Ads.AdsClient();

			amsNetIdTextBox.Text = ConfigurationManager.AppSettings["defaultAmsNetId"] ?? "127.0.0.1.1.1";
		}

		private void connectButtonClick(object sender, RoutedEventArgs e)
		{
			symbolInfos.Clear();

			if (!AmsNetId.TryParse(amsNetIdTextBox.Text, out AmsNetId amsNetId))
			{
				MessageBox.Show("Invalid AMS net ID '" + amsNetIdTextBox.Text + "'");
				return;
			}

			if (!int.TryParse(amsPortTextBox.Text, out int amsPort))
			{
				MessageBox.Show("Invalid AMS port " + amsPortTextBox.Text);
				return;
			}

			try
			{
				adsClient.Connect(amsNetId, amsPort);

				IDynamicSymbolLoader symbolLoader = (IDynamicSymbolLoader)SymbolLoaderFactory.Create(adsClient, SymbolLoaderSettings.DefaultDynamic);

				treeView.ItemsSource = symbolLoader.SymbolsDynamic;

				connectButton.IsEnabled = false;
				disconnectButton.IsEnabled = true;
			}
			catch (Exception ex)
			{				
				MessageBox.Show(ex.GetBaseException().Message);
			}
		}

		private void disconnectButtonClick(object sender, RoutedEventArgs e)
		{
			try
			{
				treeView.ItemsSource = null;

				adsClient.Disconnect();

				disconnectButton.IsEnabled = false;
				connectButton.IsEnabled = true;
			}
			catch { }
		}

		private IAdsSymbol getAdsSymbolBySymbol(DynamicSymbol symbol)
		{
			if (symbol == null)
				return null;

			if (!symbolInfos.TryGetValue(symbol, out IAdsSymbol symbolInfo))
			{
				try
				{
					symbolInfo = adsClient.ReadSymbol(symbol.InstancePath);
				}
				catch {}

				symbolInfos.Add(symbol, symbolInfo);
			}

			return symbolInfo;
		}

		private Type getClrType(IAdsSymbol adsSymbol)
		{
			if (adsSymbol == null || adsSymbol.DataType == null)
				return null;

			Type result = adsSymbol.DataType.GetType().GetProperty("ManagedType")?.GetValue(adsSymbol.DataType) as Type;

			if (result != null)
				return result;

			return null;
		}

		private IDimensionCollection getArrayDimensions(IAdsSymbol adsSymbol)
		{
			if (adsSymbol == null || !(adsSymbol.DataType is ArrayType arrayType))
				return null;

			if (arrayType.Dimensions == null || arrayType.Dimensions.Count == 0)
				return null;

			return arrayType.Dimensions;
		}

		private string dimensionsToString(IDimensionCollection dimensions)
		{
			if (dimensions == null || dimensions.Count == 0)
				return "";

			StringBuilder result = new StringBuilder();

			foreach (Dimension dimension in dimensions)
			{
				if (result.Length > 0)
					result.Append(",");

				result.Append("[" + dimension.LowerBound + ".." + dimension.UpperBound + "]");
			}

			return result.ToString();
		}

		private int? getElementSize(IAdsSymbol adsSymbol)
		{
			if (adsSymbol == null || !(adsSymbol.DataType is ArrayType arrayType))
				return null;

			return arrayType.ElementSize;
		}

		private void treeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			DynamicSymbol dynamicSymbol = e.NewValue as DynamicSymbol;

			IAdsSymbol adsSymbol = getAdsSymbolBySymbol(dynamicSymbol);

			pathTextBox.Text = adsSymbol?.InstancePath;

			dataTypeTextBox.Text = adsSymbol?.DataType?.Name;

			indexGroupTextBox.Text = (adsSymbol != null) ? adsSymbol.IndexGroup.ToString() : "";

			indexOffsetTextBox.Text = (adsSymbol != null) ? adsSymbol.IndexOffset.ToString() : "";

			clrTypeTextBox.Text = getClrType(adsSymbol)?.FullName ?? "< not supported >";

			IDimensionCollection dimensions = getArrayDimensions(adsSymbol);

			arrayDimensionsTextBox.Text = dimensionsToString(dimensions);

			arrayElementSizeTextBox.Text = getElementSize(adsSymbol).ToString();

			if (adsSymbol != null && adsSymbol.DataType is IStructType rpcStruct)
			{
				StringBuilder methodsString = new StringBuilder();

				foreach (IRpcMethod method in rpcStruct.RpcMethods.Where(m => !m.Name.StartsWith("__")))
				{
					methodsString.Append(method.IsVoid ? "void " : method.ReturnType + " ");

					methodsString.Append(method.Name + "(");

					foreach (var inParameter in method.InParameters)
						methodsString.Append("in " + inParameter.TypeName + " " + inParameter.Name + ",");

					foreach (var outParameter in method.OutParameters)
						methodsString.Append("out " + outParameter.TypeName + " " + outParameter.Name + ",");

					if (methodsString[methodsString.Length - 1] == ',')
						methodsString.Remove(methodsString.Length - 1, 1);

					methodsString.Append(")\n");
				}

				methodsTextBox.Text = methodsString.ToString();
			}
			else
				methodsTextBox.Text = "";

			try
			{
				dynamic value = dynamicSymbol.ReadValue();

				DynamicValue dynamicValue = value as DynamicValue;

				if (!(value is string) && value is IEnumerable)
				{
					StringBuilder arrayString = new StringBuilder();

					Array result = adsClient.ReadValue(adsSymbol) as Array;

					ArraySegment<object> arraySegment = new ArraySegment<object>(result.Cast<object>().ToArray(), 0, dimensions[0].ElementCount);

					Array subArray = arraySegment.ToArray();

					foreach (object item in subArray)
					{
						if (arrayString.Length > 0)
							arrayString.Append(",");

						if (item != null)
							arrayString.Append(item.ToString());
					}

					arrayString.Insert(0, "[");
					arrayString.Append("]");

					valueTextBox.Text = arrayString.ToString();
				}
				else if (Equals(null, value))
					valueTextBox.Text = "< null >";
				else
					valueTextBox.Text = value.ToString();
			}
			catch (Exception ex)
			{
				valueTextBox.Text = "< " + ex.Message + " >";
			}
		}
	}
}